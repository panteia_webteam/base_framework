﻿component accessors=true {

	public any function init(struct fw) {
		variables.fw = arguments.fw;
		return this;
	}

	public void function before (struct rc) {}

}