component accessors=true output=false hint="" {

	property shortUrlService;
	property utilityService;

	public any function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
		return this;
	}

	public void function before(required struct rc) output=false hint="" {
		// prevent XSS attacks
		rc = utilityService.XSSProtect(arguments.rc);
	}

	public void function to(required struct rc) output=false hint="" {
		var targeturl = shortUrlService.getTargetUrl(arguments.rc.url);
		if (len(targeturl.fullUrl)) {
			variables.fw.redirect(targeturl.targeturl);
		} else {
			for (var item in listToArray(targeturl.queryString, "&")) {
				rc[listFirst(item, "=")] = listLast(item, "=");
			}
			var keyList = structKeyList(rc);
			keyList = listFilter(keyList, function(item) {
				var reservedKeyArr = ["CURRENTPARTICIPANT", "CURRENTUSER", "action", "url"];
				if (! arrayFindNoCase(reservedKeyArr, item)) {
					return true;
				}
				return false;
			});
			variables.fw.redirect(targeturl.action, keyList);
		}
	}

}