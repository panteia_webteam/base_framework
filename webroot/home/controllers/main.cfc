﻿component accessors=true {

	public void function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
		return this;
	}

	public void function before(struct rc) output=false hint="" {
		session.doXSSProtect = true;
	}

	public void function main(struct rc) output=false hint="" {}

}