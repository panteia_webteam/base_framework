﻿component accessors=true output=false hint="" {

	property cfUtilService;
	property config;
	property emailService;
	property logService;
	property passwordService;
	property resourceBundleService;
	property shortUrlService;
	property userService;
	property utilityService;

	public void function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
	}

	public void function before(required struct rc) output=false hint="" {
		session.doXSSProtect = true;
		/* prevent XSS attacks */
		rc = utilityService.XSSProtect(arguments.rc);
	}

	public void function main(struct rc) output=false hint="" {
		param name="rc.email" default="";
		// if already logged in redirect to the default subsystem
		if ((structKeyExists(session, "auth") && session.auth.isLoggedIn) && structKeyExists(arguments.rc, "currentUser")) {
			var userObj = arguments.rc.currentUser;
			variables.fw.redirect(userObj.getDefaultAction());
		}
	}

	public void function processMain(required struct rc) output=false hint="" {
		/* abort execution in case of CSRF attack */
		utilityService.abortOnCSRFAttack(arguments.rc.csrfToken);
		/* get validation resourcebundle */
		var validationRB = resourceBundleService.getResourceBundle("validation", session.locale, session.debug);
		/* validate data */
		var valid = {};
		/* email valid */
		if (arguments.rc.email.isEmpty()) {
			valid.email = { valid : false, message : validationRB.emailNotExists };
		} else if (! isValid("email", arguments.rc.email)) {
			valid.email = { valid : false, message : validationRB.emailNotValid };
		}
		/* password empty */
		if (arguments.rc.password.isEmpty()) {
			valid.password = { valid : false, message : validationRB.passwordNotExists };
		}
		/* if basic validation passed, check user credentials
		check PANTEIA user */
		if (structIsEmpty(valid)) {
			var givenEmail = utilityservice.dataEncryptString(arguments.rc.email);
			var userObj = userService.getObjBy({ email : givenEmail });
			/* no user found */
			if (isNull(userObj.getId())) {
				valid.user = { valid : false, message : validationRB.userNotFound };
			/* no password found */
			} else if (arrayIsEmpty(userObj.getPasswords())) {
				rc.message = { valid : false, class : "warning", text : validationRB.passwordNotSet };
				variables.fw.redirect("home:login.retrievePassword", "message,email");
			/* password does not match database */
			} else if (hash(arguments.rc.password & userObj.getPasswords()[1].getSalt(), "SHA-512", "utf-8") != userObj.getPasswords()[1].getPassword()) {
				valid.password = { valid : false, message : validationRB.passwordInValid };
			}
		}
		/* validation failed, return to page and give feedback */
		if (! structIsEmpty(valid)) {
			rc.message = { class : "warning", text : validationRB.validationFailed & utilityService.buildMessage(valid) };
			/* do not log the user credentials to the database */
			logService.log(rc, 0);
			variables.fw.redirect(utilityService.getOriginAction(fw=variables.fw), "message,email");
		}
		/* set the user in session */
		setUserSession(userObj);
		rc.message = { class : "success", text : "User logged in." };
		logService.log(rc, 0);
		variables.fw.redirect(userObj.getDefaultaction());
	}

	public void function resetPassword(required struct rc) output=false hint="" {
		validateTemporaryPassword(arguments.rc.key);
	}

	public void function processResetPassword(required struct rc) output=false hint="" {
		/* abort execution in case of CSRF attack */
		utilityService.abortOnCSRFAttack(arguments.rc.csrfToken);
		var userId = validateTemporaryPassword(arguments.rc.tmpPassword);
		/* get the user */
		var userObj = userService.getObj(userId);
		/* get validation resourcebundle */
		var validationRB = resourceBundleService.getResourceBundle("validation", session.locale, session.debug);
		/* validate data */
		var valid = {};
		/* passwords are equal */
		if (compare(arguments.rc.password, arguments.rc.confirm_password) != 0 || arguments.rc.password.isEmpty()) {
			valid.password = { valid : false, message : validationRB.passwordsNotEqual };
		}
		/* validation failed, return to page and give feedback */
		if (! structIsEmpty(valid)) {
			rc.message = { class : "warning", text : validationRB.validationFailed & utilityService.buildMessage(valid) };
			logService.log(rc, 1);
			rc.key = arguments.rc.tmpPassword;
			variables.fw.redirect(utilityService.getOriginAction(fw=variables.fw), "message,key");
		}
		var givenEmail = utilityservice.dataDecryptString(userObj.getEmail());
		userObj.setFirstName(utilityService.dataDecryptString(userObj.getFirstName()));
		userObj.setPrefix(utilityService.dataDecryptString(userObj.getPrefix()));
		userObj.setLastName(utilityService.dataDecryptString(userObj.getLastName()));
		userObj.setGender(utilityService.dataDecryptString(userObj.getGender()));
		userObj.setEmail(givenEmail);
		/* validate, fill and save the userObj */
		var passwordObj = passwordService.getObj();
		passwordObj.setPassword(arguments.rc.password);
		userObj.addPassword(passwordObj);
		/* save userObj */
		userService.save(userObj);
		/* email the user to be saved
		get email resourcebundle */
		var emailRB = resourceBundleService.getResourceBundle("email", session.locale, session.debug);
		/* build textstrings for email */
		var textArgs = [config.displayName];
		/* send email */
		if (structKeyExists(arguments.rc, "createPassword") && arguments.rc.createPassword) {
			/* send e-mail for a newly set password for a new user */
			emailService.sendEmail(
				emailTo=givenEmail
				, subject=resourceBundleService.messageFormat(utilityService.cleanStringRich(inputString=emailRB.subjectPasswordSet), textArgs, session.locale)
				, contentHTML=resourceBundleService.messageFormat(emailRB.textHTMLPasswordSet, textArgs, session.locale)
				, contentPlain=resourceBundleService.messageFormat(utilityService.cleanStringRich(inputString=emailRB.textPlainPasswordSet), textArgs, session.locale)
			);
		} else {
			/* send e-mail for a reset password */
			emailService.sendEmail(
				emailTo=givenEmail
				, subject=resourceBundleService.messageFormat(utilityService.cleanStringRich(inputString=emailRB.subjectPasswordChanged), textArgs, session.locale)
				, contentHTML=resourceBundleService.messageFormat(emailRB.textHTMLPasswordChanged, textArgs, session.locale)
				, contentPlain=resourceBundleService.messageFormat(utilityService.cleanStringRich(inputString=emailRB.textPlainPasswordChanged), textArgs, session.locale)
			);
		}
		/* redirect and give feedback */
		rc.message = { class : "success", text : emailRB.emailPasswordChanged };
		logService.log(rc, 0);
		variables.fw.redirect("home:login", "message");
	}

	public void function retrievePassword(struct rc) output=false hint="" {}

	public void function processRetrievePassword(required struct rc) output=false hint="" {
		/* abort execution in case of CSRF attack */
		utilityService.abortOnCSRFAttack(arguments.rc.csrfToken);
		/* get validation resourcebundle */
		var validationRB = resourceBundleService.getResourceBundle("validation", session.locale, session.debug);
		/* validate user input */
		var valid = {};
		var givenEmail = utilityservice.dataEncryptString(arguments.rc.email);
		/* check if email exists and isValid */
		if (! structKeyExists(arguments.rc, "email") || ! len(arguments.rc.email)) {
			valid.email = { valid : false, message : validationRB.emailNotExists };
		}
		if (structKeyExists(arguments.rc, "email") && ! isValid("email", arguments.rc.email)) {
			valid.email = { valid : false, message : validationRB.emailNotValid };
		}
		transaction {
			/* get userObject */
			if (structIsEmpty(valid)) {
				/* only get unique, active e-mail adresses */
				var userObj = entityLoad("user", { email : givenEmail, isActive : 1 }, true);
				/* check if user exists */
				if (isNull(userObj)) {
					valid.userObj = { valid : false, message : validationRB.userNotFound };
				}
			}
			/* validation failed, return to page and give feedback */
			if (! structIsEmpty(valid)) {
				rc.message = { class : "warning", text : validationRB.validationFailed & utilityService.buildMessage(valid) };
				logService.log(rc, 1);
				variables.fw.redirect(utilityService.getOriginAction(fw=variables.fw), "message,email");
			}
			/* validation passed, reset password and email the user */
			userObj.setFirstName(utilityService.dataDecryptString(userObj.getFirstName()));
			userObj.setPrefix(utilityService.dataDecryptString(userObj.getPrefix()));
			userObj.setLastName(utilityService.dataDecryptString(userObj.getLastName()));
			userObj.setGender(utilityService.dataDecryptString(userObj.getGender()));
			userObj.setEmail(utilityService.dataDecryptString(userObj.getEmail()));
			var passwordObj = entityNew("password");
			var newPassword = utilityService.randomPassword();
			passwordObj.setPassWord(newPassword);
			passwordObj.setReset(1);
			userObj.addPassword(passwordObj);
			userService.save(userObj);
		}
		/* get email resourcebundle */
		var emailRB = resourceBundleService.getResourceBundle("email", session.locale, session.debug);
		/* build custom string based on given password */
		var sendPassword = encrypt(newPassword & "|" & userObj.getId(), config.security.key, "AES", "HEX");
		var shortUrl = shortUrlService.shortenUrl(action="home:login.resetPassword", querystring="key=#sendPassword#");
		var textArgs = [config.displayName, utilityService.buildFullUrl(shortUrl)];
		/* email the user the newpassword link */
		emailService.sendEmail(
			emailTo=userObj.getEmail()
			, subject=resourceBundleService.messageFormat(utilityService.cleanStringRich(inputString=emailRB.subjectNewPassword), textArgs, session.locale)
			, contentHTML=resourceBundleService.messageFormat(emailRB.textHTMLNewPassword, textArgs, session.locale)
			, contentPlain=resourceBundleService.messageFormat(utilityService.cleanStringRich(inputString=emailRB.textPlainNewPassword), textArgs, session.locale)
		);
		rc.message = { class : "success", text : emailRB.emailSentNewPassword };
		logService.log(rc, 1);
		variables.fw.redirect(utilityService.getOriginAction(fw=variables.fw), "message,email");
	}

	public void function createPassword(required struct rc) output=false hint="LET OP! Deze functie bieden we niet aan in deze webapp - wordt altijd ingesteld in Admin. Daarna kan gebruiker zelf via Wachtwoord vergeten een nieuwe w8w instellen." {
		validateTemporaryPassword(arguments.rc.key);
	}

	/* @key encrypted key containing temporary password and userId */
	private string function validateTemporaryPassword(required string key) output=false hint="validate the given temporary password to make sure it is still active and it is not older than 24 hours. If fail, redirect to login.retrievePassword for a new password request. If pass, return the userId." {
		/* check tmppassword */
		var localKey = { original : arguments.key };
		localKey.decrypt = decrypt(arguments.key, config.security.key, "AES", "HEX");
		localKey.userId = listLast(localKey.decrypt, "|");
		localKey.password = listFirst(localKey.decrypt, "|");
		/* retrieve user and check if 24 hours hasn't passed */
		var userQuery = ormExecuteQuery("
			SELECT new map(
				p.password AS password
				, p.salt AS salt
			)
			from user u
				join u.passwords p
			where u.isActive = 1
			and u.id = :userId
			and p.isActive = 1
			and p.reset = 1
			and dateAdd(hh,24, p.createDate) >= getDate()
			order by p.createDate desc
		", { userId : localKey.userid });
		/* check with first array element from getPassword as getCurrentPassword gets the first non-reset password */
		if (arrayIsEmpty(userQuery) || hash(localKey.password & userQuery[1].salt, "SHA-512", "UTF-8") != userQuery[1].password) {
			/* get validation resourcebundle */
			var errorRB = resourceBundleService.getResourceBundle("error", session.locale, session.debug);
			rc.message = { class : "danger", text : errorRB.expiredResetPassword };
			logService.log(rc);
			variables.fw.redirect("home:login.retrievePassword", "message");
		}
		return localKey.userId;
	}

	private void function setUserSession(any userobj) output=false hint="vullen van sessie object obv gebruikersgegevens, tegelijkertijd de sessie een nieuwe unieke ID geven (werkt alleen voor application sessies (CFID), werkt niet bij JEE sessies), zodat session hijacking of session fixation niet mogelijk is" {
		if (! isNull(arguments.userobj.getId())) {
			session.locale = isNull(arguments.userObj.getLocale()) || ! len(arguments.userObj.getLocale()) ? config.defaultLocale : arguments.userObj.getLocale();
			session.debug = 0;
			session.auth = {
				isLoggedIn : true
				, roles : arguments.userObj.getRoleList()
				, userId : arguments.userObj.getId()
			};
			sessionRotate();
		}
	}

}