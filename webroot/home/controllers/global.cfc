component accessors=true output=false hint="" {

	property bugLogService;
	property config;
	property errorService;
	property utilityService;

	public any function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
		return this;
	}

	public void function error(required struct rc) output=false hint="Error handling action." {
		rc.errorcode = createUUID();
		try {
			var extraInfo = { errorcode : rc.errorcode };
			if (structKeyExists(session, "auth")) {
				extraInfo.auth = session.auth;
			}
			extraInfo.rc = utilityService.cleanInputData(rc);
			bugLogService.notifyService(request.exception.cause.message, request.exception, extraInfo);
		} catch(any a) {
			try {
				bugLogService.notifyService(request.exception.cause.message, request.exception, "errorcode: #rc.errorcode#");
			} catch(any a) {
				errorService.sendErrorMail(rc, config.mail.error, config.displayName);
			}
		}
	}

}