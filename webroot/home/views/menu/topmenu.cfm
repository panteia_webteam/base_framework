﻿<cfscript>
	param name="local.active" default="";
	param name="local.srBlock" default="";
	if(getSection() == "main" && getItem() == "main") {
		local.active = " active";
		local.srBlock = "<span class=""sr-only"">(current)</span>";
	}
</cfscript>
<cfoutput>
<ul class="nav navbar-nav mr-auto" id="topmenu">
	<li class="nav-item#local.active#"><a class="nav-link" href="#buildUrl('#getSubsystem()#:')#">welkom#local.srBlock#</a></li>
	<cfif getSection() eq "login">
		<cfset local.active = " active">
		<cfset local.srBlock = "<span class=""sr-only"">(current)</span>">
	</cfif>
	<li class="nav-item#local.active#"><a class="nav-link" href="#buildUrl('login.')#">inloggen#local.srBlock#</a></li>
</ul>
</cfoutput>