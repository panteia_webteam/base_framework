﻿<cfscript>
	rc.title = rc.textConstants.forgotPassword;
	local.btnText = rc.textConstants.retrievePassword;
	local.email = (structKeyExists(rc, "email") ? rc.email : "");
	local.emailPlaceholderText = rc.textConstants.email;
	arrayAppend(rc.features, "validation");
	arrayAppend(rc.arrJSExternal, "#application.applicationUrl#assets/js/views/home/login/retrievepassword.js");
</cfscript>
<cfoutput>
<div class="container">
	<div class="row">
		<div class="col-12 col-sm-8 col-md-6 col-lg-5">
			<h1 class="mb-3">#rc.title#</h1>
			#view("global:main/message")#
			#rc.cs.showContent(getFullyQualifiedAction(), session.locale)#
			<form action="#buildUrl('.processRetrievePassword')#" method="post" role="form">
			<input type="hidden" name="csrfToken" value="#session.CSRFToken#">
			<div class="form-group">
				<input type="email" name="email" id="email" value="#local.email#" class="form-control" placeholder="#local.emailPlaceholderText#" required>
			</div>
			<p><button type="submit" class="btn btn-primary rounded-0">#local.btnText#</button></p>
			</form>
		</div>
	</div>
</div>
</cfoutput>