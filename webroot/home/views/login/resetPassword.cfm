﻿<cfscript>
	rc.title = rc.textConstants.newPassword;
	local.btnText = rc.textConstants.submit;
	local.passwordNewPlaceholderText = rc.textConstants.newPassword;
	local.passwordConfirmPlaceholderText = rc.textConstants.confirmNewPassword;
	local.passwordStrengthText = rc.cs.showContent(getFullyQualifiedAction(), session.locale);
	arrayAppend(rc.features, "validation");
	arrayAppend(rc.features, "pwstrength");
	arrayAppend(rc.arrJSExternal, "#application.applicationUrl#assets/js/views/home/login/resetpassword.js");
</cfscript>
<cfoutput>
<div class="container">
	<div class="row">
		<div class="col-12 col-sm-8 col-md-6 col-lg-5">
			<h1 class="mb-3">#rc.title#</h1>
			#view("global:main/message")#
			<form action="#buildUrl('.processResetPassword')#" method="post" role="form">
				<input type="hidden" name="csrfToken" value="#session.CSRFToken#">
				<input type="hidden" name="tmppassword" value="#rc.key#">
				<div class="form-group">
					<input type="password" name="password" id="password" class="form-control" placeholder="#local.passwordNewPlaceholderText#" autocomplete="false" required>
				</div>
				<div class="form-group">
					<input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="#local.passwordConfirmPlaceholderText#" autocomplete="false" required>
				</div>
				<p><button type="submit" class="btn btn-primary rounded-0">#local.btnText#</button></p>
				<div class="mt-3" id="pwd-container">
					<div class="pwstrength_viewport_progress mb-3"></div>
					<div class="alert alert-info">#local.passwordStrengthText#</div>
				</div>
			</form>
		</div>
	</div>
</div>
</cfoutput>