﻿<cfscript>
	rc.title = rc.textConstants.createPassword;
	arrayAppend(rc.features, "validation");
	arrayAppend(rc.features, "pwstrength");
	arrayAppend(rc.arrJS, "js/views/home.login.resetPassword.js");
</cfscript>
<cfoutput>
<div class="container">
	<div class="row">
		<div class="col-12 col-sm-8 col-md-6 col-lg-5">
			<h1 class="mb-3">#rc.title#</h1>
			#view("home:global/message")#
			<form action="#buildUrl('.processResetPassword')#" method="post" role="form">
			<input type="hidden" name="csrfToken" value="#session.CSRFToken#">
			<input type="hidden" name="tmppassword" value="#rc.key#">
			<input type="hidden" name="createPassword" value="true">
			<div class="form-group">
				<input type="password" name="password" id="password" class="form-control" placeholder="#rc.textConstants.password#" autocomplete="false" required>
			</div>
			<div class="form-group">
				<input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="#rc.textConstants.confirmNewPassword#" autocomplete="false" required>
			</div>
			<p><button type="submit" class="btn btn-primary rounded-0">#rc.textConstants.submit#</button></p>
			<div class="mt-3" id="pwd-container">
				<div class="pwstrength_viewport_progress mb-3"></div>
				<div class="alert alert-info">#rc.cs.showContent(getFullyQualifiedAction(), session.locale)#</div>
			</div>
			</form>
		</div>
	</div>
</div>
</cfoutput>