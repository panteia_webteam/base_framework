﻿<cfscript>
	rc.title = rc.rb.getRBString("global", "login", session.locale);
	local.btnText = rc.textConstants.login;
	local.emailText = rc.textConstants.email;
	local.passwordText = rc.textConstants.password;
	local.passwordRemember = rc.textConstants.rememberPassword;
	local.passwordForgot = rc.textConstants.forgotPassword;
	arrayAppend(rc.features, "validation");
	arrayAppend(rc.arrJSExternal, "#application.applicationUrl#assets/js/views/home/login/main.js");
</cfscript>
<cfoutput>
<div class="container">
	<div class="row">
		<div class="col-12 col-sm-8 col-md-6 col-lg-5">
			<h1 class="mb-3">#rc.title#</h1>
			#view("home:global/message")#
			#rc.cs.showContent(getFullyQualifiedAction(), session.locale)#
			<form action="#buildUrl('.processMain')#" method="post" role="form">
				<input type="hidden" name="csrfToken" value="#session.CSRFToken#">
				<div class="form-group">
					<input type="email" name="email" id="email" class="form-control" placeholder="#local.emailText#" required>
				</div>
				<div class="form-group">
					<input type="password" name="password" id="password" class="form-control" placeholder="#local.passwordText#" required>
				</div>
				<div class="form-check mb-3">
					<label class="form-check-label" for="savePassword">
						<input type="checkbox" class="form-check-input" name="savePassword" id="savePassword" value="1">
						#local.passwordRemember#
					</label>
				</div>
				<p><button type="submit" class="btn btn-primary">#local.btnText#</button> <a href="#buildUrl('.retrievePassword')#" class="ms-3">#local.passwordForgot#</a></p>
			</form>
		</div>
	</div>
</div>
</cfoutput>