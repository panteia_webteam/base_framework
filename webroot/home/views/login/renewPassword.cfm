﻿<cfscript>
	param name="local.defaultUrl" default=getSubSystem()&":";
	local.passwordpolicyService = new model.services.custom.passwordpolicyService();
	local.passwordPolicy = local.passwordpolicyService.activeUser(rc.userId);
	local.sub = {
		"nl_NL" : {
			"title" : "Nieuw wachtwoord instellen"
			, "sub" : "vul onderstaande veld(en) in"
		}
		,  "en_US" : {
			"title" : "Set new password"
			, "sub" : "complete the field(s) below"
		}
	};
	rc.pageTitle = rc.rb.getRBString("global", "newPassword", session.locale);
	local.subTitle = local.passwordPolicy.showAlert && local.passwordPolicy.daysLeftToResetPassword >= 0 ? local.passwordPolicy.alerts.activeTxt : local.passwordPolicy.alerts.redirectTxt;
</cfscript>
<cfoutput>
<h3>#local.sub[session.locale].title#</h3>
<div class="info--subtitle" id="cardSubtitle">#local.subTitle#</div>
#view("global:main/message")#
<form action="#buildUrl('.processRenewPassword')#" method="post" role="form" class="form row" id="form">
	<input type="hidden" name="csrfToken" value="#session.CSRFToken#">
	<input type="hidden" name="userId" value="#rc.userId#">
	<div class="mb-2 col-12">
		<div class="input-group">
			<input type="password" name="password" id="password" class="form-control" placeholder="#LCase(rc.textConstants.newPassword)#" autocomplete="false" required>
		</div>
	</div>
	<div class="mb-3 col-12">
		<div class="input-group">
			<input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="#LCase(rc.textConstants.confirmNewPassword)#" autocomplete="false" required>
		</div>
	</div>
	<div class="mb-0 col-12">
		<button type="submit" class="btn btn-primary btn-sm float-end" id="submitbtn">#LCase(local.passwordPolicy.alerts.btnTxt)#</button>
	</div>
</form>
</cfoutput>