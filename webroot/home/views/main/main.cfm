﻿<cfscript>
	rc.title = rc.cs.showTitle(getFullyQualifiedAction(), session.locale);
	rc.contentText = rc.cs.showContent(getFullyQualifiedAction(), session.locale);
</cfscript>
<cfoutput>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="mb-3">#rc.title#</h1>
			#view("home:global/message")#
			#rc.contentText#
		</div>
	</div>
</div>
</cfoutput>