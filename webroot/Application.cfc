component extends="framework.one" output=false hint="" {

	/* ------------------------ APPLICATION SETTINGS ------------------------ */
	this.applicationroot = getDirectoryFromPath(getCurrentTemplatePath());
	// use the directoryname before the webroot folder to create a recognizable application name and add a hash of the current path to make it unique.
	this.name = listGetAt(getCurrentTemplatePath(), listContainsNoCase(this.applicationroot, "webroot", "\\")-1, "\\") & "_" & hash(this.applicationroot);
	this.applicationTimeout = createTimeSpan(0, 1, 20, 10);
	this.sessionManagement = true;
	// this checks if a cookie is created, for bots this will return false and use the low session timeout
	this.sessionTimeout = createTimeSpan(0, 0, 0, 2);
	if (structKeyExists(cookie, "cfid") || structKeyExists(cookie, "jsessionid")) {
		this.sessionTimeout = createTimeSpan(0, 0, 30, 0);
	}
	this.development = getEnvironment() == "dev" ? true : false;

	/* ------------------------ ORM SETTINGS ------------------------ */
	include "config/datasources.cfm";

	/* ------------------------ FW/1 SETTINGS ------------------------ */
	include "config/fw1config.cfm";
	include "config/mappings.cfm";

	public void function setupApplication() output=false hint="" {
		// clear all caching (pages and CFC's)
		pagePoolClear();
		componentCacheClear();
		application.development = this.development;
		// include application variables (global and local)
		include "config/applicationVariables.cfm";
	}

	public void function setupSession() output=false hint="" {
		// set defaultSession - function sets session-struct elements
		new model.services.default.utilityService().defaultSession();
		// CSRF Token, unique for each user/session - function returns session-struct element
		new model.services.default.utilityService().generateCSRFToken();
	}

	public void function setupRequest() output=false hint="" {
		var globalSettings = new config.global().getConfiguration();
		// set debug
		if (this.development && structKeyExists(request.context, "debug")) {
			session.debug = request.context.debug;
		}
		// reset CRSFtoken
		if (! structKeyExists(session, "CSRFToken")) {
			new model.services.default.utilityService().generateCSRFToken();
		}
		// queue the authorisation
		if (request.base != "/services/") {
			controller("global:security.checkAuthorisation");
		}
		// loading some settings before the actual controller loads; general subsystems
		if (structKeyExists(globalSettings, "arrSubsystem")) {
			var arrSubsystem = globalSettings.arrSubsystem;
			if (arrSubsystem.find(getSubSystem())) {
				controller("global:main.initialSubsystem");
			}
		}
		// loading some settings before the actual controller loads; special subsystems
		if (structKeyExists(globalSettings, "arrSubsystemSpecial")) {
			var arrSubsystem = globalSettings.arrSubsystemSpecial;
			if (arrSubsystem.find(getSubSystem())) {
				var sub = getSubSystem();
				controller("global:#sub#.initialSubsystem");
			}
		}
	}

	/* ------------------------ CALLED WHEN VIEW RENDERING STARTS ------------------------ */
	public void function setupView() output=false hint="" {
		var bf = getBeanFactory();
		var config = bf.getBean("config");
		/* i18n, Locale handeling */
		var strLocale = structKeyExists(session, "locale") ? session.locale : config.defaultLocale;
		setLocale(strLocale);
		rc.rb = bf.getBean("resourcebundleService");
		rc.textConstants = rc.rb.getResourceBundle("global", strLocale, session.debug);
		/* content, handlers to retrieve contentitems */
		rc.cs = bf.getBean("contentService");
		/* set version */
		rc.version = config.version;
		/* set top menu - wellicht overbodig geworden in deze versie van het framework */
		rc.topMenu = bf.getBean("menuService").getTopMenu();
		/* opmaak params CSS/JS */
		param name="rc.features" default="#[]#";
		param name="rc.arrJS" default="#[]#";
		param name="rc.arrJSExternal" default="#[]#";
		param name="rc.arrCustomJS" default="#[]#";
		param name="rc.arrCSS" default="#[]#";
		param name="rc.arrCSSExternal" default="#[]#";
		param name="rc.arrCustomCSS" default="#[]#";
		/* omzetten van CSP-app-vars naar request-vars */
		rc.cspHeaderConnect = application.cspHeaderConnect;
		rc.cspHeaderFont = application.cspHeaderFont;
		rc.cspHeaderForm = application.cspHeaderForm;
		rc.cspHeaderFrame = application.cspHeaderFrame;
		rc.cspHeaderImg = application.cspHeaderImg;
		rc.cspHeaderObject = application.cspHeaderObject;
		rc.cspHeaderScript = application.cspHeaderScript;
		rc.cspHeaderStyle = application.cspHeaderStyle;
	}

	/* ------------------------ CALLED WHEN VIEW IS MISSING ------------------------ */
 	public any function onMissingView() output=false hint="" {
 		var bf = getBeanFactory();
 		/* content handlers to retrieve contentitems */
		var cs = bf.getBean("contentService");
		var logService = bf.getBean("logService");
		/* get the content and clean the action, just to be sure */
		var cleanedAction = canonicalize(getFullyQualifiedAction(), true, true);
		request.context.contentObj = cs.getObjBy({ action : cleanedAction });
		/* if controller and view not found in framework code, but action is solvable and found in database, try and render the content */
		if (! isNull(request.context.contentObj.getId())) {
			/* use the content view to display content */
			return view("global:main/content");
		} else {
			/* prevent onMissingView to handle unhandled path dirs, but otherwise show 404-error page */
			if (listFindNoCase(variables.framework.unhandledExtensions, listLast(request.base, ".")) || REFindNoCase("^(" & variables.framework.unhandledPathRegex & ")", request.base) == 0) {
				request.context.message = { class : "danger", text : "Pagina niet gevonden (#cleanedAction#)." };
				logService.log(request.context);
				getPageContext().getResponse().setStatus(404);
				return view("global:main/404");
			}
		}
	}

	/* ------------------------ ENVIRONMENT CONTROLS ------------------------ */
	public void function factoryConfig(required struct bf, struct framework) output=false hint="imported in the loadlistener of config/fw1config.cfm" {
		// add assetsLibrary to the factory
		bf.addBean("assetsLibrary", new config.assetsLibrary());
		// add config bean to factory
		bf.addBean("config", new config.global().getConfiguration());
		var config = bf.getBean("config");
		// add beans for bugLogService
		bf.addBean("bugLogListener", config.bugLogListener);
		// make them directly accessible for buglogHQ
		bf.addBean("apiKey", config.bugLogApiKey);
		// connection to the participants database
		bf.addBean("mridsn", "web_mri_dummy");
	}

	private string function getEnvironment() output=false hint="" {
		if (findNoCase(".dev.", CGI.SERVER_NAME)) {
			return "dev";
		}
		return "prod";
	}

}