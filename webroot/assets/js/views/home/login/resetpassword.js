$('form').validate({
	rules: {
		password: {
			minlength: 5
		}
		, confirm_password: {
			minlength: 5
			, equalTo: '#password'
		}
	}
});
'use strict';
var options = {};
options.ui = {
	container: '#pwd-container'
	, showVerdicts: false
	, viewports: {
		progress: '.pwstrength_viewport_progress'
	}
};
options.common = {
	debug: false
	, minChar: 5
};
$('#password:password').pwstrength(options);