(function($){
	/* set class to a grouped input formfield */
	$(':input.form-control').focus(function() {
		$(this).parent().addClass('focused');
	});
	$(':input.form-control').blur(function() {
		$(this).parent().removeClass('focused');
	});
	/* trigger tooltips in Admin and Editor */
	var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
	var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
		return new bootstrap.Tooltip(tooltipTriggerEl)
	});
	/* trigger popovers in Registered */
	var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'));
	var popoverList = popoverTriggerList.map(function(popoverTriggerEl){
		return new bootstrap.Popover(popoverTriggerEl);
	});
	/* top-of-page block show/hide + functionality */
	var offset = 120;
	var duration = 500;
	$(window).scroll(function() {
		if ($(this).scrollTop() > offset) {
			$('.back-to-top').fadeIn(duration);
		} else {
			$('.back-to-top').fadeOut(duration);
		}
	});
	$('.back-to-top').click(function(event) {
		event.preventDefault();
		$('html,body').animate({scrollTop:0},duration);
		return false;
	});
	/* unobtrusive opening of links */
	$('a[rel="external"]').each(function(){
		this.target = '_blank';
	});
})(jQuery);