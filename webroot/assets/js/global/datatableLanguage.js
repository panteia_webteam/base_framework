let languageStruct = {
	'lengthMenu': '_MENU_ per page'
	, 'search': '<i class="fa fa-search"></i>'
	, 'info': 'records _START_ - _END_ | tot. _TOTAL_'
	, 'infoEmpty': 'record(s) 0 | tot. 0'
	, 'infoFiltered':  '(from tot. _MAX_)'
	, 'paginate': {
		'first': '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'
		, 'last': '<i class="fa fa-angle-double-right" aria-hidden="true"></i>'
		, 'next': '<i class="fa fa-angle-right" aria-hidden="true"></i>'
		, 'previous': '<i class="fa fa-angle-left" aria-hidden="true"></i>'
	}
	, 'decimal': ','
	, 'thousands': '.'
}
, menuArray = [
	[10, 25, 50, -1]
	, [10, 25, 50, 'Alles']
];