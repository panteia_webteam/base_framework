document.addEventListener('DOMContentLoaded', function(){
	window.addEventListener('scroll', function(){
		if (window.scrollY > 115) {
			document.getElementById('navbar-webapp').classList.add('fixed-top');
			document.getElementById('header-logo-nav').classList.add('header-height');
			// add padding top to show content behind navbar
			navbar_height = document.querySelector('.navbar').offsetHeight;
			document.body.style.paddingTop = navbar_height + 'px';
		} else {
			document.getElementById('navbar-webapp').classList.remove('fixed-top');
			document.getElementById('header-logo-nav').classList.remove('header-height');
			// remove padding top from body
			document.body.style.paddingTop = '0';
		}
	});
});