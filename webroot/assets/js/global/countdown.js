let seconds
	, countdown
	, alertTxt = {
		nl_NL: {
			acceptbtnHTML: 'sessie verlengen'
			, declinebtnHTML: 'sluiten'
			, notification: `Je sessie is bijna verlopen, nog <span id="timeleft"></span>.<br>Klik op '{acceptbtn}' om de sessie opnieuw te activeren.`
			, timesup: `Helaas, je sessie is verlopen.<br>Na het klikken op '{declinebtn}' kan je opnieuw inloggen.`
		}
		, en_US: {
			acceptbtnHTML: 'renew session'
			, declinebtnHTML: 'close'
			, notification: `Your session is about to expire, still <span id="timeleft"></span>.<br>Click '{acceptbtn}' to reactivate the session.`
			, timesup: `Unfortunately, your session has expired.<br>After clicking on '{declinebtn}' you can log in again.`
		}
	};
document.addEventListener('DOMContentLoaded', function() {
	seconds = refreshtime-1;
	startTimer();
});
/* Helpers */
const startTimer = () => {
	const showcountdown = document.getElementById('showcountdown');
	if (showcountdown != undefined) countdown = setInterval(updateTimer, 1000);
}
const updateTimer = () => {
	const showcountdown = document.getElementById('showcountdown'), 
	modalElems = {
		btnAcceptClass: 'btn btn-sm btn-primary'
		, btnAcceptTHML: 'nu verlengen'
		, btnDeclineClass: 'btn btn-sm btn-outline-primary'
		, btnDeclineTHML: 'sluiten'
	};
	let time2Show = properTimeText(seconds);
	showcountdown.innerHTML = time2Show;
	if (document.getElementById('timeleft')) {
		document.getElementById('timeleft').innerHTML = time2Show
	}
	if (seconds === 150) {
		modalElems.btnAcceptTHML = alertTxt[countdownLanguage].acceptbtnHTML;
		modalElems.btnDeclineTHML = alertTxt[countdownLanguage].declinebtnHTML;
		modalElems.txt = `${alertTxt[countdownLanguage].notification.replace('{acceptbtn}', alertTxt[countdownLanguage].acceptbtnHTML)}`;
		modalElems.modalpresent = document.getElementById('staticBackdrop');
		modalElems.sessionend = false;
		activateCountdownModal(modalElems);
	}
	if (seconds <= 0) {
		modalElems.btnDeclineTHML = alertTxt[countdownLanguage].declinebtnHTML;
		modalElems.txt = `${alertTxt[countdownLanguage].timesup.replace('{declinebtn}', alertTxt[countdownLanguage].declinebtnHTML)}`;
		modalElems.modalpresent = document.getElementById('staticBackdrop');
		modalElems.sessionend = true;
		activateCountdownModal(modalElems);
	}
	if (seconds > 0) {
		seconds--;
	}
}
const resetTimer = () => {
	clearInterval(countdown);
	seconds = refreshtime-1;
}
const activateCountdownModal = (optStruct) => {
	let defStruct = { modalId: 'staticBackdrop' }
		, structObj = { ...defStruct, ...optStruct }
		, sessionEnd = structObj.sessionend ? true : false;
	if (! structObj.modalpresent) {
		createModal(structObj);
	}
	document.getElementById('txtcontainer').innerHTML = `${structObj.txt}`;
	if (! structObj.modalpresent) {
		$(`#${structObj.modalId}`).modal('show');
	}
	const btnAccept = document.getElementById('modalaccept')
		, btnDecline = document.getElementById('modaldecline');
	btnDecline.innerHTML = `${structObj.btnDeclineTHML}`;
	if (sessionEnd && btnAccept) {
		btnAccept.remove();
	}
	if(btnAccept) {
		btnAccept.addEventListener('click', function(event) {
			heartbeat();
			resetTimer();
			startTimer();
			$(`#${structObj.modalId}`).modal('hide');
		});
	}
	if (sessionEnd) {
		btnDecline.addEventListener('click', function(event) {
			window.location = logoutUrl
		});
	}
}
// call 2 sever to extend session
const heartbeat = () => {
	getTheData({ url: heartbeatUrl })
	.then(function(results) {
		let returnData = formattingjson(results);
		console.log(returnData);
	})
	.catch(function(err) {
		console.log('Fetch problem: ' + err.message);
	});
}