$.validator.setDefaults({
	errorElement: 'span',
	errorClass: 'help-block',
	highlight: function(element, errorClass, validClass) {
		$(element).closest('.form-group').removeClass('has-no-error');
		$(element).closest('.form-group').addClass('has-error');
		$(element).addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).closest('.form-group').addClass('has-no-error');
		$(element).closest('.form-group').removeClass('has-error');
		$(element).removeClass('error');
	},
	errorPlacement: function(error, element) {
		if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
			error.insertAfter(element.parent());
		} else {
			error.insertAfter(element);
		}
	}
});