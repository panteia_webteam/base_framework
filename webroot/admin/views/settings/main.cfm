<cfscript>
	rc.title = "Change settings";
	rc.breadcrumbArr = [{ title : rc.title }];
	arrayAppend(rc.features, "validation");
	arrayAppend(rc.features, "select2");
	arrayAppend(rc.arrJSExternal, "#application.applicationUrl##getSubSystem()#/assets/js/views/settings/main.js");
	local.formInputDomain = isNull(rc.settingsObj.getDomainName()) ? cgi.server_name : rc.settingsObj.getDomainName();
	local.formInputName = isNull(rc.settingsObj.getDisplayName()) ? "" : rc.settingsObj.getdisplayName();
	local.formInputUrl = isNull(rc.settingsObj.getCopyrightUrl()) ? "" : rc.settingsObj.getCopyrightUrl();
	local.formInputUa = isNull(rc.settingsObj.getUaKey()) ? "" : rc.settingsObj.getUaKey();
	local.formInputFrom = isNull(rc.settingsObj.getEmailFrom()) ? "" : rc.settingsObj.getEmailFrom();
	local.formInputHelpdesk = isNull(rc.settingsObj.getEmailHelpdesk()) ? "" : rc.settingsObj.getEmailHelpdesk();
	local.formInputSubSys = isNull(rc.settingsObj.getPublicSubSystems()) ? "" : rc.settingsObj.getPublicSubSystems();
	local.formInputVersion = isNull(rc.settingsObj.getVersion()) ? "" : rc.settingsObj.getVersion();
</cfscript>
<cfoutput>
#view("global:main/adminmessage")#
<form action="#buildUrl('.processMain')#" method="post" role="form">
	<input type="hidden" name="csrfToken" value="#session.CSRFToken#">
	<input type="hidden" name="id" value="#rc.settingsObj.getId()#">
	<div class="row">
		<div class="col-lg-8">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Global settings</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="domainname" class="mb-2">Domainname</label>
								<input type="text" name="domainname" id="domainname" value="#local.formInputDomain#" class="form-control" placeholder="domainname" disabled>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="displayName" class="mb-2">displayName</label>
								<input type="text" name="displayName" id="displayName" value="#local.formInputName#" class="form-control" placeholder="displayName" required>
							</div>
						</div>
					</div>
					<div class="row mt-2">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="copyrightUrl" class="mb-2">copyrightUrl</label>
								<input type="url" name="copyrightUrl" id="copyrightUrl" value="#local.formInputUrl#" class="form-control" placeholder="copyrightUrl" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="uaKey" class="mb-2">uaKey</label>
								<input type="text" name="uaKey" id="uaKey" value="#local.formInputUa#" class="form-control" placeholder="uaKey">
							</div>
						</div>
					</div>
					<div class="row mt-2">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="emailFrom" class="mb-2">Email From</label>
								<input type="email" name="emailFrom" id="emailFrom" value="#local.formInputFrom#" class="form-control" placeholder="emailFrom" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="emailHelpdesk" class="mb-2">Email Helpdesk</label>
								<input type="email" name="emailHelpdesk" id="emailHelpdesk" value="#local.formInputHelpdesk#" class="form-control" placeholder="emailHelpdesk" required>
							</div>
						</div>
					</div>
					<div class="row mt-2">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="defaultLocale" class="mt-2 mb-2">Default Locale</label>
								<select name="defaultLocale" id="defaultLocale" class="form-select" size="1">
									<option></option>
									<cfloop array="#rc.locales#" item="locale">
										<option value="#locale.toString()#"#rc.settingsObj.getdefaultLocale() eq locale.toString() ? " selected" : ""#>#locale.toString()#</option>
									</cfloop>
								</select>
							</div>
						</div>
					</div>
					<div class="row mt-2">
						<div class="col-lg-6">
							<label class="mb-2">SSL</label>
							<div class="form-check">
								<input type="radio" name="hasSSL" id="hasSSL1" value="true"#rc.settingsObj.getHasSSL() ? " checked" : ""# class="form-check-input">
								<label for="hasSSL1" class="form-check-label">This site uses SSL in production (https://#cgi.server_name#)</label>
							</div>
							<div class="form-check">
								<input type="radio" name="hasSSL" id="hasSSL2" value="false"#! rc.settingsObj.getHasSSL() ? " checked" : ""# class="form-check-input">
								<label for="hasSSL2" class="form-check-label">This site doesn't use SSL in production (http://#cgi.server_name#)</label>
							</div>
						</div>
						<div class="col-lg-6">
							<label class="mb-2">Data encryption</label>
							<div class="form-check">
								<input type="radio" name="protectInput" id="protectInput1" value="true"#rc.settingsObj.getProtectInput() ? " checked" : ""# class="form-check-input">
								<label for="protectInput1" class="form-check-label">yes</label>
							</div>
							<div class="form-check">
								<input type="radio" name="protectInput" id="protectInput2" value="false"#! rc.settingsObj.getProtectInput() ? " checked" : ""# class="form-check-input">
								<label for="protectInput2" class="form-check-label">no</label>
							</div>
						</div>
					</div>
					<div class="row mt-2">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="publicSubSystems" class="mb-2">Public subsystems</label>
								<input type="string" name="publicSubSystems" id="publicSubSystems" value="#local.formInputSubSys#" class="form-control" placeholder="publicSubSystems" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="version" class="mb-2">Version</label>
								<input type="number" name="version" id="version" value="#local.formInputVersion#" class="form-control" placeholder="version" step="0.01" required>
							</div>
						</div>
					</div>
				</div>
			</div>
			<cfif ArrayLen(rc.settingsObj.getCustomProperties())>
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Application specific settings</h3>
					</div>
					<div class="box-body">
						<cfset customPropertyStruct = {}>
						<cfloop array="#rc.settingsObj.getCustomProperties()#" index="customProperty">
							<div class="form-group">
								<label for="name">#customProperty#</label>
								<input type="text" name="#customProperty#" value="#rc.settingsObj.getPropertyValuesStruct()[customProperty]#" class="form-control" required>
							</div>
						</cfloop>
					</div>
				</div>
			</cfif>
			<div class="box box-primary">
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="small-box bg-purple">
				<div class="inner">
					<h3>#lsDateFormat(rc.settingsObj.getCreateDate(), "Long", session.locale)#</h3>
					<p>
						<cfif not isNull(rc.settingsObj.getCreatedByUser().getId())>
							Created by #rc.settingsObj.getCreatedByUser().getFullName()#
						<cfelse>
							Create date
						</cfif>
					</p>
				</div>
				<div class="icon"><i class="fa fa-calendar"></i></div>
				<div class="small-box-footer">&nbsp;</div>
			</div>
			<cfif not isNull(rc.settingsObj.getUpdateDate())>
				<div class="small-box bg-green">
					<div class="inner">
						<h3>#lsDateFormat(rc.settingsObj.getUpdateDate(), "Long", session.locale)#</h3>
						<p>Last changed by #rc.settingsObj.getUpdatedByUser().getFullName()#</p>
					</div>
					<div class="icon"><i class="fa fa-calendar"></i></div>
					<div class="small-box-footer">&nbsp;</div>
				</div>
			</cfif>
			<div class="small-box bg-red">
				<div class="inner">
					<h3>Application specific</h3>
					<p>If there is a need for some global variables to be used throughout the application, this can be done by adding them to the &quot;settings&quot; bean. Contact the developers for this.</p>
					<p>Custom settings can automagically be edited from this page.</p>
				</div>
				<div class="icon"><i class="fa fa-cog"></i></div>
				<div class="small-box-footer">&nbsp;</div>
			</div>
		</div>
	</div>
</form>
</cfoutput>