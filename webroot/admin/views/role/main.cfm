<cfscript>
	rc.title = "Role overview";
	arrayAppend(rc.arrCSSExternal, application.dataTablesCSS);
	arrayAppend(rc.arrJSExternal, application.dataTablesJS);
	arrayAppend(rc.arrJSExternal, "#application.applicationURL##getSubSystem()#/assets/js/global/datatableLanguage.js");
	arrayAppend(rc.arrJSExternal, "#application.applicationURL##getSubSystem()#/assets/js/views/role/main.js");
</cfscript>
<cfoutput>
#view("global:main/adminmessage")#
<div class="row">
	<div class="col-12">
		<div class="box box-solid">
			<div class="box-body">
				#rc.cs.showContent(getFullyQualifiedAction(), session.locale)#
			</div>
		</div>
	</div>
</div>
<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title">#rc.title#</h3>
		<div class="box-tools float-end">
			<a href="#buildUrl('.role')#" class="btn btn-success btn-sm"><i class="icol-add"></i> Add role</a>
		</div>
	</div>
	<div class="box-body table-responsive">
		<table id="dataTable" class="table table-bordered table-striped responsive" style="width:100%;">
			<thead>
				<tr>
					<th scope="col">Name</th>
					<th scope="col">Description</th>
					<th scope="col" class="text-center">Active</th>
					<th scope="col" class="text-center">Change</th>
					<th scope="col" class="text-center">Delete</th>
				</tr>
			</thead>
			<tbody>
				<cfloop array="#rc.roleArr#" index="currentItem">
					 <tr>
						<td>#currentItem.name#</td>
						<td>#currentItem.description#</td>
						<td class="text-center">
							<span class="badge text-bg-#currentItem.isActive ? 'success' : 'warning'#">#currentItem.isActive ? "Active" : "Inactive"#</span>
						</td>
						<td class="text-center">
							<a href="#buildUrl(action='.role', queryString='id=#currentItem.id#')#"><i class="fa fa-pencil-square"></i></a>
						</td>
						<td class="text-center">
							<a href="#buildUrl(action='.delete', queryString='id=#currentItem.id#')#" class="removeIcon" title="Remove #currentItem.name#"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
				</cfloop>
			</tbody>
		</table>
	</div>
</div>
</cfoutput>