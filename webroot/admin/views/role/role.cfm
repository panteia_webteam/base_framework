<cfscript>
	rc.title = ! len(rc.id) ? "Add role" : "Edit role: " & rc.roleObj.getName();
	rc.breadcrumbArr = [
		{
			action : "#getSubSystem()#:role"
			, title : "Role overview"
		}, {
			title : rc.title
		}
	];
	arrayAppend(rc.arrJSExternal, "#application.applicationUrl##getSubSystem()#/assets/js/views/role/roleitem.js");
	arrayAppend(rc.features, "validation");
</cfscript>
<cfoutput>
#view("global:main/adminmessage")#
<div class="row">
	<div class="col-md-8">
		<form action="#buildUrl('.processRole')#" method="post" role="form">
			<input type="hidden" name="csrfToken" value="#session.CSRFToken#">
			<input type="hidden" name="id" value="#rc.roleObj.getId()#">
			<div class="box box-primary">
				<div class="box-header pb-0">
					<h3 class="box-title">Global information</h3>
				</div>
				<div class="box-body pt-0">
					<div class="form-group">
						<label for="name" class="mt-2 mb-2">Name</label>
						<input type="text" name="name" id="name" value="#rc.roleObj.getName()#" class="form-control" placeholder="name" required>
					</div>
					<div class="form-group">
						<label for="" class="mt-2 mb-2">Description</label>
						<input type="text" name="description" id="description" value="#rc.roleObj.getDescription()#" class="form-control" placeholder="Enter a brief description for the given role">
					</div>
				</div>
			</div>
			<div class="box pb-0">
				<div class="box-header pb-0">
					<h3 class="box-title">Access settings</h3>
				</div>
				<div class="box-body pt-0">
					<div class="row">
						<div class="form-group col-12 col-lg-6">
							<label for="name" class="mt-2 mb-2">Whitelist</label>
							<input type="text" name="whitelist" id="whitelist" value="#rc.roleObj.getWhitelist()#" class="form-control" placeholder="for example: home:, registered:userview.main">
						</div>
						<div class="form-group col-12 col-lg-6">
							<label for="name" class="mt-2 mb-2">Blacklist</label>
							<input type="text" name="blacklist" id="blacklist" value="#rc.roleObj.getBlacklist()#" class="form-control" placeholder="for example: home:, registered:userview.main">
						</div>
					</div>
				</div>
			</div>
			<div class="box">
				<div class="box-header pb-0">
					<h3 class="box-title">Site settings</h3>
				</div>
				<div class="box-body pt-0">
					<label class="mt-2 mb-2">Role active <i class="fa fa-info-circle" data-toggle="tooltip" title="Determines if the current role is active"></i></label>
					<div class="form-check">
						<input type="checkbox" name="isActive" id="isActive" value="1"#rc.roleObj.getIsActive() ? " checked" : ""# class="form-check-input">
						<label for="isActive" class="form-check-label">Active</label>
					</div>
				</div>
			</div>
			<div class="box box-primary">
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-4">
		<div class="small-box bg-purple">
			<div class="inner">
				<h3>#lsDateFormat(rc.roleObj.getCreateDate(), "Long", session.locale)#</h3>
				<p>
					<cfif not isNull(rc.roleObj.getCreatedByUser().getId())>
						Created by #rc.roleObj.getCreatedByUser().getFullName()#
					<cfelse>
						Create date
					</cfif>
				</p>
			</div>
			<div class="icon"><i class="fa fa-calendar"></i></div>
			<div class="small-box-footer">&nbsp;</div>
		</div>
		<cfif not isNull(rc.roleObj.getUpdateDate())>
			<div class="small-box bg-green">
				<div class="inner">
					<h3>#lsDateFormat(rc.roleObj.getUpdateDate(), "Long", session.locale)#</h3>
					<p>Last changed by #rc.roleObj.getUpdatedByUser().getFullName()#</p>
				</div>
				<div class="icon"><i class="fa fa-calendar"></i></div>
				<div class="small-box-footer">&nbsp;</div>
			</div>
		</cfif>
		<div class="small-box bg-blue">
			<div class="inner">
				<h3>#rc.roleObj.getUserCount()#</h3>
				<p>Members</p>
			</div>
			<div class="icon"><i class="fa fa-users"></i></div>
			<a href="#buildUrl(action='user', queryString='roleId=#rc.roleObj.getId()#')#" class="small-box-footer">show members <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
</div>
</cfoutput>