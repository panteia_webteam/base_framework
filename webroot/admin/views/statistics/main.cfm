<cfscript>
	rc.title = "User statistics";
	arrayAppend(rc.features, "jqueryKnob");
	arrayAppend(rc.arrJSExternal, "#application.applicationUrl##getSubSystem()#/assets/js/views/statistics/main.js");
</cfscript>
<cfoutput>
#view("global:main/adminmessage")#
<div class="row">
	<div class="col-12">
		<div class="box box-solid">
			<div class="box-body">
				#rc.cs.showContent(getFullyQualifiedAction(), session.locale)#
			</div>
		</div>
	</div>
</div>
<div class="box box-primary">
	<div class="box-header">
		<i class="fa fa-bar-chart"></i>
		<h3 class="box-title">Totals alls users</h3>
	</div>
	<div class="box-body">
		<cfloop array="#rc.userStats#" item="userCount" index="i">
			<cfif listFindNoCase("1,5,9", i)>
				<div class="row">
			</cfif>
			<div class="col-12 col-md-6 col-xl-3 text-center">
				<input type="text" value="#userCount.userCount#" class="knob" data-width="#userCount.size#" data-height="#userCount.size#" data-fgColor="##3c8dbc" data-readonly="true" data-max="#userCount.sumCount#">
				<div class="knob-label">#userCount.title#</div>
			</div>
			<cfif listFindNoCase("4,8,12", i)>
				</div>
				<hr>
			</cfif>
			<cfif i eq arrayLen(rc.userStats)>
				<div class="row">
					<div class="col-12 text-center">
						<h1 class="headline"><span class="knobTotalNumber">#userCount.sumCount#</span> total logins</h1>
					</div>
				</div>
			</cfif>
		</cfloop>
	</div>
</div>
</cfoutput>