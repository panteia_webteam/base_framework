<cfscript>
	rc.title = "i18n overview";
	arrayAppend(rc.arrCSSExternal, application.dataTablesCSS);
	arrayAppend(rc.arrJSExternal, application.dataTablesJS);
	arrayAppend(rc.arrJSExternal, application.dataTablesDateSorting);
	arrayAppend(rc.arrJSExternal, "#application.applicationUrl##getSubSystem()#/assets/js/global/datatableLanguage.js");
	arrayAppend(rc.arrJSExternal, "#application.applicationUrl##getSubSystem()#/assets/js/views/content/i18n.js");
</cfscript>
<cfoutput>
#view("global:main/adminmessage")#
<div class="row">
	<div class="col-12">
		<div class="box box-solid">
			<div class="box-body">
				#rc.cs.showContent(getFullyQualifiedAction(), session.locale)#
			</div>
		</div>
	</div>
</div>
<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title">Current i18n items</h3>
		<div class="box-tools float-end">
			<a href="#buildUrl('i18n.i18n')#" class="btn btn-success btn-sm"><i class="icol-add"></i> Add i18n</a>
		</div>
	</div>
	<div class="box-body table-responsive">
		<table id="dataTable" class="table table-bordered table-striped responsive" style="width:100%;">
			<thead>
				<tr>
					<th scope="col">Bundle</th>
					<th scope="col">Locale</th>
					<th scope="col">Variable</th>
					<th scope="col">Create Date</th>
					<th scope="col">Update Date</th>
					<th scope="col" class="text-center">Change</th>
				</tr>
			</thead>
			<tbody>
				<cfloop collection="#rc.resourcebundleStruct#" index="bundle">
					<cfloop collection="#rc.resourcebundleStruct[bundle]#" index="item">
						<tr>
							<td>#rc.resourcebundleStruct[bundle][item]["bundle"]#</td>
							<td>#rc.resourcebundleStruct[bundle][item]["locale"]#</td>
							<td>#rc.resourcebundleStruct[bundle][item]["item"]#</td>
							<td>#rc.rb.messageFormat("{1,date,dd-MM-yyyy}", [rc.resourcebundleStruct[bundle][item]["createDate"]], session.locale)#</td>
							<td>#not isNull(rc.resourcebundleStruct[bundle][item]["updateDate"]) ? rc.rb.messageFormat("{1,date,dd-MM-yyyy}", [rc.resourcebundleStruct[bundle][item]["updateDate"]], session.locale) : ""#</td>
							<td class="text-center"><a href="#buildUrl(action='i18n.i18n', queryString='item=#rc.resourcebundleStruct[bundle][item]['item']#&bundle=#rc.resourcebundleStruct[bundle][item]['bundle']#')#"><i class="fa fa-pencil-square"></i></a></td>
						</tr>
					</cfloop>
				</cfloop>
			</tbody>
		</table>
	</div>
</div>
</cfoutput>