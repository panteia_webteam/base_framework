<cfscript>
	rc.title = ! len(rc.item) ? "Add i18n" : "Edit i18n: " & rc.resourcebundlesArr[1]["item"];
	rc.breadcrumbArr = [
		{
			action : "#getSubSystem()#:i18n"
			, title : "i18n overview"
		}, {
			title : rc.title
		}
	];
	arrayAppend(rc.features, "validation");
	arrayAppend(rc.features, "select2");
	arrayAppend(rc.arrJSExternal, application.ckEditor5);
	arrayAppend(rc.arrJSExternal, "#application.applicationUrl##getSubSystem()#/assets/js/views/content/i18nitem.js");
</cfscript>
<cfoutput>
#view("global:main/adminmessage")#
<form action="#buildUrl('.processI18N')#" method="post" role="form">
	<input type="hidden" name="csrfToken" value="#session.CSRFToken#">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">General description</h3>
		</div>
		<div class="box-body">
			<cfif not rc.hasResourceBundles>
				<p class="alert alert-warning">A bundle/item combination cannot be changed.</p>
			</cfif>
			<div class="row">
				<div class="form-group col-12 col-lg-6">
					<label for="item" class="mt-2 mb-2">Variable <i class="fa fa-info-circle" data-toggle="tooltip" title="Variable used in the code, to be replaced by the text in the chosen locale"></i></label>
					<input type="text" name="item" class="form-control" placeholder="Enter variable name" value="#not rc.hasResourceBundles ? rc.resourcebundlesArr[1]['item'] : ''#"#not rc.hasResourceBundles ? " readonly" : ""# required>
				</div>
				<div class="form-group col-12 col-lg-6">
					<label for="bundle" class="mt-2 mb-2">Bundle <i class="fa fa-info-circle" data-toggle="tooltip" title="The bundle the variable belongs too, for example: global, validation, email"></i></label>
					<input type="text" name="bundle" class="form-control" placeholder="Enter bundle name" value="#not rc.hasResourceBundles ? rc.resourcebundlesArr[1]['bundle'] : ''#"#not rc.hasResourceBundles ? " readonly" : ""# required>
				</div>
			</div>
		</div>
	</div>
	<div class="box">
		<div class="box-header pb-0">
			<h3 class="box-title">Locale settings</h3>
			<div class="box-tools float-end">
				<a href="##" id="addLocale" class="btn btn-primary btn-sm">Add Locale</a>
			</div>
		</div>
		<div class="box-body pt-0" id="locales">
			<div id="localeSelect">
				<cfif not rc.hasResourceBundles>
					<cfloop array="#rc.resourcebundlesArr#" item="resourceBundle" index="i">
						<div class="form-group">
							<label for="locale" class="mt-2 mb-2">Locale</label>
							<select name="locale[]" id="locale#i#" class="form-control locale" size="1">
								<option></option>
								<cfloop array="#rc.locales#" item="locale">
									<option value="#locale.toString()#"#resourceBundle["locale"] eq locale.toString() ? " selected" : ""# placeholder="Enter variable name">#locale.toString()#</option>
								</cfloop>
							</select>
						</div>
						<div class="form-group">
							<label for="text" class="mt-2 mb-2">Text</label>
							<textarea name="content[]" id="content#i#" class="form-control" required>#resourceBundle.text#</textarea>
						</div>
					</cfloop>
				<cfelse>
					<div class="form-group">
						<label for="locale" class="mt-2 mb-2">Locale</label>
						<select name="locale[]" id="locale1" class="form-control locale" size="1">
							<option></option>
							<cfloop array="#rc.locales#" item="locale">
								<option value="#locale.toString()#"#session.locale eq locale.toString() ? " selected" : ""#>#locale.toString()#</option>
							</cfloop>
						</select>
					</div>
					<div class="form-group">
						<label for="text" class="mt-2 mb-2">Text</label>
						<textarea name="content[]" id="content1" class="form-control" required></textarea>
					</div>
				</cfif>
			</div>
		</div>
	</div>
	<div class="box box-primary">
		<div class="box-footer">
			<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
</form>
</cfoutput>