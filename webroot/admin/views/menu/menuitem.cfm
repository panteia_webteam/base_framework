<cfscript>
	rc.title = "Edit Menuitem";
	rc.breadcrumbArr = [
		{
			action : "admin:menu"
			, title : "Menu overview"
		}, {
			title : rc.title
		}
	];
	rc.inlineScriptNonce = CreateGUID();
	rc.cspHeaderScript &= " 'nonce-#rc.inlineScriptNonce#'";
	arrayAppend(rc.features, "validation");
	arrayAppend(rc.features, "select2");
	arrayAppend(rc.arrJSExternal, "#application.applicationUrl#admin/assets/js/views/menu/menuitem.js");
	arrayAppend(rc.arrCustomJS, "
		var menugroups = #serializeJSON(rc.menuArr)#;
	");
	arrayAppend(rc.arrCustomCSS, "
		.parent-title { font-size:1.1em; }
		.parent-description { font-size:.9em; color:##888; }
		.parent-action { font-weight:normal; float:right; color:##888; }
	");
	local.parrentsArrLen = arrayLen(rc.parrentsArr);
</cfscript>
<cfoutput>
#view("global:main/adminmessage")#
<form action="#buildUrl('.processMenuItem')#" method="post" role="form">
	<input type="hidden" name="csrfToken" value="#session.CSRFToken#">
	<input type="hidden" name="id" value="#rc.menuItemObj.getId()#">
	<div class="row">
		<div class="col-lg-8">
			<div class="box">
				<div class="box-header pb-0">
					<h3 class="box-title">General description</h3>
				</div>
				<div class="box-body pt-0">
					<div class="row">
						<div class="col-12 col-lg-5">
							<div class="form-group">
								<label for="actionField" class="mt-2 mb-2">Action</label>
								<input type="text" name="actionField" class="form-control" placeholder="Enter FW/1 action" value="#rc.menuItemObj.getAction()#" required>
							</div>
						</div>
						<div class="col-12 col-lg-7">
							<div class="form-group">
								<label for="title" class="mt-2 mb-2">Title</label>
								<input type="text" name="title" class="form-control" placeholder="The title of the menuitem" value="#rc.menuItemObj.getTitle()#" required>
							</div>
						</div>
					</div>
					<div class="row mt-2">
						<div class="col-12">
							<div class="form-group">
								<label for="description" class="mb-2">Description</label>
								<input type="text" name="description" class="form-control" placeholder="The description of the menuitem" value="#rc.menuItemObj.getDescription()#">
							</div>
						</div>
					</div>
					<div class="row mt-2">
						<div class="form-group col-12 col-lg-7">
							<label for="menu" class="mb-2">Menugroup <i class="fa fa-info-circle" data-toggle="tooltip" title="In which menu on the page this item is shown"></i></label>
							<input type="text" name="menu" class="form-control" placeholder="Enter menu name" value="#rc.menuItemObj.getMenu()#" autocomplete="off" required>
						</div>
						<div class="form-group col-12 col-lg-5">
							<label for="listorder" class="mb-2">Listorder <i class="fa fa-info-circle" data-toggle="tooltip" title="The order in which its sibling menuitems are shown"></i></label>
							<input type="number" name="listorder" class="form-control" placeholder="Enter the numeric ordering position" value="#rc.menuItemObj.getListOrder()#" required>
						</div>
					</div>
					<div class="row mt-2">
						<div class="form-group col-12 col-lg-7">
							<label for="listorder" class="mb-2">Parent <i class="fa fa-info-circle" data-toggle="tooltip" title="The parent for the current menuitem"></i></label>
							<select class="form-select col-2" id="parents" name="parent">
								<option></option>
								<cfloop array="#rc.parrentsArr#" index="local.i" item="parent">
									<cfif local.i eq 1>
										<optgroup label="#parent['menu']#">
									<cfelseif local.previousMenu neq parent["menu"]>
										</optgroup>
										<optgroup label="#parent['menu']#">
									</cfif>
									<option #not isNull(parent["description"]) ? "data-description=""#parent['description']#""" : ""# #not isNull(parent["action"]) ? "data-action=""#parent['action']#""" : ""# value="#parent['id']#">#parent["title"]#</option>
									<cfset local.previousMenu = parent["menu"]>
									<cfif i eq local.parrentsArrLen>
										</optgroup>
									</cfif>
								</cfloop>
							</select>
						</div>
						<div class="col-12 col-lg-5">
							<label class="mb-2">Menuitem active <i class="fa fa-info-circle" data-toggle="tooltip" title="Determines if the current menuitem is active"></i></label>
							<div class="form-check">
								<label for="isActive" class="form-check-label">Active</label>
								<input type="checkbox" name="isActive" id="isActive" value="1"#rc.menuItemObj.getIsActive() ? " checked" : ""# class="form-check-input">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box box-primary">
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="small-box bg-purple">
				<div class="inner">
					<h3>#lsDateFormat(rc.menuItemObj.getCreateDate(), "Long", session.locale)#</h3>
					<p>
						<cfif not isNull(rc.menuItemObj.getCreatedByUser().getId())>
							Created by #rc.menuItemObj.getCreatedByUser().getFullName()#
						<cfelse>
							Create date
						</cfif>
					</p>
				</div>
				<div class="icon">
					<i class="fa fa-calendar"></i>
				</div>
				<div class="small-box-footer">
					&nbsp;
				</div>
			</div>
			<cfif not isNull(rc.menuItemObj.getUpdateDate())>
				<div class="small-box bg-green">
					<div class="inner">
						<h3>#lsDateFormat(rc.menuItemObj.getUpdateDate(), "Long", session.locale)#</h3>
						<p>Last changed by #rc.menuItemObj.getUpdatedByUser().getFullName()#</p>
					</div>
					<div class="icon">
						<i class="fa fa-calendar"></i>
					</div>
					<div class="small-box-footer">
						&nbsp;
					</div>
				</div>
			</cfif>
		</div>
	</div>
</form>
</cfoutput>