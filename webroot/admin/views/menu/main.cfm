﻿<cfscript>
	rc.title = "Menu overview";
	arrayAppend(rc.arrCSSExternal, application.dataTablesCSS);
	arrayAppend(rc.arrJSExternal, application.dataTablesJS);
	arrayAppend(rc.arrJSExternal, "#application.applicationURL#admin/assets/js/global/datatableLanguage.js");
	arrayAppend(rc.arrJSExternal, "#application.applicationURL#admin/assets/js/views/menu/main.js");
</cfscript>
<cfoutput>
#view("global:main/adminmessage")#
<div class="row">
	<div class="col-12">
		<div class="box box-solid">
			<div class="box-body">
				#rc.cs.showContent(getFullyQualifiedAction(), session.locale)#
			</div>
		</div>
	</div>
</div>
<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title">Current menuitems</h3>
		<div class="box-tools float-end">
			<a href="#buildUrl('.menuitem')#" class="btn btn-success btn-sm"><i class="icol-add"></i> Add menu</a>
		</div>
	</div>
	<div class="box-body table-responsive">
		<table id="dataTable" class="table table-bordered table-striped responsive" style="width:100%;">
			<thead>
				<tr>
					<th scope="col">Action</th>
					<th scope="col">Title</th>
					<th scope="col">Menu</th>
					<th scope="col" class="text-center">Active</th>
					<th scope="col" class="text-center">Change</th>
					<th scope="col" class="text-center">Delete</th>
				</tr>
			</thead>
			<tbody>
				<cfloop array="#rc.menu#" index="currentItem">
					<tr>
						<td>#not isNull(currentItem["action"]) ? currentItem["action"] : ""#</td>
						<td>#not isNull(currentItem["title"]) ? currentItem["title"] : ""#</td>
						<td>#not isNull(currentItem["menu"]) ? currentItem["menu"] : ""#</td>
						<td class="text-center">
							<span class="badge text-bg-#(currentItem['isActive'] ? 'success' : 'warning')#">#(currentItem["isActive"] ? "Active" : "Inactive")#</span>
						</td>
						<td class="text-center"><a href="#buildUrl(action='.menuItem', queryString='id=#currentItem["id"]#')#" ><i class="fa fa-pencil-square"></i></a></td>
						<td class="text-center"><a href="#buildUrl(action='.deleteMenuItem', queryString='id=#currentItem["id"]#')#" class="removeIcon" title="Remove #not isNull(currentItem['title']) ? currentItem['title'] : ''#"><i class="fa fa-trash"></i></a></td>
					</tr>
				</cfloop>
			</tbody>
		</table>
	</div>
</div>
</cfoutput>