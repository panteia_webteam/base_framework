<cfscript>
	rc.title = "Backup";
</cfscript>
<cfoutput>
#view("home:global/message")#
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Choose tables to export</h3>
			</div>
			<form method="post" action="#buildUrl('.download')#" role="form">
				<div class="box-body">
					<div class="row">
						<div class="col-md-4">
							<cfset count = 0>
							<cfloop query="rc.tables">
								<cfset count += 1>
								<div class="checkbox">
									<label for="tables#count#">
										<input type="checkbox" name="tables" id="tables#count#" value="#rc.tables.table_Name#"> #rc.tables.table_Name#
									</label>
								</div>
								<cfif rc.tables.CurrentRow MOD ceiling(rc.tables.recordCount/3) is 0 OR rc.tables.currentRow EQ rc.tables.recordCount>
									</div>
									<cfif rc.tables.currentRow LT rc.tables.recordCount>
										<div class="col-md-4">
									</cfif>
								</cfif>
							</cfloop>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>
</cfoutput>