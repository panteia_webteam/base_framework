<cfscript>
	rc.title = ! len(rc.id) ? "Add answergroup" : "Edit answergroup: " & rc.answergroupObj.getName();
	rc.breadcrumbArr = [
		{
			action : "admin:answergroup"
			, title : "Answergroup overview"
		}, {
			title : rc.title
		}
	];
	arrayAppend(rc.features, "validation");
	arrayAppend(rc.features, "bootstrapswitch");
	arrayAppend(rc.arrJS, "../admin/assets/js/views/answergroup/answergroup.js");
</cfscript>
<cfoutput>
#view("home:global/message")#
<div class="row">
	<div class="col-md-8">
		<form action="#buildUrl('.processAnswergroup')#" method="post" role="form">
			<input type="hidden" name="csrfToken" value="#session.CSRFToken#">
			<input type="hidden" name="id" value="#rc.answergroupObj.getId()#">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Global information</h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" name="name" class="form-control" placeholder="name" value="#rc.answergroupObj.getName()#"#not isNull(rc.answergroupObj.getId()) ? " disabled" : ""#>
					</div>
				</div>
			</div>
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Answer options</h3>
					<div class="box-tools pull-right">
						<button type="button" id="addAnswer" class="btn btn-primary btn-xs">Add Answer</button>
					</div>
				</div>
				<div class="box-body" id="locales">
					<div class="row">
						<div class="col-md-6">
							<label for="locale">Name</label>
						</div>
						<div class="col-md-3">
							<label for="text">Value</label>
						</div>
						<div class="col-md-2">
							<label for="text">Listorder</label>
						</div>
						<div class="col-md-1">
							<label for="text">Active</label>
						</div>
					</div>
					<div id="answer">
						<cfif rc.hasAnswers>
							<cfloop array="#rc.answerArr#" item="answer" index="i">
								<div class="row">
									<div class="col-md-6">
										<input type="hidden" class="form-control id" name="answerId[]" value="#answer.answerid#">
										<div class="form-group">
											<input type="hidden" class="form-control" name="answername[]" value="#answer.answername#">
											<input type="text" class="form-control" name="answernameShown[]" value="#answer.answername#" disabled>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<input type="hidden" class="form-control" name="value[]" value="#answer.value#">
											<input type="number" class="form-control" name="valueShown[]" value="#answer.value#" disabled>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<input type="number" class="form-control" name="listorder[]" value="#answer.listorder#">
										</div>
									</div>
									<div class="col-md-1">
										<div class="checkbox">
											<input type="checkbox" class="activeCheck"#answer.isactive ? " checked" : ""#>
											<input type="hidden" name="isActive[]" value="#answer.isactive#">
										</div>
									</div>
								</div>
							</cfloop>
						<cfelse>
							<div class="row">
								<div class="col-md-6">
									<input type="hidden" class="form-control id" name="answerId[]">
									<div class="form-group">
										<input type="text" class="form-control" name="answername[]" placeholder="Answer name">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<input type="text" class="form-control" name="value[]" placeholder="Answer value" value="1">
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<input type="number" class="form-control" name="listorder[]" placeholder="Ordering nr." value="1">
									</div>
								</div>
								<div class="col-md-1">
									<div class="checkbox">
										<input type="checkbox" class="activeCheck" checked>
										<input type="hidden" name="isActive[]" value="1">
									</div>
								</div>
							</div>
						</cfif>
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-4">
		<div class="small-box bg-blue">
			<div class="inner">
				<h3>Attention!</h3>
				<p>Answergroups and its values cannot be changed. This is due to pointers from historic data. If a change is necessary, contact a developer, so he can recode the existing data too.</p>
				<p>It is possible to add an answer, change the order or toggle the active state.</p>
				<p>Inactive answers will still be shown here and in historic data, but cannot be selected for new data.</p>
			</div>
			<div class="icon">
				<i class="fa fa-exclamation"></i>
			</div>
			<div class="small-box-footer">
				&nbsp;
			</div>
		</div>
		<div class="small-box bg-purple">
			<div class="inner">
				<h3>#lsDateFormat(rc.answergroupObj.getCreateDate(), "Long", session.locale)#</h3>
				<p>
					<cfif NOT isNull(rc.answergroupObj.getCreatedByUser().getId())>
						Created by #rc.answergroupObj.getCreatedByUser().getFullName()#
					<cfelse>
						Create date
					</cfif>
				</p>
			</div>
			<div class="icon">
				<i class="fa fa-calendar"></i>
			</div>
			<div class="small-box-footer">
				&nbsp;
			</div>
		</div>
		<cfif NOT isNull(rc.answergroupObj.getUpdateDate())>
			<div class="small-box bg-green">
				<div class="inner">
					<h3>#lsDateFormat(rc.answergroupObj.getUpdateDate(), "Long", session.locale)#</h3>
					<p>Last changed by #rc.answergroupObj.getUpdatedByUser().getFullName()#</p>
				</div>
				<div class="icon">
					<i class="fa fa-calendar"></i>
				</div>
				<div class="small-box-footer">
					&nbsp;
				</div>
			</div>
		</cfif>
	</div>
</div>
</cfoutput>