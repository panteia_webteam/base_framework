<cfscript>
	rc.title = "Answergroup overview";
	arrayAppend(rc.arrCSSExternal, application.dataTablesCSS);
	arrayAppend(rc.arrJSExternal, application.dataTablesJS);
 	arrayAppend(rc.arrCustomJs, "
		$(function() {
			$('##dataTable').dataTable({
				'aoColumnDefs': [{
					'bSortable': false,
					'aTargets': [2]
				}]
			});
		});
	");
</cfscript>
<cfoutput>
#view("home:global/message")#
<div class="box">
	<div class="box-header">
		<h3 class="box-title">#rc.title#</h3>
		<div class="box-tools pull-right">
			<a href="#buildUrl('.answergroup')#" class="btn btn-success btn-sm"><i class="icol-add"></i> Add</a>
		</div>
	</div>
	<div class="box-body">
		<p>Anwergroups cannot be set to inactive or deleted by the administrator. To set an answergroup or one of its answers to inactive, contact a developer, so they can assess the impact of the changes.</p>
		<table id="dataTable" class="table table-bordered table-striped table-responsive">
			<thead>
				<tr>
					<th>Name</th>
					<th class="text-center" style="width:70px;">Active</th>
					<th class="text-center" style="width:50px;">Change</th>
				</tr>
			</thead>
			<tbody>
				<cfloop array="#rc.answergroupArr#" index="currentItem">
					 <tr>
						<td>#currentItem.name#</td>
						<td class=" text-center">
							<span class="label label-#currentItem.isactive ? 'success' : 'warning'#">#currentItem.isactive ? 'Active' : 'Inactive'#</span>
						</td>
						<td class=" text-center">
							<a href="#buildUrl(action='.answergroup',querystring='id=#currentItem.id#')#"><i class="fa fa-pencil-square-o"></i></a>
						</td>
					</tr>
				</cfloop>
			</tbody>
		</table>
	</div>
</div>
</cfoutput>