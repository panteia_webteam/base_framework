﻿<cfscript>
	rc.title = "Dashboard";
</cfscript>
<cfoutput>
#view("global:main/adminmessage")#
<div class="row">
	<div class="col-12">
		<div class="box box-solid">
			<div class="box-body">
				#rc.cs.showContent(getFullyQualifiedAction(), session.locale)#
			</div>
		</div>
	</div>
</div>
<cfif rc.adminUser>
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">Server information</h3>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-12 col-sm-6">
					<h4>Lucee</h4>
					<ul>
						<li><strong>version</strong>: <em>#server.lucee.version#</em></li>
						<li><strong>version name</strong>: <em>#server.lucee.versionName#</em></li>
						<li><strong>released</strong>: <em>#lCase(dateFormat(server.lucee['release-date'], 'dd mmm. yyyy'))#</em></li>
						<li><strong>state</strong>: <em>#server.lucee.state#</em></li>
						<li><strong>engine</strong>: <em>#server.servlet.name#</em></li>
						<li><strong>java</strong>: <em>#server.java.version# #server.java.archModel#bit</em></li>
						<li><strong>java by</strong>: <em>#server.java.vendor#</em></li>
					</ul>
				</div>
				<div class="col-12 col-sm-6">
					<h4>System</h4>
					<ul>
						<li><strong>host</strong>: <em>#cgi.local_host#</em></li>
						<li><strong>os</strong>: <em>#server.os.name#</em></li>
						<li><strong>os version</strong>: <em>#server.os.version#</em></li>
						<li><strong>processor</strong>: <em>#server.os.arch# #server.os.archModel#bit</em></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</cfif>
</cfoutput>