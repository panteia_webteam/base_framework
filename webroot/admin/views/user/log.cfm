<cfscript>
	rc.title = "Userlog";
	if (structKeyExists(rc, "id") && isValid("GUID", rc.id)) {
		rc.title &= ": " & rc.userObj.getFullname();
	}
	rc.breadcrumbArr = [
		{
			action : "admin:user"
			, title : "User overview"
		}, {
			title : rc.title
		}
	];
	arrayAppend(rc.features, "validation");
	local.classStruct = {
		danger : {
			color : 'red'
			, icon : 'fa-times'
		} , success : {
			color : 'green'
			, icon : 'fa-check'
		}
		, warning : {
			color : 'yellow'
			, icon : 'fa-exclamation-triangle'
		}
		, info : {
			color : 'blue'
			, icon : 'fa-exclamation'
		}
	};
</cfscript>
<cfoutput>
<div class="row">
	<div class="col-12">
		<ul class="timeline">
			<cfloop query="#rc.logQry#">
				<cfset local.currentDate = dateFormat(rc.logQry.createDate)>
				<cfif not structKeyExists(local,"currentDate") or currentDate neq dateFormat(rc.logQry.createDate)>
					<li class="time-label">
						<span class="bg-blue">
							#lsDateFormat(rc.logQry.createDate, "dd mmm. yyyy")#
						</span>
					</li>
				</cfif>
				<li>
					<cfif not structKeyExists(local.classStruct, rc.logQry.class)>
						<cfset local.classStruct[rc.logQry.class] = { icon : "", color : "" }>
					</cfif>
					<i class="fa #local.classStruct[rc.logQry.class].icon# bg-#local.classStruct[rc.logQry.class].color#" title="#rc.logQry.class#"></i>
					<div class="timeline-item" data-id="#rc.logQry.id#">
						<span class="time"><i class="fa fa-clock-o"></i> #lsTimeFormat(rc.logQry.createDate,"HH:MM")#</span>
						<h3 class="timeline-header">#rc.logQry.description#</h3>
						<div class="timeline-body">
							<cfif len(rc.logQry.ACTION)>
								<p>The action performed is: <a href="#buildUrl(rc.logQry.ACTION)#">#rc.logQry.ACTION#</a></p>
							</cfif>
							<cfif len(rc.logQry.IP_HASH)>
								<p class="text-muted">IP adress (hashed): #rc.logQry.IP_HASH#</p>
							</cfif>
							<cfif len(rc.logQry.CREATEDBY) and not structKeyExists(rc,"id")>
								<p class="text-muted">userId: #rc.logQry.createdBy# (#rc.logQry.firstName# #rc.logQry.prefix# #rc.logQry.lastName#)</p>
							</cfif>
							<cfif len(rc.logQry.SERIALIZEDDATA)>
								<hr>
								<p>The data sent to the page:</p>
								<pre>#rc.logQry.SERIALIZEDDATA#</pre>
							</cfif>
						</div>
					</div>
				</li>
			</cfloop>
			<li>
				<i class="fa fa-clock"></i>
			</li>
		</ul>
	</div>
</div>
</cfoutput>