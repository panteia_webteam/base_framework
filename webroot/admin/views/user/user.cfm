<cfscript>
	rc.title = ! len(rc.userObj.getId()) ? "Add user" : "Edit user: " & rc.userObj.getLastName();
	rc.breadcrumbArr = [
		{
			action : "#getSubSystem()#:user"
			, title : "User overview"
		}, {
			title : rc.title
		}
	];
	arrayAppend(rc.features, "validation");
	arrayAppend(rc.features, "select2");
	arrayAppend(rc.features, "inputMask");
	arrayAppend(rc.features, "pwstrength");
	arrayAppend(rc.arrJSExternal, "#application.applicationUrl##getSubSystem()#/assets/js/views/user/user.js");
	local.birthday = ! isNull(rc.userObj.getBirthDay()) ? dateFormat(rc.userObj.getBirthDay(), "dd/mm/yyyy") : "";
</cfscript>
<cfoutput>
#view("global:main/adminmessage")#
<div class="row">
	<div class="col-lg-8">
		<form action="#buildUrl('.processUser')#" method="post" role="form">
			<input type="hidden" name="csrfToken" value="#session.CSRFToken#">
			<input type="hidden" name="id" value="#rc.userObj.getId()#">
			<div class="box">
				<div class="box-header pb-0">
					<h3 class="box-title">Personal information</h3>
				</div>
				<div class="box-body pt-0">
					<div class="row">
						<div class="col-12 col-lg-5">
							<div class="form-group">
								<label for="firstname" class="mt-2 mb-2">Firstname</label>
								<input type="text" name="firstname" value="#rc.userObj.getFirstName()#" class="form-control" placeholder="Firstname" required>
							</div>
						</div>
						<div class="col-12 col-lg-2">
							<div class="form-group">
								<label for="prefix" class="mt-2 mb-2">Prefix</label>
								<input type="text" name="prefix" value="#rc.userObj.getPrefix()#" class="form-control" placeholder="Prefix">
							</div>
						</div>
						<div class="col-12 col-lg-5">
							<div class="form-group">
								<label for="lastname" class="mt-2 mb-2">Lastname</label>
								<input type="text" name="lastname" value="#rc.userObj.getLastName()#" class="form-control" placeholder="Lastname" required>
							</div>
						</div>
					</div>
					<div class="row mt-2">
						<div class="col-12 col-lg-4">
							<label class="mb-2">Gender</label><br>
							<div class="form-check form-check-inline">
								<input type="radio" name="gender" id="genderm" value="m"#rc.userObj.getGender() == "m" ? " checked" : ""# class="form-check-input">
								<label class="form-check-label" for="genderm">Male</label>
							</div>
							<div class="form-check form-check-inline">
								<input type="radio" name="gender" id="genderf" value="f"#rc.userObj.getGender() == "f" ? " checked" : ""# class="form-check-input">
								<label class="form-check-label" for="genderf">Female</label>
							</div>
							<div class="form-check form-check-inline">
								<input type="radio" name="gender" id="gendero" value="o"#rc.userObj.getGender() == "o" ? " checked" : ""# class="form-check-input">
								<label class="form-check-label" for="gendero">Other</label>
							</div>
						</div>
						<div class="col-12 col-lg-4">
							<div class="form-group">
								<label for="birthday" class="mb-2">Birthday</label>
								<div class="input-group">
									<span class="input-group-text"><i class="fa fa-calendar"></i></span>
									<input type="text" name="birthday" id="datemask" value="#local.birthday#" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'">
								</div>
							</div>
						</div>
						<div class="col-12 col-lg-4">
							<div class="form-group select2-container">
								<label for="locale" class="mb-2">Locale</label><br>
								<select name="locale" id="locale" class="form-control" size="1">
									<option></option>
									<cfloop array="#rc.locales#" item="locale">
										<option value="#locale.toString()#"#rc.userObj.getLocale() == locale.toString() || (isNull(rc.userObj.getLocale()) && locale.toString() == "nl_NL") ? " selected" : ""#>#locale.toString()#</option>
									</cfloop>
								</select>
							</div>
						</div>
					</div>
					<div class="row mt-2">
						<div class="col-12 col-lg-6">
							<div class="form-group">
								<label for="email" class="mb-2">Email</label>
								<div class="input-group">
									<span class="input-group-text"><i class="fa fa-envelope"></i></span>
									<input type="email" name="email" value="#rc.userObj.getEmail()#" class="form-control" placeholder="Email" required>
								</div>
							</div>
						</div>
						<div class="col-12 col-lg-6">
							<label class="mb-2">Email opt-out</label>
							<div class="form-check">
								<input type="checkbox" name="email_optout" id="email_optout" value="1"#rc.userObj.getEmail_optout() == 1 ? " checked" : ""# class="form-check-input">
								<label for="email_optout" class="form-check-label">Do not contact this user by email</label>
							</div>
						</div>
					</div>
					<div class="row mt-2">
						<div class="col-12 col-lg-6">
							<div class="form-group select2-container">
								<label for="hierarchicalGroupId" class="mb-2">Hierarchical group</label>
								<select name="hierarchicalGroupId" id="hierarchicalGroup" class="form-select">
									<option></option>
									<cfloop array="#rc.hierarchicalGroupArr#" item="group">
										<option value="#group.id#"#rc.userObj.getHierarchicalGroup().getId() == group.id ? " selected" : ""#>#group.groupName#</option>
									</cfloop>
								</select>
							</div>
						</div>
						<div class="col-12 col-lg-6">
							<div class="form-group select2-container">
								<label for="groupIds" class="mb-2">Groups</label>
								<select name="groupIds" id="groups" class="form-select" multiple="true">
									<option></option>
									<cfloop array="#rc.groupArr#" item="group">
										<option value="#group.id#"#listFindNoCase(rc.userObj.getGroupIdList(),group.id) ? " selected" : ""#>#group.groupName#</option>
									</cfloop>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box">
				<div class="box-header pb-0">
					<h3 class="box-title">Security settings</h3>
				</div>
				<div class="box-body pt-0">
					<div class="row" id="pwd-container">
						<div class="col-12 col-lg-6">
							<div class="form-group">
								<label for="password" class="mt-2 mb-2">New password</label>
								<div class="input-group">
									<span class="input-group-text"><i class="fa fa-key"></i></span>
									<input type="password" name="password" id="password" class="form-control" autocomplete="false">
								</div>
							</div>
							<div class="form-group">
								<label for="confirm_password" class="mt-2 mb-2">Confirm new password</label>
								<div class="input-group">
									<span class="input-group-text"><i class="fa fa-key"></i></span>
									<input type="password" name="confirm_password" id="confirm_password" class="form-control" autocomplete="false">
								</div>
							</div>
						</div>
						<div class="col-12 col-lg-5 col-lg-offset-1">
							<label for="passwordstrength" class="mt-2 mb-3">Password strength</label>
							<div class="pwstrength_viewport_progress"></div>
							<p class="alert alert-info p-2 mt-4">For security reasons it is wise to use a complex password. The bar above indicates the complexity of your chosen password.</p>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-lg-6">
							<div class="form-group">
								<label for="netwerkLoginName" class="mt-2 mb-2">Network loginname</label>
								<div class="input-group">
									<span class="input-group-text"><i class="fa-solid fa-right-to-bracket"></i></span>
									<input type="text" name="netwerkLoginName" class="form-control" placeholder="Network loginname" value="#rc.userObj.getNetwerkLoginName()#" maxlength="15">
								</div>
							</div>
						</div>
						<div class="col-12 col-lg-6">
							<div class="form-group select2-container">
								<label for="roleIds" class="mt-2 mb-2">Roles</label>
								<select name="roleIds" id="roles" class="form-select" multiple="true" required>
									<option></option>
									<cfloop array="#rc.roleArr#" item="role">
										<option value="#role.id#"#arrayFindNoCase(rc.currentRoles, role.id) ? " selected" : ""#>#role.name#</option>
									</cfloop>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box">
				<div class="box-header pb-0">
					<h3 class="box-title">Site settings</h3>
				</div>
				<div class="box-body pt-0">
					<div class="row mt-2">
						<div class="form-group col-12 col-lg-6">
							<label for="defaultaction" class="mt-2 mb-2">Default FW/1 action</label>
							<div class="input-group">
								<span class="input-group-text"><i class="fa fa-cog"></i></span>
								<input type="text" name="defaultaction" value="#rc.userObj.getDefaultaction()#" class="form-control" placeholder="Default FW/1 action" required>
							</div>
						</div>
						<div class="col-12 col-lg-3">
							<label class="mt-2 mb-2">New user <i class="fa fa-info-circle" data-toggle="tooltip" title="Determines if the user gets a walktrough and disclaimer on first login"></i></label>
							<div class="form-check">
								<input type="checkbox" name="new" id="new" value="1" #rc.userObj.getNew() eq 1 ? " checked" : ""# class="form-check-input"> 
								<label for="new" class="form-check-label">New</label>
							</div>
						</div>
						<div class="col-12 col-lg-3">
							<label class="mt-2 mb-2">User active <i class="fa fa-info-circle" data-toggle="tooltip" title="Determines if the current user is active"></i></label>
							<div class="form-check">
								<label for="isActive" class="form-check-label">Active</label>
								<input type="checkbox" name="isActive" id="isActive" value="1"#rc.userObj.getIsActive() ? " checked" : ""# class="form-check-input">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box box-primary">
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>
	</div>
	<div class="col-lg-4">
		<div class="small-box bg-green">
			<div class="inner">
				<h3>#rc.userObj.getLastLoginDate() eq lsParseDateTime("1-1-1900", session.locale) ? lsDateFormat(rc.userObj.getLastLoginDate(), "Long", session.locale) : ""#</h3>
				<p>Last login date</p>
			</div>
			<div class="icon"><i class="fa fa-calendar"></i></div>
			<div class="small-box-footer">&nbsp;</div>
		</div>
		<div class="small-box bg-blue">
			<div class="inner">
				<h3>#rc.userObj.getLoginCount()#</h3>
				<p>Login count</p>
			</div>
			<div class="icon"><i class="fa fa-key"></i></div>
			<a href="#buildUrl('.mimic?id=#rc.userObj.getId()#')#" class="small-box-footer">
				Login as user <i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
		<div class="small-box bg-yellow">
			<div class="inner">
				<h3>#rc.userObj.getLogCount()#</h3>
				<p>Log entries</p>
			</div>
			<div class="icon"><i class="fa fa-book"></i></div>
			<a href="#buildUrl('.log?id=#rc.userObj.getId()#')#" class="small-box-footer">
				More info <i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
		<div class="small-box bg-purple">
			<div class="inner">
				<h3>#lsDateFormat(rc.userObj.getCreateDate(), "Long", session.locale)#</h3>
				<cfif not isNull(rc.userObj.getCreatedByUser().getId())>
					Created by #rc.userObj.getCreatedByUser().getFullName()#
				<cfelse>
					Member since
				</cfif>
			</div>
			<div class="icon"><i class="fa fa-user"></i></div>
			<div class="small-box-footer">&nbsp;</div>
		</div>
		<cfif not isNull(rc.userObj.getUpdateDate())>
			<div class="small-box bg-green">
				<div class="inner">
					<h3>#lsDateFormat(rc.userObj.getUpdateDate(), "Long", session.locale)#</h3>
					<p>Last changed by #rc.userObj.getUpdatedByUser().getFullName()#</p>
				</div>
				<div class="icon"><i class="fa fa-calendar"></i></div>
				<div class="small-box-footer">&nbsp;</div>
			</div>
		</cfif>
	</div>
</div>
</cfoutput>