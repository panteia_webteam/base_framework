<cfscript>
	rc.title = "user overview";
	if (structKeyExists(rc, "groupObj")) {
		rc.title &= ", group: " & rc.groupObj.getGroupName();
	}
	if (structKeyExists(rc, "roleObj")) {
		rc.title &= ", role: " & rc.roleObj.getName();
	}
	arrayAppend(rc.arrCSSExternal, application.dataTablesCSS);
	arrayAppend(rc.arrJSExternal, application.dataTablesJS);
	arrayAppend(rc.arrJSExternal, "#application.applicationURL##getSubSystem()#/assets/js/global/datatableLanguage.js");
	arrayAppend(rc.arrJSExternal, "#application.applicationURL##getSubSystem()#/assets/js/views/user/main.js");
</cfscript>
<cfoutput>
#view("global:main/adminmessage")#
<div class="row">
	<div class="col-12">
		<div class="box box-solid">
			<div class="box-body">
				#rc.cs.showContent(getFullyQualifiedAction(), session.locale)#
			</div>
		</div>
	</div>
</div>
<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title">Current users</h3>
		<div class="box-tools float-end">
			<a href="#buildUrl(action='.user')#" class="btn btn-success btn-sm"><i class="icol-add"></i> Add user</a>
			<a href="#buildUrl(action='.log')#" class="btn btn-primary btn-sm"><i class="icol-add"></i> View logs</a>
		</div>
	</div>
	<div class="box-body table-responsive">
		<table id="dataTable" class="table table-bordered table-striped responsive" style="width:100%;">
			<thead>
				<tr>
					<th scope="col">Email</th>
					<th scope="col">Name</th>
					<th scope="col">Roles</th>
					<th scope="col" class="text-center">Active</th>
					<th scope="col" class="text-center">Log</th>
					<th scope="col" class="text-center">Mimic</th>
					<th scope="col" class="text-center">Change</th>
					<th scope="col" class="text-center">Delete</th>
				</tr>
			</thead>
			<tbody>
				<cfloop array="#rc.userArr#" index="currentItem">
					<tr>
						<td>#currentItem.email#</td>
						<td>
							#not isNull(currentItem.firstname) ? currentItem.firstname : ""#
							#not isNull(currentItem.prefix) ? currentItem.prefix : ""#
							#not isNull(currentItem.lastname) ? currentItem.lastname : ""#
							<cfif not isNull(currentItem.newUser) and currentItem.newUser>
								<small class="badge float-end bg-green">new</small>
							</cfif>
						</td>
						<td>
							<cfif structKeyExists(rc.userStruct, currentItem.id)>
								<cfloop collection="#rc.userStruct[currentItem.id]#" item="structItem">
									<span class="badge text-bg-secondary #rc.userStruct[currentItem.id][structItem].id#">#rc.userStruct[currentItem.id][structItem].name#</span>
								</cfloop>
							</cfif>
						</td>
						<td class="text-center">
							<span class="badge text-bg-#currentItem.isActive ? 'success' : 'warning'#">#currentItem.isActive ? "Active" : "Inactive"#</span>
						</td>
						<td class="text-center">
							<a href="#buildUrl(action='.log', queryString='id=#currentItem.id#')#"><i class="fa fa-book"></i></a>
						</td>
						<td class="text-center">
							<a href="#buildUrl(action='.mimic', queryString='id=#currentItem.id#')#"><i class="fa fa-key"></i></a>
						</td>
						<td class="text-center">
							<a href="#buildUrl(action='.user', queryString='id=#currentItem.id#')#"><i class="fa fa-pencil-square"></i></a>
						</td>
						<td class="text-center">
							<a href="#buildUrl(action='.deleteUser', queryString='id=#currentItem.id#')#" class="removeIcon" title="Remove #currentItem.email#"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
				</cfloop>
			</tbody>
		</table>
	</div>
</div>
</cfoutput>