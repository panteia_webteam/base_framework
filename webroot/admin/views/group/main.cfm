﻿<cfscript>
	local.groupType = "Horizontal group";
	if (structKeyExists(rc, "hierarchical") && rc.hierarchical) {
		local.groupType = "Hierarchical group";
	}
	rc.title = local.groupType & " overview";
	rc.inlineScriptNonce = CreateGUID();
	rc.cspHeaderScript &= " 'nonce-#rc.inlineScriptNonce#'";
	arrayAppend(rc.arrCSSExternal, application.dataTablesCSS);
	arrayAppend(rc.arrJSExternal, application.dataTablesJS);
	arrayAppend(rc.arrJSExternal, "#application.applicationUrl##getSubSystem()#/assets/js/global/datatableLanguage.js");
	arrayAppend(rc.arrJSExternal, "#application.applicationUrl##getSubSystem()#/assets/js/views/group/main.js");
	arrayAppend(rc.arrCustomJS, "
		var groups = #rc.groups#
			, hgroupURL = '#buildUrl(action='.', queryString='hierarchical/1/parentId/')#'
			, groupURL = '#buildUrl(action='.', queryString='hierarchical/0/parentId/')#'
			, userURL = '#buildUrl(action='user.main', queryString='groupId/')#'
			, editURL = '#buildUrl(action='.group', queryString='id/')#'
			, deleteURL = '#buildUrl(action='.delete.', queryString='id/')#'
			, hierarchical = #rc.hierarchical# ? true : false;
	");
	local.queryString = "hierarchical=" & rc.hierarchical;
	if (structKeyExists(rc, "parentGroupObj")) {
		local.queryString &= "&parentId="&rc.parentGroupObj.getId();
	}
</cfscript>
<cfoutput>
#view("global:main/adminmessage")#
<div class="row">
	<div class="col-12">
		<div class="box box-solid">
			<div class="box-body">
				#rc.cs.showContent(getFullyQualifiedAction(), session.locale)#
			</div>
		</div>
	</div>
</div>
<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title">Current groups#structKeyExists(rc, "parentGroupObj") ? ", #rc.parentGroupObj.getGroupName()#" : ""#</h3>
		<div class="box-tools float-end">
			<a href="#buildUrl(action='.group', queryString=local.queryString)#" class="btn btn-success btn-sm"><i class="icol-add"></i> Add</a>
		</div>
	</div>
	<div class="box-body table-responsive">
		<table id="groupTable" class="table table-bordered table-striped responsive" style="width:100%;">
			<thead>
				<tr>
					<th scope="col">id</th>
					<th scope="col">parentId</th>
					<th scope="col">Name</th>
					<th scope="col" class="text-center">Active</th>
					<th scope="col" class="text-center">Organization</th>
					<th scope="col" class="text-center">Groups</th>
					<th scope="col" class="text-center">Users</th>
					<th scope="col" class="text-center">Change</th>
					<th scope="col" class="text-center">Delete</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>
</cfoutput>