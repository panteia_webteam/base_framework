﻿<cfscript>
	local.groupType = "Horizontal group";
	if (structKeyExists(rc, "hierarchical") && rc.hierarchical) {
		local.groupType = "Hierarchical group";
	}
	rc.title = "Edit " & local.groupType & ": " & rc.groupObj.getGroupName();
	if (! len(rc.id)) {
		rc.title = "Add " & local.groupType;
	}
	rc.breadcrumbArr = [
		{
			action : "admin:group?hierarchical=#rc.hierarchical#"
			, title : local.groupType & " overview"
		}, {
			title : rc.title
		}
	];
	rc.inlineScriptNonce = CreateGUID();
	rc.cspHeaderScript &= " 'nonce-#rc.inlineScriptNonce#'";
	arrayAppend(rc.features, "validation");
	arrayAppend(rc.features, "select2");
	local.forceDefaultsToUsers = rc.groupObj.getId() != "" && rc.groupObj.getHierarchical() && rc.parentId == "" ? true : false;
	if (local.forceDefaultsToUsers) {
		local.actionUrl = buildUrl(action=getSubSystem() & ":" & getSection() & ".jsonForceDefaults", queryString="id=" & rc.groupObj.getId());
		arrayAppend(rc.arrCustomJS, "var actionUrl = '#local.actionUrl#';");
	}
	local.groupName = ! isNull(rc.groupObj.getGroupName()) ? rc.groupObj.getGroupName() : "";
</cfscript>
<cfoutput>
#view("global:main/adminmessage")#
<div class="row">
	<div class="col-lg-8">
		<form action="#buildUrl(action='.processGroup')#" method="post" role="form">
			<input type="hidden" name="csrfToken" value="#session.CSRFToken#">
			<input type="hidden" name="id" value="#rc.groupObj.getId()#">
			<input type="hidden" name="hierarchical" value="#rc.groupObj.getHierarchical()#">
			<input type="hidden" name="parentId" value="#rc.parentId#">
			<div class="box">
				<div class="box-header pb-0">
					<h3 class="box-title">Global information</h3>
				</div>
				<div class="box-body pt-0">
					<div class="form-group">
						<label for="groupname" class="mt-2 mb-2">Group name</label>
						<input type="text" name="groupName" id="groupName" value="#local.groupName#" class="form-control" placeholder="Groupname" required>
					</div>
				</div>
			</div>
			<cfif rc.hierarchical>
				<div class="box">
					<div class="box-header pb-0">
						<h3 class="box-title">Custom information</h3>
					</div>
					<div class="box-body pt-0">
						<div class="form-group">
							<label for="relationId" class="mt-2 mb-2">Relatie ID</label>
							<div class="input-group">
								<span class="input-group-text"><i class="fa-solid fa-handshake-simple"></i></span>
								<input type="text" name="relationId" id="relationId" value="#rc.groupObj.getRelationId()#" class="form-control" placeholder="relationId" readonly required>
							</div>
						</div>
						<cfif local.forceDefaultsToUsers>
							<cfset local.groupSettings = rc.groupObj.getGroupsetting()>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-4">
										<label for="defaultaction" class="mt-2 mb-2">Default action</label>
										<div class="input-group">
											<span class="input-group-text"><i class="fa fa-cog"></i></span>
											<input type="text" name="defaultaction" id="defaultaction" value="#local.groupSettings.getDefaultaction()#" class="form-control" placeholder="default action">
										</div>
									</div>
									<div class="col-lg-8">
										<label for="defaultroles" class="mt-2 mb-2">Default role(s)</label>
										<select name="defaultroles" id="defaultroles" class="form-select" multiple="true">
											<option></option>
											<cfloop array="#rc.roleArr#" item="role">
												<cfset local.selected = listFindNoCase(local.groupSettings.getDefaultroles(), role.name) ? " selected" : "">
												<option value="#role.name#"#local.selected#>#role.name#</option>
											</cfloop>
										</select>
									</div>
								</div>
							</div>
							<div class="box-footer">
								<div id="eventinfo" style="display:none;"></div>
								<span class="btn btn-primary" id="setdefaults">Set for all users in this group</span>
							</div>
						</cfif>
					</div>
				</div>
			</cfif>
			<div class="box">
				<div class="box-header pb-0">
					<h3 class="box-title">Site settings</h3>
				</div>
				<div class="box-body pt-0">
					<div class="row">
						<div class="col-12 col-lg-6">
							<label class="mt-2 mb-2">Group active <i class="fa fa-info-circle" data-toggle="tooltip" title="Determines if the current group is active"></i></label>
							<div class="form-check">
								<input type="checkbox" name="isActive" id="isActive" value="1"#rc.groupObj.getIsActive() ? " checked" : ""# class="form-check-input">
								<label for="isActive" class="form-check-label">Active</label>
							</div>
						</div>
						<div class="col-12 col-lg-6">
							<label class="mt-2 mb-2">Demo group <i class="fa fa-info-circle" data-toggle="tooltip" title="Determines if this group is a demoaccount"></i></label>
							<div class="form-check">
								<label for="demoAccount" class="form-check-label">Demoaccount</label>
								<input type="checkbox" name="demoAccount" id="demoAccount" value="1"#rc.groupObj.getDemoAccount() ? " checked" : ""# class="form-check-input">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box box-primary">
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>
	</div>
	<div class="col-lg-4">
		<div class="small-box bg-purple">
			<div class="inner">
				<h3>#lsDateFormat(rc.groupObj.getCreateDate(), "Long", session.locale)#</h3>
				<p>
					<cfif not isNull(rc.groupObj.getCreatedByUser().getId())>
						Created by #rc.groupObj.getCreatedByUser().getFullname()#
					<cfelse>
						Create date
					</cfif>
				</p>
			</div>
			<div class="icon"><i class="fa fa-calendar"></i></div>
			<div class="small-box-footer">&nbsp;</div>
		</div>
		<cfif not isNull(rc.groupObj.getUpdateDate())>
			<div class="small-box bg-green">
				<div class="inner">
					<h3>#lsDateFormat(rc.groupObj.getUpdateDate(), "Long", session.locale)#</h3>
					<p>Last changed by #rc.groupObj.getUpdatedByUser().getFullName()#</p>
				</div>
				<div class="icon"><i class="fa fa-calendar"></i></div>
				<div class="small-box-footer">&nbsp;</div>
			</div>
		</cfif>
		<div class="small-box bg-blue">
			<div class="inner">
				<h3>#rc.groupObj.getUserCount()#</h3>
				<p>Members</p>
			</div>
			<div class="icon"><i class="fa fa-users"></i></div>
			<a href="#buildUrl(action='user', queryString='groupId=#rc.groupObj.getId()#')#" class="small-box-footer">show members <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
</div>
</cfoutput>