<cfscript>
	rc.title = ! len(rc.id) ? "Add content" : "Edit content: " & rc.contentItemObj.getTitle();
	rc.breadcrumbArr = [
		{
			action : "#getSubSystem()#:content"
			, title : "Content overview"
		}, {
			title : rc.title
		}
	];
	arrayAppend(rc.features, "validation");
	arrayAppend(rc.features, "select2");
	arrayAppend(rc.arrJSExternal, application.ckEditor5);
	arrayAppend(rc.arrJSExternal, "#application.applicationUrl##getSubSystem()#/assets/js/views/content/contentitem.js");
</cfscript>
<cfoutput>
#view("global:main/adminmessage")#
<form action="#buildUrl('.processContent')#" method="post" role="form">
	<input type="hidden" name="csrfToken" value="#session.CSRFToken#">
	<input type="hidden" name="id" value="#rc.contentItemObj.getId()#">
	<div class="row">
		<div class="col-lg-8">
			<div class="row">
				<div class="col-lg-12">
					<div class="box">
						<div class="box-header pb-0">
							<h3 class="box-title">General description</h3>
						</div>
						<div class="box-body pt-0">
							<div class="row">
								<div class="form-group col-12 col-lg-6">
									<label for="actionField" class="mt-2 mb-2">Action</label>
									<input type="text" name="actionField" class="form-control" placeholder="Enter FW/1 action" value="#rc.contentAction#">
								</div>
								<div class="form-group col-12 col-lg-6">
									<label for="title" class="mt-2 mb-2">Title</label>
									<input type="text" name="title" class="form-control" placeholder="The title of the page" value="#rc.contentItemObj.getTitle()#" required>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-2">
				<div class="col-lg-12">
					<div class="box">
						<div class="box-header pb-0">
							<h3 class="box-title">Content text</h3>
						</div>
						<div class="box-body pad pt-0">
							<div class="form-group">
								<label for="locale" class="mt-2 mb-2">Locale</label>
								<select name="locale" id="locale" class="form-select" size="1">
									<option></option>
									<cfloop array="#rc.locales#" item="locale">
										<option value="#locale.toString()#"#rc.currentLocale eq locale.toString() ? " selected" : ""#>#locale.toString()#</option>
									</cfloop>
								</select>
							</div>
							<div class="form-group">
								<label for="text" class="mt-2 mb-2">Text</label>
								<textarea name="content" id="content" required>#rc.contentItemObj.getContent()#</textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-2">
				<div class="col-lg-12">
					<div class="box">
						<div class="box-header pb-0">
							<h3 class="box-title">Global settings</h3>
						</div>
						<div class="box-body pad pt-0">
							<div class="row">
								<div class="form-group col-12 col-lg-7">
									<label for="group" class="mt-2 mb-2">Grouping <i class="fa fa-info-circle" data-toggle="tooltip" title="Optional attribute to group certain contentitems, for example: global, alerts, errors, faq, etc"></i></label>
									<input type="text" class="form-control" name="group" placeholder="Enter group name" value="#rc.contentItemObj.getGroup()#">
								</div>
								<div class="col-12 col-lg-5">
									<label class="mt-2 mb-2">Contentitem active <i class="fa fa-info-circle" data-toggle="tooltip" title="Determines if the current contentitem is shown (active) or not (inactive)"></i></label>
									<div class="form-check">
										<label for="isActive" class="form-check-label">Active</label>
										<input type="checkbox" name="isActive" id="isActive" value="1"#rc.contentItemObj.getIsActive() ? " checked" : ""# class="form-check-input">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box box-primary">
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="small-box bg-purple">
				<div class="inner">
					<h3>#lsDateFormat(rc.contentItemObj.getCreateDate(), "Long", session.locale)#</h3>
					<p>
						<cfif not isNull(rc.contentItemObj.getCreatedByUser().getId())>
							Created by #rc.contentItemObj.getCreatedByUser().getFullName()#
						<cfelse>
							Create date
						</cfif>
					</p>
				</div>
				<div class="icon"><i class="fa fa-calendar"></i></div>
				<div class="small-box-footer">&nbsp;</div>
			</div>
			<cfif not isNull(rc.contentItemObj.getUpdateDate())>
				<div class="small-box bg-green">
					<div class="inner">
						<h3>#lsDateFormat(rc.contentItemObj.getUpdateDate(), "Long", session.locale)#</h3>
						<p>Last changed by #rc.contentItemObj.getUpdatedByUser().getFullName()#</p>
					</div>
					<div class="icon"><i class="fa fa-calendar"></i></div>
					<div class="small-box-footer">&nbsp;</div>
				</div>
			</cfif>
		</div>
	</div>
</form>
</cfoutput>