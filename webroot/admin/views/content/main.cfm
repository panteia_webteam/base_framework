﻿<cfscript>
	rc.title = "Content overview";
	arrayAppend(rc.arrCSSExternal, application.dataTablesCSS);
	arrayAppend(rc.arrJSExternal, application.dataTablesJS);
	arrayAppend(rc.arrJSExternal, "#application.applicationURL##getSubSystem()#/assets/js/global/datatableLanguage.js");
	arrayAppend(rc.arrJSExternal, "#application.applicationURL##getSubSystem()#/assets/js/views/content/content.js");
</cfscript>
<cfoutput>
#view("global:main/adminmessage")#
<div class="row">
	<div class="col-12">
		<div class="box box-solid">
			<div class="box-body">
				#rc.cs.showContent(getFullyQualifiedAction(), session.locale)#
			</div>
		</div>
	</div>
</div>
<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title">#rc.title#</h3>
		<div class="box-tools float-end">
			<a href="#buildUrl('content.content')#" class="btn btn-success btn-sm"><i class="icol-add"></i> Add content</a>
		</div>
	</div>
	<div class="box-body table-responsive">
		<table id="dataTable" class="table table-bordered table-striped responsive" style="width:100%;">
			<thead>
				<tr>
					<th scope="col">Action</th>
					<th scope="col">Title</th>
					<th scope="col">Group</th>
					<th scope="col" class="text-center">Version</th>
					<th scope="col" class="text-center">Active</th>
					<th scope="col" class="text-center">Change</th>
					<th scope="col" class="text-center">Delete</th>
				</tr>
			</thead>
			<tbody>
				<cfloop array="#rc.content#" index="contentItem">
					<tr>
						<td>#contentItem["action"]#</td>
						<td>#contentItem["title"]#</td>
						<td>#(NOT isNull(contentItem["group"]) ? contentItem["group"] : "")#</td>
						<td class="text-center">#contentItem["ormVersion"]#</td>
						<td class="text-center">
							<span class="badge text-bg-#(contentItem['isActive'] ? 'success' : 'warning')#">#(contentItem["isActive"] ? "Active" : "Inactive")#</span>
						</td>
						<td class="text-center">
							<a href="#buildUrl(action='content.content', queryString='id=#contentItem["id"]#')#"><i class="fa fa-pencil-square"></i></a>
						</td>
						<td class="text-center">
							<a href="#buildUrl(action='content.deleteContent', queryString='id=#contentItem["id"]#')#" class="removeIcon" title="Remove #contentItem['title']#"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
				</cfloop>
			</tbody>
		</table>
	</div>
</div>
</cfoutput>