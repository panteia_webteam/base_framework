<cfoutput>
<ol class="breadcrumb">
	<li><a href="#buildUrl('#getSubSystem()#:')#"><i class="fa fa-dashboard"></i> Home</a></li>
	<cfif not structKeyExists(rc, "breadcrumbArr")>
		<li class="active">#rc.title#</li>
	<cfelse>
		<cfloop array="#rc.breadcrumbArr#" index="breadcrumbItem">
			<cfif structKeyExists(breadcrumbItem, "action")>
				<li><a href="#buildUrl(breadcrumbItem.action)#">#breadcrumbItem.title#</a></li>
			<cfelse>
				<li class="active">#breadcrumbItem.title#</li>
			</cfif>
		</cfloop>
	</cfif>
</ol>
</cfoutput>