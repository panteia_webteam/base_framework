<cfscript>
	local.utilityService = getBeanFactory().getBean("utilityService");
	local.assetsFolder = "/#getSubSystem()#/assets/";
	local.avatar = "#local.assetsFolder#img/#rc.currentUser.getGender() == 'm' ? 'avatar4' : 'avatar3'#.png";
	local.roles = rc.currentUser.getRoles();
</cfscript>
<cfoutput>
<nav class="navbar navbar-static-top pt-0 pb-0" role="navigation">
	<a href="##" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		<span class="sr-only">Toggle navigation</span>
	</a>
	<div class="navbar-custom-menu">
		<ul class="nav navbar-nav">
			<li class="dropdown user user-menu">
				<button class="btn btn-dark dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
					<img src="#local.avatar#" class="user-image" alt="User Image">
				</button>
				<ul class="dropdown-menu">
					<li class="user-header">
						<img src="#local.avatar#" class="img-circle" alt="User Image">
						<p>
							#rc.currentUser.getFullName()#
							<cfif arrayLen(local.roles) eq 1>
								- #rc.currentUser.getRoles()[1].getName()#
							</cfif>
							<small>Member since #lsDateFormat(rc.currentUser.getCreateDate(), "mmm yyyy")#</small>
						</p>
					</li>
					<li class="user-body row">
						<div class="col-6 text-start">
							<cfloop array="#rc.subsystemList#" item="elem">
								<a href="#buildUrl(elem&':')#">#elem#</a><br>
							</cfloop>
						</div>
						<div class="col-6 text-end">
							<a href="http://buglog.panteia.local/" rel="external"><nobr>buglogHQ <i class="fa fa-external-link fa-1"></i></nobr></a><br>
							<a href="https://panteia.atlassian.net/" rel="external"><nobr>jira <i class="fa fa-external-link fa-1"></i></nobr></a>
						</div>
					</li>
					<li class="user-footer">
						<div class="float-start">
							<a href="#buildUrl(action='#getSubSystem()#:user.user', queryString='id=#rc.currentUser.getId()#')#" class="btn btn-default btn-flat">Profile</a>
						</div>
						<div class="float-end">
							<a href="#buildUrl('global:security.logout')#" class="btn btn-default btn-flat">Sign out</a>
						</div>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</nav>
</cfoutput>