<cfoutput>
<ul class="sidebar-menu">
	<li class="header"><hr></li>
	<li class="#getSection() eq 'main' ? 'active' : ''#">
		<a href="#buildUrl('admin:')#">
			<i class="fa fa-dashboard"></i> <span>Dashboard</span>
		</a>
	</li>
	<li class="treeview #arrayFindNoCase(['backup', 'settings'], getSection()) ? 'active' : ''#">
		<a href="##">
			<i class="fa fa-cog" aria-hidden="true"></i>
			<span>System</span>
			<i class="fa fa-angle-left float-end"></i>
		</a>
		<ul class="treeview-menu">
			<li class="#getSection() == 'settings' ? 'active' : ''#"><a href="#buildUrl('settings.')#"><i class="fa fa-circle"></i> Global settings</a></li>
			<li class="#getSection() == 'backup' ? 'active' : ''#"><a href="#buildUrl('backup.')#"><i class="fa fa-circle"></i> Backup</a></li>
		</ul>
	</li>
	<li class="#arrayFindNoCase(['answergroup'], getSection()) ? 'active' : ''#">
		<a href="#buildUrl('answergroup.')#">
			<i class="fa fa-check-square"></i>
			<span>Answergroups</span>
		</a>
	</li>
	<li class="treeview #arrayFindNoCase(['content', 'menu', 'i18n'], getSection()) ? 'active' : ''#">
		<a href="##">
			<i class="fa fa-file-text" aria-hidden="true"></i>
			<span>Content</span>
			<i class="fa fa-angle-left float-end"></i>
		</a>
		<ul class="treeview-menu">
			<li class="#getSection() == 'content' ? 'active' : ''#"><a href="#buildUrl('content.')#"><i class="fa fa-circle"></i> Content</a></li>
			<li class="#getSection() == 'menu' ? 'active' : ''#"><a href="#buildUrl('menu.')#"><i class="fa fa-circle"></i> Menu</a></li>
			<li class="#getSection() == 'i18n' ? 'active' : ''#"><a href="#buildUrl('i18n.')#"><i class="fa fa-circle"></i> i18n</a></li>
		</ul>
	</li>
	<li class="#arrayFindNoCase(['user'], getSection()) ? 'active' : ''#">
		<a href="#buildUrl('user.')#">
			<i class="fa fa-users" aria-hidden="true"></i>
			<span class="ps-1">Users</span>
		</a>
	</li>
	<li class="treeview #arrayFindNoCase(['group'], getSection()) ? 'active' : ''#">
		<a href="##">
			<i class="fa fa-building" aria-hidden="true"></i>
			<span>Groups</span>
			<i class="fa fa-angle-left float-end"></i>
		</a>
		<ul class="treeview-menu">
			<li class="#getSection() == 'group' && ((structKeyExists(rc, 'hierarchical') && rc.hierarchical == 1) || (structKeyExists(rc, 'parentId') && isValid('GUID', rc.parentId))) ? 'active' : ''#">
				<a href="#buildUrl('group.?hierarchical=1')#"><i class="fa fa-circle" aria-hidden="true"></i> Hierarchical</a>
			</li>
			<li class="#getSection() == 'group' && ((! structKeyExists(rc, 'hierarchical') || rc.hierarchical == 0) && ! structKeyExists(rc, 'parentId')) ? 'active' : ''#">
				<a href="#buildUrl('group.')#"><i class="fa fa-circle" aria-hidden="true"></i> Global</a>
			</li>
		</ul>
	</li>
	<li class="#arrayFindNoCase(['role', 'rule'], getSection()) ? 'active' : ''#">
		<a href="#buildUrl('role.')#">
			<i class="fa fa-unlock" aria-hidden="true"></i>
			<span>Roles</span>
		</a>
	</li>
	<li class="#arrayFindNoCase(['plugin'],getSection()) ? 'active' : ''#">
		<a href="#buildUrl('plugin.')#">
			<i class="fa fa-plug" aria-hidden="true"></i>
			<span>Plugins</span>
		</a>
	</li>
	<li class="#arrayFindNoCase(['statistics', 'rule'], getSection()) ? 'active' : ''#">
		<a href="#buildUrl('statistics.')#">
			<i class="fa fa-bar-chart" aria-hidden="true"></i>
			<span>Statistics</span>
		</a>
	</li>
</ul>
</cfoutput>