component accessors=true output=false hint="" {

	property answergroupManager;
	property config;
	property logService;
	property utilityService;

	public void function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
	}

	public void function before(required struct rc) output=false hint="" {
		// prevent XSS attacks
		rc = variables.utilityService.XSSProtect(rc);
	}

	public void function main(required struct rc) output=false hint="" {
		// retrieve the items
		var queryString = "
			SELECT new map (
				a.id AS id
				, a.isActive AS isactive
				, a.name AS name
			)
			FROM answergroup a
			WHERE a.isActive <> -1
			ORDER BY a.name desc
		";
		rc.answergroupArr = ormExecuteQuery(queryString);
	}

	public void function answergroup(required struct rc) output=false hint="" {
		param name="rc.id" default="";
		rc.answergroupObj = answergroupManager.getObj(rc.id);
		rc.answerArr = [];
		if(! isNull(rc.answergroupObj.getId())) {
			rc.answerArr = ORMExecuteQuery("
				SELECT new map(
					a.id AS answerid
					, a.name AS answername
					, a.value AS value
					, a.listOrder AS listorder
					, a.isActive AS isactive
					, ag.name AS answergroupname
				)
				FROM answergroup ag
					join ag.answers a
				WHERE a.isActive <> -1
				AND ag.isActive <> -1
				AND ag.id = :answergroupId
				ORDER BY a.listOrder
					, a.name
			", { answergroupId : rc.id }
			);
		}
		rc.hasAnswers = arrayIsEmpty(rc.answerArr) ? false : true;
	}

	public void function processAnswergroup(required struct rc) output=false hint="" {
		param name="rc.id" value="";
		// abort execution in case of CRSF attack
		variables.utilityService.abortOnCSRFAttack(rc.csrfToken);
		// validate data
		var valid = {};
		// name valid
		if (isNull(rc.id) && isNull(rc.name)) {
			valid.name = { valid : false, message : "Name is a required field" };
		}
		// answername valid
		for (answernameItem in rc.answername) {
			if (isNull(answernameItem) || answernameItem == "") {
				valid.answername = { valid : false, message : "An answername is required" };
			}
		}
		// value valid
		for (valueItem in rc.value) {
			if (isNull(valueItem) || valueItem == "") {
				valid.value = { valid : false, message : "An answervalue is required" };
			}
		}
		//listorder valid
		for (listorderItem in rc.listorder) {
			if (isNull(listorderItem) || listorderItem == "") {
				valid.listorder = { valid : false, message : "A listorder number is required" };
			}
		}
		// validation failed, return to page and give feedback
		if (! structIsEmpty(valid)) {
			rc.message = { class : "warning", text : "The following error(s) must first be corrected: " & utilityService.buildMessage(valid) };
			// don't log the user credentials to the database
			logService.log(rc, 0);
			variables.fw.redirect(utilityService.getOriginAction(fw=variables.fw), "message");
		}
		// validate, fill and save the object
		var answergroupObj = answergroupManager.getObj(rc.id);
		if (isNull(answergroupObj.getId())) {
			// autofill fields
			variables.fw.populate(answergroupObj);
			answergroupObj.setIsActive(1);
			try {
				answergroupManager.save(answergroupObj);
			} catch (any a) {
				writeDump(var=answergroupObj, top=4, abort=0);
				writeDump(var=a, top=4, abort=1);
			}
		}
		// save answers
		var answerObj = "";
		var answersLen = arrayLen(rc.answerId);
		for (var i=1; i <= answersLen; i++) {
			if (len(rc.answername[i])) {
				transaction {
					if(! len(rc.answerId[i])) {
						// if no id is given, create a new object
						answerObj = entityNew("answer");
					} else {
						answerObj = entityLoadByPK("answer", answerId[i]);
					}
					// fill answerObj - set once and never update (defined trough ORM update=false)
					answerObj.setName(rc.answername[i]);
					answerObj.setValue(rc.value[i]);
					if (structKeyExists(rc, "isActive")) {
						answerObj.setIsActive(rc.isActive[i]);
					}
					answerObj.setAnswerGroup(answergroupObj);
					// can be updated
					answerObj.setListOrder(rc.ListOrder[i]);
					// save answerObj
					entitySave(answerObj);
				}
			}
		}
		// log, redirect and give feedback
		rc.message = { class : "success", text : "Changes have been saved" };
		variables.logService.log(rc, 0);
		rc.id = answergroupObj.getId();
		variables.fw.redirect(action=".main", preserve="message,id");
	}

}