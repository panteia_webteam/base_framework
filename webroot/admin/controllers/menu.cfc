﻿component accessors=true output=false hint="" {

	property logService;
	property menuService;
	property utilityService;

	public void function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
	}

	public void function before(required struct rc) output=false hint="" {
		// prevent XSS attacks
		rc = utilityService.XSSProtect(arguments.rc);
	}

	public void function main(struct rc) output=false hint="" {
		rc.menu = ORMExecuteQuery("
			SELECT new map(
				id AS id
				, isActive AS isActive
				, title AS title
				, action AS action
				, description AS description
				, listOrder AS listOrder
				, menu AS menu
			)
			FROM menuitem
			WHERE isActive <> -1
			ORDER BY menu
				, listOrder
		");
	}

	public void function menuitem(struct rc) output=false hint="" {
		// get menuitem
		param name="rc.id" default="";
		rc.menuItemObj = menuService.getObj(rc.id);
		// get possible parents
		rc.parrentsArr = ORMExecuteQuery("
			SELECT NEW MAP (
				id AS id
				, title AS title
				, action AS action
				, description AS description
				, menu AS menu
			)
			FROM menuitem
			WHERE isActive = 1
			AND id <> :currentId
			ORDER BY menu
				, listOrder
				, title
			", { currentId : rc.id }
		);
		// get present menugroups
		rc.menuArr = ORMExecuteQuery("
			SELECT menu
			FROM menuitem
			WHERE isActive = 1
			GROUP BY menu
			ORDER BY menu
		");
	}

	public void function processMenuitem(required struct rc) output=false hint="" {
		// abort execution in case of CRSF attack
		variables.utilityService.abortOnCSRFAttack(rc.csrfToken);
		// validate data
		var valid = {};
		// title valid
		if (rc.title.isEmpty()) {
			valid.title = { valid : false, message : "Title is a required field" };
		}
		// validation failed, return to page and give feedback
		if (! structIsEmpty(valid)) {
			rc.message = { class : "warning", text : "The following error(s) must first be corrected: " & utilityService.buildMessage(valid) };
			// don't log the user credentials to the database
			logService.log(rc, 0);
			variables.fw.redirect(utilityService.getOriginAction(fw=variables.fw), "message");
		}
		// set defaults (think checkboxes and radio buttons)
		param name="rc.isActive" default="0";
		// validate, fill and save the object
		param name="rc.id" value="";
		var menuItemObj = menuService.getObj(rc.id);
		variables.fw.populate(menuItemObj);
		if (structKeyExists(rc, "actionField")) {
			menuItemObj.setAction(rc.actionField);
		}
		if (structKeyExists(rc, "parent") && Trim(rc.parent) != "") {
			var parentObj = menuService.getObj(rc.parent);
			menuItemObj.setParent(parentObj);
		}
		// save userObjs
		menuService.save(menuItemObj);
		// log, redirect and give feedback
		rc.message = { class : "success", text : "Changes have been saved" };
		logService.log(rc, 0);
		rc.id = menuItemObj.getId();
		variables.fw.redirect(action=".main", preserve="message,id");
	}

	public void function deleteMenuItem(required struct rc) output=false hint="" {
		// abort execution in case of CRSF attack
		//variables.utilityService.abortOnCSRFAttack(rc.csrfToken);
		if (! isValid("GUID", rc.id)) {
			rc.message = { class : "warning", text : "The given id is not valid" };
			logService.log(rc, 1);
			variables.fw.redirect(variables.fw.getSection(), "message");
		}
		transaction {
			var menuItemObj = entityLoadByPK("menuitem", rc.id);
			menuItemObj.setIsActive(-1);
			entitySave(menuItemObj);
		}
		// log the action
		rc.message = { class : "success", text : "The selected item has been deleted" };
		logService.log(rc, 0);
		variables.fw.redirect(variables.fw.getSection(), "message");
	}

}