component accessors=true output=false hint="" {

	property config;

	public any function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
		return this;
	}

	public void function before(struct rc) output=false hint="" {
		rc.displayName = config.displayName;
		rc.adminUser = false;
		if(structKeyExists(session.auth, "roles") && listFindNoCase(session.auth.roles, "admin")) {
			rc.adminUser = true;
		}
	}

	public void function main(required struct rc) output=false hint="" {}

}