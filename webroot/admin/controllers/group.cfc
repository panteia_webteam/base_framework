﻿component accessors=true output=false hint="" {

	property config;
	property groupService;
	property logService;
	property utilityService;

	public void function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
	}

	public void function before(required struct rc) output=false hint="" {
		// prevent XSS attacks
		rc = utilityService.XSSProtect(arguments.rc);
	}

	public void function main(required struct rc) output=false hint="" {
		param name="rc.hierarchical" default=0;
		var queryString = "
			WITH groups AS (
				SELECT id
					, groupname
					, isActive
					, fk_parentId
					, hierarchical
				FROM [group]
				WHERE isActive <> -1
			)
			SELECT id
				, fk_parentId
				, groupname
				, isActive
			FROM groups g
		";
		var queryService = new Query();
		if (structKeyExists(rc, "parentId") && isValid("GUID", rc.parentId)) {
			queryString &= " WHERE g.fk_parentId = :parentId";
			queryService.addParam(name="parentId", value=rc.parentId, cfsqltype="CF_SQL_IDSTAMP");
		} else {
			queryString &= " WHERE g.fk_parentId IS NULL";
		}
		if (rc.hierarchical) {
			queryString &= " AND g.hierarchical = :hierarchical";
			queryService.addParam(name="hierarchical", value=int(rc.hierarchical), cfsqltype="cf_sql_integer");
		}
		var groupQry = queryService
			.setSQL(queryString)
			.execute()
			.getResult();
		rc.groups = SerializeJSON(groupQry);
		if (structKeyExists(rc, "parentId")) {
			rc.parentGroupObj = groupService.getObj(rc.parentId);
		}
	}

	public void function group(required struct rc) output=false hint="" {
		param name="rc.parentId" default="";
		param name="rc.id" default="";
		rc.groupObj = groupService.getObj(rc.id);
		if (isNull(rc.groupObj.getId())) {
			param name="rc.hierarchical" default=0;
			rc.groupObj.setHierarchical(rc.hierarchical);
		}
		rc.hierarchical = rc.groupObj.getHierarchical();
		var ownerid = ! isNull(rc.groupObj.getId()) ? rc.groupObj.getId() : rc.currentUser.getId();
		rc.formDefaults = { intervaltype : "none", ownerid : ownerid, ownerobjecttype : "group" };
		rc.parentId = ! isNull(rc.groupObj.getParent()) ? rc.groupObj.getParent().getId() : rc.parentId;
		rc.roleArr = ORMExecuteQuery("
			SELECT NEW MAP(name as name)
			FROM role
			WHERE isActive = 1
			ORDER BY name
		");
	}

	public void function processGroup(required struct rc) output=false hint="" {
		// abort execution in case of CRSF attack
    	variables.utilityService.abortOnCSRFAttack(rc.csrfToken);
    	// standard keys to return in redirects
		var preserveKeyList = "message,id,hierarchical";
		// validate data
		var valid = {};
		// groupName valid
		if (rc.groupName.isEmpty()) {
			valid.groupName = { valid : false, message : "Groupname is a required field" };
		}
		// validation failed, return to page and give feedback
		if (! structIsEmpty(valid)) {
			rc.message = { class : "warning", text : "The following error(s) must first be corrected: " & utilityService.buildMessage(valid) };
			// don't log the user credentials to the database
			logService.log(rc, 0);
			variables.fw.redirect(utilityService.getOriginAction(fw=variables.fw), "message");
		}
		// set defaults (think checkboxes and radio buttons)
		param name="rc.isActive" default="0";
		param name="rc.demoAccount" default="0";
		// validate, fill and save the object
		param name="rc.id" value="";
		var groupObj = groupService.getObj(rc.id);
		// autofill fields
		variables.fw.populate(groupObj);
		// parentgroup
		if (structKeyExists(rc, "parentId") && isValid("GUID", rc.parentId)) {
			var parentObj = groupService.getObj(rc.parentId);
			groupObj.setParent(parentObj);
		}
		groupObj.setGroupname(rc.groupName);
		// several groupsetting variables
		param name="rc.defaultaction" default="registered:";
		param name="rc.defaultroles" default="registered";
		var groupsettingObj = groupObj.getGroupsetting();
		groupsettingObj.setDefaultAction(rc.defaultaction);
		groupsettingObj.setDefaultRoles(rc.defaultroles);
		// save groupObj
		groupService.save(groupObj);
		rc.id = groupObj.getId();
		rc.hierarchical = groupObj.getHierarchical();
		// log, redirect and give feedback
		rc.message = { class : "success", text : "Changes have been saved" };
		logService.log(rc, 0);
		if (rc.hierarchical) {
			var parentObj = groupObj.getParent();
			if (! isNull(parentObj)) {
				rc.parentId = groupObj.getParent().getId();
				preserveKeyList = listAppend(preserveKeyList, "parentId");
			}
		}
		rc.formDefaults = { intervaltype : "none", ownerid : rc.id, ownerobjecttype : "group" };
		structAppend(rc, rc.formDefaults);
		variables.fw.redirect(action=".main", preserve=preserveKeyList);
	}

	public void function delete(required struct rc) output=false hint="" {
		// abort execution in case of CRSF attack
		//variables.utilityService.abortOnCSRFAttack(rc.csrfToken);
		if (! isValid("GUID", rc.id)) {
			rc.message = { class : "warning", text : "The given id is not valid" };
			logService.log(rc, 1);
			variables.fw.redirect(variables.fw.getSection(), "message");
		}
		transaction {
			var groupObj = entityLoadByPK("group", rc.id);
			groupObj.setIsActive(-1);
			entitySave(groupObj);
		}
		// log the action
		rc.message = { class : "success", text : "The selected item has been deleted" };
		logService.log(rc, 0);
		rc.hierarchical = groupObj.getHierarchical();
		var preserveKeyList = "message,hierarchical";
		if (rc.hierarchical) {
			var parentObj = groupObj.getParent();
			if (! isNull(parentObj)) {
				rc.parentId = groupObj.getParent().getId();
				preserveKeyList = listAppend(preserveKeyList, "parentId");
			}
		}
		variables.fw.redirect(variables.fw.getSection(), preserveKeyList);
	}

	public void function jsonForceDefaults(required struct rc) output=false hint="" {
		// abort execution in case of CRSF attack
		variables.utilityService.abortOnCSRFAttack(rc.csrfToken);
		var result = {};
		result["data"] = groupService.forceDefaultsToAllUsers(rc.id);
		variables.fw.renderData("json", SerializeJSON(result));
	}

}