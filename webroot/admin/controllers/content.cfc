component accessors=true output=false hint="" {

	property contentService;
	property logService;
	property utilityService;

	public void function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
	}

	public void function before(required struct rc) output=false hint="" {
		// prevent XSS attacks
		rc = utilityService.XSSProtect(arguments.rc);
	}

	public void function main(required struct rc) output=false hint="" {
		rc.content = ormExecuteQuery("
			SELECT new map(
				id AS id
				, action as action
				, title as title
				, group as group
				, isActive as isActive
				, ormVersion AS ormVersion
			)
			FROM contentitem
			WHERE isActive <> -1
			ORDER BY title
		");
	}

	public void function content(required struct rc) output=false hint="" {
		// set defaults
		param name="rc.id" default="";
		// get content
		rc.contentItemObj = contentService.getObj(rc.id);
		if (! isNull(rc.contentItemObj.getId())) {
			rc.contentAction = rc.contentItemObj.getAction();
		} else if (! structKeyExists(rc, "contentAction")) {
			rc.contentAction = "";
		}
		// get locales
		var localeObj = createObject("java", "java.util.Locale");
		rc.locales = localeObj.getAvailableLocales();
		if (! isNull(rc.contentItemObj.getLocale())) {
			rc.currentLocale = rc.contentItemObj.getLocale();
		} else {
			rc.currentLocale = session.locale;
		}
	}

	public void function processContent(required struct rc) output=false hint="" {
		param name="rc.id" default="";
		// abort execution in case of CRSF attack
		variables.utilityService.abortOnCSRFAttack(rc.csrfToken);
		// validate data
		var valid = {};
		// title valid
		if (rc.title.isEmpty()) {
			valid.title = { valid : false, message : "Title is a required field" };
		}
		// content valid
		if (rc.content.isEmpty()) {
			valid.content = { valid : false, message : "Content is a required field" };
		}
		// action valid
		if (rc.actionField.isEmpty()) {
			valid.actionField = { valid : false, message : "Action is a required field" };
		}
		// locale valid
		if (rc.locale.isEmpty()) {
			valid.locale = { valid : false, message : "Locale is a required field" };
		}
		// validation failed, return to page and give feedback
		if (! structIsEmpty(valid)) {
			rc.message = { class : "warning", text : "The following error(s) must first be corrected: " & utilityService.buildMessage(valid) };
			// don't log the user credentials to the database
			logService.log(rc, 0);
			var urlRedirect = rc.id != "" ? utilityService.getOriginAction(fw=variables.fw) : "admin:content";
			if (rc.id == "") {
				rc.message.text &= "<br>Not all criteria have been met. Please start over.";
			}
			variables.fw.redirect(action=urlRedirect, preserve="message,id");
		}
		// set defaults (think checkboxes and radio buttons)
		param name="rc.isActive" default="0";
		var contentItemObj = contentService.getObj(rc.id);
		variables.fw.populate(contentItemObj);
		// action is used by FW/1 so the form has to use an alternate fieldname
		contentItemObj.setAction(rc.actionField);
		if(rc.id != "") {
			contentItemObj.setUpdateDate(now());
			contentItemObj.setUpdatedBy(session.auth.userId);
		}
		// save userObjs
		contentService.save(contentItemObj);
		// reset Id for newly saved objects
		rc.id = contentItemObj.getId();
		// log, redirect and give feedback
		rc.message = { class : "success", text : "Changes have been saved" };
		logService.log(rc, 0);
		variables.fw.redirect(action=".main", preserve="message,id");
	}

	public void function deleteContent(required struct rc) output=false hint="" {
		// abort execution in case of CRSF attack
		//variables.utilityService.abortOnCSRFAttack(rc.csrfToken);
		if (! isValid("GUID", rc.id)) {
			rc.message = { class : "warning", text : "The given id is not valid" };
			logService.log(rc, 1);
			variables.fw.redirect(variables.fw.getSection(), "message");
		}
		transaction {
			var obj = entityLoadByPK("contentitem", rc.id);
			obj.setIsActive(-1);
			entitySave(obj);
		}
		// log the action
		rc.message = { class : "success", text : "The selected item has been deleted" };
		logService.log(rc, 0);
		variables.fw.redirect(variables.fw.getSection(), "message");
	}

}