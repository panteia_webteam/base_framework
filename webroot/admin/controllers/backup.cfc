component accessors=true output=false hint="" {

	property utilityService;

	public any function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
		return this;
	}

	public void function main(required struct rc) output=false hint="" {
		// get database table information
		var dbData = new Query();
		cfdbinfo(name="dbData", type="table");
		rc.tables = new Query()
			.setDBType("query")
			.setAttributes(sourceQuery=dbData)
			.setSQL("
				SELECT *
				FROM sourceQuery
				WHERE table_schem NOT IN ('sys', 'information_schema')
			")
			.execute()
			.getResult();
	}

	public void function download(required struct rc) output=false hint="" {
		if(StructKeyExists(arguments.rc, "tables")) {
			rc.filePath = "ram://data.zip";
			cfzip(action="zip", file=rc.filePath, overwrite=true) {
				var tableArr = listToArray(rc.tables);
				for (var table in tableArr) {
					cfzipparam(content="#utilityService.exportSQLTable('[#table#]')#", entrypath="#table#.sql");
				}
			};
		} else {
			rc.message = { class : "warning", text : "No table chosen" };
			variables.fw.redirect(variables.fw.getSection(), "message");
		}
	}

}