﻿component accessors=true output=false hint="" {

	property groupService;
	property logService;
	property passwordService;
	property roleService;
	property userService;
	property utilityService;

	public void function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
	}

	public void function before(required struct rc) output=false hint="" {
		// prevent XSS attacks
		rc = utilityService.XSSProtect(arguments.rc);
	}

	public void function main(struct rc) output=false hint="" {
		// retrieve the items
		var queryString = "
			SELECT NEW MAP (
				u.id AS id
				, u.isActive AS isActive
				, u.firstname AS firstname
				, u.prefix AS prefix
				, u.lastname AS lastname
				, u.gender AS gender
				, u.locale AS locale
				, u.email AS email
			)
			FROM user u
				LEFT JOIN u.groups g
				LEFT JOIN u.roles r
			WHERE u.isActive <> -1
			AND u.email IS NOT NULL
		";
		var queryStringEnd = "
			 GROUP BY u.id
				, u.isActive
				, u.firstname
				, u.prefix
				, u.lastname
				, u.gender
				, u.locale
				, u.email
			ORDER BY lastname
				, firstName
		";
		if (structKeyExists(rc, "groupId") && isValid("GUID", rc.groupId)) {
			queryString &= " AND g.id = :groupId";
			rc.userArr = ormExecuteQuery(queryString & queryStringEnd, { groupId : rc.groupId });
			rc.groupObj = groupService.getObj(rc.groupId);
		} else if (structKeyExists(rc, "roleId") && isValid("GUID", rc.roleId)) {
			queryString &= " AND r.id = :roleId";
			rc.userArr = ormExecuteQuery(queryString & queryStringEnd, { roleId : rc.roleId });
			rc.roleObj = roleService.getObj(rc.roleId);
		} else {
			rc.userArr = ormExecuteQuery(queryString & queryStringEnd);
		}
		var userRoleArr = ormExecuteQuery("
			SELECT NEW MAP (
				u.id AS fk_userId
				, r.id AS id
				, r.name AS name
			)
			FROM user u
				JOIN u.roles r
			WHERE u.isActive <> -1
		");
		rc.userStruct = {};
		for (var item in userRoleArr) {
			if (! structKeyExists(rc.userStruct, item.fk_userId)) {
				rc.userStruct[item.fk_userId] = {};
			}
			rc.userStruct[item.fk_userId][item.id] = { name : item.name, id : item.id };
		}
	}

	public void function user(struct rc) output=false hint="" {
		param name="rc.id" default="";
		rc.userObj = userService.getObj(rc.id);
		rc.currentRoles = ormExecuteQuery("
			SELECT r.id
			FROM user u
				JOIN u.roles r
			WHERE r.isActive = 1
			AND u.id = :userId
		", { userId : rc.userObj.getId() });
		// get locales
		var localeObj = createObject("java", "java.util.Locale");
		rc.locales = localeObj.getAvailableLocales();
		// get groups
		rc.groupArr = ORMExecuteQuery("
			SELECT NEW MAP(
				id AS id
				, groupName AS groupName
				, demoAccount AS demoAccount
			)
			FROM group
			WHERE isActive = 1
			AND hierarchical = 0
			ORDER BY listOrder
				, groupName
		");
		// get hierarchicalgroups
		rc.hierarchicalGroupArr = ORMExecuteQuery("
			SELECT NEW MAP(
				id AS id
				, groupName AS groupName
				, demoAccount AS demoAccount
			)
			FROM group
			WHERE isActive = 1
			AND hierarchical = 1
			ORDER BY listOrder
				, groupName
		");
		// get roles
		rc.roleArr = ORMExecuteQuery("
			SELECT NEW MAP(
				id AS id
				, name AS name
				, description AS description
			)
			FROM role
			WHERE isActive = 1
			ORDER BY name
		");
	}

	public void function processUser(required struct rc) output=false hint="" {
		// abort execution in case of CRSF attack
		variables.utilityService.abortOnCSRFAttack(rc.csrfToken);
		// get the user
		param name="rc.id" value="";
		var userObj = userService.getObj(rc.id);
		// validate data
		var valid = {};
		// email valid
		if (rc.email.isEmpty()) {
			valid.email = { valid : false, message : "Email is a required field" };
		} else if (! isValid("email", rc.email)) {
			valid.email = { valid : false, message : "The given email is not properly formatted" };
		} else {
			// check if email already exists
			var checkUserObj = ORMExecuteQuery(" FROM user WHERE email = :email AND isActive = 1", { email : rc.email }, true);
			if (! isNull(checkUserObj) && ! isNull(checkUserObj.getId()) && ! isNull(userObj) && (checkUserObj.getId() != userObj.getId())) {
				valid.email = { valid : false, message : "Email already exists" };
			}
		}
		// defaultaction valid
		if (rc.defaultaction.isEmpty()) {
			valid.defaultaction = { valid : false, message : "Default action is a required field" };
		}
		// group
		if (structKeyExists(rc, "hierarchicalGroupId") && isValid("guid", rc.hierarchicalGroupId)) {
			var groupObj = groupService.getObj(rc.hierarchicalGroupId);
			if (isNull(groupObj.getId())) {
				valid.group = { valid : false, message : "The hierarchical group cannot be found." };
			}
		}
		// validation failed, return to page and give feedback
		if (! structIsEmpty(valid)) {
			rc.message = { class : "warning", text : "The following error(s) must first be corrected: " & uitlityService.buildMessage(valid) };
			// don't log the user credentials to the database
			logService.log(rc, 0);
			variables.fw.redirect(utilityService.getOriginAction(fw=variables.fw), "message,id");
		}
		// set defaults (think checkboxes and radio buttons)
		param name="rc.email_optout" default="0";
		param name="rc.isActive" default="0";
		param name="rc.new" default="0";
		// validate, fill and save the object
		variables.fw.populate(userObj);
		// if password is given, set it
		if (len(rc.password)) {
			var passwordObj = passwordService.getObj();
			passwordObj.setPassword(rc.password);
			userObj.addPassword(passwordObj);
		}
		// set roles
		if (! rc.roleIds.isEmpty()) {
			var rolesArr = ormExecuteQuery(" FROM role WHERE id IN (:roleIdsArr)", { roleIdsArr : listToArray(rc.roleIds) });
			if (! rc.id.isEmpty()) {
				userObj.deleteRoles();
			}
			userObj.setRoles(rolesArr);
		}
		// set groups
		if (! isNull(rc.groupIds)) {
			// add all (new) refs user-group
			var groupsArr = ormExecuteQuery(" FROM group WHERE id IN (:groupIdsArr)", { groupIdsArr : listToArray(rc.groupIds) });
			// delete all refs user-group
			if (! rc.id.isEmpty()) {
				userObj.deleteGroups();
			}
			userObj.setGroups(groupsArr);
		}
		// add hierarchical group
		if (structKeyExists(local, "groupObj")) {
			if (userObj.getHierarchicalGroup().getId() != groupObj.getId()) {
				userObj.removeGroup(userObj.getHierarchicalGroup());
			}
			userObj.addGroup(groupObj);
		}
		if (isNull(userObj.getEncryptsalt())) {
			userObj.setEncryptsalt(utilityService.generateRandomString());
		}
		// save userObj
		userService.save(userObj);
		// log, redirect and give feedback
		rc.message = { class : "success", text : "Changes have been saved" };
		logService.log(rc, 0);
		rc.id = userObj.getId();
		variables.fw.redirect(".main", "message,id");
	}

	public void function mimic(required struct rc) output=false hint="" {
		/* set mimic variables */
		session.auth.mimic = rc.id;
		var userobj = userService.getObj(rc.id);
		variables.fw.redirect(userobj.getDefaultAction());
	}

	public void function log(struct rc) output=false hint="" {
		var queryString = "
			SELECT l.id
				, l.action
				, l.ip_hash
				, l.class
				, l.description
				, l.serializedData
				, l.createDate
				, l.createdBy
				, u.prefix
				, u.lastname
				, u.firstname
				, u.email
				, u.locale
				, u.isActive
				, u.gender
			FROM [log] l
				LEFT OUTER JOIN [user] u ON l.createdBy = u.id
		";
		var queryStringEnd = " ORDER BY l.createDate desc"
		var queryService = new Query();
		// retrieve logs for one user
		if (structKeyExists(rc, "id") && isValid("GUID", rc.id)) {
			queryString &= " WHERE l.createdBy = :userId";
			queryService.addParam(name="userId", value=rc.id, cfsqltype="CF_SQL_IDSTAMP");
			// get data for single user
			rc.userObj = userService.getObj(rc.id);
		}
		// retrieve logs for one/all user(s)
		queryString &= queryStringEnd;
		rc.logQry = queryService
			.setSQL(queryString)
			.execute()
			.getResult();
	}

	public void function deleteUser(required struct rc) output=false hint="" {
		// abort execution in case of CRSF attack
		//variables.utilityService.abortOnCSRFAttack(rc.csrfToken);
		if (! isValid("GUID", rc.id)) {
			rc.message = { class : "warning", text : "The given id is not valid" };
			logService.log(rc, 1);
			variables.fw.redirect(variables.fw.getSection(), "message");
		}
		transaction {
			var userObj = entityLoadByPK("user", rc.id);
			userObj.setIsActive(-1);
			entitySave(userObj);
			// delete all refs user-group
			userObj.deleteGroups();
		}
		// log the action
		rc.message = { class : "success", text : "The selected item has been deleted" };
		logService.log(rc, 0);
		variables.fw.redirect(variables.fw.getSection(), "message");
	}

}