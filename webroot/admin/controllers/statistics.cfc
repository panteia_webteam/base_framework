component accessors=true output=false hint="" {

	property utilityService;

	public any function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
		return this;
	}

	public void function before(required struct rc) output=false hint="" {}

	public void function main(struct rc) output=false hint="" {
		rc.userStats = [];
		var userCount = new Query()
			.setSQL("
				SELECT count(id) AS userCount
				FROM [user]
				WHERE isActive <> -1
			")
			.execute()
			.getResult();
		var sumCount = new Query()
			.setDBType("query")
			.setAttributes(sourceQuery=userCount)
			.setSQL("
				SELECT sum(userCount) AS sumCount
				FROM sourceQuery
			")
			.execute()
			.getResult();
		arrayAppend(rc.userStats, {
			title : "Total active users"
			, size : 240
			, userCount : new Query()
				.setSQL("
					SELECT count(id) AS userCount
					FROM [user]
					WHERE isActive = 1
				")
				.execute()
				.getResult()
				.userCount[1]
			, sumCount : sumCount.sumCount[1]
		});
		arrayAppend(rc.userStats, {
			title : "Total registered users"
			, size : 120
			, userCount : userCount.userCount[1]
			, sumCount : sumCount.sumCount[1]});
		var totalLogins = new Query()
			.setSQL("
				SELECT count(createdBy) as userCount
				FROM log
				WHERE (
					action = 'home:main.processmain'
					OR action = 'home:login.processmain'
					OR action = 'home:security.processlogin'
				)
			")
			.execute()
			.getResult();
		arrayAppend(rc.userStats, {
			title : "Unique logins today"
			, size : 240
			, userCount : new Query()
				.setSQL("
					SELECT count(distinct createdBy) as userCount
					FROM log
					WHERE (
						action = 'home:main.processmain'
						OR action = 'home:login.processmain'
						OR action = 'home:security.processlogin'
					)
					AND dateDiff(d,getDate(), createDate) = 0
				")
				.execute()
				.getResult()
				.userCount[1]
			, sumCount : totalLogins.userCount[1]
		});
		arrayAppend(rc.userStats, {
			title : "Logins today"
			, size : 120
			, userCount :  new Query()
				.setSQL("
					SELECT count(createdBy) as userCount
					FROM log
					WHERE (
						action = 'home:main.processmain'
						OR action = 'home:login.processmain'
						OR action = 'home:security.processlogin'
					)
					AND dateDiff(d,getDate(), createDate) = 0
				")
				.execute()
				.getResult()
				.userCount[1]
			, sumCount : totalLogins.userCount[1]
		});
		arrayAppend(rc.userStats, {
			title : "Logins this week"
			, size : 120
			, userCount : new Query()
				.setSQL("
					SELECT count(createdBy) as userCount
					FROM log
					WHERE (
						action = 'home:main.processmain'
						OR action = 'home:login.processmain'
						OR action = 'home:security.processlogin'
					)
					AND dateDiff(wk,getDate(), createDate) = 0
				")
				.execute()
				.getResult()
				.userCount[1]
			, sumCount : totalLogins.userCount[1]
		});
		arrayAppend(rc.userStats, {
			title : "Unique logins this week"
			, size : 240
			, userCount : new Query()
				.setSQL("
					SELECT count(distinct createdBy) as userCount
					FROM log
					WHERE (
						action = 'home:main.processmain'
						OR action = 'home:login.processmain'
						OR action = 'home:security.processlogin'
					)
					AND dateDiff(wk,getDate(), createDate) = 0
				")
				.execute()
				.getResult()
				.userCount[1]
			, sumCount : totalLogins.userCount[1]
		});
		arrayAppend(rc.userStats, {
			title : "Logins this month"
			, size : 120
			, userCount : new Query()
				.setSQL("
					SELECT count(createdBy) as userCount
					FROM log
					WHERE (
						action = 'home:main.processmain'
						OR action = 'home:login.processmain'
						OR action = 'home:security.processlogin'
					)
					AND dateDiff(m,getDate(), createDate) = 0
				")
				.execute()
				.getResult()
				.userCount[1]
			, sumCount : totalLogins.userCount[1]
		});
		arrayAppend(rc.userStats, {
			title : "Unique logins this month"
			, size : 240
			, userCount : new Query()
				.setSQL("
					SELECT count(distinct createdBy) as userCount
					FROM log
					WHERE (
						action = 'home:main.processmain'
						OR action = 'home:login.processmain'
						OR action = 'home:security.processlogin'
					)
					AND dateDiff(m,getDate(), createDate) = 0
				")
				.execute()
				.getResult()
				.userCount[1]
			, sumCount : totalLogins.userCount[1]
		});
	}

}