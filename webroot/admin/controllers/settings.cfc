component accessors=true output=false hint="" {

	property config;
	property groupService;
	property logService;
	property settingsService;
	property userService;
	property utilityService;

	public void function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
	}

	public void function before(required struct rc) output=false hint="" {
		//prevent XSS attacks
		rc = utilityService.XSSProtect(arguments.rc);
	}

	public void function main(struct rc) output=false hint="" {
		param name="rc.id" default="";
		rc.settingsObj = settingsService.getObjBy({ domainname : cgi.server_name });
		var localeObj = createObject("java", "java.util.Locale");
		rc.locales = localeObj.getAvailableLocales();
	}

	public void function processMain(required struct rc) output=false hint="" {
		// abort execution in case of CRSF attack
		variables.utilityService.abortOnCSRFAttack(rc.csrfToken);
		// validate data
		var valid = {};
		// displayName
		if (rc.displayName.isEmpty()) {
			valid.displayName = { valid : false, message : "displayName is a required field" };
		}
		// copyrightUrl
		if (rc.copyrightUrl.isEmpty()) {
			valid.copyrightUrl = { valid : false, message : "copyrightUrl is a required field" };
		}
		// emailFrom
		if (rc.emailFrom.isEmpty()) {
			valid.emailFrom = { valid : false, message : "emailFrom is a required field" };
		}
		// emailHelpdesk
		if (rc.emailHelpdesk.isEmpty()) {
			valid.emailHelpdesk = { valid : false, message : "emailHelpdesk is a required field" };
		}
		// defaultLocale
		if (rc.defaultLocale.isEmpty()) {
			valid.defaultLocale = { valid : false, message : "defaultLocale is a required field" };
		}
		// hasSSL
		if (rc.hasSSL.isEmpty()) {
			valid.hasSSL = { valid : false, message : "hasSSL is a required field" };
		}
		// version
		if (rc.version.isEmpty()) {
			valid.version = { valid : false, message : "version is a required field" };
		}
		// validation failed, return to page and give feedback
		if (! structIsEmpty(valid)) {
			rc.message = { class : "warning", text : "The following error(s) must be corrected first: " & utilityService.buildMessage(valid) };
			// don't log the user credentials to the database
			logService.log(rc, 0);
			variables.fw.redirect(utilityService.getOriginAction(fw=variables.fw), "message");
		}
		// set defaults (think checkboxes and radio buttons)
		param name="rc.isActive" default="1";
		param name="rc.protectInput" default="0";
		var changeProtectInput = config.security.protectInput != rc.protectInput ? true : false;
		// validate, fill and save the object
		param name="rc.id" value="";
		var settingsObj = settingsService.getObj(rc.id);
		// autofill fields
		variables.fw.populate(settingsObj);
		// save settingsObj
		settingsService.save(settingsObj);
		// log, redirect and give feedback
		rc.message = { class : "success", text : "Changes have been saved" };
		logService.log(rc, 0);
		rc.id = settingsObj.getId();
		if (changeProtectInput) {
			switchEncryption(rc.protectInput);
		}
		variables.fw.redirect(utilityService.getOriginAction(fw=variables.fw), "message,id");
	}

	private boolean function switchEncryption(boolean protectInput) output=false hint="" {
		if (! arguments.protectInput) {
			userService.removeEncryption();
			groupService.removeEncryption();
		} else {
			userService.addEncryption();
			groupService.addEncryption();
		}
		return true;
	}

}