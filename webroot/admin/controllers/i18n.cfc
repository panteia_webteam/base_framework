﻿component accessors=true output=false hint="" {

	property logService;
	property utilityService;

	public void function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
	}

	public void function before(required struct rc) output=false hint="" {
		// prevent XSS attacks
		rc = utilityService.XSSProtect(arguments.rc);
	}

	public void function main(struct rc) output=false hint="" {
		var resourcebundleArr = ormExecuteQuery("
			SELECT new map(
				r.bundle AS bundle
				, r.item AS item
				, r.createDate AS createDate
				, r.updateDate AS updateDate
				, r.locale AS locale
			)
			FROM resourcebundle r
			WHERE r.isActive <> -1
			GROUP BY r.bundle
				, r.item
				, r.createDate
				, r.updateDate
				, r.locale
			ORDER BY bundle
				, item
		");
		rc.resourcebundleStruct = {};
		for (var item in resourcebundleArr) {
			// create bundle element
			if (! structKeyExists(rc.resourcebundleStruct, item["bundle"])) {
				rc.resourcebundleStruct[item["bundle"]] = {};
			}
			// create item element
			if (! structKeyExists(rc.resourcebundleStruct[item["bundle"]], item["item"])) {
				rc.resourcebundleStruct[item["bundle"]][item["item"]] = item;
			} else {
				rc.resourcebundleStruct[item["bundle"]][item["item"]]["locale"] = listAppend(rc.resourcebundleStruct[item["bundle"]][item["item"]]["locale"], item["locale"]);
			}
		}
	}

	public void function i18n(struct rc) output=false hint="" {
		// set defaults
		param name="rc.bundle" default="global";
		param name="rc.item" default="";
		// get I18N
		rc.resourcebundlesArr = ORMExecuteQuery("
			SELECT NEW MAP (
				item AS item
				, bundle AS bundle
				, locale AS locale
				, text AS text
				, isActive AS isActive
			)
			FROM resourcebundle
			WHERE isActive <> -1
			AND bundle = :bundle
			AND item = :item
		", { bundle : rc.bundle, item : rc.item });
		rc.hasResourceBundles = arrayIsEmpty(rc.resourcebundlesArr);
		// get locales
		var localeObj = createObject("java", "java.util.Locale");
		rc.locales = localeObj.getAvailableLocales();
	}

	public void function processI18N(required struct rc) output=false hint="" {
		// abort execution in case of CRSF attack
		variables.utilityService.abortOnCSRFAttack(rc.csrfToken);
		// validate data
		var valid = {};
		// bundle valid
		if (rc.bundle.isEmpty()) {
			valid.bundle = { valid : false, message : "Bundle is a required field" };
		}
		// item/variable valid
		if (rc.item.isEmpty()) {
			valid.item = { valid : false, message : "Variable is a required field" };
		}
		// locale valid
		if (rc.locale.isEmpty()) {
			valid.locale = { valid : false, message : "Locale is a required field" };
		}
		// text valid
		if (rc.content.isEmpty()) {
			valid.content = { valid : false, message : "Text is a required field" };
		}
		//validation failed, return to page and give feedback
		if (! structIsEmpty(valid)) {
			rc.message = { class : "warning", text : "The following error(s) must first be corrected: " & utilityService.buildMessage(valid) };
			// don't log the user credentials to the database
			logService.log(rc, 0);
			variables.fw.redirect(action=utilityService.getOriginAction(fw=variables.fw), preserve="message");
		}
		var resourcebundleObj = "";
		var localesLen = arrayLen(rc.locale);
		for (var i=1; i <= localesLen; i++) {
			transaction {
				resourcebundleObj = entityLoad("resourcebundle", { bundle : rc.bundle, item : rc.item, locale : rc.locale[i] });
				if(arrayIsEmpty(resourcebundleObj)) {
					resourcebundleObj = entityNew("resourcebundle");
				} else {
					resourcebundleObj = resourcebundleObj[1];
				}
				resourcebundleObj.setBundle(rc.bundle);
				resourcebundleObj.setItem(rc.item);
				resourcebundleObj.setLocale(rc.locale[i]);
				resourcebundleObj.setText(rc.content[i]);
				entitySave(resourcebundleObj);
			}
		}
		// log, redirect and give feedback
		rc.message = { class : "success", text : "Changes have been saved" };
		logService.log(rc, 0);
		variables.fw.redirect(action=".main", preserve="message,bundle,item");
	}

}