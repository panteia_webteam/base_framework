﻿component accessors=true output=false hint="" {

	property logService;
	property roleService;
	property utilityService;

	public void function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
	}

	public void function before(required struct rc) output=false hint="" {
		// prevent XSS attacks
		rc = utilityService.XSSProtect(arguments.rc);
	}

	public void function main(struct rc) output=false hint="" {
		var queryString = "
			SELECT new map (
				r.id AS id
				, r.isActive AS isActive
				, r.name AS name
				, r.description AS description
			)
			FROM role r
			WHERE r.isActive <> -1
			ORDER BY r.name desc
		";
		rc.roleArr = ORMExecuteQuery(queryString);
	}

	public void function role(struct rc) output=false hint="" {
		param name="rc.id" default="";
		rc.roleObj = roleService.getObj(rc.id);
	}

	public void function processRole(required struct rc) output=false hint="" {
		// abort execution in case of CRSF attack
		variables.utilityService.abortOnCSRFAttack(rc.csrfToken);
		// validate data
		var valid = {};
		// name valid
		if (rc.name.isEmpty()) {
			valid.name = { valid : false, message : "Name is a required field" };
		}
		// validation failed, return to page and give feedback
		if (! structIsEmpty(valid)) {
			rc.message = { class : "warning", text : "The following error(s) must first be corrected: " & utilityService.buildMessage(valid) };
			//don't log the user credentials to the database
			logService.log(rc, 0);
			variables.fw.redirect(utilityService.getOriginAction(fw=variables.fw), "message");
		}
		// set defaults (think checkboxes and radio buttons)
		param name="rc.isActive" default="0";
		// Valid, fill and save the object
		param name="rc.id" value="";
		var roleObj = roleService.getObj(rc.id);
		// autofill fields
		variables.fw.populate(roleObj, "name,description,isActive,blacklist,whitelist,isActive");
		// save groupObj
		roleService.save(roleObj);
		// log, redirect and give feedback
		rc.message = { class : "success", text : "Changes have been saved" };
		logService.log(rc, 0);
		rc.id = roleObj.getId();
		variables.fw.redirect(".main", "message,id");
	}

	public void function delete(required struct rc) output=false hint="" {
		// abort execution in case of CRSF attack
		//variables.utilityService.abortOnCSRFAttack(rc.csrfToken);
		if (! isValid("GUID", rc.id)) {
			rc.message = { class : "warning", text : "The given id is not valid" };
			logService.log(rc, 1);
			variables.fw.redirect(variables.fw.getSection(), "message");
		}
		var obj = entityLoadByPK("role", rc.id);
		roleService.delete(obj);
		// log the action
		rc.message = { class : "success", text : "The selected item has been deleted" };
		logService.log(rc, 0);
		variables.fw.redirect(variables.fw.getSection(), "message");
	}

}