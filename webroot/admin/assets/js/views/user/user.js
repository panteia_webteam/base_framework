$(function(){
	// select2
	$('#locale').select2({
		placeholder: 'Select the locale'
		, allowClear: true
		, escapeMarkup: function(m) { return m; }
	});
	$('#roles').select2({
		placeholder: 'Select one or more roles'
	});
	$('#groups').select2({
		placeholder: 'Select one or more groups'
	});
	$('#hierarchicalGroup').select2({
		placeholder: 'Select the hierarchical group'
	})
	// datemask
	$('#datemask').inputmask();
	// validation
	$('form').validate({
		rules: {
			password: {
				minlength: 5
			}
			, confirm_password: {
				minlength: 5
				, equalTo: '#password'
			}
		}
	});
	'use strict';
	var options = {};
	options.ui = {
		container: '#pwd-container'
		, showVerdicts: false
		, viewports: {
			progress: '.pwstrength_viewport_progress'
		}
	};
	options.common = {
		debug: false
		, minChar: 5
	};
	$('#password:password').pwstrength(options);
});