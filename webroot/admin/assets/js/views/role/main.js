$(function() {
	$('#dataTable').dataTable({
		'aoColumnDefs': [{
			'bSortable': false
			, 'aTargets': [3,4]
		}]
		, 'lengthMenu': menuArray
		, 'language': languageStruct
	});
	$('.removeIcon').click(function(){
		return confirm('Are you sure you want to delete this item permanently?');
	});
});