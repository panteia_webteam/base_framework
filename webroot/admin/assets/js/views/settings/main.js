$(function(){
	// add locale
	$('#defaultLocale').select2({
		placeholder: 'Select the locale'
		, allowClear: true
		, escapeMarkup: function(m) { return m; }
	});
	// validation
	$('form').validate();
});