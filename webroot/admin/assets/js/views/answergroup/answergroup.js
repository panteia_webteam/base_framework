//validation
$('form').validate();
//isActive toggle
function addEvent(){
	$('input.activeCheck').on('ifChecked ifUnchecked', function(event){
	  var hiddenInput = $($(this).parent()).next();
	  if (event.type == 'ifChecked') {
	  	hiddenInput.val(1);
	  } else {
	  	hiddenInput.val(0);
	  }
	});
}
addEvent();
function addAnswer() {
	$('input[type="checkbox"], input[type="radio"]').iCheck('destroy');
	// clone the row and insert it in the DOM
	var newElem = $('#answer').children('.row').first().clone();
	//hide dummy field answernameShown
	newElem.find('input[name="answernameShown[]"]').val('').attr('type','hidden');
	//hide dummy field valueShown
	newElem.find('input[name="valueShown[]"]').val('').attr('type','hidden');
	//clear id
	newElem.find('input[name="answerId[]"]').val('');
	//clear and show answername
	newElem.find('input[name="answername[]"]').val('').attr('type','text');
	//clear and show value
	newElem.find('input[name="value[]"]').val('').attr('type','text');
	//clear listorder
	var listMax = 0;
	[].forEach.call($('input[name="listorder[]"]'), function(e){
	  if (parseInt(e.value) > listMax) {
	  	listMax = e.value;
	  }
	});
	newElem.find('input[name="listorder[]"]').val(parseInt(listMax)+1);
	newElem.find('input[name="value[]"]').val(parseInt(listMax)+1);
	//
	$('input.activeCheck').off();
	//clone the row on screen
	$('#answer').append(newElem);
	addEvent();
	$('input[type="checkbox"], input[type="radio"]').iCheck({
        checkboxClass: 'icheckbox_minimal',
        radioClass: 'iradio_minimal'
    });
}
//add answer
$('#addAnswer').click(function(){
	addAnswer();
});
$('form').on('blur', 'input[name="answername[]"]', function(){
	addAnswer();
});