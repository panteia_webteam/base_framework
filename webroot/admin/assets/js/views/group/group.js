$(function(){
	$('form').validate();
	$('#defaultroles').select2({ placeholder: 'Select one or more roles' });
	var actionStart
		, rolesStart
		, styleInfo = {
			marginBottom: '15px'
			, padding: '8px'
			, color: '#856404'
			, backgroundColor: '#fff3cd'
			, borderWidth: '1px'
			, borderStyle: 'solid'
			, borderColor: 'ffeeba'
		}
		, styleConfirm = {
			marginBottom: '15px'
			, padding: '8px'
			, color: '#004085'
			, backgroundColor: '#cce5ff'
			, borderWidth: '1px'
			, borderStyle: 'solid'
			, borderColor: '#b8daff'
		}
		, styleDanger = {
			marginBottom: '15px'
			, padding: '8px'
			, color: '#721c24'
			, backgroundColor: '#f8d7da'
			, borderWidth: '1px'
			, borderStyle: 'solid'
			, borderColor: '#f5c6cb'
		};
	actionStart = $('#defaultaction').val(), rolesStart = $('#defaultroles').val();
	$(document).on('click', '#setdefaults', function(event) {
		event.stopPropagation();
		var	actionSet = $('#defaultaction').val()
			, rolesSet = $('#defaultroles').val()
			, attr = $('#setdefaults').attr('disabled')
			, hasAttribute = typeof attr !== typeof undefined && attr !== false ? true : false;
		if (! hasAttribute) {
			if (actionSet != actionStart || convertToText(rolesSet) != convertToText(rolesStart)) {
				alert('no good...!');
				$('#setdefaults').attr('disabled', 'disabled');
				$('#eventinfo').html('First save form').css(styleInfo).show();
			} else {
				forceDefaults();
			}
		}
	});
	function forceDefaults(options) {
		var strValue = $.extend({ }, options);
		datacallOverview(function(results) {
			var parsedjson = $.parseJSON(results)
				, dataSet = parsedjson.data;
			if (dataSet) {
				$('#eventinfo').html('Defaults has been set for all users').css(styleConfirm).show();
			} else {
				$('#eventinfo').html('Something went wrong').css(styleDanger).show();
			}
		});
	}
	function datacallOverview(callback) {
		$.ajax({ url: actionUrl, type: 'post', dataType: 'json' })
		.done(function(results) {
			callback(results);
		})
		.fail(function(results) {
			$('#eventinfo').html('Something went wrong').css(styleDanger).show();
		});
	}
	function convertToText(arr) {
		var blkstr = arr;
		return blkstr.join(', ');
	}
});