﻿$(function(){
	var table = $('#groupTable').DataTable({
		'data': groups.DATA
		, 'columnDefs': [
			{
				'targets': 4
				, 'className' : 'text-center'
				, 'data': null
				, 'render': function(data,type,full,meta){
					if (data[4] == 1) {
						return '<span class="badge text-bg-success">Active</span>';
					} else {
						return '<span class="badge text-bg-danger">Inactive</span>';
					}
				}
			}, {
				'targets': 5
				, 'className' : 'text-center'
				, 'data': null
				, 'defaultContent': '<a class="hgroupURL" href=""><i class="fa fa-sitemap"></i></a>'
			}, {
				'targets': 6
				, 'className' : 'text-center'
				, 'data': null
				, 'defaultContent': '<a class="groupURL" href=""><i class="fa fa-users"></i></a>'
			}, {
				'targets': 7
				, 'className' : 'text-center'
				, 'data': null
				, 'defaultContent': '<a class="userURL" href=""><i class="fa fa-user"></i></a>'
			}, {
				'targets': 8
				, 'className' : 'text-center'
				, 'data': null
				, 'defaultContent': '<a class="editURL" href=""><i class="fa fa-pencil-square"></i></a>'
			}, {
				'targets': 9
				, 'className' : 'text-center'
				, 'data': null
				, 'defaultContent': '<a class="deleteURL" href="" title="Remove #currentItem.groupName#"><i class="fa fa-trash"></i></a>'
			}
		]
		, 'aoColumns': [
			{ 'bVisible': false, 'bSortable': false, 'bSearchable': false }/* id */
			, { 'bVisible': false, 'bSortable': false, 'bSearchable': false }/* fk_parentId */
			, { 'bVisible': true, 'bSortable': true, 'bSearchable': true }/* relationid */
			, { 'bVisible': true, 'bSortable': true, 'bSearchable': true }/* GROUPNAME */
			, { 'bVisible': true, 'bSortable': true, 'bSearchable': true }/* active */
			, { 'bVisible': hierarchical, 'bSortable': false, 'bSearchable': false }/* Organization */
			, { 'bVisible': hierarchical, 'bSortable': false, 'bSearchable': false }/* Groups */
			, { 'bVisible': true, 'bSortable': false, 'bSearchable': false }/* Users */
			, { 'bVisible': true, 'bSortable': false, 'bSearchable': false }/* Change */
			, { 'bVisible': true, 'bSortable': false, 'bSearchable': false }/* Delete */
		]
		, 'order': [3, 'asc']
		, 'lengthMenu': menuArray
		, 'language': languageStruct
	});
	$('#groupTable tbody').on('click', 'a.hgroupURL', function(e){
		e.preventDefault();
		var data = table.row( $(this).parents('tr')).data();
		window.location.href = hgroupURL + data[0];
	});
	$('#groupTable tbody').on('click', 'a.groupURL', function(e){
		e.preventDefault();
		var data = table.row($(this).parents('tr')).data();
		window.location.href = groupURL + data[0];
	});
	$('#groupTable tbody').on('click', 'a.userURL', function(e){
		e.preventDefault();
		var data = table.row($(this).parents('tr')).data();
		window.location.href = userURL + data[0];
	});
	$('#groupTable tbody').on('click', 'a.editURL', function(e){
		e.preventDefault();
		var data = table.row($(this).parents('tr')).data();
		window.location.href = editURL + data[0];
	});
	$('#groupTable tbody').on('click', 'a.deleteURL', function(e){
		e.preventDefault();
		var data = table.row($(this).parents('tr')).data();
		if(confirm("Are you sure you want to delete this item permanently?")) {
			window.location.href = deleteURL + data[0];
		}
	});
});