$(function() {
	$('#dataTable').dataTable({
		'aoColumnDefs': [{
			'bSortable': false
			, 'aTargets': [3,4,5]
		}]
		, 'lengthMenu': menuArray
		, 'language': languageStruct
		, 'deferRender': true
		, 'pagingType': 'full_numbers'
	});
	$('.removeIcon').click(function(){
		 return confirm('Are you sure you want to delete this item permanently?');
	});
});