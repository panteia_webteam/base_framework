$(function(){
	// validation
	$('form').validate();
	// select2
	function format(parent) {
		var originalOption = parent.element;
		var markup = '<div class="parent-title">' + parent.text;
		if (! parent.id) {
			return markup; // optgroup
		}
		if ($(originalOption).data('action') !== undefined) {
			markup += '<span class="parent-action">action: ' + $(originalOption).data('action') + '</span>';
		}
		markup += '</div>';
		if (! $(originalOption).data('description')) {
			return markup;
		}
		return markup + '<p class="parent-description">' + $(originalOption).data('description') + '</p>';
	}
	$('#parents').select2({
		placeholder: 'Select a parent'
		, allowClear: true
		, formatResult: format
		, formatSelection: format
		, escapeMarkup: function(m) { return m; }
	});
});