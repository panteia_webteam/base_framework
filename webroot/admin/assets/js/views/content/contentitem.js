const ckEditorStruct = {
	width: '100%'
	, placeholder: 'start typing'
	, removePlugins: ['heading','imageUpload','mediaEmbed','insertTable']
	, toolbar: ['bold','italic','|','bulletedList','numberedList','outdent','indent','blockQuote','link','|','undo','redo']
};
$(function(){
	// CKEditor 5
	ClassicEditor
		.create(document.querySelector('#content'), ckEditorStruct)
		.catch(error => { console.error(error); });
	// select2
	$('#locale').select2({
		placeholder: 'Select the locale'
		, allowClear: true
		, escapeMarkup: function(m) { return m; }
	});
	// validation
	$('form').validate();
});