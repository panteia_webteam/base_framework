$(function() {
	$('#dataTable').dataTable({
		'aoColumnDefs': [
			{
				'bSortable': false
				, 'aTargets': [5]
			}, {
				'sType': 'date-eu'
				, 'targets': [3,4]
			}
		]
		, 'lengthMenu': menuArray
		, 'language': languageStruct
		, 'deferRender': true
		, 'pagingType': 'full_numbers'
	});
});