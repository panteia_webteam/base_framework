const ckEditorStruct = {
	width: '100%'
	, placeholder: 'start typing'
	, removePlugins: ['heading','imageUpload','mediaEmbed','insertTable','link']
	, toolbar: ['bold','italic','|','bulletedList','numberedList','outdent','indent','blockQuote','|','undo','redo']
};
$(function(){
	// CKEditor 5
	ClassicEditor
		.create(document.querySelector('#content1'), ckEditorStruct)
		.catch(error => { console.error(error); });
	/* for now only three locales are supported (Dutch, English and Flemish) */
	if(typeof(document.querySelector('#content2')) != 'undefined' && document.querySelector('#content2') != null) {
		ClassicEditor
			.create(document.querySelector('#content2'), ckEditorStruct)
			.catch(error => { console.error(error); });
	}
	if(typeof(document.querySelector('#content3')) != 'undefined' && document.querySelector('#content3') != null) {
		ClassicEditor
			.create(document.querySelector('#content3'), ckEditorStruct)
			.catch(error => { console.error(error); });
	}
	// dropdowns
	$('#localeSelect').find('select').select2({
		placeholder: 'Select the locale'
		, allowClear: true
		, escapeMarkup: function(m) { return m; }
	});
	$('#addLocale').click(function(){
		$('#localeSelect')
			.find('select')
			// call destroy to revert the changes made by Select2
			.select2('destroy')
			.end()
			.append(
				// clone the row and insert it in the DOM
				$('#localeSelect')
					.children('.row')
					.first()
					.clone()
			);
		// enable Select2 on the select elements
		$('#localeSelect').find('select').select2({
			placeholder: 'Select the locale'
			, allowClear: true
			, escapeMarkup: function(m) { return m; }
		});
	});
	// validation
	$('form').validate();
});