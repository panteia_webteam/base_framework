﻿<cfscript>
	request.layout = false;
	local.config = getBeanFactory().getBean("config");
	local.assetsService = getBeanFactory().getBean("assetsService");
	rc = assetsService.addAssetsToRC(rc, session.locale);
	local.avatar = (rc.currentUser.getGender() eq "m" ? "avatar4" : "avatar3")&".png";
	local.title = rc.title != "" ? " - " & LCase(rc.title) : "";
	param name="rc.title" default="";
	param name="rc.topmenu" default="menu/topmenu";
	rc.cspHeaderStyle &= " #application.bootstrapCSSVersionCdn# #application.fontAwesomeVersionCdn#";
	rc.cspHeaderScript &= " #application.bootstrapJSVersionCdn# #application.jQueryVersionCdn#";
	include template="/global/views/main/importCSP.cfm";
</cfscript>
<cfoutput>
<!DOCTYPE html>
<html lang="#ListFirst(session.locale, '_')#">
	<head>
		<meta charset="UTF-8">
		<title>Admin - #local.config.displayName&local.title#</title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link href="#application.applicationUrl#assets/gfx/favicon/favicon.ico" type="image/x-icon" rel="icon">
		<link href="#application.bootstrapCSSVersionCdn#" type="text/css" rel="stylesheet" media="all" crossorigin="anonymous">
		<link href="#application.fontAwesomeVersionCdn#" type="text/css" rel="stylesheet" media="all" crossorigin="anonymous">
		<link href="#application.applicationUrl##getSubSystem()#/assets/css/AdminLTE.min.css" type="text/css" rel="stylesheet">
		<link href="#application.applicationUrl##getSubSystem()#/assets/css/skins/skin-#application.adminSkin#.min.css" type="text/css" rel="stylesheet">
		<link href="#application.applicationUrl##getSubSystem()#/assets/css/custom.css?v=#rc.version#" type="text/css" rel="stylesheet">
		<cfinclude template="/global/views/main/importViewCSS.cfm">
	</head>
	<body class="skin-#application.adminSkin#">
		<div class="wrapper">
			<header class="main-header">
				<a href="##" class="logo">
					<strong>Admin</strong>
				</a>
				#view("global/navbar")#
			</header>
			<aside class="main-sidebar">
				<section class="sidebar">
					<div class="user-panel">
						<div class="float-start image">
							<img src="#application.applicationUrl##getSubSystem()#/assets/img/#local.avatar#" class="img-circle" alt="User Image">
						</div>
						<div class="float-start info">
							<p>Hello, #rc.currentUser.getFirstName()#</p>
							<a href="##"><i class="fa fa-circle text-success"></i> <span id="idleTimeText">Online</span></a>
						</div>
					</div>
					#view("global/sidebar")#
				</section>
			</aside>
			<div class="content-wrapper">
				<section class="content-header">
					<h1>#rc.title#</h1>
					#view("global/breadcrumb")#
				</section>
				<section class="content">
					<div class="row">
						<div class="col-12">#body#</div>
					</div>
				</section>
			</div>
			<footer class="main-footer">
				<div class="float-end d-none d-sm-block">
					<strong>Version</strong> #local.config.version#
				</div>
				<strong>Copyright &copy; #year(now())# &nbsp; | &nbsp; <a href="#local.config.copyrightUrl#">#local.config.copyrightUrl#</a></strong> &nbsp; <img src="#application.applicationUrl#assets/gfx/panteia-mail-logo.png" class="img-fluid" alt="Logo Panteia"> &nbsp; | &nbsp; All rights reserved
			</footer>
		</div>
		<script src="#application.jQueryVersionCdn#" crossorigin="anonymous"></script>
		<script src="#application.bootstrapJSVersionCdn#" crossorigin="anonymous"></script>
		<script src="#application.applicationUrl##getSubSystem()#/assets/js/app.min.js"></script>
		<script src="#application.applicationUrl#assets/js/global/main.js"></script>
		<cfinclude template="/global/views/main/importViewJS.cfm">
	</body>
</html>
</cfoutput>