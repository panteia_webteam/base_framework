<cfoutput>
#view("global:templates/global/header")#
<main role="main" class="flex-shrink-0 mt-4" id="#getSubsystem()#">
	#body#
</main>
#view("global:templates/global/footer")#
</cfoutput>