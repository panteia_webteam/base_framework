﻿<cfscript>
	local.active = getSection() == "main" && getItem() == "main" ? " active" : "";
	WriteOutput('<ul class="navbar-nav me-auto mb-2 mb-lg-0">');
		WriteOutput('<li class="nav-item"><a href="#buildUrl('#getSubsystem()#:')#" class="nav-link#local.active#">dashboard</a></li>');
		if(structKeyExists(session.auth, "roles") && listFindNoCase(session.auth.roles, "admin")) {
			WriteOutput(view("global:templates/global/adminmenu", { subsystemList : rc.subsystemList }));
		}
		WriteOutput('<li class="nav-item"><a href="#buildUrl('home:security.logout')#" class="nav-link">uitloggen</a></li>');
	WriteOutput('</ul>');
</cfscript>