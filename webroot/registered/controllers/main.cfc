﻿component accessors=true output=false hint="" {

	public any function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
		return this;
	}

	public void function before(required struct rc) output=false hint="" {}

	public void function main(required struct rc) output=false hint="" {}

}