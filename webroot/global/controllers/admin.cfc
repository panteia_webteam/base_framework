component accessors=true output=false hint="" {

	property config;
	property utilityService;

	public any function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
		return this;
	}

	public any function initialSubsystem(required struct rc) output=false hint="Overall function for all the admin-controllers (from the Application.cfc - setupRequest())" {
		var subsystemList = utilityService.checkSubSystems(arrExclude=config.arrayExcluded);
		rc.subsystemList = subsystemList.sort("text", "asc");
		rc.currentLocale = rc.currentUser.getLocale();
		session.doXSSProtect = true;
	}

}