﻿component accessors=true output=false hint="" {

	property cfUtilService;
	property config;
	property logService;
	property resourceBundleService;
	property userService;
	property utilityService;

	public void function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
	}

	public void function before(required struct rc) output=false hint="" {
		// prevent XSS attacks
		rc = utilityService.XSSProtect(arguments.rc);
	}

	public void function checkAuthorisation(required struct rc) output=false hint="" {
		param name="session.locale" default="nl_NL";
		param name="session.debug" default=false;
		// get global resourcebundle
		var globalRB = resourceBundleService.getResourceBundle("global", session.locale, session.debug);
		var isLoggedIn = structKeyExists(session, "auth") && session.auth.isLoggedIn ? true : false;
		// login failed
		if (! isLoggedIn && ! arrayContains(config.publicSubSystems, variables.fw.getSubSystem())) {
			rc.message = { class : "danger", text : globalRB.accessDeniedMessage };
			logService.log(rc, 1);
			variables.fw.redirect("home:login", "message");
		} else {
			// logged in
			rc.currentUser = setCurrentUserInRC(rc);
			// check credentials for user through the toolbox application (only panteia users)
			var accessByCredentials = checkForCredentials(rc);
			var access = false;
			// determine access
			if (rc.currentUser.hasRole()) {
				// build list of current action
				var lstCurrent = variables.fw.getSubsystem() & ":"
					& "," & variables.fw.getSubsystem() & ":" & variables.fw.getSection()
					& "," & variables.fw.getSubsystem() & ":" & variables.fw.getSection() & "."
					& "," & variables.fw.getSection() & "."
					& ",." & variables.fw.getItem()
					& "," & variables.fw.getSectionAndItem()
					& "," & variables.fw.getFullyQualifiedAction();
				// whitelist
				for (var roleObj in rc.currentUser.getRoles()) {
					for(var i=1; i <= listLen(roleObj.getWhiteList()); i++) {
						if (listFindNoCase(lstCurrent, listGetAt(roleObj.getWhiteList(), i)) || listGetAt(roleObj.getWhiteList(), i) == "*" || ListFindNoCase("home,global", variables.fw.getSubsystem())) {
							access = true;
						}
					}
				}
				// blacklist, seperate loop on roles and each blacklist to make sure that blacklists always win from the whitelist
				for (var roleObj in rc.currentUser.getRoles()) {
					for(var i=1; i <= listLen(roleObj.getBlackList()); i++) {
						if (listFindNoCase(lstCurrent, listGetAt(roleObj.getBlackList(), i)) || listGetAt(roleObj.getBlackList(), i) == "*" || ListFindNoCase("home,global", variables.fw.getSubsystem())) {
							access = false;
						}
					}
				}
			}
			// if access denied and requested subsystem is not a public subsystem, log and throw back to main subsystem and page
			if ((! access || ! accessByCredentials) && arrayContains(config.publicSubSystems, variables.fw.getSubSystem()) == 0) {
				rc.message = { class : "danger", text : globalRB.accessDeniedMessage };
				logService.log(rc, 1);
				variables.fw.redirect("global:main.accessDenied", "message");
			}			
			if (structKeyExists(session, "passwordPolicy")) {
				structDelete(session, "passwordPolicy");
			}
			if (isLoggedIn && variables.fw.getItem() != "renewPassword") {
				session.passwordPolicy = runPasswordPolicyCheck(rc); // policy is set within the toolbox > webApps
			}
		}
	}

	public void function logout(struct rc) output=false hint="" {
		// set the default session and CSRF-token
		utilityService.defaultSession();
		utilityService.generateCSRFToken();
		// log logoff
		if(structKeyExists(rc, "outofTime")) {
			var log = { message : { class : "notice", text : "Session expired" }, action : "global:security.logout" };
		} else {
			var log = { message : { class : "notice", text : "Logout" }, action : "global:security.logout" };
		}
		logService.log(log);
		// redirect to the homepage
		variables.fw.redirect("home:main");
	}

	private model.beans.default.user function setCurrentUserInRC(struct rc) output=false hint="" {
		// mimic user
		if (
			(structKeyExists(session, "auth") && structKeyExists(session.auth, "mimic"))
			&& variables.fw.getSubSystem() != "admin"
			&& session.auth.mimic != session.auth.userId) {
			return userService.getObj(session.auth.mimic);
		} else if (structKeyExists(session, "auth") && structKeyExists(session.auth, "mimic")) {
			structDelete(session.auth, "mimic");
		}
		// return the logged in user
		if (structKeyExists(session, "auth")
			&& structKeyExists(session.auth, "isLoggedIn")
			&& session.auth.isLoggedIn
			&& request.base != "/services/") {
			return userService.getObj(session.auth.userId);
		}
		// return an empty user without access rights
		return entityNew("user");
	}

	private void function setUserSession(any userobj) output=false hint="vullen van sessie object obv gebruikersgegevens, tegelijkertijd de sessie een nieuwe unieke ID geven (werkt alleen voor application sessies (CFID), werkt niet bij JEE sessies), zodat session hijacking of session fixation niet mogelijk is" {
		if (! isNull(arguments.userobj.getId())) {
			session.locale = isNull(arguments.userObj.getLocale()) || ! len(arguments.userObj.getLocale()) ? config.defaultLocale : arguments.userObj.getLocale();
			session.debug = 0;
			session.auth = {
				isLoggedIn : true
				, roles : arguments.userObj.getRoleList()
				, userId : arguments.userObj.getId()
			};
			sessionRotate();
		}
	}

	private boolean function checkForCredentials(required struct rc) output=false hint="determine access for all panteia users" {
		var returnBool = true;
		if (session.auth.isLoggedIn && ! isNull(rc.currentUser.getEmail())) {
			var givenEmail = utilityservice.dataDecryptString(rc.currentUser.getEmail());
			if (findNoCase("panteia.nl", givenEmail, 0) > 0) {
				var isActive = 0;
				var userObj = userService.getObj(rc.currentUser.getId());
				var queryString = "
					select top 1 [isActive]
					from [user]
					where [email] = :email
				";
				var qTemp = new Query()
					.setDatasource("web_credentials")
					.setSQL(queryString)
					.addParam(name="email", value=givenEmail, sqltype="cf_sql_varchar")
					.execute()
					.getResult();
				if (qTemp.recordCount() == 1) {
					isActive = qTemp.isActive[1];
				}
				if (isActive <= 0) {
					userObj.setIsActive(isActive);
					userService.save(userObj);
					returnBool = false;
				}
			}
		}
		return returnBool;
	}

	private any function runPasswordPolicyCheck(struct rc) output=false hint="password policy for all users" {
		var mustRenew = passwordpolicyService.activeUser(rc.currentUser.getId());
		var alertTxt = { 
			"nl_NL": { "nodaysLeft": "Laatste kans. Wachtwoord dient te worden vernieuwd.", "expired": "Wachtwoord is verlopen." }
			, "en_Us": { "nodaysLeft": "Last chance. Password needs to be renewed.", "expired": "Password has expired." }
		};
		var message = "";
		if (mustRenew.redirect) {
			var userLanguage = rc.currentUser.getLocale();
			var alertClass = "warning";
			var message = alertTxt[rc.currentUser.getLocale()].nodaysLeft;
			if (mustRenew.daysLeftToResetPassword < 0) {
				alertClass = "danger";
				message = alertTxt[rc.currentUser.getLocale()].expired;
				defaultSession();
			}
			rc.userId = rc.currentUser.getId();
			rc.message = { class: alertClass, text: message };
			logService.log(rc, 1);
			variables.fw.redirect("home:login.renewPassword", "userId, message");
		}
 		return mustRenew;
	}

}