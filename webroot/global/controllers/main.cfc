component accessors=true output=false hint="" {

	property config;
	property groupService;
	property utilityService;

	public any function init(required struct fw) output=false hint="" {
		variables.fw = arguments.fw;
		return this;
	}

	public any function initialSubsystem(required struct rc) output=false hint="Overall function for all the controllers (from the Application.cfc - setupRequest())" {
		var subSystem = true;
		var subsystemList = utilityService.checkSubSystems(arrExclude=config.arrayExcluded);
		rc.subsystemList = subsystemList.sort("text", "asc");
		rc.groupObj = subSystem ? rc.currentUser.getHierarchicalGroup() : "";
		rc.horizontalGroups = subSystem ? rc.currentUser.getHorizontalGroups() : [];
		rc.hierarchicalGroups = subSystem ? hierarchicalgroupArray(rc) : [];
		rc.mimiced = isMimic();
		rc.currentLocale = rc.currentUser.getLocale();
		session.doXSSProtect = true;
	}

	private array function hierarchicalgroupArray(required struct rc) output=false hint="" {
		var hierarchicalGroup = rc.currentUser.getHierarchicalGroup();
		var hierarchicalGroups = [];
		if (! isNull(hierarchicalGroup.getId()) && isValid("guid", hierarchicalGroup.getId())) {
			var hierarchicalRange = rc.currentUser.getHierarchicalGroup().getHierarchicalRange();
			if (hierarchicalRange.recordCount > 0) {
				for (var elem in hierarchicalRange) {
					var groupObj = groupService.getObj(elem.id);
					arrayAppend(hierarchicalGroups, { "groupobj" : groupObj , "level" : elem.level });
				}
			}
		}
		return hierarchicalGroups;
	}

	private string function isMimic() output=false hint="" {
		var mimicStr = "";
		if ((structKeyExists(session, "auth") && structKeyExists(session.auth, "mimic")) && (session.auth.mimic <> session.auth.userId)) {
			var userQry = queryExecute("
				select gender
					, prefix
					, firstname
					, lastname
				from [user]
				where isActive = 1
				and [id] = :usrId
			", { usrId : { value : session.auth.mimic, sqltype : "cf_sql_idstamp" } });
			if(userQry.recordcount > 0) {
				var salutation = userQry.gender == "m" ? "dhr." : userQry.gender == "f" ? "mevr." : "dhr./mevr.";
				var prefix = Trim(userQry.prefix) != "" ? " #userQry.prefix#" : "";
				var mimicStr = salutation & " " & userQry.firstname & prefix & " " & userQry.lastname;
			}
		}
		return mimicStr;
	}

}