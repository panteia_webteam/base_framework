﻿<cfscript>
	local.config = getBeanFactory().getBean("config");
	local.assetsService = getBeanFactory().getBean("assetsService");
	rc = assetsService.addAssetsToRC(rc, session.locale);
	param name="rc.title" default="";
	param name="rc.topmenu" default="menu/topmenu";
	local.metaDescription = structKeyExists(rc, "metaDescription") && len(rc.metaDescription) ? ", " & rc.metaDescription : "";
	local.description = local.config.displayName & local.metaDescription;
	local.keywords = structKeyExists(rc, "metaKeywords") && len(rc.metaKeywords) ? rc.metaKeywords : "";
	local.author = "Panteia";
	local.title = local.config.displayName;
	if (! isNull(rc.currentUser.getLocale()) && rc.currentUser.getLocale() != session.locale) {
		session.locale = rc.currentUser.getLocale();
	}
	local.userLoggedin = structKeyExists(session.auth, "isLoggedIn") && session.auth.isLoggedIn ? true : false;
	local.bodyClass = local.userLoggedin ? "d-flex flex-column" : "text-center";
	rc.cspHeaderStyle &= " #application.bootstrapCSSVersionCdn# #application.fontAwesomeVersionCdn#";
	rc.cspHeaderScript &= " #application.bootstrapJSVersionCdn# #application.jQueryVersionCdn#";
	include template="/global/views/main/importCSP.cfm";
</cfscript>
<cfoutput>
<!DOCTYPE html>
<html lang="#ListFirst(session.locale, '_')#">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="#local.description#">
		<meta name="keywords" content="#local.keywords#">
		<meta name="author" content="#local.author#">
		<title>#local.title#</title>
		<link href="#application.applicationUrl#assets/gfx/favicon/favicon.ico" type="image/x-icon" rel="icon">
		<link href="#application.bootstrapCSSVersionCdn#" type="text/css" rel="stylesheet" media="all" crossorigin="anonymous">
		<link href="#application.fontAwesomeVersionCdn#" type="text/css" rel="stylesheet" media="all" crossorigin="anonymous">
		<cfif local.userLoggedin>
			<link href="#application.applicationUrl#assets/css/global/main.css" type="text/css" rel="stylesheet" media="all">
			<link href="#application.applicationUrl#assets/css/global/print.css" type="text/css" rel="stylesheet" media="print">
		</cfif>
		<cfinclude template="/global/views/main/importViewCSS.cfm">
	</head>
	<body class="#local.bodyClass#" id="body">
		#body#
		<script src="#application.jQueryVersionCdn#" crossorigin="anonymous"></script>
		<script src="#application.bootstrapJSVersionCdn#" crossorigin="anonymous"></script>
		<script src="#application.applicationUrl#assets/js/global/main.js"></script>
		<cfinclude template="/global/views/main/importViewJS.cfm">
	</body>
</html>
</cfoutput>