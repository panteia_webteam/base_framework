<cfscript>
	if(ArrayLen(rc.arrJSExternal)) {
		loop array="#rc.arrJSExternal#" index="j" {
			rc.cspHeaderScript &= " #j#";
		}
	}
	if(ArrayLen(rc.arrCSSExternal)) {
		loop array="#rc.arrCSSExternal#" index="c" {
			rc.cspHeaderStyle &= " #c#";
		}
	}
	/* LET OP! De ; is belangrijk, want geeft aan IIS aan dat het directief ten einde is gekomen en het volgende volgt */
	header name="Content-Security-Policy" value="base-uri 'none'; default-src 'self'; #rc.cspHeaderConnect#; #rc.cspHeaderFont#; #rc.cspHeaderForm#; #rc.cspHeaderFrame#; #rc.cspHeaderImg#; #rc.cspHeaderObject#; #rc.cspHeaderScript#; #rc.cspHeaderStyle#;";
</cfscript>