<cfscript>
	rc.title = rc.textConstants.accessDeniedTitle;
	arrayAppend(rc.arrCustomCSS, application.globalPagesCss);
</cfscript>
<cfoutput>
<div class="globalPages">
	<h1>#rc.title#</h1>
	<cfif structKeyExists(rc, "message") and structKeyExists(rc.message, "panteiaUser") and rc.message.panteiaUser>
		#view("global:main.message")#
	<cfelse>
		#rc.cs.showContent(getFullyQualifiedAction(), session.locale)#
	</cfif>
</div>
</cfoutput>