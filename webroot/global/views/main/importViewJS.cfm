<cfoutput>
<!--- Load javascript plugin calls from the views (local and/or external JS-libs) --->
<cfif structKeyExists(rc, "arrJS") and not rc.arrJS.isEmpty()>
	<cfset rc.arrJS = createObject("java", "java.util.LinkedHashSet").init(rc.arrJS).toArray()>
	<cfloop array="#rc.arrJS#" index="local.item">
		<script src="#application.applicationUrl#assets/#local.item#?v=#rc.version#"></script>
	</cfloop>
</cfif>
<cfif structKeyExists(rc, "arrJSExternal") and not rc.arrJSExternal.isEmpty()>
	<cfset rc.arrJSExternal = createObject("java", "java.util.LinkedHashSet").init(rc.arrJSExternal).toArray()>
	<cfloop array="#rc.arrJSExternal#" index="local.item">
		<script src="#local.item#?v=#rc.version#"></script>
	</cfloop>
</cfif>
<!--- Load javascript custom script from the views --->
<cfif structKeyExists(rc, "arrCustomJS") and not rc.arrCustomJs.isEmpty()>
	<cfparam name="rc.inlineScriptNonce" default="#CreateGUID()#">
	<script nonce="#rc.inlineScriptNonce#">
		<cfloop array="#rc.arrCustomJS#" index="local.item">
			#local.item#
		</cfloop>
	</script>
</cfif>
</cfoutput>