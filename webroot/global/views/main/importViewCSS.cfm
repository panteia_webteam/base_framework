<cfoutput>
<!--- include CSS files --->
<cfif structKeyExists(rc, "arrCSS") and not rc.arrCSS.isEmpty()>
	<cfset rc.arrCSS = createObject("java", "java.util.LinkedHashSet").init(rc.arrCSS).toArray()>
	<cfloop array="#rc.arrCSS#" index="local.item">
		<link href="#application.applicationUrl#assets/#local.item#?v=#rc.version#" type="text/css" rel="stylesheet" media="all">
	</cfloop>
</cfif>
<cfif structKeyExists(rc, "arrCSSExternal") and not rc.arrCSSExternal.isEmpty()>
	<cfset rc.arrCSSExternal = createObject("java", "java.util.LinkedHashSet").init(rc.arrCSSExternal).toArray()>
	<cfloop array="#rc.arrCSSExternal#" index="local.item">
		<link href="#local.item#?v=#rc.version#" type="text/css" rel="stylesheet" media="all">
	</cfloop>
</cfif>
<!--- include custom view CSS files --->
<cfif structKeyExists(rc, "arrCustomCSS") and not rc.arrCustomCSS.isEmpty()>
	<cfset local.customCss = "">
	<cfloop array="#rc.arrCustomCSS#" index="local.item">
		<cfset local.customCss = local.customCss & " " & local.item>
	</cfloop>
	<style type="text/css" media="all">
		#local.customCss#
	</style>
</cfif>
</cfoutput>