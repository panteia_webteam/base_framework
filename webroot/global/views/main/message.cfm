<cfif structKeyExists(rc, "message") and (structKeyExists(rc.message, "class") and structKeyExists(rc.message, "text"))>
	<cfoutput>
	<div class="alert alert-#rc.message.class# alert-dismissible fade show rounded-0 mt-3 mb-3" role="alert">
		#rc.message.text#
		<button type="button" class="btn-close float-end" data-bs-dismiss="alert" aria-label="Close"></button>
	</div>
	</cfoutput>
</cfif>