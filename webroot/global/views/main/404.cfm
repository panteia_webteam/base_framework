﻿<cfscript>
	rc.title = rc.textConstants.404;
	arrayAppend(rc.arrCustomCSS, application.globalPagesCss);
</cfscript>
<cfoutput>
<div class="globalPages">
	<h1>#rc.title#</h1>
	<section>#rc.cs.showContent("global:main.404", session.locale)#</section>
</div>
</cfoutput>