<cfif structKeyExists(rc, "message")>
	<cfset local.msgStruct = {
		danger : { icon : "ban" }
		, info : { icon : "info" }
		, success : { icon : "check" }
		, warning : { icon : "warning" }
	}>
	<cfset local.currentMessage = local.msgStruct[rc.message.class]>
	<cfoutput>	
		<div class="alert alert-#rc.message.class# alert-dismissable fade show mt-3 mb-3" role="alert">
			<i class="fa fa-#local.currentMessage.icon# pe-1"></i>
			#rc.message.text#
			<button type="button" class="btn-close float-end" data-bs-dismiss="alert" aria-label="Close"></button>
		</div>
	</cfoutput>
</cfif>