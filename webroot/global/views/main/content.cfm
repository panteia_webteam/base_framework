<cfscript>
	rc.title = rc.contentObj.getTitle();
	rc.bodyContent = rc.contentObj.getContent();
</cfscript>
<cfoutput>
<div class="container h-100">
	<div class="row">
		<div class="col-12">
			<h1>#rc.title#</h1>
			<section>#rc.bodyContent#</section>
		</div>
	</div>
</div>
</cfoutput>