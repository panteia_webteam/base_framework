﻿<cfscript>
	request.layout = false;
	variables.errorMessage = "Onbekend";
	if(structKeyExists(request, "failedAction")) {
		variables.errorMessage = EncodeForHTML(request.failedAction);
	}
	WriteOutput('
		<link href="/assets/gfx/favicon/favicon.ico" type="image/x-icon" rel="icon">
		<link href="#application.bootstrapCSSVersionCdn#" type="text/css" rel="stylesheet" media="all" crossorigin="anonymous">
		<div class="container mt-5">
			<div class="row">
				<div class="col-12">
					<div class="alert alert-danger">
						<h1>FOUT!</h1>
						<p>Er is een fout opgetreden. Onze excuses voor het ongemak.</p>
						<h2>Foutmelding</h2>
						<p>#variables.errorMessage#</p>
					</div>
	');
	if(structKeyExists(request, "exception") && structKeyExists(request.exception, "cause")) {
		WriteOutput('
			<div class="alert alert-danger">
				<dl>
					<dt>Error</dt>
						<dd>#request.exception.cause.message#</dd>
					<dt>Type</dt>
						<dd>#request.exception.cause.type#</dd>
					<dt>Details</dt>
						<dd>#request.exception.cause.detail#</dd>
				</dl>
			</div>
		');
	}
	WriteOutput('</div></div></div>');
	if(application.development
		|| (
			structKeyExists(rc, "currentUser")
			&& ! isNull(rc.currentUser.getId())
			&& (
				listFindNoCase(rc.currentuser.getRoleList(), "admin")
				|| listFindNoCase(session.auth.roles, "admin")
			)
		)
		|| (
			structKeyExists(rc, "debug") && rc.debug == "appeltaart"
		)) {
		getPageContext().getResponse().setStatus(500);
		if(structKeyExists(request, "exception")) {
			WriteDump(var=request.exception, label="exception", top=5);
		}
	}
</cfscript>