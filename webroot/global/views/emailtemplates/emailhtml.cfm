﻿<cfscript>
	/* DEFAULT */
		local.bodyHtmlColor = "##fff"/* wit: rgb(255,255,255)  */
		local.bodyRgbColor = "rgb(255,255,255)"/* wit: html #fff */
	/* OVERIG */
		local.headercolor = "rgb(244,124,34)";
		local.hrefcolor = "rgb(73,148,206)";
		local.white = "##fff";
		local.whiteGrey = "##f8f8f8";
		local.greyDark = "##B0B0B0";
		local.grey = "##ccc";
		local.greyLight = "##F0F0F0";
</cfscript><cfoutput>
<!DOCTYPE html>
<html lang="nl">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="format-detection" content="telephone=no"><!--- disable auto telephone linking in iOS --->
<title></title>
<style type="text/css">
	/* RESET STYLES */
	html{background-color:#local.bodyHtmlColor#;margin:0;padding:0;}
	body, ##bodyTable, ##bodyCell, ##bodyCell{height:100% !important;margin:0;padding:0;width:100% !important;font-family:Helvetica,Arial,"Lucida Grande",sans-serif;}
	table{border-collapse:collapse;}
	table[id=bodyTable] {width:100%!important;margin:auto;max-width:600px!important;color:##7A7A7A;font-weight:normal;}
	img, a img{ border:0;outline:none;text-decoration:none;height:auto;line-height:100%;}
	a, a:focus, a:hover, a:active{text-decoration:underline;color:#local.hrefcolor#!important;}
	h1, h2, h3, h4, h5, h6{ color:#local.headercolor#;font-weight:normal;font-family:Helvetica;font-size:20px;line-height:125%;text-align:left;letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;}
	/* CLIENT-SPECIFIC STYLES */
	.ReadMsgBody{width:100%;} .ExternalClass{width:100%;}/* Force Hotmail/Outlook.com to display emails at full width. */
	.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;}/* Force Hotmail/Outlook.com to display line heights normally. */
	table, td{mso-table-lspace:0;mso-table-rspace:0;}/* Remove spacing between tables in Outlook 2007 and up. */
	##outlook a{padding:0;}/* Force Outlook 2007 and up to provide a "view in browser" message. */
	img{-ms-interpolation-mode:bicubic;display:block;outline:none;text-decoration:none;}/* Force IE to smoothly render resized images. */
	body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;font-weight:normal!important;}/* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */
	.ExternalClass td[class="ecxflexibleContainerBox"] h3 {padding-top:10px !important;}/* Force hotmail to push 2-grid sub headers down */
	/* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */
	/* ========== Page Styles ========== */
	h1{display:block;font-size:26px;font-style:normal;font-weight:normal;line-height:100%;}
	h2{display:block;font-size:20px;font-style:normal;font-weight:normal;line-height:120%;}
	h3{display:block;font-size:20px;font-style:normal;font-weight:normal;line-height:110%;}
	h4{display:block;font-size:17px;font-style:italic;font-weight:normal;line-height:100%;}
	.flexibleImage{height:auto;}
	.linkRemoveBorder{border-bottom:0 !important;}
	table[class=flexibleContainerCellDivider] {padding-bottom:0 !important;padding-top:0 !important;}
	body, ##bodyTable{background-color:#local.bodyHtmlColor#;}
	##emailHeader{background-color:#local.bodyHtmlColor#;}
	##emailBody{background-color:#local.white#;}
	##emailFooter{background-color:#local.white#;}
	.nestedContainer{background-color:#local.whiteGrey#;border:1px solid #local.grey#;}
	.emailButton{background-color:##205478;border-collapse:separate;}
	.buttonContent{color:#local.white#;font-family:Helvetica;font-size:18px;font-weight:bold;line-height:100%;padding:15px;text-align:center;}
	.buttonContent a{color:#local.white#;display:block;text-decoration:none!important;border:0!important;}
	.emailCalendar{background-color:#local.white#;border:1px solid #local.grey#;}
	.emailCalendarMonth{background-color:##205478;color:#local.white#;font-family:Helvetica, Arial, sans-serif;font-size:16px;font-weight:bold;padding-top:10px;padding-bottom:10px;text-align:center;}
	.emailCalendarDay{color:##205478;font-family:Helvetica, Arial, sans-serif;font-size:60px;font-weight:bold;line-height:100%;padding-top:20px;padding-bottom:20px;text-align:center;}
	.imageContentText {margin-top:10px;line-height:0;}
	.imageContentText a {line-height:0;}
	##invisibleIntroduction {display:none !important;}/* Removing the introduction text from the view */
	/*FRAMEWORK HACKS & OVERRIDES */
	span[class=ios-color-hack] a {color:##275100!important;text-decoration:none!important;}/* Remove all link colors in IOS (below are duplicates based on the color preference) */
	span[class=ios-color-hack2] a {color:##205478!important;text-decoration:none!important;}
	span[class=ios-color-hack3] a {color:##8B8B8B!important;text-decoration:none!important;}
	/* A nice and clean way to target phone numbers you want clickable and avoid a mobile phone from linking other numbers that look like, but are not phone numbers. Use these two blocks of code to "unstyle" any numbers that may be linked. The second block gives you a class to apply with a span tag to the numbers you would like linked and styled. Inspired by Campaign Monitor's article on using phone numbers in email:https://www.campaignmonitor.com/blog/post/3571/using-phone-numbers-in-html-email/. */
	.a[href^="tel"], a[href^="sms"] {text-decoration:none!important;color:##606060!important;pointer-events:none!important;cursor:default!important;}
	.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration:none!important;color:##606060!important;pointer-events:auto!important;cursor:default!important;}
	.button{text-decoration:none !important;text-align:center;color:#local.white#;}
	.button:focus, .button:hover{background-color:rgb(36,119,188);color:#local.white#;}
	.button:active{background-color:rgb(36,119,188);color:#local.white#;}
	.buttonexit{text-decoration:none !important;text-align:center;color:#local.white#;}
	.buttonexit:focus, .buttonexit:hover{background-color:#local.greyLight#;color:#local.greyDark#;}
	.buttonexit:active{background-color:#local.greyLight#;color:#local.greyDark#;}
	/* MOBILE STYLES */
	@media only screen and (max-width:480px){
		/*////// CLIENT-SPECIFIC STYLES //////*/
		body{width:100% !important;min-width:100% !important;}/* Force iOS Mail to render the email at full width. */
		/* FRAMEWORK STYLES */
		/* CSS selectors are written in attribute selector format to prevent Yahoo Mail from rendering media query styles on desktop. */
		table[id="emailHeader"],
		table[id="emailBody"],
		table[id="emailFooter"],
		table[class="flexibleContainer"],
		td[class="flexibleContainerCell"] {width:100% !important;}
		td[class="flexibleContainerBox"], td[class="flexibleContainerBox"] table {display:block;width:100%;text-align:left;}
		/* The following style rule makes any image classed with 'flexibleImage' fluid when the query activates. Make sure you add an inline max-width to those images to prevent them from blowing out. */
		td[class="imageContent"] img {height:auto !important;width:100% !important;max-width:100% !important;}
		img[class="flexibleImage"]{height:auto !important;width:100% !important;max-width:100% !important;}
		img[class="flexibleImageSmall"]{height:auto !important;width:auto !important;}
		/* Create top space for every second element in a block */
		table[class="flexibleContainerBoxNext"]{padding-top:10px !important;}
		/* Make buttons in the email span the full width of their container, allowing for left- or right-handed ease of use. */
		table[class="emailButton"]{width:100% !important;}
		td[class="buttonContent"]{padding:0 !important;}
		td[class="buttonContent"] a{padding:15px !important;}
	}
</style>
<!--- Outlook Conditional CSS: These two style blocks target Outlook 2007 & 2010 specifically, forcing columns into a single vertical stack as on mobile clients. This is primarily done to avoid the 'page break bug' and is optional. More information here: https://templates.mailchimp.com/development/css/outlook-conditional-css --->
<!--[if mso 12]>
<style type="text/css">
	.flexibleContainer{display:block !important;width:100% !important;}
</style>
<![endif]-->
<!--[if mso 14]>
<style type="text/css">
	.flexibleContainer{display:block !important;width:100% !important;}
</style>
<![endif]-->
</head>
<body bgcolor="#local.bodyHtmlColor#" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<center style="background-color:#local.bodyRgbColor#;">
	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout:fixed;max-width:100% !important;width:100% !important;min-width:100% !important;">
		<tr>
			<td align="center" valign="top" id="bodyCell">
				<!--- LOGO --->
				<table bgcolor="#local.bodyHtmlColor#" border="0" cellpadding="0" cellspacing="0" width="600" id="emailHeader">
					<tr>
						<td align="center" valign="top">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td align="center" valign="top">
										<table border="0" cellpadding="10" cellspacing="0" width="600" class="flexibleContainer">
											<tr>
												<td valign="top" width="600" class="flexibleContainerCell">
													<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td align="left" valign="middle" class="flexibleContainerBox">
																<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																	<tr>
																		<td align="right" class="textContent">
																			<img src="#application.applicationUrl#assets/gfx/panteia-mail-logo.png" align="right" title="logo Panteia" style="float:right;clear:both;">
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!--- BODY --->
				<table bgcolor="#local.bodyHtmlColor#"  border="0" cellpadding="0" cellspacing="0" width="600" id="emailBody">
					<!--- ACTUAL BODY TEXT --->
					<tr>
						<td align="center" valign="top" style="padding:15px 0 0;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td align="center" valign="top">
										<table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
											<tr>
												<td valign="top" width="600" class="flexibleContainerCell">
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td valign="top" class="textContent" style="border:1px solid #local.bordercolor#;padding:30px 30px 20px;">
																<h3 style="color:#local.headercolor#;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:22px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">{TITLE}</h3>
																<div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:14px;margin-bottom:0;color:#local.bodycolor#;line-height:135%;">
																	{BODY}
																</div>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<!--- FOOTER --->
					<tr>
						<td align="center" valign="top">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td align="center" valign="top" style="padding:15px 0 0;">
										<table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
											<tr>
												<td align="center" valign="top" width="600" class="flexibleContainerCell">
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td align="center" valign="top" style="border:1px solid #local.bordercolor#;padding:30px 30px 15px;">
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td align="center" valign="top" class="textContent">
																			<h3 style="color:#local.headercolor#;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:22px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Meer informatie</h3>
																			<div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:14px;margin-bottom:0;line-height:135%;">
																				<p><a href="{URL}" style="color:#local.hrefcolor#;">{URL}</a> |
																				<a href="tel:+31793222997" style="color:#local.hrefcolor#;">079 322 29 97</a> |
																				<a href="mailto:info@panteia.nl" style="color:#local.hrefcolor#;">info@panteia.nl</a></p>
																			</div>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<!--- DISCLAIMER --->
					<tr>
						<td align="center" valign="top">
							<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="##f8f8f8">
								<tr>
									<td align="center" valign="top">
										<table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
											<tr>
												<td align="center" valign="top" width="600" class="flexibleContainerCell">
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td align="center" valign="top" style="background-color:#local.backgroundcolor#;border:1px solid #local.bordercolorbottom#;border-top:none;padding:15px;">
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td align="center" valign="top" class="textContent">
																			<div style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:11px;margin-bottom:0;color:##767676;line-height:135%;">
																				Dit bericht is alleen bestemd voor de geadresseerde. Aan dit bericht kunnen geen rechten worden ontleend.
																			</div>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</center>
</body>
</html></cfoutput>