<cfscript>
	param name="local.parentClass" default="mb-3";
	param name="local.label" default="";
	param name="local.labelClass" default="";
	param name="local.labelCol" default="3";
	param name="local.type" default="text";
	param name="local.name" default="";
	param name="local.value" default="";
	param name="local.id" default="";
	param name="local.placeholder" default="#local.name#";
	param name="local.inputClass" default="";
	param name="local.inputRequired" default="false";
	param name="local.fieldInLine" default="false";
	param name="local.lastItem" default="false";
	local.inputCol = 12-local.labelCol;
</cfscript>
<cfoutput>
<cfif local.fieldInLine>
	<div class="form-group row #(local.lastItem ? 'lastItem' : '')#">
		<cfif len(trim(local.label))>
			<label class="col-#local.labelCol# #esapiEncode('html_attr', local.labelClass)#">
				#esapiEncode('html', local.label)#
			</label>
		</cfif>
		<div class="col-#local.inputCol#">
			<input type="#esapiEncode('html_attr', local.type)#" name="#esapiEncode('html_attr', local.name)#" value="#esapiEncode('html_attr', local.value)#"<cfif local.type NEQ "hidden"> class="form-control #esapiEncode('html_attr', local.inputClass)#" placeholder="#esapiEncode('html_attr', local.placeholder)#"<cfif len(trim(local.id))> id="#esapiEncode('html_attr', local.id)#"</cfif><cfif local.inputRequired> required</cfif></cfif>>
		</div>
	</div>
<cfelse>
	<cfif local.type NEQ "hidden">
		<div class="#esapiEncode('html', local.parentClass)#">
			<cfif len(trim(local.label))>
				<label class="#esapiEncode('html_attr', local.labelClass)#">
					#esapiEncode('html', local.label)#
				</label>
			</cfif>
	</cfif>
	<input type="#esapiEncode('html_attr', local.type)#" name="#esapiEncode('html_attr', local.name)#" value="#esapiEncode('html_attr', local.value)#"<cfif local.type NEQ "hidden"> class="form-control #esapiEncode('html_attr', local.inputClass)# #(local.lastItem ? 'lastItem' : '')#" <cfif len(trim(local.id))> id="#esapiEncode('html_attr', local.id)#"</cfif><cfif local.inputRequired> required</cfif> placeholder="#esapiEncode('html_attr', local.placeholder)#"</cfif>>
	<cfif local.type NEQ "hidden">
		</div>
	</cfif>
</cfif>
</cfoutput>