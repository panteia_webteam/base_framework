<cfscript>
	param name="local.label" default="";
	param name="local.labelClass" default="";
	param name="local.textareaParentClass" default="";
	param name="local.textareaClass" default="";
	param name="local.name" default="";
	param name="local.value" default="";
	param name="local.id" default="";
	param name="local.placeholder" default="#local.name#";
	param name="local.ckEditorStruct" default="false";
	param name="local.ckeditorHeight" default=200;
	param name="local.textEditorLanguage" default="en";
</cfscript>
<cfoutput>
<cfif len(local.label)>
	<div class="#esapiEncode('html_attr', local.labelClass)#">#esapiEncode('html', local.label)#</div>
</cfif>
<cfif len(local.textareaParentClass)>
	<div class="#esapiEncode('html_attr', local.textareaParentClass)#">
</cfif>
<textarea class="form-control #esapiEncode('html_attr', local.textareaClass)#" name="#esapiEncode('html_attr', local.name)#" placeholder="#esapiEncode('html_attr', local.placeholder)#" id="#esapiEncode('html_attr', local.id)#">#esapiEncode('html', local.value)#</textarea>
<cfif len(local.textareaParentClass)>
	</div>
</cfif>
<cfif local.ckEditorStruct>
	<cfscript>
		// templatefile for loading some variables for the ck-editor.
		arrayAppend(rc.arrCustomJS, "
			var ckEditorStruct = {
				height : #local.ckeditorHeight#
				, language : '#local.textEditorLanguage#'
			};
			CKEDITOR.replace('#esapiEncode('html', local.id)#', ckEditorStruct);
		");
	</cfscript>
</cfif>
</cfoutput>