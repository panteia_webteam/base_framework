<cfif thisTag.executionMode is "start">
	<!--- gebruik <cfimport prefix="tag" taglib="/assets/tags">
		op de aanroepende pagina.
		Roep een veld aan met:
		<tag:FormField
			type="text"
			name="keuze"
			value=""
			placeholder="Keuze"
			class="span4" />

		<tag:FormField
			type="radio"
			name="keuze"
			valueOptions=#[{label='Ja',value=1},{label='Nee',value=0}]#
			value=1
			class="span4" />

		of

		<tag:FormField
			type="radio"
			name="keuze"
			valueOptions=#['Ja','Nee']#
			value=1
			class="span4" />

		etc. Zie hieronder voor de attributes
	 --->
	<!--- Algemeen --->
	<cfparam name="attributes.type" type="string" default="text">
	<cfparam name="attributes.name" type="string" default="inp_#listFirst(createUUID(),'-')#">
	<cfparam name="attributes.value" type="any" default="">
	<cfparam name="attributes.valueOptions" type="array" default="#arrayNew(1)#"><!--- select, checkbox, radio --->
	<cfparam name="attributes.optionLabel" type="text" default="label">
	<cfparam name="attributes.optionValue" type="text" default="value">
	<cfparam name="attributes.indexAsValue" type="numeric" default="0">
	<!--- Styling en van die dingen --->
	<cfparam name="attributes.class" type="string" default="">
	<cfparam name="attributes.label" type="string" default="">
	<cfparam name="attributes.fieldAttributes" type="string" default="">
	<cfparam name="attributes.placeholder" type="string" default="#attributes.label#">
	<cfparam name="attributes.id" type="string" default="inp_#listFirst(createUUID(),'-')#">
	<cfparam name="attributes.language" type="string" default="nl">
	<cfparam name="attributes.datemask" type="string" default="YYYY/MM/DD">
	<cfparam name="attributes.timemask" type="string" default="HH:mm">
	<cfswitch expression="#attributes.type#">
		<cfcase value="text">
			<cfoutput>
			<input type="text" name="#attributes.name#" value="#htmlEditFormat(attributes.value)#" class="#attributes.class#" placeholder="#attributes.placeholder#" id="#attributes.id#" #attributes.fieldAttributes#>
			</cfoutput>
		</cfcase>
		<cfcase value="date">
			<cfoutput>
			<cfif isDate(attributes.value)>
				<cfset attributes.value = LSDateFormat(attributes.value, attributes.datemask)>
			</cfif>
			<div class="input-group">
				<div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
				<input type="text" name="#attributes.name#" value="#htmlEditFormat(attributes.value)#" class="#attributes.class# date" placeholder="#attributes.placeholder#" id="#attributes.id#" #attributes.fieldAttributes#>
			</div>
			</cfoutput>
		</cfcase>
		<cfcase value="hidden">
			<cfoutput>
			<input type="hidden" name="#attributes.name#" value="#htmlEditFormat(attributes.value)#" class="#attributes.class#" placeholder="#attributes.placeholder#" id="#attributes.id#" #attributes.fieldAttributes#>
			</cfoutput>
		</cfcase>
		<cfcase value="valueonly">
			<cfoutput>
			<input type="hidden" name="#attributes.name#" value="#htmlEditFormat(attributes.value)#" class="#attributes.class#" placeholder="#attributes.placeholder#" id="#attributes.id#" #attributes.fieldAttributes#>#htmlEditFormat(attributes.value)#
			</cfoutput>
		</cfcase>
		<cfcase value="textarea">
			<cfoutput>
			<textarea name="#attributes.name#" class="#attributes.class#" placeholder="#attributes.placeholder#" id="#attributes.id#" #attributes.fieldAttributes#>#htmlEditFormat(attributes.value)#</textarea>
			</cfoutput>
		</cfcase>
		<cfcase value="password">
			<cfoutput>
			<input type="password" name="#attributes.name#" class="#attributes.class#" autocomplete="off" #attributes.fieldAttributes#>
			</cfoutput>
		</cfcase>
		<cfcase value="radio">
			<cfoutput>
			<!--- if attributes.value is not a valid option default to first one, Array find can't find empty value so we need to loop through --->
			<cfset valueExists = false>
			<cfloop from="1" to="#arraylen(attributes.valueOptions)#" index="i">
				<cfif attributes.indexAsValue>
					<cfset thisOptionValue = i>
				<cfelse>
					<cfset thisOptionValue = isSimpleValue(attributes.valueOptions[i]) ? attributes.valueOptions[i] : attributes.valueOptions[i][attributes.optionValue]>
				</cfif>
				<cfif thisOptionValue EQ attributes.value>
					<cfset valueExists = true>
					<cfbreak>
				</cfif>
			</cfloop>
			<cfif NOT valueExists>
				<cfset attributes.value = isSimpleValue(attributes.valueOptions[1]) ? attributes.valueOptions[1] : attributes.valueOptions[1][attributes.optionValue]>
			</cfif>
			<ul class="#attributes.class#">
				<cfloop from="1" to="#arraylen(attributes.valueOptions)#" index="i">
					<cfif attributes.indexAsValue>
						<cfset thisOptionValue = i>
					<cfelse>
						<cfset thisOptionValue = isSimpleValue(attributes.valueOptions[i]) ? attributes.valueOptions[i] : structKeyExists(attributes.valueOptions[i], attributes.optionValue) ? structFind(attributes.valueOptions[i], attributes.optionValue) : ''>
					</cfif>
					<cfset thisOptionName = isSimpleValue(attributes.valueOptions[i]) ? lcase(attributes.valueOptions[i]) : structFind(attributes.valueOptions[i], attributes.optionlabel)>
					<li>
						<input id="#attributes.id#_#i#" type="radio" name="#attributes.name#" value="#thisOptionValue#"<cfif attributes.value EQ thisOptionValue> checked</cfif> #attributes.fieldAttributes#>
						<label for="#attributes.id#_#i#">#thisOptionName#</label>
					</li>
				</cfloop>
			<ul>
			</cfoutput>
		</cfcase>
		<cfcase value="radio_basic">
			<cfoutput>
			<!--- if attributes.value is not a valid option default to first one, Array find can't find empty value so we need to loop through --->
			<cfset valueExists = false>
			<cfloop array="#attributes.valueOptions#" index="option">
				<cfset thisOptionValue = isSimpleValue(option) ? option : option[attributes.optionValue]>
				<cfif thisOptionValue EQ attributes.value>
					<cfset valueExists = true>
					<cfbreak>
				</cfif>
			</cfloop>
			<cfif NOT valueExists>
				<cfset attributes.value = attributes.valueOptions[1][attributes.optionValue]>
			</cfif>
			<cfloop array="#attributes.valueOptions#" index="option">
				<cfset thisOptionValue = isSimpleValue(attributes.valueOptions[i]) ? attributes.valueOptions[i] : structKeyExists(attributes.valueOptions[i], attributes.optionValue) ? structFind(attributes.valueOptions[i], attributes.optionValue) : ''>
				<cfset thisOptionName = isSimpleValue(option) ? option : structFind(option, attributes.optionlabel)>
				<label class="radio"><input type="radio" name="#attributes.name#" value="#thisOptionValue#" class="#attributes.class#"<cfif attributes.value EQ thisOptionValue> checked</cfif> #attributes.fieldAttributes#> #thisOptionName#</label>
			</cfloop>
			</cfoutput>
		</cfcase>
		<cfcase value="checkbox">
			<cfoutput>
			<!--- if attributes.value is not a valid option default to first one, Array find can't find empty value so we need to loop through --->
			<ul class="#attributes.class#">
				<cfloop from="1" to="#arraylen(attributes.valueOptions)#" index="i">
					<cfif attributes.indexAsValue>
						<cfset thisOptionValue = i>
					<cfelse>
						<cfset thisOptionValue = isSimpleValue(attributes.valueOptions[i]) ? attributes.valueOptions[i] : structKeyExists(attributes.valueOptions[i], attributes.optionValue) ? structFind(attributes.valueOptions[i], attributes.optionValue) : ''>
					</cfif>
						<cfset thisOptionName = isSimpleValue(attributes.valueOptions[i]) ? lcase(attributes.valueOptions[i]) : structFind(attributes.valueOptions[i], attributes.optionlabel)>
					<li>
						<input id="#attributes.id#_#i#" type="checkbox" name="#attributes.name#" value="#thisOptionValue#"<cfif listFind(attributes.value,thisOptionValue)> checked</cfif> #attributes.fieldAttributes#>
						<label for="#attributes.id#_#i#">#thisOptionName#</label>
					</li>
				</cfloop>
			<ul>
			</cfoutput>
		</cfcase>
		<cfcase value="checkbox_basic">
			<cfoutput>
			<!--- if attributes.value is not a valid option default to first one, Array find can't find empty value so we need to loop through --->
			<cfloop array="#attributes.valueOptions#" index="option">
				<cfset thisOptionValue = isSimpleValue(option) ? option : structKeyExists(option, attributes.optionValue) ? structFind(option, attributes.optionValue) : ''>
				<cfset thisOptionName = isSimpleValue(option) ? option : structFind(option, attributes.optionlabel)>
				<label class="checkbox"><input type="checkbox" name="#attributes.name#" value="#thisOptionValue#" class="#attributes.class#"<cfif listFind(attributes.value,thisOptionValue)> checked</cfif> #attributes.fieldAttributes#> #thisOptionName#</label>
			</cfloop>
			</cfoutput>
		</cfcase>
		<cfcase value="select">
			<cfoutput>
			<select name="#attributes.name#" class="#attributes.class#" #attributes.fieldAttributes#>
			<cfloop array="#attributes.valueOptions#" index="option">
				<cfset thisOptionValue = isSimpleValue(option) ? option : structKeyExists(option, attributes.optionValue) ? structFind(option, attributes.optionValue) : ''>
				<cfset thisOptionName = isSimpleValue(option) ? option : structFind(option, attributes.optionlabel)>
				<option value="#thisOptionValue#"<cfif attributes.value EQ thisOptionValue> selected</cfif>>#thisOptionName#</option>
			</cfloop>
			</select>
			</cfoutput>
		</cfcase>
		<cfcase value="multiselect">
			<cfoutput>
			<select multiple="true" name="#attributes.name#" class="#attributes.class#" #attributes.fieldAttributes#>
			<cfloop array="#attributes.valueOptions#" index="option">
				<cfset thisOptionValue = isSimpleValue(option) ? option : structKeyExists(option, attributes.optionValue) ? structFind(option, attributes.optionValue) : ''>
				<cfset thisOptionName = isSimpleValue(option) ? option : structFind(option, attributes.optionlabel)>
				<option value="#thisOptionValue#"<cfif listFind(attributes.value,thisOptionValue)> selected</cfif>>#thisOptionName#</option>
			</cfloop>
			</select>
			</cfoutput>
		</cfcase>
		<cfcase value="submit">
			<cfoutput>
			<input type="submit" name="#attributes.name#" value="#htmlEditFormat(attributes.value)#" class="#attributes.class#" #attributes.fieldAttributes#>
			</cfoutput>
		</cfcase>
		<cfdefaultcase>
			<cfoutput>
			<input type="text" name="#attributes.name#" placeholder="#attributes.placeholder#" value="#htmlEditFormat(attributes.value)#" class="#attributes.class#" #attributes.fieldAttributes#>
			</cfoutput>
		</cfdefaultcase>
	</cfswitch>
</cfif>