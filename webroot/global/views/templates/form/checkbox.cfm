<cfscript>
	param name="local.label" default="";
	param name="local.labelClass" default="";
	param name="local.labelCol" default="3";
	param name="local.name" default="";
	param name="local.value" default="";
	param name="local.id" default="checkbox";
	param name="local.placeholder" default="#local.name#";
	param name="local.inputClass" default="";
	param name="local.inputRequired" default="false";
	param name="local.fieldInLine" default="false";
	param name="local.lastItem" default="false";
	param name="local.arrItems" default="#[]#";
	param name="local.firstCharacter" default="false";
	local.inputCol = 12-local.labelCol;
	local.mb = ! local.lastItem ? 'mb-radio' : 'mb-radio-lastItem';
	local.value = ! isArray(local.value) ? listToArray(local.value) : local.value;
</cfscript>
<cfoutput>
<cfif local.fieldInLine>
	<div class="form-group row #local.mb#">
		<cfif len(trim(local.label))>
			<label class="col-#local.labelCol# #esapiEncode('html_attr', local.labelClass)#">#esapiEncode('html', local.label)#</label>
		</cfif>
		<div class="col-#local.inputCol#">
			<ul class="list-inline list-checkbox">
				<cfloop array="#local.arrItems#" item="elem" index="iCount">
					<cfset isChecked = local.value.find(elem.VALUE) ? ' checked' : ''>
					<cfset labelName = local.firstCharacter ? left(elem.NAME, 1) : elem.NAME>
					<li class="list-inline-item">
						<input type="checkbox" value="#elem.VALUE#" id="#esapiEncode('html_attr', local.id)&iCount#" name="#esapiEncode('html_attr', local.name)#"#isChecked#>
						<label for="#esapiEncode('html_attr', local.id)&iCount#" class="select-checkbox mr-2">#labelName#</label>
					</li>
				</cfloop>
			</ul>
		</div>
	</div>
<cfelse>
	<cfif len(trim(local.label))>
		<label class="col-#local.labelCol# #local.mb# #esapiEncode('html_attr', local.labelClass)#">#esapiEncode('html', local.label)#</label>
	</cfif>
	<div class="col-#local.inputCol# #local.mb#">
		<ul class="list-inline list-checkbox">
			<cfloop array="#local.arrItems#" item="elem" index="iCount">
				<cfset isChecked = local.value.find(elem.VALUE) ? ' checked' : ''>
				<cfset labelName = local.firstCharacter ? left(elem.NAME, 1) : elem.NAME>
				<li class="list-inline-item">
					<input type="checkbox" value="#elem.VALUE#" id="#esapiEncode('html_attr', local.id)&iCount#" name="#esapiEncode('html_attr', local.name)#"#isChecked#>
					<label for="#esapiEncode('html_attr', local.id)&iCount#" class="select-checkbox mr-2">#labelName#</label>
				</li>
			</cfloop>
		</ul>
	</div>
</cfif>
</cfoutput>