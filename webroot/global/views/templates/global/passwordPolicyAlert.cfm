<cfscript>
	param name="local.class" default="alert alert-primary";
	param name="local.extraClass" default="";
	param name="local.btnClass" default="";
	local.showPasswordAlert = structKeyExists(session, "passwordPolicy") && session.passwordPolicy.showAlert ? true : false;
	if (local.showPasswordAlert) {
		local.btnId = session.passwordPolicy.alerts.btnId;
	}
</cfscript>
<cfoutput>
<cfif local.showPasswordAlert>
	<div class="#local.class##local.extraClass#">
		#session.passwordPolicy.alerts[session.passwordPolicy.state]#
		<form action="#buildUrl('home:login.renewPassword')#" method="post" role="form">
			<input type="hidden" name="CSRFToken" value="#session.CSRFToken#">
			<input type="hidden" name="userId" value="#rc.currentUser.getId()#">
			<p class="mb-0"><button type="submit" class="page-link">#session.passwordPolicy.alerts.btnTxt#</button></p>
		</form>
	</div>
</cfif>
</cfoutput>