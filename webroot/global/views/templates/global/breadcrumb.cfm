﻿<cfscript>
	param name="local.arrBreadcrumb" default="#[]#";
</cfscript>
<cfoutput>
<ul class="breadcrumb list-inline">
	<cfloop array="#local.arrBreadcrumb#" index="arrItem">
		<li class="list-inline-item">
			<cfif structKeyExists(arritem,"url") and len(arritem.url)>
				<cfset breadActive = getFullyQualifiedAction() eq arrItem.url ? "active" : "">
				<a href="#buildUrl(arritem.url)#" class="#breadActive#">#trim(arritem.title)#</a>
			<cfelse>
				#arritem.title#
			</cfif>
		</li>
	</cfloop>
</ul>
</cfoutput>