<cfscript>
	local.config = getBeanFactory().getBean("config");
	local.panteiaMail = local.config.mail.error;
	local.panteiaTel = local.config.telephonePanteia;
	local.panteiaUrl = local.config.urlPanteia;
	param name="local.sessionTimer" default=true;
</cfscript>
<cfoutput>
<a href="##" class="back-to-top" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-custom-class="custom-popover" data-bs-content="#rc.rb.getRBString(bundle=getSubSystem(), key='bovenkantpagina', locale=session.locale, cleanString=true)#"><i aria-label="">&##65085;</i></a>
<footer class="footer mt-auto py-3">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-4">
				<p>E-mail: <a href="mailto:#local.panteiaMail#">#local.panteiaMail#</a></p>
			</div>
			<div class="col-12 col-sm-4">
				<p>Telefoon: #local.panteiaTel#</p>
			</div>
			<div class="col-12 col-sm-4">
				<p>&copy; 2022 <a href="#local.panteiaUrl#" rel="external">Panteia</a></p>
			</div>
			<cfif local.sessionTimer>
				<div class="col-12 col-sm-4 text-end">
					<p>#view("global:templates/global/countdown", { sessionTimer : local.sessionTimer })#</p>
				</div>
			</cfif>
		</div>
	</div>
</footer>
</cfoutput>