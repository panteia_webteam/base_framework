﻿<cfscript>
	param name="local.subsystemList" default=[];
</cfscript>
<cfoutput>
<cfif NOT arrayIsEmpty(local.subsystemList)>
	<li class="nav-item dropdown">
		<a href="##" class="nav-link dropdown-toggle" id="admindropdown" data-bs-toggle="dropdown" aria-expanded="false">
			<em>admin</em>
		</a>
		<ul class="dropdown-menu" aria-labelledby="admindropdown">
			<li><a href="#buildUrl('admin:')#" class="dropdown-item">admin</a></li>
			<cfloop array="#local.subsystemList#" item="elem">
				<cfif elem neq getSubsystem()>
					<li><a href="#buildUrl(elem&':')#" class="dropdown-item">#elem#</a></li>
				</cfif>
			</cfloop>
		</ul>
	</li>
</cfif>
</cfoutput>