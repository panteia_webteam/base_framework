﻿<cfscript>
	local.countdownLanguage = session.auth.isLoggedIn && ! isNull(rc.currentUser.getId()) ? rc.currentUser.getLocale() : session.locale;
	local.countdownTxt = {
		"nl_NL" : {
			"start" : "je wordt na "
			, "end" : " automatisch uitgelogd"
			, "logout" : "Nu uitloggen"
		}
		, "en_US" : {
			"start" : "you will be logged out automatically after "
			, "end" : ""
			, "logout" : "Log out now"
		} 
	}
	local.heartbeatUrl = buildUrl("global:main.heartBeat");
	local.refreshtime = structKeyExists(application, "sessionTimeout") ? application.sessionTimeout : 0;
	local.logout = buildUrl("global:security.logout");
	arrayAppend(rc.arrCustomJS, "
		let heartbeatUrl='#local.heartbeatUrl#'
			, logoutUrl='#local.logout#'
			, refreshtime=#local.refreshtime#
			, countdownLanguage='#local.countdownLanguage#'
	");
	arrayAppend(rc.arrJSExternal, "/assets/js/global/countdown.js");
</cfscript>
<cfoutput>
<span class="footertxt">#local.countdownTxt[local.countdownLanguage].start#</span>
<span class="footertxt" id="showcountdown"></span>
<span class="footertxt">#local.countdownTxt[local.countdownLanguage].end#. </span> 
<a href="#local.logout#">#local.countdownTxt[local.countdownLanguage].logout#</a>
</cfoutput>