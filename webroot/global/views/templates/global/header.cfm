﻿<cfscript>
	param name="local.topmenu" default="menu/topmenu";
	arrayAppend(rc.arrJSExternal, "#application.applicationUrl#assets/js/global/header.js");
</cfscript>
<cfoutput>
<header id="header-logo-nav">
	<div class="container">
		<div class="row">
			<div class="col logo">
				<p>
					<a href="#buildUrl(local.defaultUrl)#">
						<img src="#application.applicationURL#assets/gfx/panteia-logo.svg" class="mr-2">
					</a>
				</p>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-expand-md navbar-dark bg-WEBAPP" id="navbar-WEBAPP">
		<div class="container">
			<div class="row">
				<button type="button" class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="##topmenu" aria-controls="topmenu" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="topmenu">
					#view(local.topmenu)#
				</div>
			</div>
		</div>
	</nav>
</header>
</cfoutput>