﻿component persistent=true extends="model.beans.default.base" output=false hint="Settings per user" {

	/* properties */
	property name="serializedData" ormtype="text" sqltype="nvarchar(MAX)";
	property name="serializedTiles" ormtype="text" sqltype="nvarchar(MAX)";

	/* relations */
	property name="user" fieldtype="one-to-one" cfc="user" fkcolumn="fk_userId";

}