﻿component persistent=true extends="model.beans.default.base" output=false hint="Settings per group" {

	/* properties */
	property name="defaultaction" ormtype="string" default="registered:" sqltype="nvarchar(255)";
	property name="defaultroles" ormtype="string" default="registered" sqltype="nvarchar(255)";

	/* relations */
	property name="group" fieldtype="one-to-one" cfc="group" fkcolumn="fk_groupId";

}