component persistent=true extends="model.beans.default.base" output=false {

	// properties
	property name="resultYear" ormtype="int" notnull=false;
	property name="resultPeriodInterval" ormtype="int" notnull=false;
	property name="intervalType" ormtype="string" notnull=false sqltype="nvarchar(50)";
	property name="isTempData" ormtype="short" notnull=true default="0" dbdefault="0";
	property name="fk_ownerId" ormtype="string" notnull=true length="36" sqltype="varchar(36)";
	property name="ownerObjectType" ormtype="string" notnull=true sqltype="nvarchar(50)";

	// non persistent properties
	property name="attributename" persistent=false;
	property name="valuetype" persistent=false;
	property name="resultValue" persistent=false;
	property name="resultValueStruct" persistent=false;

	//relations
	property name="attribute"
		fieldtype="many-to-one"
		cfc="attribute"
		notnull="true"
		cascade="all"
		fkcolumn="fk_attributeId";

	// custom functions
	public any function getAttributename() {
		return getAttribute().getName();
	}

	public any function getValueType() {
		return getAttribute().getValueType();
	}

	public array function getResultValue() {
		return entityLoad("resultMonitor", { fk_resultId : getId() });
	}

	public any function getDirectValue() {
		var objects = getResultValue();
		var values = [];
		for(var object in objects) {
			ArrayAppend(values, object.getValue());
		}
		return values;
	}

}