component persistent=true extends="model.beans.default.base" output=false hint="This component represents an enumeration. The Answer component represents the actual enumeration value. Enumerations are immutable and thus read-only" {

	//properties
	property name="name" ormtype="string" update=false notnull=true unique=true sqltype="nvarchar(150)";

	//joins
	property name="answers" singularname="answer" type="array" fieldtype="one-to-many" cfc="answer" fkcolumn="fk_answergroupId" inverse="true" update=false;

	public string function getNameFromValue(required numeric value) {
		var answers = getAnswers();
		var answerssize = ArrayLen(answers);
		for(var i = 1; i <= answerssize; i++) {
			var answer = answers[i];
			if(answer.getValue() == arguments.value) {
				return answer.getName();
			}
		}
		return "";
	}

	public boolean function hasNameForValue(required numeric value) {
		return (len(getNameFromValue(argumentCollection=arguments)) > 0);
	}

	public numeric function getValueFromName(required string name) {
		var answers = getAnswers();
		var answerssize = ArrayLen(answers);
		for(var i = 1; i <= answerssize; i++) {
			var answer = answers[i];
			if(answer.getName() == arguments.name) {
				return answer.getValue();
			}
		}
		return 0;
	}

}