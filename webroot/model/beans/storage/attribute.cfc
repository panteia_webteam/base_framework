component persistent=true extends="model.beans.default.base" output=false {

	//properties
	property name="name" ormtype="string" notnull=true sqltype="nvarchar(50)";
	property name="valueType" ormtype="string" notnull=true sqltype="nvarchar(50)";
	property name="periodData" ormtype="boolean" sqltype="bit" notnull=true;
	property name="fullname" ormtype="string" notnull=true sqltype="nvarchar(150)";

	//joins
	property name="children" singularname="child" type="array" fieldtype="one-to-many" cfc="attribute" fkcolumn="fk_parentId" inverse="true" where="isActive=1";
	property name="parent" fieldtype="many-to-one" cfc="attribute" fkcolumn="fk_parentId";

	public void function addChild(required model.beans.storage.attribute child) {
		lock name="attribute-#variables.fullname#" type="exclusive" timeout="10" {
			if(application.comparisonchecker.unsafeStrictEquality(arguments.child, this)) {
				throw(message="Parent can't have itself as child.", type="IllegalArgumentException");
			}
			if(IsNull(arguments.child.getParent()) || application.comparisonchecker.unsafeStrictEquality(arguments.child.getParent(), this)) {
				param name="variables.children" default="#ArrayNew(1)#";
				arrayAppend(variables.children, arguments.child);
				arguments.child.setParent(this);
			} else {
				throw(message="Can't adopt child that already has a parent.", type="IllegalArgumentException");
			}
		}
	}

	public boolean function removeChild(required model.beans.storage.attribute child) {
		lock name="attribute-#variables.fullname#" type="exclusive" timeout="10" {
			if(hasChild(arguments.child) && ArrayDelete(getChildren(),arguments.child)) {
				arguments.child.clearParent();
				return true;
			}
			return false;
		}
	}

	public void function setParent(required model.beans.storage.attribute parent) {
		lock name="attribute-#variables.fullname#" type="exclusive" timeout="10" {
			if(! isNull(this.getParent()) && application.comparisonchecker.unsafeStrictEquality(arguments.parent, this.getParent())){
				return;//No need to set parent again.
			}
			if(! IsNull(variables.parent)) {
				if(! (getParent().removeChild(this))) {
					throw(message="Failed to remove previous parent from child attribute.", type="IllegalStateException");
				}
			}
			variables.parent = arguments.parent;
			arguments.parent.addChild(this);
		}
	}

	public void function clearParent() {
		lock name="attribute-#variables.fullname#" type="exclusive" timeout="10" {
			structDelete(variables, "parent");
		}
	}

	public any function convertStringAnswerToNumeric(required any fieldValue) {
		if(isValid("regex", arguments.fieldValue, "^(0|((-?[1-9])\d*))$")) {
			return arguments.fieldValue;
		}
		var valType = getValueType();
		var typesArray = ["int", "integer", "float", "varchar", "boolean", "datetime", "text"];
		if(!typesArray.contains(valType)) {
			var result = ORMExecuteQuery("FROM answergroup WHERE name = :name", { name:valType }, true).getValueFromName(arguments.fieldValue);
			if(isNull(result)) {
				throw(message="No answergroup exists with name #name#.", type="IllegalArgumentException");
			}
			if(result != 0) {
				return result;
			} else {
				throw(message="Can't convert ""#arguments.fieldValue#"" to numerical value in answergroup #valType#. If you are trying to save a multiresponse answer, you probably forgot to put [] at the end of the fieldname.", type="IllegalArgumentException");
			}
		} else {
			return arguments.fieldValue;
		}
	}

}