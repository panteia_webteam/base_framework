component persistent=true table="UBAResultText" schema="dbo" output=false {

	/* properties */
	property name="id" 	ormtype="string" fieldtype="id" generator="guid" length="36";
	property name="text" ormtype="text" sqltype="text";

	//joins
	property name="resultmonitor" fieldtype="one-to-one" cfc="resultmonitor" fkcolumn="fk_resultMonitorId";

}