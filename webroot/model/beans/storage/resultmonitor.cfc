component persistent=true table="UBAResult" schema="dbo" output=false {

	/* properties */
	property name="id" ormtype="string" fieldtype="id" generator="guid" length="36" sqltype="varchar(36)";
	property name="fk_resultId" ormtype="string" length="36" sqltype="varchar(36)";
	property name="bit" ormtype="boolean";
	property name="varchar" ormtype="string" sqltype="nvarchar(255)";
	property name="int" ormtype="int";
	property name="float" ormtype="float" dbtype="real";
	property name="datetime" ormtype="timestamp";

	//joins
	property name="text" fieldtype="one-to-one" cfc="resultmonitortext" mappedby="resultmonitor";
	property name="resultReference" persistent="false";

	public any function getValue() {
		var bit = getBit();
		var varchar = getVarchar();
		var integer = getInt();
		var float = getFloat();
		var datetime = getDatetime();
		if(! IsNull(bit)) {
			return bit;
		}
		if(! IsNull(varchar)) {
			return varchar;
		}
		if(! IsNull(int)) {
			return int;
		}
		if(! IsNull(float)) {
			return float;
		}
		if(! IsNull(datetime)) {
			return datetime;
		}
		if(hasText()) {
			return getText().getText();
		}
	}

	public any function setResultReference(required model.beans.storage.result result) {
		variables.resultReference = arguments.result;
	}

	public model.beans.storage.result function getResultReference() {
		return variables.resultReference;
	}

	public any function resolveResultReference() {
		setFk_resultId(variables.resultReference.getId());
	}

	public any function setValue(required any value) {
		var valueType = getResultReference().getValueType();
		param name="session.locale" default="nl_NL";
		//todo remove session
		if(valueType.equalsIgnoreCase("float")) {
			if (len(arguments.value)) {
				setFloat(lsParseNumber(arguments.value,session.locale));
			}
		} else if(valueType.equalsIgnoreCase("varchar")) {
			setVarchar(arguments.value);
		} else if(valueType.equalsIgnoreCase("boolean")) {
			setBit(arguments.value);
		} else if(valueType.equalsIgnoreCase("datetime")) {
			setDatetime(arguments.value);
		} else if(valueType.equalsIgnoreCase("int") || valueType.equalsIgnoreCase("integer")) {
			if (len(arguments.value)) {
				setInt(lsParseNumber(arguments.value, session.locale));
			}
		} else if(valueType.equalsIgnoreCase("text")) {
			//Is done in the monitoringservice, due to transaction constraints
		} else {
			var name = Replace(valueType, "[]", "");
			var answergroup = ORMExecuteQuery("FROM answergroup WHERE name = :name", { name : name, isActive : 1 }, true);
			if(isNull(answergroup)) {
				throw(message="No answergroup exists with name #name#.", type="IllegalArgumentException");
			}
			if(isValid("regex", arguments.value, "^(0|((-?[1-9])\d*))$")) {
				if(answergroup.hasNameForValue(arguments.value)) {
					setInt(arguments.value);
				} else {
					throw(message="No name for enumeration value #arguments.value# in answergroup #valueType#", type="IllegalArgumentException");
				}
			} else {
				var val = answergroup.getValueFromName(arguments.value);
				if(val != 0) {
					setInt(val);
				} else {
					throw(message="No enumeration value for #arguments.value# in answergroup #valueType#", type="IllegalArgumentException");
				}
			}
		}
	}

}