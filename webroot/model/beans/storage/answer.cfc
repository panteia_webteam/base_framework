component persistent=true extends="model.beans.default.base" output=false hint="This component represents an enumeration value. For that purpose, an answer object is read-only." {

	//properties
	property name="name" ormtype="string" update=false notnull=true;
	property name="value" ormtype="int" update=false notnull=true;
	property name="listOrder" ormtype="int" update=false;

	//joins
	property name="answerGroup"
		fieldtype="many-to-one"
		cfc="answergroup"
		fkcolumn="fk_answergroupId"
		update=false;

}