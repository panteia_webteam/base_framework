component persistent=true extends="base" output=false hint="Menu item" {

	/* properties */
	property name="title" ormtype="string" length="255" sqltype="nvarchar(255)";
	property name="action" ormtype="string" length="80" sqltype="nvarchar(80)";// url/action to execute
	property name="description" ormtype="text" sqltype="nvarchar(MAX)";//possibly shown as hover information
	property name="listOrder" ormtype="integer";//order in which the items are shown
	property name="menu" ormtype="string" sqltype="nvarchar(80)";//descriptive for the menu placement
	property name="fk_parentId" ormtype="string" sqltype="varchar(36)";//id of other menuitem

}