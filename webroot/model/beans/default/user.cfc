﻿component persistent=true extends="base" output=false cacheuse="transactional" {

	/* properties */
	property name="firstname" ormtype="string" sqltype="nvarchar(50)" length="50";
	property name="prefix" ormtype="string" sqltype="nvarchar(10)" length="10";
	property name="lastname" ormtype="string" sqltype="nvarchar(50)" length="50";
	property name="gender" ormtype="string" sqltype="nvarchar(20)" length="20";
	property name="locale" ormtype="string" sqltype="nvarchar(20)" length="20";
	property name="birthday" ormtype="timestamp" sqltype="datetime";
	property name="email" ormtype="string" sqltype="nvarchar(100)" length="100";
	property name="email_optout" ormtype="boolean" default=0 dbdefault=0;
	property name="netwerkLoginName" ormtype="string" sqltype="varchar(15)" length="15";
	property name="defaultaction" ormtype="string" sqltype="nvarchar(50)" length="50" default="registered:";
	property name="new" ormtype="boolean" default=0;
	property name="lastEmailDate" ormtype="timestamp" sqltype="datetime";//last time a request to fill in the questionnaire has been sent
	property name="hardbounced" ormtype="timestamp" sqltype="datetime";//amount of times a email hardbounced (email does not exist)
	property name="softbounced" ormtype="timestamp" sqltype="datetime";//amount of times a email softbounced (for example: emailbox is full)
	property name="noBounced" ormtype="int" dbdefault="0" default=0;//amount of times a email has been returned (for example: out of office replies)
	property name="encrypted" ormtype="short" default="0" dbdefault="0" notnull=true; // 1 = yes, 0 = no
	property name="encryptsalt" ormtype="string" length="16" sqltype="nvarchar(16)"; //salt for encryption

	/* relations */
	property name="passwords"
		singularname="password"
		fieldtype="one-to-many"
		inverse="true"
		cfc="password"
		fkcolumn="fk_userId"
		type="array"
		where="isActive=1"
		orderby="createDate desc";
	property name="roles"
		singularname="role"
		fieldType="many-to-many"
		cfc="role"
		linktable="ref_user_role"
		fkcolumn="fk_userId"
		inversejoincolumn="fk_roleId"
		lazy="false"
		cascade="all";
	property name="groups"
		singularname="group"
		fieldType="many-to-many"
		cfc="group"
		linktable="ref_user_group"
		fkcolumn="fk_userId"
		inversejoincolumn="fk_groupId"
		inverse="false"
		lazy="false";
	property name="topGroup"
		fieldtype="many-to-one"
		cfc="group"
		fkcolumn="fk_topGroupId";
	property name="usersetting"
		fieldtype="one-to-one"
		cfc="usersetting"
		mappedby="user";

	/* override setters & getters */
	// USER
	public string function getFullName() output=false hint="Returns the fullname" {
		var utilityService = new model.services.default.utilityService();
		return utilityService.dataDecryptString(getFirstName())&" "&utilityService.dataDecryptString(getPrefix())&" "&utilityService.dataDecryptString(getLastName());
	}

	// LOGS
	public void function addLog(required Log Log) output=false hint="" {
		param name="variables.logs" default="#arrayNew(1)#";
		if(! hasLog(arguments.log)) {
			arrayAppend(variables.logs, arguments.log);
			arguments.log.setUser(this);
		}
	}

	public date function getLastLoginDate() output=false hint="" {
		if (isNull(variables.id)) {
			return lsParseDateTime("1-1-1900", session.locale);
		}
		var queryString = "
			SELECT TOP 1 createDate
			FROM log
			WHERE createdBy = :userId
			AND (
				action = 'home:main.processmain'
				OR action = 'home:login.processmain'
				OR action = 'home:security.processlogin'
			)
			ORDER BY createDate DESC
		";
		var result = new Query()
			.setSQL(queryString)
			.addParam(name="userId", value=variables.id, cfsqltype="CF_SQL_IDSTAMP")
			.execute()
			.getResult()
			.createDate[1];
		if (! len(result)) {
			return lsParseDateTime("1-1-1900", session.locale);
		}
		return result;
	}

	public numeric function getLoginCount() output=false hint="" {
		if (isNull(variables.id)) {
			return 0;
		}
		var queryString = "
			SELECT count(id) AS amount
			FROM log
			WHERE createdBy = :userId
			AND (
				action = 'home:main.processmain'
				OR action = 'home:login.processmain'
				OR action = 'home:security.processlogin'
			)
		";
		var result = new Query()
			.setSQL(queryString)
			.addParam(name="userId", value=variables.id, cfsqltype="CF_SQL_IDSTAMP")
			.execute()
			.getResult()
			.amount[1];
		if (! len(result)) {
			return 0;
		}
		return result;
	}

	public numeric function getLogCount() output=false hint="" {
		if (isNull(variables.id)) {
			return 0;
		}
		var queryString = "
			SELECT count(id) AS amount
			FROM log
			WHERE createdBy = :userId
		";
		var result = new Query()
			.setSQL(queryString)
			.addParam(name="userId", value=variables.id, cfsqltype="CF_SQL_IDSTAMP")
			.execute()
			.getResult()
			.amount[1];
		if (! len(result)) {
			return 0;
		}
		return result;
	}

	// PASSWORD
	public void function addPassword(required password password) output=false hint="" {
		param name="variables.passwords" default="#arrayNew(1)#";
		if (! hasPassword(arguments.password)) {
			transaction {
				// set new to false as this is not the first password
				arguments.password.setNew(0);
				// set passwords isActive too false
				var oldPassword = "";
				for (oldPassword in variables.passwords ) {
					if (oldPassword.getIsActive()) {
						oldPassword.setIsActive(-1);
					}
				}
				// set new password
				arrayAppend(variables.passwords, arguments.password);
				arguments.password.setUser(this);
				entitySave(arguments.password);
			}
		}
	}

	// ROLE
	public void function addRole(required role role) output=false hint="" {
		param name="variables.roles" default="#ArrayNew(1)#";
		if (! hasRole(arguments.role)) {
			arrayAppend(variables.roles, arguments.role);
			arguments.role.addUser(this);
		}
	}

	public void function removeRole(required role role, boolean inverse=true) output=false hint="" {
		if(hasRole(arguments.role)) {
			arrayDelete(variables.roles, arguments.role);
			if (arguments.inverse) {
				arguments.role.removeUser(this);
			}
		}
	}

	public void function deleteRoles() output=false hint="" {
		var queryString = "
			DELETE
			FROM ref_user_role
			WHERE fk_userId = :userId
		";
		new Query()
			.setSQL(queryString)
			.addParam(name="userId", value=variables.id, cfsqltype="CF_SQL_IDSTAMP")
			.execute();
	}

	public function getRoleList() output=false hint="returns a list with roles for the current user" {
		var roleList = "";
		for (var roleObj in getRoles()) {
			roleList = listAppend(roleList, roleObj.getName());
		}
		roleList = ListSort(roleList, "Text");
		return roleList;
	}

	public function getRoleIdList() output=false hint="returns a list with roleIds for the current user" {
		var roleIdList = "";
		if (isNull(getRoles())) {
			return "";
		}
		for (var roleObj in getRoles()) {
			roleIdList = listAppend(roleIdList, roleObj.getId());
		}
		return roleIdList;
	}

	// GROUP
	public void function removeGroup(required group group, boolean inverse=true) output=false hint="" {
		if(hasGroup(arguments.group)) {
			arrayDelete(variables.groups, arguments.group);
			if (arguments.inverse) {
				arguments.group.removeUser(this);
			}
		}
	}

	public void function deleteGroups() output=false hint="" {
		var queryString = "
			DELETE
			FROM ref_user_group
			WHERE fk_userId = :userId
		";
		new Query()
			.setSQL(queryString)
			.addParam(name="userId", value=variables.id, cfsqltype="CF_SQL_IDSTAMP")
			.execute();
	}

	public model.beans.default.group function getHierarchicalGroup() output=false hint="Returns the hierarchical group" {
		if (! isNull(variables.id)) {
			var groupArray = ormExecuteQuery("
				select g
				from group g
					join g.users u
				where g.hierarchical = 1
				and u.id = :userId
			", { userId : variables.id });
			if (arrayIsEmpty(groupArray)) {
				return entityNew("group");
			}
			return groupArray[1];
		}
		return entityNew("group");
	}

	public string function getTopGroupName() output=false hint="returns the top level group" {
		var topRange = getHierarchicalGroup().getHierarchicalRange();
		return topRange.groupName[1];
	}

	public guid function getTopGroupId() output=false hint="returns the top level group" {
		var topRange = getHierarchicalGroup().getHierarchicalRange();
		return topRange.id[1];
	}

	public function getGroupList() output=false hint="returns a list with group for the current user" {
		var groupList = "";
		for (var groupObj in getGroups()) {
			groupList = listAppend(groupList, groupObj.getGroupName());
		}
		groupList = Replace(ListSort(groupList, "Text"), ",", ", ", "ALL");
		return groupList;
	}

	public function getGroupIdList() output=false hint="returns a list with groupIds for the current user" {
		var groupIdList = "";
		if (isNull(getGroups())) {
			return "";
		}
		for (var groupObj in getGroups()) {
			groupIdList = listAppend(groupIdList, groupObj.getId());
		}
		return groupIdList;
	}

	public function getHorizontalGroups() output=false hint="returns an array with horizontalgroups for the current user" {
		var groupArray = [];
		for (var groupObj in getGroups()) {
			if (! groupObj.getHierarchical()) {
				arrayAppend(groupArray, { "groupobj" : groupObj , "level" : 0 });
			}
		}
		return groupArray;
	}

}