component persistent=true extends="base" output=false hint="" {

	/* properties */
	property name="password" ormtype="string" notnull=true sqltype="nvarchar(150)";
	property name="salt" ormtype="string" notnull=true sqltype="nvarchar(15)";
	property name="new" ormtype="boolean" default="0" dbDefault="0";
	property name="reset" ormtype="boolean" default="0" dbDefault="0";

	/* non persistent */
	property name="verifyPassword" type="string" persistent=false;
	property name="isNewPassword" type="string" persistent=false;

	/* relations */
	property name="user" fieldtype="many-to-one" cfc="user" fkcolumn="fk_userId";

	/* override setters & getters */
	public void function setPassword(required string password) {
		if (isNull(variables.salt)) {
			variables.salt = rand("SHA1PRNG");
		}
		variables.password = hash(arguments.password & variables.salt, "SHA-512", "utf-8");
	}

	/* public functions */
	public boolean function hasExpired() {
		return sgn(dateDiff("d", now(), dateAdd("d", application.security.passwordValidInDays, getCreateDate())));
	}

}