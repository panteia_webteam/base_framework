component persistent=true extends="base" output=false hint="Logs the user action" {

	/* properties */
	property name="action" ormtype="string" sqltype="nvarchar(100)";
	property name="ip_hash" ormtype="string" sqltype="nvarchar(15)";
	property name="class" ormtype="string" sqltype="nvarchar(50)";
	property name="description" ormtype="string" sqltype="nvarchar(255)";
	property name="serializedData" ormtype="text" sqltype="nvarchar(MAX)";

}