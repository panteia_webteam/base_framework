component persistent=true extends="base" output=false {

	/* properties */
	property name="shortUrl" ormtype="string" notnull=true sqltype="nvarchar(255)";
	property name="action" ormtype="string" sqltype="nvarchar(100)";
	property name="querystring" ormtype="string" sqltype="nvarchar(MAX)";
	property name="fullUrl"	ormtype="string" sqltype="nvarchar(255)";
	property name="clickCount" ormtype="integer" default="0" dbDefault="0";
	property name="salt" ormtype="string" sqltype="nvarchar(16)";

}