﻿component persistent=true extends="base" output=false hint="Global settings for the application" {

	/* properties */
	property name="domainname" ormtype="string" sqltype="nvarchar(150)";
	property name="displayName" ormtype="string" default="Panteia" sqltype="nvarchar(150)";
	property name="copyrightUrl" ormtype="string" default="https://panteia.nl/" sqltype="nvarchar(150)";
	property name="uaKey" ormtype="string" default="" sqltype="nvarchar(150)";/* Google Analytics Key */
	property name="emailFrom" ormtype="string" default="info@panteia.nl" sqltype="nvarchar(150)";
	property name="emailHelpdesk" ormtype="string" default="helpdesk@panteia.nl" sqltype="nvarchar(150)";
	property name="defaultLocale" ormtype="string" default="nl_NL" sqltype="nvarchar(150)";
	property name="hasSSL" ormtype="string" default=false sqltype="nvarchar(150)";
	property name="version" ormtype="string" default="0.1" sqltype="nvarchar(150)";
	property name="publicSubSystems" ormtype="string" default="home,plugin,remote,tasks" sqltype="nvarchar(150)";
	property name="protectInput" ormtype="short" default="0" dbdefault="0" notnull=true;// 1 = yes, 0 = no

	/* public functions */
	public array function getDefaultProperties() output=false hint="define all the default properties for this application" {
		return [
			"id"
			, "isActive"
			, "ormVersion"
			, "createDate"
			, "createdBy"
			, "updateDate"
			, "updatedBy"
			, "domainname"
			, "displayName"
			, "copyrightUrl"
			, "uaKey"
			, "emailFrom"
			, "emailHelpdesk"
			, "defaultLocale"
			, "hasSSL"
			, "version"
			, "publicSubSystems"
			, "protectInput"
		];
	}

	public array function getCustomProperties() output=false hint="loop over all default properties and filter out the custom properties for this application" {
		var customPropertyArr = arrayFilter(listToArray(getPropertyList()), function(val) {
		    return ! ArrayContains(getDefaultProperties(),val);
		});
		return customPropertyArr;
	}

}