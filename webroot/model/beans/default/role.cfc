component persistent=true extends="base" output=false cachename="roleCache" cacheuse="transactional" {

	/* properties */
	property name="name" length="60" ormtype="string" notnull=true sqltype="nvarchar(60)";
	property name="description" length="255" ormtype="string" sqltype="nvarchar(255)";
	property name="whiteList" ormtype="text" hint="Actions that are allowed" sqltype="nvarchar(MAX)";
	property name="blackList" ormtype="text" hint="Actions that are not allowed" sqltype="nvarchar(MAX)";

	/* relations */
	property name="users"
		singularname="user"
		fieldType="many-to-many"
		cfc="user"
		linktable="ref_user_role"
		fkcolumn="fk_roleId"
		inversejoincolumn="fk_userId"
		lazy=true
		cascade="all"
		inverse=true;

	/* override setters & getters */
	public void function addUser(required user user) {
		param name="variables.users" default="#ArrayNew(1)#";
		if (! hasUser(arguments.user)) {
			arrayAppend(variables.users, arguments.user);
			arguments.user.addRole(this);
		}
	}

	public void function removeUser(required user user, boolean inverse=true) {
		if(hasUser(arguments.user)) {
			arrayDelete(variables.users, arguments.user);
			if (arguments.inverse) {
				arguments.user.removeRole(this);
			}
		}
	}

	public void function setUsers(required array users) {
		param name="variables.users" default="#ArrayNew(1)#";
		var user = "";
		// loop through existing users
		for (user in variables.users ) {
			if (! arrayContains(arguments.users, user)) {
				user.removeRole(this,false);
			}
		}
		// loop through passed users
		for (user in arguments.users) {
			if (! user.hasRole(this)) {
				user.addRole(this);
			}
		}
		variables.users = arguments.users;
	}

	/* public functions */
 	public array function getUserIdList() hint="returns a array with userIds for the current role" {
		local.userList = "";
		if (hasUser()) {
			return ORMExecuteQuery("
				SELECT id
				FROM user u
					JOIN u.role r
				WHERE r.id = :roleId
				AND u.isActive = 1
			", { roleId : variables.id });
		}
		return [];
	}

	public numeric function getUserCount() hint="returns a count of the associated users to this group" {
		if (hasUser()) {
			return userCount = ORMExecuteQuery("
				SELECT count(u.id) AS userCount
				FROM role r
					JOIN r.users u
				WHERE r.id = :roleId
				AND u.isActive = 1
			", { roleId : variables.id })[1];
		}
		return 0;
	}

}