﻿component extends="base" persistent=true output=false cacheuse="transactional" {

	/* properties */
	property name="groupName" ormtype="string" sqltype="nvarchar(100)" length="100" notnull=false;
	property name="hierarchical" ormtype="boolean" notnull=false;
	property name="listorder" ormtype="int" notnull=false;
	property name="demoAccount" ormtype="boolean" dbdefault=0 default=0 notnull=false;
	property name="logo" ormtype="string" sqltype="nvarchar(255)" length="255" notnull=false;
	property name="relationId" ormtype="string" sqltype="varchar(36)" length="36" notnull=false;
	property name="encrypted" ormtype="short" default="0" dbdefault="0" notnull=true;// 1 = yes, 0 = no

	/* relations */
	property name="children"
		singularname="child"
		type="array"
		fieldtype="one-to-many"
		cfc="group"
		fkcolumn="fk_parentId"
		inverse=true
		where="isActive=1"
		orderby="listorder DESC";
	property name="parent"
		fieldtype="many-to-one"
		cfc="group"
		fkcolumn="fk_parentId";
	property name="users"
		singularname="user"
		fieldType="many-to-many"
		cfc="user"
		linktable="ref_user_group"
		fkcolumn="fk_groupId"
		inversejoincolumn="fk_userId"
		lazy=true
		inverse=false;
	property name="groupsetting"
		fieldtype="one-to-one"
		cfc="groupsetting"
		mappedby="group";

	/* override setters & getters */
	public void function addUser(required user user) {
		param name="variables.users" default="#ArrayNew(1)#";
		if (! hasUser(arguments.user)) {
			arrayAppend(variables.users, arguments.user);
			arguments.user.addGroup(this);
		}
	}

	public void function removeUser(required user user, boolean inverse=true) {
		if(hasUser(arguments.user)) {
			arrayDelete(variables.users, arguments.user);
			if (arguments.inverse) {
				arguments.user.removeGroup(this);
			}
		}
	}

	public void function setUsers(required array users) {
		param name="variables.users" default="#ArrayNew(1)#";
		var user = "";
		// loop through existing users
		for (user in variables.users) {
			if (! arrayContains(arguments.users, user)) {
				user.removeGroup(this,false);
			}
		}
		// loop through passed users
		for (user in arguments.users) {
			//check if there already is a hierarchical group present and if so and this is one as well, remove the prior group
			if (! isNull(user.getHierarchicalGroup().getId()) && variables.hierarchical) {
				user.removeGroup(user.getHierarchicalGroup(), false);
			}
		}
		variables.users = arguments.users;
	}

	/* hierarchical functions */
	public query function getHierarchicalRange(string orderingBy="TopDown") output=false hint="orderingBy to switch the ordering: 'TopDown' (default) and 'BottomUp'" {
		var queryString = "
			WITH theGroupsTopDown AS (
				SELECT id
					, groupName
					, fk_parentid
					, 0 as [level]
					, cast(char(floor(((row_number() over(order by groupName)-1)/26)+97)) as varchar(255))
						+ cast(char(((row_number() over(order by groupName)-1)%26)+97) as varchar(255))
					as orderCode
				from [group]
				where id = :groupId
				and isactive = 1
				and hierarchical = 1
			UNION ALL
				SELECT childTable.id
					, childTable.groupName
					, childTable.fk_parentid
					, (theGroupsTopDown.[level] + 1) as [level]
					, case
						when childTable.fk_parentid = theGroupsTopDown.id
							then
								cast(theGroupsTopDown.ordercode
									+ char(floor(((row_number() over(order by childTable.[groupName])-1)/26)+97)) as varchar(255))
									+ cast(Char(((row_number() over(order by childTable.[groupName])-1)%26)+97) as varchar(255))
					end as orderCode
				from [group] childTable
					inner join theGroupsTopDown on theGroupsTopDown.id = childTable.fk_parentid
				where childTable.isactive = 1
				and childTable.hierarchical = 1
			)
			, theGroupsBottomUp as (
				SELECT id
					, groupName
					, fk_parentid
					, 0 as [level]
					, cast(char(floor(((row_number() over(order by [groupName])-1)/26)+97)) as varchar(255))
						+ cast(char(((row_number() over(order by [groupName])-1)%26)+97) as varchar(255))
					as orderCode
				from [group]
				where id = :groupId
				and isactive = 1
				and hierarchical = 1
			UNION ALL
				SELECT childTable.id
					, childTable.groupName
					, childTable.fk_parentid
					, (theGroupsBottomUp.[level] - 1) as [level]
					, case
						when childTable.fk_parentid = theGroupsBottomUp.id
							then
								cast(theGroupsBottomUp.ordercode
									+ char(floor(((row_number() over(order by childTable.groupName)-1)/26)+97)) as varchar(255))
									+ cast(char(((row_number() over(order by childTable.groupName)-1)%26)+97) as varchar(255))
					end as orderCode
				from [group] childTable
					inner join theGroupsBottomUp on theGroupsBottomUp.fk_parentid = childTable.id
				where childTable.isactive = 1
				and childTable.hierarchical = 1
			)
			SELECT id
				, groupName
				, [level]
				, orderCode
			from theGroups#arguments.orderingBy#
			order by orderCode
		";
		if (arguments.orderingBy == "BottomUp") {
			querystring &= " desc";
		}
		querystring &= ", groupName asc";
		return new Query()
			.setSQL(querystring)
			.addParam(name="groupId",value=variables.id,cfsqltype="CF_SQL_IDSTAMP")
			.execute()
			.getResult();
	}

	public model.beans.default.group function getTopGroup() {
		var hierarchicalRange = getHierarchicalRange();
		var groupObj = entityLoadByPK("group", hierarchicalRange.id[1]);
		return groupObj;
	}

	/* public functions */
 	public numeric function getUserCount() hint="returns the amount of associated users" {
		if (hasUser()) {
			return userCount = ORMExecuteQuery("
				SELECT count(u.id) as userCount
				FROM group g
					JOIN g.users u
				WHERE g.id = :groupId
				AND u.isActive = 1
			", { groupId : variables.id })[1];
		}
		return 0;
	}

 	public array function getUserIdArray() hint="returns an array with userIds for the current group" {
		if (hasUser()) {
			return ORMExecuteQuery("
				SELECT u.id
				FROM user u
					JOIN u.[group] g
				WHERE g.id = :groupId
				AND u.isActive = 1
			", { groupId : variables.id });
		}
		return [];
	}

	public array function getUserEmailArray() hint="returns an array with emails of users for the current group" {
		if (hasUser()) {
			return ORMExecuteQuery("
				SELECT u.email
				FROM user u
					JOIN u.[group] g
				WHERE g.id = :groupId
				AND u.isActive = 1
			", { groupId : variables.id });
		}
		return [];
	}

}