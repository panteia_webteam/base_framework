﻿component mappedSuperclass=true output=false hint="This is Base.cfc" {

	/* default properties for all tables that via their own controller CFC extend this base.cfc */
	property name="id" ormtype="string" fieldType="id" generator="guid" length="36";
	property name="isActive" ormtype="short" notnull=true default="1" dbdefault="1";// 1 = active, 0 = not active, -1 = deleted
	property name="createDate" ormtype="timestamp" notnull=true insert="true" update="false" sqltype="datetime" default="#now()#" dbdefault="getDate()";
	property name="createdBy" ormtype="string" notnull=false insert="true" update="false" length="36";
	property name="updateDate" ormtype="timestamp" notnull=false insert="false" update="true" sqltype="datetime";
	property name="updatedBy" ormtype="string" notnull=false insert="false" update="true" length="36";
	property name="ormVersion" fieldType="version" ormtype="int" dbdefault="0";

	/* events */
	public void function preInsert() {
		if (structKeyExists(session, "auth")) {
			this.setCreatedBy(session.auth.userID);
		}
		this.setCreateDate(now());
	}

	public void function preUpdate() {
		if (structKeyExists(session, "auth")) {
			this.setUpdatedBy(session.auth.userID);
		}
		this.setUpdateDate(now());
	}

	/* public functions */
	public user function getCreatedByUser() {
		if (! isNull(variables.createdBy)) {
			var userObj = entityLoadByPK("user", variables.createdBy);
			if (! isNull(userObj)) {
				return userObj;
			}
		}
		return entityNew("user");
	}

	public array function getProperties() {
		var props = getMetaData(this).properties;
		return props;
	}

	public string function getPropertyList() {
		var props = getProperties();
		var propList = "";
		for(var i=1; i <= arrayLen(props); i++) {
			propList = listAppend(propList, props[i].name);
		}
		return propList;
	}

	public struct function getPropertyValuesStruct() {
		var props = getProperties();
		var result = {};
		var propsCount = arrayLen(props);
		for(var i=1; i <= propsCount ; i++) {
			if (! structKeyExists(props[i], "cfc")) {
				var tempFunc = this["get"&props[i].name];
				if (! isNull(tempFunc())) {
					propList[props[i].name] = tempFunc();
				} else {
					propList[props[i].name] = "";
				}
			}
		}
		return propList;
	}

	public user function getUpdatedByUser() {
		if (! isNull(variables.updatedBy)) {
			var userObj = entityLoadByPK("user", variables.updatedBy);
			if (! isNull(userObj)) {
				return userObj;
			}
		}
		return entityNew("user");
	}

}