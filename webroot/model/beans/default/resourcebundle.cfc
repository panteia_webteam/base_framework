component persistent=true extends="base" output=false hint="contains resource translations" {

	/* properties */
	property name="bundle" ormtype="string" length="150" sqltype="nvarchar(150)";
	property name="locale" ormtype="string" length="10" sqltype="nvarchar(10)";
	property name="item" ormtype="string" length="80" sqltype="nvarchar(80)";
	property name="text" ormtype="string" length="2000" sqltype="nvarchar(MAX)";

}