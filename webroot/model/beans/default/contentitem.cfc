component persistent=true extends="base" output=false hint="Content items for the website" {

	/* properties */
	property name="action" ormtype="string" sqltype="nvarchar(80)";
	property name="title" ormtype="string" sqltype="nvarchar(255)";
	property name="teaser" ormtype="string" sqltype="nvarchar(max)";
	property name="content" ormtype="text" sqltype="nvarchar(max)";
	property name="group" ormtype="string" sqltype="nvarchar(80)";
	property name="locale" ormtype="string" length="10" sqltype="nvarchar(10)";

}