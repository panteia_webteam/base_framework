component accessors=true output=false hint="" {

	property utilityService;

	public any function activeUser(string id) output=false hint="" {
		param name="arguments.id" default="";
		var userService = new model.services.default.userservice();
		var utilityService = new model.services.default.utilityservice();
		var userObj = userService.getObj(arguments.id);
		var defaultLocale = "nl_NL";
		var isLoggedIn = structKeyExists(session, "auth") && session.auth.isLoggedIn ? true : false;
		var nowDate = createDateTime(year(now()), month(now()), day(now()), 0, 0, 0);
		var passwordDate = createDateTime(year(now()), month(now()), day(now()), 0, 0, 0);
		var passwordPolicy = fetchPolicy(weburl="development.dev.onderzoek.nl");
		if (! isNull(userObj.getId())) {
			var userDate = ! isNull(userObj.getPasswords()[1].getCreateDate()) && userObj.getPasswords()[1].getIsActive() ? dateAdd("m", passwordPolicy.value, userObj.getPasswords()[1].getCreateDate()) : now();
			defaultLocale = userObj.getLocale() == "nl_NL" ? "nl_NL" : "en_US";
			passwordDate = createDateTime(year(userDate), month(userDate), day(userDate), 0, 0, 0);
		}
		var daysLeftToResetPassword = dateDiff("d", nowDate, passwordDate);
		daysLeftToResetPassword = 20;
		var state = daysLeftToResetPassword > 1 ? "active" : "reactivate";
		var alertTxt = getAlerts(defaultLocale, daysLeftToResetPassword, passwordDate);
		var returnObj = structNew("ordered");
		returnObj.showAlert = isLoggedIn && passwordPolicy.value > 0 && passwordPolicy.startAlert.find(daysLeftToResetPassword) > 0 ? true : false;
		returnObj.redirect = isLoggedIn && passwordPolicy.value > 0 && daysLeftToResetPassword <= 0 ? true : false;
		returnObj.daysLeftToResetPassword = daysLeftToResetPassword;
		// change some variables into the right text
		alertTxt.activeTxt = replace(alertTxt.activeTxt, "{username}", utilityService.dataDecryptString(userObj.getFirstName()));
		alertTxt.activeTxt = replace(alertTxt.activeTxt, "{btntxt}", alertTxt.btnTxt);
		alertTxt.redirectTxt = replace(alertTxt.redirectTxt, "{username}", utilityService.dataDecryptString(userObj.getFirstName()));
		alertTxt.redirectTxt = replace(alertTxt.redirectTxt, "{btntxt}", alertTxt.btnTxt);
		returnObj.alerts = alertTxt;
		if (returnObj.showAlert) {
			returnObj.expirationDate = passwordDate;
			returnObj.fetchPolicy = passwordPolicy;
			returnObj.state = state;
		}
		return returnObj;
	}

	private struct function fetchPolicy(string weburl=cgi.server_name) output=false hint="" {
		var weburl = arguments.weburl;
		var returnVal = 0;
		var queryString = "
			DECLARE @weburl varchar(225);
			SET @weburl = :weburl;
			SELECT [id]
				, [customer]
				, [passwordPolicyDev]
				, [passwordPolicyAcc]
				, [passwordPolicyProd]
				, case
					when [domainNameDev] = @weburl then 'passwordPolicyDev'
					when [domainNameAcc] = @weburl then 'passwordPolicyAcc'
					when [domainNameProd] = @weburl then 'passwordPolicyProd'
					else ''
				end as 'passwordPolicy'
			from [pdomain]
			where [domainNameDev] = @weburl
			or [domainNameAcc] = @weburl
			or [domainNameProd] = @weburl
		";
		var qData = new Query()
			.setDatasource("web_credentials")
			.setSQL(queryString)
			.addParam(name="weburl", value=weburl, sqltype="cf_sql_varchar")
			.execute()
			.getResult();
		if (qData.recordCount() == 1) {
			var wichPolicy = qData.passwordPolicy[1];
			returnVal = qData[wichPolicy][1];
		}
		var policy = getPasswordPolicies(returnVal);
		return policy;
	}

	private struct function getPasswordPolicies(number arrElem=0) output=false hint="" {
		var arrReturn = [
			{ "name":"no policy", "value":0, "startAlert": 0 }
			, { "name":"3 months", "value":3, "startAlert": [15,10,5,4,3,2,1] }
			, { "name":"6 months", "value":6, "startAlert": [15,10,5,4,3,2,1] }
			, { "name":"12 months", "value":12, "startAlert": [15,10,5,4,3,2,1] }
		];
		var returnStruct = arrReturn[1]
		for (var elem in arrReturn) {
			if (elem.value == arguments.arrElem) {
				returnStruct = elem;
				break;
			}
		}
		return returnStruct;
	}

	private struct function getAlerts(string lng="nl_NL", numeric daysLeft=0, datetime passwordDate=now()) output=false hint="" {
		var alertTxt = {
			"nl_NL": {
				"active": "<p>Je wachtwoord is nog #arguments.daysLeft# dag(en) geldig voordat deze op #dateFormat(arguments.passwordDate, "dd-mm-yyyy")# verloopt.<br><strong>Let op</strong>: als het wachtwoord is verlopen kun je niet meer inloggen.</p>"
				, "reactivate": "<p>Je wachtwoord is verlopen</p>"
				, "btnTxt": "nu wachtwoord instellen"
				, "btnClass": "btn btn-outline-primary"
				, "btnId": "changepassword"
				, "redirectTxt": "Hallo {username},<br>Voordat je verder kunt, dien je eerst een nieuw wachtwoord in te stellen.<br>Vul hieronder de velden in en klik op '{btntxt}'."
				, "activeTxt": "Hallo {username},<br>Je hebt er voor gekozen een nieuw wachtwoord in te stellen.<br>Vul hieronder de velden in en klik op '{btntxt}'."
			}
			, "en_US": {
				"active": "<p>Your password is valid for #arguments.daysLeft# day(s) before it expires on #dateFormat(arguments.passwordDate, "dd-mm-yyyy")#.<br><strong>Please note</strong>: if your password has expired you will no longer be able to log in.</p>"
				, "reactivate": "<p>Your password has expired.</p>"
				, "btnTxt": "setting password now"
				, "btnClass": "btn btn-outline-primary"
				, "btnId": "changepassword"
				, "redirectTxt": "Hello {username},<br>Before you can continue, you must first set a new password.<br>Fill in the fields below and click '{btntxt}'.<br>Afterwards you can login as usual."
				, "activeTxt": "Hallo {username},<br>You have chosen to set a new password.<br>Fill in the fields below and click '{btntxt}'."
			}
		}
		return alertTxt[arguments.lng];
	}

}
