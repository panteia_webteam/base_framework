component accessors=true output=false hint="" {

	/* nodig in utilityService.cfc die een extension is van deze functie */
	property config;
	property logService;
	property resourcebundleService;

	/**
	* @hint get some date issues: iso week, iso year and the monday of current week...
	* @dateInput put in a date, that is it...
	**/
	public struct function getDateInfo(any dateInput="#now()#") output=false hint="" {
		var structReturn = {};
		var queryString = "
			SET dateFirst 1;
			DECLARE @mydate datetime;
			SET @mydate = :mydate;
			SELECT
				datePart(iso_week, @mydate) as isoweek
				, year(dateAdd(dd, (1-datePart(dw, @mydate)), @mydate)) as isoyear
				, dateAdd(dd, (1-datePart(dw, @mydate)), @mydate) as monday
		";
		var getData = new Query()
			.setSQL(queryString)
			.addParam(name="mydate", value=arguments.dateInput, cfsqltype="cf_sql_date")
			.execute()
			.getResult();
		structReturn.isoweek = local.getData.isoweek[1];
		structReturn.isoyear = local.getData.isoyear[1];
		structReturn.monday = local.getData.monday[1];
		return structReturn;
	}

	public numeric function getIsoWeek(dateInput="#now()#") output=false hint="" {
		var returnValue = getDateInfo(arguments.dateInput).isoweek;
		return returnValue;
	}

	public numeric function getIsoYear(dateInput="#now()#") output=false hint="" {
		var returnValue = getDateInfo(arguments.dateInput).isoyear;
		return returnValue;
	}

	public date function getMonday(dateInput=now()) output=false hint="" {
		var returnValue = getDateInfo(arguments.dateInput).monday;
		return returnValue;
	}

	public date function startOfWeek(dateInput="#now()#", boolean startSunday=false) output=false hint="" {
		var returnValue = getDateInfo(arguments.dateInput).monday;
		var returnDate = arguments.startSunday ? dateAdd("d", -1, returnValue) : returnValue;
		return returnDate;
	}

	public date function startOfWeekOfMonth(date inputDate = now(), boolean startSunday=false) output=false hint="" {
		// monday is the second day of the week.
		var startDate = createDate(year(arguments.inputDate), month(arguments.inputDate), 01);
		var firstWeekDayOfMonth = dayOfWeek(startDate);
		var dayDiffToAdd = firstWeekDayOfMonth gt 2 ? 7 - (firstWeekDayOfMonth-2) : firstWeekDayOfMonth lt 2 ? 1 : 0;
		var firstMondayOfMonth = dateAdd("d", dayDiffToAdd, startDate);
		var returnDate = arguments.startSunday ? dateAdd("d", -1, firstMondayOfMonth) : firstMondayOfMonth;
		return returnDate;
	}

	public numeric function weeksInYear(dateInput="#now()#") output=false hint="" {
		var firstWeekYear = startOfWeek(createDate(year(arguments.dateInput), 1, 1));
		var lastWeekYear = startOfWeek(createDate(year(arguments.dateInput)+1, 1, 1));
		var weeksInYear = dateDiff("w", firstWeekYear, lastWeekYear);
		return weeksInYear;
	}

	/**
	* @hint shortens a piece of text with a amount of characters
	* @text name to validate against
	* @characters value to validate **/
	public string function abbreviate(string text, numeric characters) output=false hint="" {
		var newString = reReplace(text, "<[^>]*>", " ", "ALL");
		var lastSpace = 0;
		newString = REReplace(newString, " \s*", " ", "ALL");
		if (len(newString) > characters) {
			newString = left(newString, characters-2);
			lastSpace = find(" ", reverse(newString));
			lastSpace = len(newString) - lastSpace;
			newString = left(newString, lastSpace) & "  &##8230;";
		}
		return newString;
	}

	/* @text: name to validate against
	@characters: value to validate */
	public string function abbreviateString(string text, numeric characters) output=false hint="shortens a piece of text with an amount of characters" {
		var newString = REReplace(text, "<[^>]*>", " ", "ALL");
		var lastSpace = 0;
		newString = REReplace(newString, " \s*", " ", "ALL");
		if (len(newString) > characters) {
			newString = left(newString, characters-2) & "  &##8230;";
		}
		return newString;
	}

	public numeric function checkDir(required string absDir) {
		var created = 0;
		if(! directoryExists(arguments.absDir)) {
			directoryCreate(arguments.absDir);
			created = 1;
		}
		return created;
	}

	public function guid() output=false hint="sets a generated uuid - call this function by: application.utilService.guid().randomUUID().toString()" {
		return createObject("java", "java.util.UUID");
	}

	public string function decryptString(
		required string encryptedString
		, required string key
		, required string salt
		, string encryptAlgorithm="AES/OFB/NoPadding"
		, string encoding="Hex") hint="decrypts a string, key: a generateSecret() key, salt: a 16 character string (use generateRandomString())" {
		return decrypt(
			encrypted_string=arguments.encryptedString
			, key=arguments.key
			, algorithm=arguments.encryptAlgorithm
			, encoding=arguments.encoding
			, ivorSalt=arguments.salt
		);
	}

	public string function encryptString(
		required string inputString
		, required string key
		, required string salt
		, string encryptAlgorithm="AES/OFB/NoPadding"
		, string encoding="Hex") hint="encrypts a string, key: a generateSecret() key, salt: a 16 character string" {
		return encrypt(
			string=arguments.inputString
			, key=arguments.key
			, algorithm=arguments.encryptAlgorithm
			, encoding=arguments.encoding
			, ivorSalt=arguments.salt
		);
	}

	public string function randomPassword() output=false hint="sets a random password" {
		var valid_password = 0;
		var loopindex = 0;
		var this_char = "";
		var seed = "";
		var new_password = "";
		var new_password_seed = "";
		while (valid_password == 0) {
			new_password = "";
			new_password_seed = createObject("java", "java.util.UUID").randomUUID().toString();
			new_password_seed = left(new_password_seed, 23) & right(new_password_seed, 12);
			for(loopindex=20; loopindex < 35; loopindex = loopindex + 2) {
				this_char = inputbasen(mid(new_password_seed, loopindex, 2), 16);
				seed = int(inputbasen(mid(new_password_seed, loopindex/2-9, 1), 16) mod 3)+1;
				switch(seed) {
					case "1":
						new_password = new_password & chr(int((this_char mod 9) + 48));
						break;
					case "2":
						new_password = new_password & chr(int((this_char mod 26) + 65));
						break;
					case "3":
						new_password = new_password & chr(int((this_char mod 26) + 97));
						break;
				}
			}
			valid_password = REFind("(O|o|0|i|l|1|I|5|S)",new_password) > 0 ? 0 : 1;
		}
		return new_password;
	}

	public string function generateRandomString(boolean alphaNumOnly=false, numeric chars=16) output=false hint="alphaNumOnly" {
		var returnValue = "";
		if(! alphaNumOnly){
			for(var i=1; i <= arguments.chars; i++) {
				returnValue &= Chr(randRange(33,126));
			}
		} else {
			for(var i=1; i <= arguments.chars; i++) {
				var tmpChr = randRange(48, 109);
				if(tmpChr > 57) tmpChr += 7;
				if(tmpChr > 90) tmpChr += 6;
				returnValue &= Chr(tmpChr);
			}
		}
		return returnValue;
	}

	public string function generateCharacterNumberString(numeric codeLength=8) output=false hint="alphaNumOnly" {
		var code = "";
		var allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		var allowedNrs = "0123456789";
		for (var i=1; i <= arguments.codeLength; i++) {
			if (i MOD 2 == 0) {
				var randomPosition = RandRange(1, Len(allowedChars), "SHA1PRNG");
				var randomChar = Mid(allowedChars, randomPosition, 1);
			} else {
				var randomPosition = RandRange(1, Len(allowedNrs), "SHA1PRNG");
				var randomChar = Mid(allowedNrs, randomPosition, 1);
			}
			code &= randomChar;
		}
		return code;
	}

	/**
	* @param	numberList: list with numbers only with a ";" seperator; e.g.: "247427;148000;157200". (Required)
	* @param	percentileNumber: percentile number; between 0 and 100. (Required)
	* @param	kindOfPercentile: "exc" or "inc"; default: "exc".
	* @return	Returns a numeric value.
	*
	**/
	public numeric function getThePercentile(required string numberList, required numeric percentileNumber, kindOfPercentile="exc") output=false hint="Calculates the right percentile from a list with numbers" {
		var returnValue = 0;
		var percentileNumber = arguments.percentileNumber;
		var orderdedNumberList = listSort(arguments.numberList, "numeric", "asc", ";");
		var arrNumberlist = listToArray(orderdedNumberList, ";");
		var arrLen = arrayLen(arrNumberlist);
		try {
			if ((len(arrLen) && len(percentileNumber)) && (isNumeric(arrLen) && isNumeric(percentileNumber))) {
				if (arguments.kindOfPercentile == "inc") calculation  = ((((arrLen-2)*percentileNumber)+percentileNumber)/100)+1;
				else calculation  = ((arrLen*percentileNumber)+percentileNumber)/100;
				if (calculation < 1) returnValue = 1
				else if (calculation > arrLen) returnValue = arrLen;
				else returnValue = calculation;
				if (isNumeric(returnValue)) {
					var floorNumber = int(returnValue);
					var ceilingNumber = ceiling(returnValue);
					var floorAmount = arrNumberlist[floorNumber];
					var ceilingAmount = arrNumberlist[ceilingNumber];
					if (floorAmount != ceilingAmount) {
						returnValue = floorAmount+((ceilingAmount-floorAmount)*(calculation-floorNumber));
					}
					else returnValue = floorAmount;
				}
			}
		} catch(any a) {
			returnValue = 0;
		}
		return returnValue;
	}

	/**
	 * Sorts an array of structures based on a key in the structures.
	 *
	 * @param aofS Array of structures. (Required)
	 * @param key Key to sort by. (Required)
	 * @param sortOrder Order to sort by, asc or desc. (Optional)
	 * @param sortType Text, textnocase, or numeric. (Optional)
	 * @param delim Delimiter used for temporary data storage. Must not exist in data. Defaults to a period. (Optional)
	 * @return Returns a sorted array.
	 * @author Nathan Dintenfass (nathan@changemedia.com)
	 * @version 1, April 4, 2013
	 */
	public array function arrayOfStructsSort(array aOfS, string key) output=false hint="" {
		//by default we will use an ascending sort
		var sortOrder = "asc";
		//by default, we will use a textnocase sort
		var sortType = "textnocase";
		//by default, use ascii character 30 as the delim
		var delim = ".";
		//make an array to hold the sort stuff
		var sortArray = arraynew(1);
		//make an array to return
		var returnArray = arraynew(1);
		//grab the number of elements in the array (used in the loops)
		var count = arrayLen(aOfS);
		//make a variable to use in the loop
		var ii = 1;
		//if there is a 3rd argument, set the sortOrder
		if(arraylen(arguments) > 2) {
			sortOrder = arguments[3];
		}
		//if there is a 4th argument, set the sortType
		if(arraylen(arguments) > 3) {
			sortType = arguments[4];
		}
		//if there is a 5th argument, set the delim
		if(arraylen(arguments) > 4) {
			delim = arguments[5];
		}
		//loop over the array of structs, building the sortArray
		for(ii = 1; ii <= count; ii = ii + 1) {
			sortArray[ii] = aOfS[ii][key] & delim & ii;
		}
		//now sort the array
		arraySort(sortArray,sortType,sortOrder);
		//now build the return array
		for(ii = 1; ii <= count; ii = ii + 1) {
			returnArray[ii] = aOfS[listLast(sortArray[ii],delim)];
		}
		return returnArray;
	}

	public string function hashString(required string inputString, required string salt, string hashAlgorithm="SHA-512") output=false hint="hashes a string, salt has no restrictions here" {
		return hash(arguments.inputString&"_"&arguments.salt,arguments.hashAlgorithm);
	}

	public struct function determineDownloadMimeType(required string fileMimeType) output=false hint="gives back the correct mimetype for downloading and icons" {
		var structReturn = {};
		structReturn.mimetype = "";
		structReturn.mimeicon = "";
		structReturn.mimetypeValid = false;
		if (len(arguments.fileMimeType)) {
			switch(arguments.fileMimeType) {
				case "application/pdf":
					structReturn.mimetype = arguments.fileMimeType;
					structReturn.mimeicon = "pdf";
					structReturn.mimetypeValid = true;
					break;
				case "image/png":
				case "image/jpeg":
				case "image/tiff":
				case "image/gif":
					structReturn.mimetype = arguments.fileMimeType;
					structReturn.mimeicon = "image";
					structReturn.mimetypeValid = true;
					break;
				case "application/msword":
				case "application/x-tika-msoffice":
					structReturn.mimetype = "application/msword";
					structReturn.mimeicon = "word";
					structReturn.mimetypeValid = true;
					break;
				// all ooxml files, docx, xlsx etc, return the same tika mimetype
				case "application/x-tika-ooxml":
					if (listLast(iAttachment.name,".") == "docx") {
						structReturn.mimetype = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
						structReturn.mimeicon = "word";
						structReturn.mimetypeValid = true;
					}
					break;
				case "application/vnd.ms-excel":
					structReturn.mimetype = arguments.fileMimeType;
					structReturn.mimeicon = "excel";
					structReturn.mimetypeValid = true;
					break;
				default:
					structReturn.mimetype = arguments.fileMimeType;
					structReturn.mimeicon = "text";
					structReturn.mimetypeValid = true;
			}
		}
		return structReturn;
	}

	public string function dataEncryptString(string elem, string encryptsalt, boolean protectInput) output=false hint="" {
		var config = new config.global().getConfiguration();
		param name="arguments.encryptsalt" default=config.security.encryptsalt;
		param name="arguments.protectInput" default=config.security.protectInput;
		var returnVal = isNull(arguments.elem) || (! isNull(arguments.elem) && arguments.elem == "") ? "" : arguments.elem;
		if (arguments.protectInput && returnVal != "") {
			returnVal = encryptString(arguments.elem, config.security.key, arguments.encryptsalt);
		}
		return returnVal;
	}

	public string function dataDecryptString(string elem, string encryptsalt, boolean protectInput) output=false hint="" {
		var config = new config.global().getConfiguration();
		param name="arguments.encryptsalt" default=config.security.encryptsalt;
		param name="arguments.protectInput" default=config.security.protectInput;
		var returnVal = isNull(arguments.elem) || (! isNull(arguments.elem) && arguments.elem == "") ? "" : arguments.elem;
		if (arguments.protectInput && returnVal != "") {
			returnVal = decryptString(arguments.elem, config.security.key, arguments.encryptsalt);
		}
		return returnVal;
	}

}