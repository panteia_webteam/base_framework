﻿component accessors=true extends="dao" output=false hint="" {

	property groupsettingService;
	property userService;
	property utilityservice;

	variables.objName = "group";

	public model.beans.default.group function getObj(string id="") output=false hint="" {
		var groupObj = super.getObj(arguments.id);
		if (isNull(groupObj.getGroupsetting())) {
			var groupsetting = groupsettingService.getObj();
			groupObj.setGroupsetting(groupsetting);
			groupsetting.setGroup(groupObj);
		}
		return groupObj;
	}

	public string function getIdByName(required string groupName) output=false hint="" {
		var queryString = "
			SELECT [id] AS groupId
			FROM [group]
			WHERE groupName = :groupName
			AND isActive = 1
		";
		var getData = new Query()
			.setSQL(queryString)
			.addParam(name="groupName", value=arguments.groupName, cfsqltype="cf_sql_varchar")
			.execute()
			.getResult();
		return getData.groupId;
	}

	public string function getIdByRelation(required string relationId) output=false hint="" {
		var queryString = "
			SELECT [id] AS groupId
			FROM [group]
			WHERE relationId = :relationId
			AND isActive = 1
		";
		var getData = new Query()
			.setSQL(queryString)
			.addParam(name="relationId", value=arguments.relationId, cfsqltype="cf_sql_varchar")
			.execute()
			.getResult();
		return getData.groupId;
	}

	public void function save(required model.beans.default.group obj) output=false hint="" {
		transaction {
			arguments.obj.setGroupName(utilityservice.dataEncryptString(arguments.obj.getGroupName()));
			super.save(arguments.obj);
			entitySave(arguments.obj.getGroupsetting());
		}
	}

	public boolean function forceDefaultsToAllUsers(string id="") output=false hint="" {
		var groupObj = super.getObj(arguments.id);
		var returnBool = false;
		if (arguments.id != "") {
			var queryString = "
				DECLARE @groupid varchar(36);
				SET @groupid = :groupId;
				SELECT [fk_userId] as [userId]
				FROM [ref_user_group]
				WHERE [fk_groupId] = @groupid
			";
			var qData = new Query()
				.setSQL(queryString)
				.addParam(name="groupId", value=arguments.id, cfsqltype="cf_sql_idstamp")
				.execute()
				.getResult();
			if (qData.recordCount() > 0) {
				var groupsettings = groupObj.getGroupsetting();
				for (item in qData) {
					var userObj = userService.getObj(item.userId);
					if (! listFindNoCase(userObj.getRoleList(), "admin")) {
						userObj.deleteRoles();
						userObj.setDefaultAction(groupsettings.getDefaultaction());
						var roleArr = [];
						for (role in listToArray(groupsettings.getDefaultroles())) {
							arrayAppend(roleArr, entityLoad("role", { name : role })[1]);
						}
						userObj.setRoles(roleArr);
						userService.save(userObj);
					}
				}
			}
			returnBool = true;
		}
		return returnBool;
	}

	public any function removeEncryption() {
		var qGroups = new Query()
			.setSQL("
				SELECT [id]
				FROM [group]
			")
			.execute()
			.getResult();
		for (var elem in qGroups) {
			var obj = getObj(elem.id);
			if (obj.getEncrypted()) {
				obj.setGroupName(utilityservice.dataDecryptString(elem=obj.getGroupName(), protectInput=true));
				obj.setEncrypted(0);
			}
			save(obj);
		}
		return true;
	}

	public any function addEncryption() {
		var qGroups = new Query()
			.setSQL("
				SELECT [id]
				FROM [group]
			")
			.execute()
			.getResult();
		for (var elem in qGroups) {
			var obj = getObj(elem.id);
			if (! obj.getEncrypted()) {
				obj.setGroupName(obj.getGroupName());
				obj.setEncrypted(1);
			}
			save(obj);
		}
		return true;
	}

}