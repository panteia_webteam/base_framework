component accessors=true extends="model.services.utils.baseUtilityService" output=false hint="utility functions" {

	property config;
	property logService;
	property resourcebundleService;

	/* @csrfToken : the token given trough the form */
	public void function abortOnCSRFAttack(required string csrfToken, string tokenName="csrfToken") output=false hint="prevents CSRF attacks by checking for valid CSRF Tokens" {
		if(arguments.csrfToken != session[arguments.tokenName]) {
			var returnText = "Invalid CSRF comparison on '#arguments.tokenName#'.";
			// log the tampering
			if(! IsDefined("logService")) {
				logService = new model.services.default.logservice();
			}
			// add IP-address if available
			if(structKeyExists(cgi, "remote_addr")) {
				returnText &= " IP: #getRemoteIp()#";
			}
			logService.log({ class : "danger", text : returnText });
			// return to startpage
			location(url=application.applicationUrl, addtoken=false, statuscode=307);
		}
	}

	/* @tokenName: name under which it is stored in the session scope */
	public string function generateCSRFToken(string tokenName="csrfToken", boolean forceNewToken=false) output=false hint="sets a CSRFToken in the session scope and returns its value" {
		if (! structKeyExists(session, "arguments.tokenName") || arguments.forceNewToken) {
			var sr = createObject("java", "java.security.SecureRandom");
			session[arguments.tokenName] = sr.nextLong();
			return hash(session[arguments.tokenName], "SHA-256");
		}
		return session[arguments.tokenName];
	}

	/* @tokenName:	name to validate against
	@keyValue:		value to validate
	@tokenScope:	session scope name to look in for the CSRFToken */
	public struct function validateCSRFToken(required string tokenName, required string keyValue, string tokenScope="csrfToken") output=false hint="validate the given CSRFToken against the token stored in the session" {
		param name="session.#arguments.tokenScope#" default={};
		param name="session.locale" default="en_US";
		param name="session.debug" default=0;
		if(! IsDefined("resourcebundleService")) {
			resourcebundleService = new model.services.default.resourcebundleservice();
		}
		var textConstants = resourcebundleService.getResourceBundle("global", session.locale, session.debug);
		if (structKeyExists(session[arguments.tokenScope], tokenName) && hash(session[arguments.tokenScope][arguments.tokenName], "SHA-256") == arguments.keyValue) {
			structDelete(session[arguments.tokenScope], arguments.tokenName);
			return { result : true };
		} else {
			//delete the CSRFToken from the session scope
			structDelete(session[arguments.tokenScope], arguments.tokenName);
			//log the tampering
			logService.log({ class : "danger", text : "Invalid CSRF comparison on '#arguments.tokenName#'" });
			//give error message back to the controller
			return { result : false, class : "warning", text : textConstants.csrf };
		}
	}

	/* @rc: FW/1 request.context scope possibly containing the rc.fieldnames to be checked for XSS attempts */
	public struct function XSSProtect(required struct rc) output=false hint="checks the rc.fieldnames voor XSS attempts" {
		var rc = arguments.rc;
		if (structKeyExists(rc, "fieldnames") && ! rc.fieldNames.isEmpty()) {
			var fieldArr = listToArray(rc.fieldNames);
			for (var field in fieldArr) {
				rc[field] = canonicalize(serialize(rc[field]),true,true);
				rc[field] = evaluate(rc[field]);
			}
		}
		return rc;
	}

	public any function XSSProtectException(required string strValue) output=false hint="returns the exception value" {
		var strReturn = arguments.strValue;
		if (isSimpleValue(strReturn)) {
			if (structKeyExists(config, "allowSpecialCharacters") && arrayFind(config.allowSpecialCharacters, "backslash") >= 1) {
				strReturn = REReplace(strReturn, "\\", "&##92", "all");
			}
			if (structKeyExists(config, "allowSpecialCharacters") && arrayFind(config.allowSpecialCharacters, "forwardslash") >= 1) {
				strReturn = REReplace(strReturn, "\/", chr(47), "all");
			}
		}
		return strReturn;
	}

	public void function defaultSession() output=false hint="opnieuw opbouwen van een sessie met default waardes, maar eerst de bestaande sessie invalideren (geeft nieuw id aan application sessies (CFID), werkt niet bij JEE sessies)" {
		sessionInvalidate();
		session.locale = "nl_NL";
		session.debug = 0;
		session.auth = {
			isLoggedIn : false
			, roles : "public"
			, userId : "00000000-0000-0000-0000-000000000000"
		};
	}

	public struct function decodeScope(required struct scope) output=false hint="check of er giftige invoer opgestuurd wordt en zo ja maak de inhoud van dat element leeg" {
		var key = "";
		for (key in arguments.scope) {
			if (IsSimpleValue(arguments.scope[key])) {
				try {
					/* do not allow multiple and mixed encodings, most likely an attack */
					arguments.scope[key] = canonicalize(arguments.scope[key], true, true);
				} catch (any cfcatch) {
					/* if logging actual string, encode it properly for context. Truncating cfcatch.logmessage so value is not included
					cflog(text="key: #Left(cfcatch.logmessage, Find(' in', cfcatch.logmessage))#", application=true, file="encodingErrors", type="error"); */
					arguments.scope[key] = "";
				}
			}
		}
		return arguments.scope;
	}

	/* v2 by Joseph Flanigan (joseph@switch-box.org)
	@param table:		Table to export. (Required)
	@param dbsource:	DSN. (Required)
	@param dbuser:		Database username. (Optional)
	@param dbpassword:	Database password. (Optional)
	@param commitAfter: Inserts commit statements after a certain number of rows. Defaults to 100. (Optional)
	@return:			Returns a string.
	@author:			Asif Rashid (joseph@switch-box.orgasifrasheed@rocketmail.com)
	@version:			2, April 18, 2006 */
	public string function exportSQLTable(required string table, numeric commitAfter=100) output=false hint="Export table data in script format (INSERT statements)." {
		var i = 1;
		var temp = "";
		var str = "";
		var queryService = new Query();
		// Getting table data
		var qryTemp = queryService.setSQL("select * from #arguments.table#").execute().getResult();
		// Getting meta information of executed query
		var tempCol = getMetaData(qryTemp);
		var k = ArrayLen(tempCol);
		var qryCount = 1;
		for (record in qryTemp) {
			temp = "INSERT INTO " & arguments.table &" (";
			for (var j=1; j <= k;j++) {
				temp = temp & "[#tempCol[j].Name#]";
				if (j < k) {
					temp = temp & ",";
				}
			}
			temp = temp & ") VALUES (";
			for (var j=1; j <= k;j++) {
				if (findNoCase("char", tempCol[j].TypeName)
					|| findNoCase("date", tempCol[j].TypeName)
					|| findNoCase("text", tempCol[j].TypeName)
					|| findNoCase("unique", tempCol[j].TypeName)
					|| findNoCase("xml", tempCol[j].TypeName)) {
					var textstr = qryTemp[tempCol[j].Name][i];
					if (find("'",textstr)) {
						textstr = Replace(textstr,"'","'","ALL");
					}
					if (len(textstr)) {
						temp = temp & "'" & REReplaceNoCase(textstr,"'","''","all") & "'";
					} else {
						temp &= 'NULL';
					}
				} else if (findNoCase("image",tempCol[j].TypeName)) {
					temp = temp & "'";
				} else {
					textstr = qryTemp[#tempCol[j].Name#][i];
					if (len(textstr)) {
						temp = temp & textstr;
					} else {
						temp &= 'NULL';
					}
				}
				if (j < k) {
					temp = temp  &  ",";
				}
			}
			temp = temp & ");";
			str = str & temp & chr(10);
			if (i mod commitAfter == 0) {
				str = str & "commit;" & chr(10);
			}
			i = i + 1;
		}
		return str;
	}

	public boolean function isRequestSecure() output=false hint="returns if a ssl connection is used or not" {
		try {
			var objRequest = getPageContext().getRequest();
			return objRequest.isSecure();
		} catch (any a) {
			if (cgi.https == "on") {
				return true;
			}
			return false;
		}
	}

	public string function getLocalAddress() output=false hint="returns the proper current used url address - LET OP! Dit werkt niet in de nieuwe reverse proxy situatie, geeft altijd .domeinnaam.local terug... Dus liever NIET gebruiken!" {
		try {
			var objRequest = getPageContext().getRequest();
			/* op DEV geeft getLocalAddr() om de een of andere reden altijd leeg/niets terug, vandaar deze if/else constructie */
			return IsNull(objRequest.getLocalAddr()) ? listFirst(cgi.http_host, ":") : objRequest.getLocalAddr();
		} catch (any a) {
			return listFirst(cgi.http_host, ":");
		}
	}

	private string function getRemoteIp() output=false hint="returns a remote IP" {
		var remote_ip = cgi.remote_addr;
		var headers = GetHttpRequestData().headers;
		if(structKeyExists(headers, "X-Forwarded-For")) {
			remote_ip = listGetAt(headers["X-Forwarded-For"], 1);
		}
		return remote_ip;
	}

	/* @buildUrlString:	a string formated by fw/1 buildUrl() */
	public string function buildFullUrl(required string buildUrlString) output=false hint="returns the full url based on given FW/1 action" {
		var urlString = application.applicationUrl;/* contains a trailing slash */
		/* if building a shortUrl string and the applicationUrl has a trailing slash, then the trailing slash needs to be slashed off ;-) */
		if(Find("/go/to/url/", arguments.buildUrlString) && Right(urlString, 1) == "/") {
			urlString = Left(urlString, Len(urlString)-1);
		}
		urlString &= arguments.buildUrlString;
		return urlString;
	}

	/* @data: the data to clean */
	public any function cleanInputData(required any data) output=false hint="cleans the given data element so it doesn't contain any objects or components anymore" {
		var cleanData = {};
		if(isSimpleValue(arguments.data)) {
			cleanData = arguments.data;
		} else if(! isObject(arguments.data) && ! isValid("component", arguments.data)) {
			if(isValid("array", arguments.data)) {
				cleanData = [];
				for (var arrItem in arguments.data) {
					arrayAppend(cleanData, cleanInputData(arrItem));
				}
			} else if(isValid("struct", arguments.data)) {
				cleanData = {};
				for (var structItem in arguments.data) {
					cleanData[structItem] = cleanInputData(arguments.data[structItem]);
				}
			}
		}
		return cleanData;
	}

	public string function cleanString(required string inputString) output=false hint="encode eerst string voor HTML zodat deze veilig weggeschreven en getoond kan worden en zet daarna aantal opgeschoonde leestekens terug" {
		var elementCleaned = EncodeForHTML(arguments.inputString);
		elementCleaned = Replace(elementCleaned, "&##xd;&##xa;", "#chr(13)##chr(10)#", "ALL");
		elementCleaned = Replace(elementCleaned, "&##x3f;", "?", "ALL");
		elementCleaned = Replace(elementCleaned, "&##x21;", "!", "ALL");
		elementCleaned = Replace(elementCleaned, "&##x3a;", ":", "ALL");
		return elementCleaned;
	}

	public string function cleanStringRich(required string inputString, string tagsAllowed="", string replaceTagsWithLineBreaks="") output=false hint="verwijderd alle tags die niet toegestaan zijn en biedt mogelijkheid tot toevoegen van tags waarvan de sluit-tag in plaintext versie van e-mails een linebreak moeten triggeren" {
		var elementCleaned = arguments.inputString;
		var elementTagsAllowed = arguments.tagsAllowed;
		/* list of all single html-tags */
		var html_tag_list_singles = "br,hr,img";
		loop list="#html_tag_list_singles#" index="html_single" delimiters="," {
			/* if tag is allowed, do not remove it from string */
			if ((elementTagsAllowed == "") || (! ListFindNoCase(elementTagsAllowed, html_single, ","))) {
				/* if tag is to be deleted, but must conserve linebreak, do this here */
				if(ListFind(replaceTagsWithLineBreaks, html_single)) {
					/* remove tag and all parameter="value" pairs from string, but add a linebreak */
					elementCleaned = REReplaceNoCase(elementCleaned, "<#html_single#[^>]*>", "#chr(13)##chr(10)#", "ALL");
				/* remove tag (and all parameter="value" pairs) from string */
				} else {
					elementCleaned = REReplaceNoCase(elementCleaned, "<#html_single#[^>]*>", "", "ALL");
				}
			}
		}
		/* list of all other html-tags */
		var html_tag_list = "a,abbr,acronym,b,blockquote,bold,col,colgroup,dd,del,div,dl,dt,em,font,form,frame,i,iframe,html,head,input,ins,li,link,meta,noscript,ol,option,p,script,select,small,span,strong,style,sub,sup,table,tbody,tfoot,td,th,thead,tr,textarea,title,ul,video";
		loop list="#html_tag_list#" index="html_tag" delimiters="," {
			/* if tag is allowed, do not remove it from string */
			if ((elementTagsAllowed == "") || (! ListFindNoCase(elementTagsAllowed, html_tag, ","))) {
				/* remove begintag (and all parameter="value" pairs) from string */
				elementCleaned = REReplaceNoCase(elementCleaned, "<#html_tag#[^>]*>", "", "ALL");
				/* if tag is to be deleted, but must conserve linebreak, do this here */
				if(ListFind(replaceTagsWithLineBreaks, html_tag)) {
					/* remove tag from string, but add linebreak */
					elementCleaned = REReplaceNoCase(elementCleaned, "</#html_tag#>", "#chr(13)##chr(10)#", "ALL");
				/* remove endtag from string */
				} else {
					elementCleaned = ReplaceNoCase(elementCleaned, "</#html_tag#>", "", "ALL");
				}
			}
		}
		return elementCleaned;
	}

	/**
	 * Removes HTML from the string.
	 * v2 - Mod by Steve Bryant to find trailing, half done HTML.
	 * v4 mod by James Moberg - empties out script/style blocks
	 * v5 mod by dolphinsboy
	 *
	 * @param string String to be modified. (Required)
	 * @return Returns a string.
	 * @author Raymond Camden (ray@camdenfamily.com)
	 * @version 4, October 4, 2010
	public string function stripHTML(required string str) output=false hint="verwijderd alle opgegeven HTML-tags uit een string - https://cflib.org/udf/stripHTML" {
		// remove the whole tag and its content
		var list = "style,script,noscript";
		var str = arguments.str;
		for (var tag in list) {
			str = REReplaceNoCase(str, "<s*(#tag#)[^>]*?>(.*?)", "", "all");
		}
		str = REReplaceNoCase(str, "<.*?>", "", "all");
		// get partial html in front
		str = REReplaceNoCase(str, "^.*?>", "");
		// get partial html at end
		str = REReplaceNoCase(str, "<.*$", "");
		return Trim(str);
	} */

	public string function removeDiacritters(required string inputString) output=false hint="vervangt alle leestekens in string met normale letters" {
		local.diacritisch_teller = 0;
		local.diacritisch_list = "�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�";
		local.diacritisch_list_clean = "A,a,A,a,A,a,A,a,A,a,C,c,E,e,E,e,E,e,E,e,I,i,I,i,I,i,I,i,N,n,O,o,O,o,O,o,O,o,O,o,U,u,U,u,U,u,U,u,Y,y,y,AE,ae,OE,oe,AA,aa,EUR";
		loop index="local.vreemde_tekens" list="#local.diacritisch_list#" delimiters="," {
			if(Find(local.vreemde_tekens, arguments.inputString)) {
				arguments.inputString = Replace(arguments.inputString, local.vreemde_tekens, ListGetAt(local.diacritisch_list_clean, local.diacritisch_teller, ","), "ALL");
			}
			local.diacritisch_teller++;
		}
		return arguments.inputString;
	}

	/* @size: size in bytes */
	public string function fileSize(required numeric size) output=false hint="Returns the given size in a human readable format (b,Kb,Mb)" {
		if (arguments.size < 1024) {
			return "#arguments.size# b";
		}
		if (arguments.size < 1024^2) {
			return "#round(arguments.size / 1024)# Kb";
		}
		if (arguments.size < 1024^3) {
			return "#decimalFormat(arguments.size/1024^2)# Mb";
		}
		return "#decimalFormat(arguments.size/1024^3)# Gb";
	}

	public any function checkSubSystems(required array arrExclude) {
		var arrayReturn = [];
		var arrayExcluded = arguments.arrExclude;
		var dirList = directoryList(expandPath("./"), false, "query");
		for (var elem in dirList) {
			if (elem.type == "Dir" && ! arrayExcluded.find(elem.name)) {
				arrayAppend(arrayReturn, elem.name);
			}
		}
		return arrayReturn;
	}

	public binary function createPDF(
		required string pdfcontent
		, struct pdfsettings={}
		, string pdfheader=""
		, string pdffooter=""
		, string pdfheadfootid="0"
		) output=false hint="" {
		var pathTmp = getTempDirectory();
		var headerHtmlFileLocation = "";
		var footerHtmlFileLocation = "";
		/* maak losse html-bestanden voor de PDF header en footer */
		if(arguments.pdfheader != "") {
			var pdfHeader = "<!doctype html><html><body>#arguments.pdfheader#</body></html>";
			var myHeaderFile = "#pathTmp#/pdfHeader-#arguments.pdfheadfootid#.html";
			FileWrite(myHeaderFile,pdfHeader);
			headerHtmlFileLocation = "#pathTmp#pdfHeader-#arguments.pdfheadfootid#.html";
			StructInsert(arguments.pdfsettings, "header-html", headerHtmlFileLocation);
		}
		if(arguments.pdffooter != "") {
			var pdfFooter = "<!doctype html><html><body>#arguments.pdffooter#</body></html>";
			var myFooterFile = "#pathTmp#/pdfFooter-#arguments.pdfheadfootid#.html";
			FileWrite(myFooterFile,pdfFooter);
			footerHtmlFileLocation = "#pathTmp#pdfFooter-#arguments.pdfheadfootid#.html";
			if(StructKeyExists(arguments.pdfsettings, "footer-html")) {
				StructUpdate(arguments.pdfsettings, "footer-html", footerHtmlFileLocation);
			} else {
				StructInsert(arguments.pdfsettings, "footer-html", footerHtmlFileLocation);
			}
		}
		/* maak PDF aan en geef deze als binair object terug */
		var wkhtmltopdf = new model.services.default.wkhtmltopdf();
		return wkhtmltopdf.fromString(str=arguments.pdfcontent,options=arguments.pdfsettings);
	}

	/* @email: string for the e-mailaddress | @saltstring: string with the saltstring where the e-mailaddress belongs to */
	public string function returnValidEmailAddress(string email="", string saltstring="", string encryptKey="") output=false hint="returns a plain e-mailaddress; if it is not a plain e-mailaddress, try to decrypt the string" {
		var returnString = arguments.email;
		var saltstring = arguments.saltstring;
		var encryptKey = arguments.encryptKey;
		if (returnString != "" && saltstring != "" && encryptKey != "") {
			var isValidEmailaddress = isValid("email", returnString) ? true : false;
			returnString = !isValidEmailaddress ? decryptString(encryptedString=returnString, key=encryptKey, salt=left(saltstring, 16)) : returnString;
			// double check...
			return isValid("email", returnString) ? returnString : "";
		} else {
			return "";
		}
	}

	/*
	=======
	voorbeeldcode om query naar CSV te converteren en vervolgens in browser aan te bieden aan gebruiker:
	=======
		var csvString = utilityService.convertQueryToCsv(query=csvQuery, headerRow=1);
		var csvBinary = ToBinary(ToBase64(csvString.Trim()));
		var csvFileName = "data_voor_website_#DateFormat(Now(),'yyyymmdd')#_#TimeFormat(Now(),'HHmmss')#.csv";
		cfheader(name="Content-Disposition", value="attachment; filename=#csvFileName#");
		cfcontent(type="text/csv", variable="#csvBinary#", reset="true");
	*/
	public string function convertQueryToCsv(required query query, numeric headerRow=0, delimiter=",") output=false hint="https://gist.github.com/cfsimplicity/e1b5a4f1db6b54b77163f8f03f88f346" {
		var result = CreateObject("Java", "java.lang.StringBuilder").init();
		var crlf = Chr(13) & Chr(10);
		var columns = query.ColumnArray();
		var hasHeaderRow = (Val(headerRow));
		if(hasHeaderRow) {
			result.Append(this.generateCsvRow(columns, delimiter));
		}
		for(var row in query) {
			var rowValues = [];
			for(var column in columns) {
				rowValues.Append(row[column]);
			}
			result.append(crlf & this.generateCsvRow(rowValues, delimiter));
		}
		return result.toString().Trim();
	}

	private string function generateCsvRow(required array values, required string delimiter) output=false hint="https://gist.github.com/cfsimplicity/e1b5a4f1db6b54b77163f8f03f88f346" {
		var result = CreateObject("Java", "java.lang.StringBuilder").init();
		for(var value in values) {
			value = Replace(value, '"', '""', "ALL");//can't use member function in case its a non-string
			result.append('#delimiter#"#value#"');
		}
		return result.toString().substring(1);
	}

	public array function query2Array(query data) output=false hint="change query into array" {
		var arrayReturn = [];
		if (! isNull(arguments.data)) {
			var inputData = arguments.data;
			var columns = ListToArray(inputData.ColumnList);
			for (elem in inputData) {
				if (structKeyExists(elem, "groupName")) {
					elem.groupName = dataDecryptString(elem.groupName);
				}
				arrayAppend(arrayReturn, elem);
			}
		}
		return arrayReturn;
	}

	public string function buildMessage(required struct valid) output=false hint="build a return message for the page" {
		var message = "<ul>";
		for (var structItem in arguments.valid) {
			message &= "<li>" & arguments.valid[structItem].message & "</li>";
		}
		message &= "</ul>";
		return message;
	}

	public string function getOriginAction(required struct fw, string removeFromString="process") output=false hint="ophalen request / pagina / actie vanwaar gebruiker vandaan kwam, om eventueel zonder bepaalde parameters weer terug te kunnen sturen" {
		return replace(arguments.fw.getFullyQualifiedAction(), arguments.removeFromString, "");
	}

}