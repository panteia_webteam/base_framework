component accessors=true extends="dao" output=false hint="" {

	property bugLogService;
	property utilityService;

	variables.objName = "log";

	/* @data request.context (RC) scope which should contain a message struct {class="",text=""}
	@extensive tells the function to serialize all the data and to save it to the database
	@serializeData specific data element to serialize and save to the database */
	public void function log(required struct data, boolean extensive=0, struct serializeData) hint="logs the action to the DB based on the given data" {
		try {
			transaction {
				var logObj = entityNew("log");
				//save message and log type based on class
				if (structKeyExists(arguments.data, "message")) {
					if(structKeyExists(arguments.data.message, "text")) {
						logObj.setDescription(arguments.data.message.text);
					}
					if(structKeyExists(arguments.data.message, "class")) {
						logObj.setClass(arguments.data.message.class);
					}
				}
				//save save action
				if (structKeyExists(arguments.data, "action")) {
					logObj.setAction(arguments.data.action);
				}
				//save the data serialized in the database
				if (arguments.extensive) {
					var dataStruct = utilityService.cleanInputData(arguments.data);
					//save all the data serialized in the log
					logObj.setSerializedData(serialize(dataStruct));
				} else if (structKeyExists(arguments, "serializeData") && isStruct(arguments.serializeData)) {
					//log just the specified data element
					logObj.setSerializedData(serialize(arguments.serializeData));
				}
				//iphash
				logObj.setIp_hash(left(hash(cgi.remote_addr), 7));
				//save log
				entitySave(logObj);
			}
 		} catch (any a) {
			//FOUTOPVANGEN buglog
			bugLogService.notifyService(a.message, a);
		}
	}

}