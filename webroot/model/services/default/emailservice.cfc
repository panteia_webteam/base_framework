﻿component accessors=true output=false hint="" {

	property config;
	property utilityService;

	/* @emailTo: email address to send message to
	@subject: subject of the email - will show in the subjectline and in the email, if the email template has a title object
	@contentHTML: HTML formatted content to be sent - take care to use inline styles and table formatting to create the wanted look and feel
	@contentPlain: Plain text content
	@shortintroduction: Short introduction that will show in the email client - most email clients won't show it in the actual email, just in the summary
	@headerImage: Defaults to "Handen" image, can be swapped out for another image by referencing to it (/assets/gfx/email_images/?)
	@emailFrom sender of the email, defaults to config setting
	@emailFail email address to be sent to if the email fails, defaults to config setting
	@emailCC optional email address to be sent to in CC
	@emailBCC optional email address to be sent to in BCC
	@templateHTML HTML template to use for the email
	@templatePlain Plain text template to use for the email */

	public void function sendEmail(
		required string emailTo
		, required string subject
		, required string contentHTML
		, string contentPlain
		, string shortintroduction=""
		, string headerImage=""
		, string headerImageLocalId=""
		, string headerImageLocalPath=""
		, string emailFrom=config.mail.from
		, string emailFail=config.mail.fail
		, string emailCC
		, string emailBCC
		, string templateHTML=config.mail.template.html
		, string templatePlain=config.mail.template.plain
		, string stringUrl=application.applicationUrl
		, string attachmentExcel
		, string attachmentPDF
		) hint="send a parameterized e-mail" {
		// load email templates
		var localTemplatePlain = "";
		var localTemplateHTML = "";
		savecontent variable="localTemplateHTML" {
			include arguments.templateHTML;
		}
		savecontent variable="localTemplatePlain" {
			include arguments.templatePlain;
		}
		/* replace variable strings in HTML-template
			- {HEADERIMAGE}
			- {SHORTINTRODUCTION}
			- {TITLE}
			- {BODY}
			- URL */
		localTemplateHTML = Replace(localTemplateHTML, "{HEADERIMAGE}", arguments.headerImage, "ALL");
		localTemplateHTML = Replace(localTemplateHTML, "{SHORTINTRODUCTION}", arguments.shortintroduction, "ALL");
		localTemplateHTML = Replace(localTemplateHTML, "{TITLE}", utilityService.cleanStringRich(inputString=arguments.subject), "ALL");
		localTemplateHTML = Replace(localTemplateHTML, "{BODY}", arguments.contentHTML, "ALL");
		localTemplateHTML = Replace(localTemplateHTML, "{URL}", arguments.stringUrl, "ALL");
		/* replace variable strings in plaintext-template
			- {TITLE}
			- {BODY} */
		if (structKeyExists(arguments, "contentPlain") && len(arguments.contentPlain)) {
			localTemplatePlain = Replace(localTemplatePlain, "{TITLE}", utilityService.cleanStringRich(inputString=arguments.subject), "ALL");
			localTemplatePlain = Replace(localTemplatePlain, "{BODY}", utilityService.cleanStringRich(inputString=arguments.contentPlain), "ALL");
		}
		// create mail object
		var emailService = new Mail()
			.setSubject(utilityService.cleanStringRich(inputString=arguments.subject))
			.setTo(arguments.emailTo)
			.setFrom(arguments.emailFrom)
			.setFailto(arguments.emailFail);
		if (structKeyExists(arguments, "contentPlain") && len(arguments.contentPlain)) {
			emailService.addPart(type="text", charset="utf-8", wraptext="200", body=localTemplatePlain);
		}
		emailService.addPart(type="html", charset="utf-8", body=localTemplateHTML);
		// set optional parameters
		if (structKeyExists(arguments, "emailCC")) {
			emailService.setCC(arguments.emailCC);
		}
		if (structKeyExists(arguments, "emailBCC")) {
			emailService.setBcc(arguments.emailBCC);
		}
		if (structKeyExists(arguments, "attachmentExcel")) {
			emailService.addParam(file=arguments.attachmentExcel, type="application/vnd.ms-excel");
		}
		if (structKeyExists(arguments, "attachmentPDF")) {
			emailService.addParam(file=arguments.attachmentPDF, type="application/pdf");
		}
		// moet er een header plaatje ge-embed worden?
		if (structKeyExists(arguments, "headerImage") && Find("cid:", arguments.headerImage) && Trim(arguments.headerImageLocalPath) != "" && Trim(arguments.headerImageLocalId) != "") {
			emailService.addParam(disposition="inline", file=arguments.headerImageLocalPath, contentid=arguments.headerImageLocalId);
		}
		emailService.send();
	}

}