﻿component accessors=true extends="dao" output=false hint="" {

	property utilityService;

	variables.msgFormat = createObject("java", "java.text.MessageFormat");
	variables.locale = createObject("java", "java.util.Locale");
	variables.objName = "resourceBundle";

	/* @bundle : container name for a group of translation resources (login,admin,etc), gives te opportunity to use the same resource/item name for multiple purposes.
	@locale : the locale to return (nl_NL,en_US,etc)
	@debug : surrounds the return string with '****' and adds the item to the string so you can easily see what resources are used on a page */
	public struct function getResourceBundle(required string bundle, string locale=variables.locale, boolean debug=false) output=false hint="reads and parses the resource bundle from the database" {
		var keys = "";// var to hold rb keys
		var resourceBundle = {};// structure to hold resource bundle
		var thisKey = "";
		var thisMSG = "";
		var sqlString = "
			SELECT NEW MAP (item as item, text as text)
			FROM resourcebundle
			WHERE bundle = :bundle
			AND isActive = 1
		";
		//check if data present based on bundle and locale
		var bundle = ormExecuteQuery(sqlString & " AND locale = :locale", { bundle : arguments.bundle, locale : arguments.locale });
		if (arrayIsEmpty(bundle)) {
			//check if data present based on bundle and just language based on locale
			bundle = ormExecuteQuery(sqlString & " AND locale = :locale", { bundle : arguments.bundle, locale : listFirst(arguments.locale, "_") });
		}
		if (arrayIsEmpty(bundle)) {
			//check if data present based only on bundle
			bundle = ormExecuteQuery(sqlString, { bundle : arguments.bundle });
		}
		if (! arrayIsEmpty(bundle)) {
			for (var element in bundle) {
				resourceBundle[element['item']] = arguments.debug ? '****' & element['text'] & ' (' & element['item'] & ')****' : element['text'];
			}
			return resourceBundle;
		} else {
			throw(message="Resource Bundle and locale combination is not found (#arguments.bundle# / #arguments.locale#).");
		}
	}

	/* @bundle : container name for a group of translation resources (login,admin,etc), gives te opportunity to use the same resource/item name for multiple purposes.
	@locale : the locale to return (nl_NL,en_US,etc) */
	public array function getRBKeys(required string bundle, string locale=variables.locale) output=false hint="returns array of keys in java resource bundle per locale" {
		return structKeyArray(getResourceBundle(argumentCollection=arguments));
	}

	/**
	* @hint : returns text for given key in given java resource bundle per locale
	* @bundle : container name for a group of translation resources (login,admin,etc), gives te opportunity to use the same resource/item name for multiple purposes.
	* @key : the key name to return from the resource bundle
	* @locale : the locale to return (nl_NL,en_US,etc)
	**/
	public any function getRBString(required string bundle, required string key, string locale=session.locale, boolean cleanString=false, string tagsAllowed="") {
		var rb = getResourceBundle(argumentCollection=arguments);
		if (! structKeyExists(rb, arguments.key)) {
			resourcebundleObj = entityNew("resourcebundle");
			resourcebundleObj.setBundle(arguments.bundle);
			resourcebundleObj.setItem(arguments.key);
			resourcebundleObj.setLocale(arguments.locale);
			resourcebundleObj.setText(arguments.key);
			entitySave(resourcebundleObj);
			ormFlush();
			return arguments.key;
		}
		// if the string contains HTML and this is not necessary or a hinderance, it will be cleaned completely here (or according to list of allowed tags)
		if(arguments.cleanString) {
			return utilityService.cleanStringRich(inputString=rb[arguments.key], tagsAllowed=arguments.tagsAllowed);
		}
		return rb[arguments.key];
	}

	/* @pattern : the pattern containing the arguments to be replaced
	@args : the arguments which have to be inserted in the given locale in the string - list or array
	@local : locale to use in formatting, defaults to nl_NL */
	public string function messageFormat(required string pattern, required any args, string locale="nl_NL") output=false hint="performs messageFormat on compound rb string" {
		var javaPattern = createObject("java", "java.util.regex.Pattern");
		var regexStr = "(\{[0-9]{1,},number.*?\})";
		var matchedGroupIndex = 0;
		var inputArgs = duplicate(arguments.args);
		var lang = listFirst(arguments.locale, "_");
		var country = "";
		var variant = "";
		try {
			// identify locale
			if (listLen(arguments.locale, "_") > 1) {
				country = listGetAt(arguments.locale, 2, "_");
				variant = listLast(arguments.locale, "_");
			}
			var tLocale = variables.locale.init(lang, country, variant);
			// cast to array
			if (! isArray(inputArgs)) {
				inputArgs = listToArray(inputArgs);
			}
			// match elements of numerical type before MessageFormat handles them
			var thisFormat = msgFormat.init(arguments.pattern, tLocale);
			// let's make sure any cf numerics are cast to java datatypes to prevent type errors
			var p = javaPattern.compile(regexStr, javaPattern.CASE_INSENSITIVE);
			var m = p.matcher(arguments.pattern);
			while (m.find()) {
				matchedGroupIndex = listFirst(replace(m.group(), "{", ""));
				inputArgs[matchedGroupIndex] = javacast("float", inputArgs[matchedGroupIndex]);
			}
			arrayPrepend(inputArgs, "");// offset element - Java starts at 0, ColdFusion at 1 - use offset element to be element 0
			// coerece to a java array of objects
			return thisFormat.format(inputArgs.toArray());
		} catch(any a) {
			throw(message=a.message, type="any", detail=a.detail);
		}
	}

	/* @pattern : the pattern to verify against java.text.MessageFormat */
	public string function verifyPattern(required string pattern) output=false hint="performs verification on MessageFormat pattern" {
		try {
			test = variables.msgFormat.init(arguments.pattern);
			return true;
		} catch (any e) {
			return false;
		}
	}

}