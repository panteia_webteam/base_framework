﻿component accessors=true output=false hint="handles default actions for the initialized object" {

	public array function getArrBy(struct getBy={ field:"", value:"" }, string orderBy="", boolean isActive=1) hint="returns an array with data elements in a structure, a structure with one field and one value can be given to filter the data ({field:'answergroup',value:'janee'})" {
		var filter = { isActive : arguments.isActive };
		var strQuery = "
			SELECT NEW MAP (
				id AS id
				, isActive AS isActive
				, createDate AS createDate
				, createdBy AS createdBy
				, updateDate AS updateDate
				, updatedBy AS updatedBy
		";
		var arrPropertyList = getObj().getProperties();
		var item = "";
		var propertyListCount = arrayLen(arrPropertyList);
		for (var i=1; i<=propertyListCount; i++) {
			item = arrPropertyList[i];
			if (! structKeyExists(item, "fieldtype")) {
				strQuery &= ", " & item.name & " AS " & item.name;
			}
		}
		strQuery &= "
			) FROM #variables.objName#
			WHERE 0 = 0
			AND isActive = :isActive
		";
		if (len(arguments.getBy.field) && len(arguments.getBy.value)) {
			strQuery &= " AND #arguments.getBy.field# = :#arguments.getBy.field#";
			filter[arguments.getBy.field] = arguments.getBy.value;
		}
		if (structKeyExists(arguments, "strSelect")) {
			strQuery &= " " & arguments.strSelect;
		}
		if (len(arguments.orderBy)) {
			strQuery &= " ORDER BY " & arguments.orderBy;
		} else {
			strQuery &= " ORDER BY createDate desc";
		}
		return ormExecuteQuery(strQuery, filter);
	}

	public any function getObj(any id="") output=false hint="Returns the object or an empty one" {
		if (! arguments.id.isEmpty() && isValid("GUID", arguments.id) && len(arguments.id) == 36) {
			var resultObj = entityLoad(variables.objName, arguments.id, true);
			if(! isNull(resultObj)) {
				return resultObj;
			}
		}
		resultObj = entityNew(variables.objName);
		return resultObj;
	}

	public any function getObjBy(required struct getBy, boolean isActive=1) output=false hint="requires a struct where the key is the fieldname and the value will be searched on" {
		if (structCount(arguments.getBy)) {
			arguments.getBy.isActive = arguments.isActive;
			var resultObj = entityLoad(variables.objName, arguments.getBy, true);
			if(! isNull(resultObj)) {
				return resultObj;
			}
		}
		resultObj = getObj();
		return resultObj;
	}

	public array function getObjsByIds(required string idList, boolean isActive=1) output=false hint="requires a list with Id's" {
		var arrIds = listToArray(arguments.idList);
		if (arrIds.len) {
			var objs = ormExecuteQuery("
				FROM #lCase(variables.objName)#
				WHERE id IN (:idList)
				AND isActive = :isActive
			", {
					idList : arrIds
					, isActive : arguments.isActive
				}
			);
			return objs;
		}
		return [];
	}

	public any function findAll(
		numeric isActive=1
		, boolean asQuery=false
		, boolean asStruct=false
		, struct getBy={}
		, string structField
		) output=false hint="requires a struct where the key is the fieldname and the value will be searched on" {
		if (arguments.isActive != -1) {
			arguments.getBy.isActive = arguments.isActive; //true/1 (default), show only active records; false/0, show only inactive records; -1, show all records active and inactive
		}
		var results = entityLoad(variables.objName, arguments.getBy);
		if(arguments.asQuery) {
			return entityToQuery(results);
		}
		if(arguments.asStruct) {
			var strResults = {};
			for (i=1; i <= arrayLen(results); i=i+1)  {
				if (! isDefined("arguments.structField")) {
					strResults[results[i].getId()] = results[i];
				} else {
					strResults[evaluate("results[i].get#arguments.structField#()")] = results[i];
				}
			}
			return strResults;
		}
		return results;
	}

	public void function save(required any obj) hint="saves the given object" {
		transaction {
			entitySave(arguments.obj);
			ormFlush();
		}
	}

	public void function saveAndClear(required any obj) {
		transaction {
			entitySave(arguments.obj);
			ormFlush();
			ormClearSession();
		}
	}

	public void function delete(required any obj) output=false hint="" {
		transaction {
			arguments.obj.setIsActive(-1);
			entitySave(arguments.obj);
			ormFlush();
		}
	}

}