﻿component accessors=true output=false hint="" {

	property name="config";

	public void function sendErrorMail(required struct rc, required string mailError, required string applicationName) output=false hint="" {
		var failAct = structKeyExists(request, "failedAction") ? request.failedAction : "unknown";
		var listAct = structKeyExists(arguments.rc, "action") ? "<li>Action: #arguments.rc.action#</li>" : "";
		var htmlContent = WriteOutput("
			<h1>An Error Occurred (Sent at: #lsDateFormat(now(), 'long', 'nl_NL')# at #lsTimeFormat(now(), 'long', 'nl_NL')#)</h1>
			<p>Details of the exception:</p>
			<ul>
				<li>Failed action: #failAct#</li>
				<li>Application event: #request.event#</li>
				<li>Exception type: #request.exception.type#</li>
				<li>Exception error: #request.exception.cause.message#</li>
				<li>Exception message: #request.exception.message#</li>
				<li>Exception detail: #request.exception.detail#</li>
				#listAct#
			</ul>
		");
		if(structKeyExists(arguments.rc, "errorcode")) {
			htmlContent &= WriteOutput("
				<p>In case you want to convey this error to our helpdesk, please include this errorcode:</p>
				<ul>
					<li>Errorcode: #arguments.rc.errorcode#</li>
				</ul>
			");
		}
		if(structKeyExists(arguments.rc, "currentUser")) {
			htmlContent &= WriteOutput("
				<h2>User</h2>
				<ul>
					<li>id: #rc.currentUser.getId()#</li>
			");
			loop collection="#rc.currentUser.getPropertyValuesStruct()#" item="item" {
				htmlContent &= WriteOutput("<li>#item#: #rc.currentUser.getPropertyValuesStruct()[item]#</li>");
			}
			htmlContent &= WriteOutput("</ul>");
		}
		htmlContent &= WriteOutput("<h2>Error context</h2>");
		htmlContent &= WriteDump(var=request.exception.cause.tagContext, top=2);
		new Mail()
			.setSubject("#arguments.applicationName# error#(structKeyExists(arguments.rc, 'errorcode') ? ': '&arguments.rc.errorcode : '')# #(structKeyExists(request, 'failedAction') ? '; '&request.failedAction : '')#")
			.setTo(arguments.mailError)
			.setFrom(arguments.mailError)
			.addPart(type="html", charset="utf-8", body=htmlContent)
			.send();
	}

}