component accessors=true extends="dao" output=false hint="" {

	variables.objName = "menuitem";

	public query function getTopMenu() hint="get all top menu items" {
		return new Query()
			.setSQL("
				SELECT title AS title
					, action AS action
				FROM menuitem
				WHERE isActive <> -1
				AND menu = 'topMenu'
				ORDER BY listOrder asc
			")
			.execute()
			.getResult();
	}
}