﻿component accessors=true extends="dao" output=false hint="" {

	property config;
	property logService;
	property usersettingService;
	property utilityService;

	variables.objName = "user";

	public model.beans.default.user function getObj(string id="") {
		var userObj = super.getObj(arguments.id);
		if (isNull(userObj.getUsersetting())) {
			var usersetting = usersettingService.getObj();
			userObj.setUsersetting(usersetting);
			usersetting.setUser(userObj);
		}
		return userObj;
	}

	public void function save(model.beans.default.user obj) {
		transaction {
			arguments.obj.setFirstname(utilityService.dataEncryptString(arguments.obj.getFirstname()));
			arguments.obj.setPrefix(utilityService.dataEncryptString(arguments.obj.getPrefix()));
			arguments.obj.setLastname(utilityService.dataEncryptString(arguments.obj.getLastname()));
			arguments.obj.setGender(utilityService.dataEncryptString(arguments.obj.getGender()));
			arguments.obj.setEmail(utilityService.dataEncryptString(arguments.obj.getEmail()));
			super.save(arguments.obj);
			var settingsObj = arguments.obj.getUsersetting();
			// fill object with variables
			entitySave(settingsObj);
		}
	}

	public any function removeEncryption() {
		var qUsers = new Query()
			.setSQL("SELECT [id] FROM [user]")
			.execute()
			.getResult();
		for (var elem in qUsers) {
			var obj = getObj(elem.id);
			if (obj.getEncrypted()) {
				obj.setFirstname(utilityService.dataDecryptString(elem=obj.getFirstname(), protectInput=true));
				obj.setPrefix(utilityService.dataDecryptString(elem=obj.getPrefix(), protectInput=true));
				obj.setLastname(utilityService.dataDecryptString(elem=obj.getLastname(), protectInput=true));
				obj.setGender(utilityService.dataDecryptString(elem=obj.getGender(), protectInput=true));
				obj.setEmail(utilityService.dataDecryptString(elem=obj.getEmail(), protectInput=true));
				obj.setEncrypted(0);
			}
			save(obj);
		}
		return true;
	}

	public any function addEncryption() {
		var qUsers = new Query()
			.setSQL("SELECT [id] FROM [user]")
			.execute()
			.getResult();
		for (var elem in qUsers) {
			var obj = getObj(elem.id);
			if (!obj.getEncrypted()) {
				obj.setFirstname(obj.getFirstname());
				obj.setPrefix(obj.getPrefix());
				obj.setLastname(obj.getLastname());
				obj.setGender(obj.getGender());
				obj.setEmail(obj.getEmail());
				obj.setEncrypted(1);
			}
			save(obj);
		}
		return true;
	}

	public any function checkPanteiaLogin(model.beans.default.user userObj) {
		var messageStruct = {};
		var userObj = arguments.userObj;
		var varUsername = "LDAPSVC1";
		var varPassword = decrypt("1894BEAC4ECC3100A094D9B3B1812326", config.security.key, "AES", "HEX");
		var arrElements = ["userAccountControl"];
		ldap action="query"
			server="10.38.1.51"
			name="ldapResults"
			start="DC=panteia, DC=local"
			filter="(&(objectclass=user)(SamAccountName=#userObj.getNetwerkLoginName()#))"
			username="panteia\#varUsername#"
			password="#varPassword#"
			attributes="*";
		var structResult = {};
		for (elem in ldapResults) {
			if (arrElements.find(elem.name) > 0) {
				structResult["#elem.name#"] = elem.name == "sAMAccountName" ? LCase(elem.value) : elem.value;
			}
		}
		// if userAccountControl equals 514 then no longer working at Panteia...
		if (! structKeyExists(structResult, "userAccountControl") || (structKeyExists(structResult, "userAccountControl") && structResult.userAccountControl == "514")) {
			userObj.setIsActive(0);
			save(userObj);
		}
		var messageText = userObj.getIsActive() ? "user loggedin" : "user has no permissions";
		messageStruct.message = { class : "success", text : "remote login: #messageText#." };
		if (! userObj.getIsActive()) {
			logService.log(messageStruct, 0);
		}
		return userObj;
	}

}