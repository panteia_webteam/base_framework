component accessors=true output=false hint="" {

	property assetsLibrary;

	public struct function addAssetsToRC(required struct rc, required string locale) output=false hint="" {
		var assetsLibraryLocal = assetsLibrary.getAssetsLibrary();
		if(structKeyExists(arguments.rc, "features")) {
			// first clean up the list and remove features that are overridden
			var assetsStruct = {
				cleanfeatureArray : []
				, overridesArray : []
			};
			for(var featureName in arguments.rc.features) {
				assetsStruct = buildAssetsStruct(featureName, assetsStruct);
			}
			// remove the overrides from the clean list
			assetsStruct.cleanfeatureArray.removeAll(assetsStruct.overridesArray);
			// append the url's to the arrJS an arrCSS arrays.
			for(featureName in assetsStruct.cleanfeatureArray) {
				// add JS assets
				if (structKeyExists(assetsLibraryLocal[featureName], "JS")) {
					if (isArray(assetsLibraryLocal[featureName].JS)) {
						arguments.rc.arrJS.addAll(0, assetsLibraryLocal[featureName].JS);
					} else if (isStruct(assetsLibraryLocal[featureName].JS)) {
						arguments.rc.arrJS.addAll(0, assetsLibraryLocal[featureName].JS.files);
						//add localization assets
						if(structKeyExists(assetsLibraryLocal[featureName].JS, "localization")) {
							arrayAppend(arguments.rc.arrJS, assetsLibraryLocal[featureName].JS.localization[arguments.locale]);
						}
					}
				}
				// add CSS assets
				if (structKeyExists(assetsLibraryLocal[featureName], "CSS")) {
					arguments.rc.arrCSS.addAll(assetsLibraryLocal[featureName].CSS);
				}
			}
		}
		return arguments.rc;
	}

	private struct function buildAssetsStruct(required string featureName, returnStruct={ cleanfeatureArray : [], overridesArray : [] }) output=false hint="" {
		if(arrayFind(arguments.returnStruct.cleanfeatureArray, arguments.featureName) <= 0) {
			var assetsLibraryLocal = assetsLibrary.getAssetsLibrary();
			// only append if not in array
			arrayAppend(arguments.returnStruct.cleanfeatureArray, arguments.featureName);
			if(structKeyExists(assetsLibraryLocal, arguments.featureName) && structKeyExists(assetsLibraryLocal[arguments.featureName], "overrides")){
				// does this feature override another? add it to the overrideArray
				arguments.returnStruct.overridesArray.addAll(assetsLibraryLocal[featureName].overrides);
			}
			if(structKeyExists(assetsLibraryLocal, arguments.featureName) && structKeyExists(assetsLibraryLocal[arguments.featureName], "dependency")) {
				// load dependencies
				for(var featureName in assetsLibraryLocal[arguments.featureName].dependency) {
					arguments.returnStruct = buildAssetsStruct(featureName, arguments.returnStruct);
				}
			}
		}
		return arguments.returnStruct;
	}

}