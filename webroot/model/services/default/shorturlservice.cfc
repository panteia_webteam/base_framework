component accessors=true extends="dao" output=false hint="" {

	property config;
	property fw;
	property utilityService;

	variables.objName = "shortUrl";

	public string function shortenUrl(
		string action
		, string querystring
		, string fullUrl
		, string predefinedSalt=""
		) output=false hint="" {
		var previousQueryService = new Query();
		var sqlString = "
			select *
			from shorturl
			where 0 = 0
		";
		if (len(arguments.predefinedSalt)) {
			previousQueryService.addParam(
				name="querystring"
				, value=utilityService.encryptString(arguments.querystring, config.security.key, arguments.predefinedSalt)
				, cfsqltype="CF_SQL_VARCHAR"
			);
			sqlString &= " AND querystring = :querystring ";
		} else if (structKeyExists(arguments, "querystring") && len(arguments.querystring) >= 0) {
			previousQueryService.addParam(
				name="querystring"
				, value=utilityService.encryptString(arguments.querystring, config.security.key, config.security.mainSalt)
				, cfsqltype="CF_SQL_VARCHAR"
			);
			sqlString &= " AND querystring = :querystring ";
		}
 		if (len(arguments.action)) {
			previousQueryService.addParam(name="action", value=arguments.action, cfsqltype="CF_SQL_VARCHAR");
			sqlString &= " AND action = :action";
		} else {
			previousQueryService.addParam(name="fullUrl", value=arguments.action, cfsqltype="CF_SQL_VARCHAR");
			sqlString &= " AND action = :fullUrl";
		}
		var previousRecord = previousQueryService.execute(sql=sqlString).getResult();
		if (previousRecord.recordCount) {
			var previousUrl = utilityService.decryptString(
				encryptedString=previousRecord.shortUrl[1]
				, key=config.security.key
				, salt=config.security.mainSalt
			);
			return variables.fw.buildUrl(action="home:go.to", querystring="url=#previousUrl#");
		}
		var uniqueUrl = false;
		while (! uniqueUrl) {
			var randomUrl = utilityService.randomPassword();
			var uniqueUrlCheck = new Query()
				.setSQL("
					select id
					from shorturl
					where shortUrl collate SQL_Latin1_General_CP1_CS_AS = :randomUrl
				")
				.addParam(name="randomUrl",value=randomUrl,cfsqltype="CF_SQL_VARCHAR")
				.execute()
				.getResult();
			if (uniqueUrlCheck.recordCount) {
				uniqueUrl = false;
			} else {
				uniqueUrl = true;
			}
		}
		transaction {
			var shortUrlObj = entityNew("shortUrl");
			if (structKeyExists(arguments,"action") && len(arguments.action)) {
				shortUrlObj.setAction(arguments.action);
			}
			if (structKeyExists(arguments,"querystring") && len(arguments.querystring) && ! len(arguments.predefinedSalt)) {
				shortUrlObj.setSalt(utilityService.generateRandomString());
				shortUrlObj.setQuerystring(utilityService.encryptString(arguments.querystring, config.security.key, shortUrlObj.getSalt()));
			} else if (structKeyExists(arguments,"querystring") && len(arguments.querystring) && len(arguments.predefinedSalt)) {
				shortUrlObj.setSalt(arguments.predefinedSalt);
				shortUrlObj.setQuerystring(utilityService.encryptString(arguments.querystring, config.security.key, arguments.predefinedSalt));
			}
			if (structKeyExists(arguments,"fullUrl") && len(arguments.fullUrl)) {
				shortUrlObj.setFullUrl(arguments.fullUrl);
			}
			shortUrlObj.setShortUrl(utilityService.encryptString(randomUrl, config.security.key, config.security.mainSalt));
			entitySave(shortUrlObj);
		}
		return variables.fw.buildUrl(action="home:go.to", querystring="url=#randomUrl#");
	}

	public struct function getTargetUrl(required string shortUrl) output=false hint="" {
		var targetUrlQry = new Query()
			.setSQL("
				select action
					, querystring
					, fullUrl
					, clickCount
					, salt
				from shortUrl
				where isActive = 1
				and shortUrl collate SQL_Latin1_General_CP1_CS_AS = :shortUrl
			")/* to ensure case sensitivity the collate SQL_Latin1_General_CP1_CS_AS is added to the query */
			.addParam(
				name="shortUrl"
				, value=utilityService.encryptString(
					arguments.shortUrl
					, config.security.key
					, config.security.mainSalt
				)
				, cfsqltype="CF_SQL_VARCHAR"
			)
			.execute()
			.getResult();
		if (! targetUrlQry.recordCount) {
			return {
				action : "home:404."
				, querystring : ""
				, fullUrl : ""
				, clickCount : 0
			};
		}
		return {
			action : targetUrlQry.action[1]
			, querystring : utilityService.decryptString(encryptedString=targetUrlQry.querystring[1], key=config.security.key, salt=targetUrlQry.salt[1])
			, fullUrl : targetUrlQry.fullUrl[1]
			, clickCount : targetUrlQry.clickCount[1]
		};
	}

}