<cfcomponent output=false hint="functions for tags that have no cfscript equivalent">

	<cffunction name="wddx2cfml" access="public" returntype="Any" output="false" hint="convert wddx to cfml, to be used">
		<cfargument name="wddx" required="true">
		<cfwddx action="wddx2cfml" input="#arguments.wddx#" output="local.result">
		<cfreturn local.result>
	</cffunction>

	<cffunction name="cfml2wddx" access="public" returntype="Any" output="false" hint="stupid coldfusion can't handle wddx in cfscript!!!, idiots">
		<cfargument name="data" required="true">
		<cfwddx action="cfml2wddx" input="#arguments.data#" output="local.result">
		<cfreturn local.result>
	</cffunction>

	<cffunction name="showDebugOutput" access="public" returntype="void" output="false" hint="Turn debugoutput on/off">
		<cfargument name="toggle" type="boolean" default=0>
		<cfsetting showdebugoutput="#arguments.toggle#">
	</cffunction>

	<cffunction name="zip" access="public" returntype="void" output="false" hint="create a zip based on the given directory (aboslute path)">
		<cfargument name="sourcePath" required="true">
		<cfargument name="destinationPath" required="true">
		<cfzip file="#arguments.destinationPath#" source="#arguments.sourcePath#" overwrite="true">
	</cffunction>

</cfcomponent>