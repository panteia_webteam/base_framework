component accessors=true extends="dao" output=false hint="" {

	property config;

	variables.objName = "contentitem";

	public string function showTitle(required string action, string locale=session.locale) output=false hint="" {
		var returnString = "";
		var contentString = "";
		var action = canonicalize(arguments.action, true, true);
		var contentItemObj = getObjBy({ action : action, locale : arguments.locale });
		if (! isNull(contentItemObj.getId())) {
			contentString = contentItemObj.getTitle();
		} else {
			var contentItemObj = getObjBy({ action : action, locale : config.defaultLocale });
			if (! isNull(contentItemObj.getId())) {
				contentString = contentItemObj.getTitle();
			}
		}
		returnString = contentString;
		return returnString;
	}

	public string function showContent(required string action, string locale=session.locale, string role="admin") output=false hint="" {
		var returnString = "";
		var contentString = "";
		var action = canonicalize(arguments.action, true, true);
		var contentItemObj = getObjBy({ action : action, locale : arguments.locale });
		if (! isNull(contentItemObj.getId())) {
			contentString = contentItemObj.getContent();
		} else {
			var contentItemObj = getObjBy({ action : action, locale : config.defaultLocale });
			if (! isNull(contentItemObj.getId())) {
				contentString = contentItemObj.getContent();
			}
		}
		// niet netjes met session, maar zo vlug geen ander idee
		if (structKeyExists(session, "auth") && structKeyExists(session.auth, "roles") && (listFind(session.auth.roles, "admin") || listFind(session.auth.roles, "editor"))) {
			returnString = "<span class='#arguments.action#'>";
			if (! len(contentString)) {
				contentString = arguments.action;
			}
			returnString &= contentString & " <a href='/index.cfm?action=#arguments.role#:content.content&id=#contentItemObj.getId()#&contentAction=#arguments.action#' target='_blank'>Wijzigen</a></span>";
		} else {
			returnString = contentString;
		}
		return returnString;
	}

}