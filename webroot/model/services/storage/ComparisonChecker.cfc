component output=false {

	public any function init() {
		return this;
	}
	/**
	* @hint tests for equality, eg == operator.
	* Returns true if both arguments are unsupplied, both are null, or both have the same hashcode.
	**/
	public boolean function equality(any object1, any object2) {
		if(structKeyExists(arguments, "object1")) {
			if(structKeyExists(arguments, "object2")) {
				if(IsNull(arguments.object1)) {
					return IsNull(arguments.object2);
				} else {
					if(IsNull(arguments.object2)) {
						return false;
					}
					//We can't make use of coldfusion.runtime.Array.contains as that won't check properly.
					var aSet = createObject("Java","java.util.HashSet").init();
					aSet.add(arguments.object1);
					aSet.add(arguments.object2);
					return aSet.size() == 1;
				}
			} else {
				return false;
			}
		} else {
			return !structKeyExists(arguments, "object2");
		}
	}

	//Memory locations, then. Goddamnit.
	//TODO: check if System.identityHashcode (java.lang.System) can replace getMemoryAddress
	private any function getUnsafe() {
		var fieldObject = createObject("Java", "sun.misc.Unsafe").getClass().getDeclaredField("theUnsafe");
		fieldObject.setAccessible(true);
		return fieldObject.get(getNull());
	}

	public boolean function unsafeStrictEquality(required any object1, required any object2) {
		var unsafeLib = getUnsafe();
		var address1 = getMemoryAddressOf(unsafeLib, arguments.object1);
		var address2 = getMemoryAddressOf(unsafeLib, arguments.object2);
		return (address1 == address2);
	}

	public boolean function unsafeStrictCollectionContentEquality(required any collection1, required any collection2) {
		if(collection1.size() != collection2.size()) {
			return false;
		}
		var unsafeLib = getUnsafe();
		var arr1 = arrayNew(1);
		var iter = arguments.collection1.iterator();
		while(iter.hasNext()) {
			ArrayAppend(arr1, getMemoryAddressOf(unsafeLib, iter.next()));
		}
		var arr2 = arrayNew(1);
		var iter2 = arguments.collection2.iterator();
		while(iter2.hasNext()) {
			ArrayAppend(arr2, getMemoryAddressOf(unsafeLib, iter2.next()));
		}
		return arrayContentEquals(arr1, arr2, "numeric");
	}

	public boolean function unsafeStrictTypedCollectionContentEquality(required any collection1, required any collection2, required string type) {
		if(collection1.size() != collection2.size()) {
			return false;
		}
		var unsafeLib = getUnsafe();
		var objectArray1 = arrayNew(1);
		var valueArray1 = arrayNew(1);
		var iter = arguments.collection1.iterator();
		while(iter.hasNext()) {
			var element = iter.next();
			if(isValid("string", element)) {
				ArrayAppend(valueArray1, element);
			} else {
				ArrayAppend(objectArray1, getMemoryAddressOf(unsafeLib, element));
			}
		}
		var objectArray2 = arrayNew(1);
		var valueArray2 = arrayNew(1);
		var iter2 = arguments.collection2.iterator();
		while(iter2.hasNext()) {
			var element = iter2.next();
			if(isValid("string", element)) {
				ArrayAppend(valueArray2, element);
			} else {
				ArrayAppend(objectArray2, getMemoryAddressOf(unsafeLib, element));
			}
		}
		return arrayContentEquals(objectArray1, objectArray2, "numeric") && arrayContentEquals(valueArray1, valueArray2, arguments.type);
	}

	private boolean function arrayContentEquals(required array array1, required array array2, required string type) {
		if(ArrayLen(arguments.array1) != ArrayLen(arguments.array2)) {
			return false;
		}
		var valueArr1A = arrayNew(1);
		var valueArr2A = arrayNew(1);
		valueArr1A.addAll(arguments.array1);
		valueArr2A.addAll(arguments.array2);
		if(arguments.type == "varchar" || arguments.type == "text") {
			valueArr1A.removeAll(arguments.array2);
			valueArr2A.removeAll(arguments.array1);
		} else if(arguments.type == "datetime") {
			for(var element in arguments.array1) {
				element = parseDateTime(element);
				for(var element2 in valueArr2A) {
					if(element == parseDateTime(element2)) {
						valueArr2A.remove(element2);
						break;
					}
				}
			}
			for(var element in arguments.array2) {
				element = parseDateTime(element);
				for(var element2 in valueArr1A) {
					if(element == parseDateTime(element2)) {
						valueArr1A.remove(element2);
						break;
					}
				}
			}
		} else {
			for(var element in arguments.array1) {
				for(var element2 in valueArr2A) {
					if(element == element2) {
						valueArr2A.remove(valueArr2A.indexOf(element2));
						break;
					}
				}
			}
			for(var element in arguments.array2) {
				for(var element2 in valueArr1A) {
					if(element == element2) {
						valueArr1A.remove(valueArr1A.indexOf(element2));
						break;
					}
				}
			}
		}
		return (ArrayLen(valueArr1A) + ArrayLen(valueArr2A)) == 0;
	}

	public boolean function unsafeStrictContains(required any collection, required any object) {
		var unsafeLib = getUnsafe();
		var arr = arrayNew(1);
		var iter = arguments.collection.iterator();
		while(iter.hasNext()) {
			ArrayAppend(arr, getMemoryAddressOf(unsafeLib, iter.next()));
		}
		var address = getMemoryAddressOf(unsafeLib, arguments.object);
		return ArrayContains(arr, address);
	}

	public boolean function unsafeStrictDisjoint(required any collection1, required any collection2) {
		var result = true;
		var iter = arguments.collection1.iterator();
		while(iter.hasNext() && result) {
			result = unsafeStrictContains(arguments.collection2, iter.next()) && result;
		}
		if(!result) {
			return result;
		}
		iter = arguments.collection2.iterator();
		while(iter.hasNext() && result) {
			result = unsafeStrictContains(arguments.collection1, iter.next()) && result;
		}
		return result;
	}

	public boolean function unsafeStrictAddIfNotContains(required struct collectionStruct, required any object) {
		var unsafeLib = getUnsafe();
		var arr = arrayNew(1);
		var iter = arguments.collectionStruct["collection"].iterator();
		while(iter.hasNext()) {
			ArrayAppend(arr, getMemoryAddressOf(unsafeLib, iter.next()));
		}
		var address = getMemoryAddressOf(unsafeLib, arguments.object);
		if(!ArrayContains(arr, address)) {
			return arguments.collectionStruct["collection"].add(arguments.object);
		} else {
			return false;
		}
	}

	public boolean function unsafeStrictAddAllIfNotContains(required struct collectionStruct, required any collection2) {
		var iter = arguments.collection2.iterator();
		var result = false;
		while(iter.hasNext()) {
			result = unsafeStrictAddIfNotContains(arguments.collectionStruct, iter.next()) || result;
		}
		return result;
	}

	//Ordered collections ONLY!
	public boolean function unsafeStrictRemove(required struct collectionStruct, required any object) {
		var unsafeLib = getUnsafe();
		var arr = arrayNew(1);
		var iter = arguments.collectionStruct["collection"].iterator();
		while(iter.hasNext()) {
			ArrayAppend(arr, getMemoryAddressOf(unsafeLib, iter.next()));
		}
		var address = getMemoryAddressOf(unsafeLib, arguments.object);
		var index = ArrayFind(arr, address);
		if(index != 0) {
			var size = arguments.collectionStruct["collection"].size();
			var arr = arrayNew(1);
			for(var i = 1; i <= size; i++) {
				if(i != index) {
					arrayAppend(arr, arguments.collectionStruct["collection"][i]);
				}
			}
			arguments.collectionStruct["collection"] = arr;
			return (size != arguments.collectionStruct["collection"].size());
		} else {
			return false;
		}
	}

	public boolean function unsafeDisjoint(required any collection1, required any collection2) {
		var unsafeLib = getUnsafe();
		var arr = arrayNew(1);
		var iter = arguments.collection1.iterator();
		while(iter.hasNext()) {
			ArrayAppend(arr, getMemoryAddressOf(unsafeLib, iter.next()));
		}
		var arr2 = arrayNew(1);
		var iter2 = arguments.collection2.iterator();
		while(iter2.hasNext()) {
			ArrayAppend(arr2, getMemoryAddressOf(unsafeLib, iter2.next()));
		}
		var total = 0;
		var aSet = createObject("Java","java.util.HashSet").init();
		aSet.addAll(arr);
		total += aSet.size();
		var bSet = createObject("Java","java.util.HashSet").init();
		bSet.addAll(arr2);
		total += bSet.size();
		aSet.addAll(bSet);
		return aSet.size() == total;
	}

	private any function getMemoryAddressOf(required any unsafeLibrary, required any object) {
		var Java_Lang_Reflect_Array = createObject("Java", "java.lang.reflect.Array");
		var objectArray = Java_Lang_Reflect_Array.newInstance(createObject("Java", "java.lang.Object").getClass(), 1);
		Java_Lang_Reflect_Array.set(objectArray, 0, arguments.object);
		var baseOffset = arguments.unsafeLibrary.arrayBaseOffset(objectArray.getClass());
		var addresssize = arguments.unsafeLibrary.addressSize();
		if(addresssize == 4) {
			return arguments.unsafeLibrary.getInt(objectArray, baseOffset);
		} else if(addresssize == 8) {
			return arguments.unsafeLibrary.getLong(objectArray, baseOffset);
		} else {
			throw(message="Can't identify address size.", type="IllegalStateException");
		}
	}

	public void function getNull() {}

	public boolean function strictDisjoint(required any collection1, required any collection2) {
		writeDump(arguments);
		writeDump(collection1.toArray());
		writeDump(collection2.toArray());
		var totalSize = arguments.collection1.size() + arguments.collection2.size();
		if(totalSize <= 1) {
			return true;
		} else if(totalSize == 2) {
			if(arguments.collection1.size() == 1 && arguments.collection2.size() == 1) {
				var iter1 = arguments.collection1.iterator();
				var iter2 = arguments.collection2.iterator();
				writeDump("Comparing!");
				var obj1 = iter1.next();
				var obj2 = iter2.next();
				writeDump(obj1);
				writeDump(obj2);
				var comparisonresult = unsafeStrictEquality(obj1, obj2);
				writeDump(comparisonresult);
				var obj3 = obj1;
				var comparisonresult = unsafeStrictEquality(obj1, obj3);
				writeDump(comparisonresult);
			} else {
				writeDump("wat");
			}
		}
		var total = 0;
		var aSet = createObject("Java","java.util.HashSet").init();
		aSet.addAll(arguments.collection1);
		total += aSet.size();
		var bSet = createObject("Java","java.util.HashSet").init();
		bSet.addAll(arguments.collection2);
		total += bSet.size();
		aSet.addAll(bSet);
		var result = aSet.size() == total;
		writeDump(result);
		return result;
	}

	public boolean function strictUniqueUnorderedCollectionContentsEquality(any collection1, any collection2) {
		if(structKeyExists(arguments, "collection1")) {
			if(structKeyExists(arguments, "collection2")) {
				if(IsNull(arguments.collection1)) {
					return IsNull(arguments.collection2);
				} else {
					if(IsNull(arguments.collection2)) {
						return false;
					}
					if(collection1.size() != collection2.size()) {
						return false;
					}
					//We can't make use of coldfusion.runtime.Array.contains as that won't check properly.
					var aSet = createObject("Java","java.util.HashSet").init();
					aSet.addAll(arguments.collection1);
					aSet.addAll(arguments.collection2);
					return aSet.size() == collection1.size();
				}
			} else {
				return false;
			}
		} else {
			return !structKeyExists(arguments, "collection2");
		}
	}

	public boolean function strictDuplicateUnorderedCollectionContentsEquality(any collection1, any collection2) {
		if(structKeyExists(arguments, "collection1")) {
			if(structKeyExists(arguments, "collection2")) {
				if(IsNull(arguments.collection1)) {
					return IsNull(arguments.collection2);
				} else {
					if(IsNull(arguments.collection2)) {
						return false;
					}
					if(collection1.size() != collection2.size()) {
						return false;
					}
				}
			} else {
				return false;
			}
		} else {
			return !structKeyExists(arguments, "collection2");
		}
	}

}