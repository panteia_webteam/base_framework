component output=false {

	public function init() {
		return this;
	}

	/**
	* @hint
	* @ownerObject 	The name pf the ownerobject from the data; company, user, etc (optional)
	* @id 			The ID (GUID) of the ownerobject of the data (optional)
	* @resultYears 	List of years to retrieve data from (optional)
	* @resultPeriodIntervals List of periodinterval to retrieve data from (optional)
	* @intervalType Intervaltype to retrieve data from; day, week, month, etc (optional)
	* @isPeriodData Boolean which tells to just retrieve period related data or not, default empty which returns period and not period related data, when set to 0 just returns non period related data (optional)
	* @isDefData 	Boolean which tells to just return defData (1), or also return tempdata (0), defaults to 0 (optional)
	* @allData		If set to 1 returns all data for all years, periods and none perioddata, ownerobjecttype and id can filter this data. (optional)
	* OPTIMIZED FOR PERFORMANCE
	**/
	public struct function getQuestionsResults(string ownerobjecttype, string id, string resultYears, string resultPeriodIntervals, string intervalType, boolean nonPeriodData, boolean isDefData=1, boolean allData=0) {
		var structReturn = {};
		var queryParams = {};
		var fieldName = "";
		var fieldValue = "";
		var sb = createObject("Java", "java.lang.StringBuilder").init(1024);//1024 characters allowed, if that limit is surpassed, you take a performance hit. Multiples of 2 preferred.
		sb.append("SELECT NEW MAP (r.id AS RESULTID, r.createDate AS CREATEDATE, r.resultYear AS RESULTYEAR, r.resultPeriodInterval AS RESULTPERIODINTERVAL, r.intervalType AS INTERVALTYPE, r.isTempData AS ISTEMPDATA, r.fk_ownerId AS FK_OWNERID, r.ownerObjectType AS OWNEROBJECTTYPE, a.id AS attributeId, a.name AS NAME, a.valueType AS VALUETYPE, a.periodData AS PERIODDATA, a.fullname AS FULLNAME) FROM result r JOIN r.attribute a WHERE r.isActive = 1 AND a.isActive = 1 ");
		//get tempdata or not
		if (arguments.isDefData) sb.append(" and r.isTempData=0");
		if (structKeyExists(arguments,"ownerobjecttype")) {
			sb.append(" and r.ownerobjecttype=:ownerobjecttype");
			queryParams.ownerobjecttype = arguments.ownerobjecttype;
		}
		if (structKeyExists(arguments,"id")) {
			sb.append(" and r.fk_ownerId=:fk_ownerId");
			queryParams.fk_ownerId = arguments.id;
		}
		if (! arguments.allData) {
			// if resultYear/Period is given and periodData = true
			if (structKeyExists(arguments,"resultYears") or structKeyExists(arguments,"resultPeriodIntervals")) {
				sb.append(" and (0=0");
				//resultyear
				if (structKeyExists(arguments,"resultYears")) {
					sb.append(" and r.resultYear in (:resultYear)");
					queryParams.resultYear = listToArray(arguments.resultYears);
				}
				//resultperiodInterval
				if (structKeyExists(arguments,"resultPeriodIntervals")) {
					sb.append(" and r.resultPeriodInterval in (:resultPeriodInterval) and r.intervalType = :intervalType");
					queryParams.resultPeriodInterval = listToArray(arguments.resultPeriodIntervals);
					if (structKeyExists(arguments,"intervalType")){
						queryParams.intervalType = arguments.intervalType;
					} else {
						queryParams.intervalType = getDefaultIntervalType();
					}
				}
				//Return nonPeriod data aswell
				if (structKeyExists(arguments,"nonPeriodData") and arguments.nonPeriodData) {
					sb.append(" or (r.resultYear IS NULL and r.resultPeriodInterval IS NULL))");
				} else {
					sb.append(")");
				}
			} else {
				//show just non period data
				if (structKeyExists(arguments,"nonPeriodData") and arguments.nonPeriodData) {
					sb.append(" and (r.resultYear IS NULL and r.resultPeriodInterval IS NULL)");
				}
			}
		}
		var arrResults = ormExecuteQuery(sb.toString(),queryParams);
		for (resultItem in arrResults) {
			try {
				fieldValue = ormExecuteQuery("
					SELECT #resultItem.valueType# as VALUE
					FROM resultmonitor
					WHERE fk_resultId = :fk_resultId
				", { fk_resultId : resultItem.resultId });
				writeDump(var=fieldValue, top=4, abort=0);
				writeDump(var=resultItem, top=4, abort=1);
			} catch (any a) {
				writeDump(var=resultItem, top=4, abort=0);
				writeDump(var=a, top=4, abort=1);
			}
		}
		writeDump(var=local, top=4, abort=1);
	}

	/**
	* @hint 			Returns the value/status for the given fieldname from the given structure with data
	* @fieldName  		Fieldname to search for in the dataStruct, will be rebuild from simple to full form
	* @compareValue 	Value to compare the data too, needed for select, multiselect,checkbox and radio (optional)
	* @returnVariable 	Value to return from the structure, defaults to value (optional)
	* @dataStruct 		Structure with the data for the given form
	* @requestScope 	Requestscope, to filter the data on the already present data; ownerObject, id, year, period, etc
	**/
	public string function getValue(
		required string fieldName
		, string compareValue
		, string returnVariable="value"
		, required struct dataStruct
		, required struct requestScope) {
		var inputStruct = arguments.requestScope;
		var fullFieldName = application.monitorService.buildQuestion(arguments.fieldName,requestScope);
		if (structKeyExists(arguments.dataStruct,fullFieldName.properFieldName)
		and (structKeyExists(arguments.dataStruct[fullFieldName.properFieldName],uCase(arguments.returnVariable))
			or structKeyExists(arguments.dataStruct[fullFieldName.properFieldName],"value"))) {
			try {
				switch (lCase(fullFieldName.questionType)) {
					case "select":
						if (arguments.dataStruct[fullFieldName.properFieldName].value eq arguments.compareValue) {
							return 'selected="selected"';
						}
						break;
					case "multiselect":
						if (ArrayContains(arguments.dataStruct[fullFieldName.properFieldName].value, arguments.compareValue)) {
							return 'selected="selected"';
						}
						break;
					case "radio":
						if (arguments.dataStruct[fullFieldName.properFieldName].value eq arguments.compareValue) {
							return 'checked="checked"';
						}
						break;
					case "checkbox":
						if (ArrayContains(arguments.dataStruct[fullFieldName.properFieldName].value, arguments.compareValue)) {
							return 'checked="checked"';
						}
						break;
					default:
						return arguments.dataStruct[fullFieldName.properFieldName][uCase(arguments.returnVariable)];
				}
			} catch (any a) {
				writeOutput("Let op! Voor een select, multiselect en radio dient een vergelijkingswaarde meegegeven te worden.");
				writeDump(var=a, abort=1);
			}
		}
		return "";
	}

}