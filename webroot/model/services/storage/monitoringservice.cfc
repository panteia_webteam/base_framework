/**
* Monitoring service handles loading and saving of data with form fields.
* IMPORTANT: Is not multi-thread safe. Multiple instances of objects may get created. Persistance layers will halt corruption of data, but requests will end in errors.
**/
component accessors=true output=false {

	property comparisonchecker;
	property development;

	variables.relatedFormVariableNames = getRelatedFormVariableNames();
	variables.formFieldFormat = getFormFieldFormat();
	variables.attributes = {};
	variables.defaultIntervalType = "month";
	variables.defaultIntervalTypeForNULLPeriodInterval = "year";
	variables.defaultOwnerObjectType = "group";
	variables.defaultIsTempData = 0;

	public array function getRelatedFormVariableNames() output=false hint="" {
		return ListToArray("resultyear,resultperiodinterval,intervaltype,ownerid,ownerobjecttype,istempdata,checkedvalue");
	}

	public array function getFormFieldFormat() output=false hint="" {
		var a = ListToArray("fieldname,valuetype");
		a.addAll(getRelatedFormVariableNames());
		return a;
	}

	public any function init(string overrideIntervalTypeDefault, string overrideOwnerObjectTypeDefault) output=false hint="" {
		if(structKeyExists(arguments, "overrideIntervalTypeDefault")) {
			variables.defaultIntervalType = arguments.overrideIntervalTypeDefault;
		}
		if(structKeyExists(arguments, "overrideOwnerObjectTypeDefault")) {
			variables.defaultOwnerObjectType = arguments.overrideOwnerObjectTypeDefault;
		}
		return this;
	}

	public struct function extractFormDataFromRC(required struct rc) output=false hint="" {
		var formData = {};
		formData.fields = {};
		formData.formDefaults = {};
		for(var rcStructKey in arguments.rc) {
			if(rcStructKey.contains(":")) {
				formData.fields[rcStructKey] = arguments.rc[rcStructKey];
			} else if(isRelatedToFormData(rcStructKey)) {
				formData.formDefaults[rcStructKey] = arguments.rc[rcStructKey];
			}
		}
		return formData;
	}

	private boolean function isRelatedToFormData(required string fieldName) output=false hint="" {
		return variables.relatedFormVariableNames.contains(lCase(arguments.fieldName));
	}

	public array function saveFormDataFromRC(required struct rc) output=false hint="" {
		timer label="total save" {
			var formData = extractFormDataFromRC(arguments.rc);
		}
		return saveFormData(formData);
	}

	public array function saveFormData(required struct formData) output=false hint="" {
		var fields = arguments.formData.fields;
		var formDefaults = arguments.formData.formDefaults;
		var formFieldsReadyToSave = {};
		for(var fieldName in fields) {
			var attributeStruct = convertFormFieldNameToAttributeStruct(fieldName);
			applyDefaultsToAttributeStruct(attributeStruct, formDefaults);
			try {
				validateAttributeStruct(attributeStruct);//Will throw exceptions if something is wrong.
			} catch (any a) {
				writeDump(var=arguments, top=4);
				writeDump(var=fieldName);
				writeDump(var=attributeStruct);
				writeDump(var=local);
				writeDump(var=a, top=4, abort=1);
			}
			formFieldsReadyToSave[fieldName] = {};
			formFieldsReadyToSave[fieldName].value = fields[fieldName];
			formFieldsReadyToSave[fieldName].attributeStruct = attributeStruct;
		}
		return saveFormFields(formFieldsReadyToSave);
	}

	public struct function loadFormData(required struct formData) output=false hint="" {
		var fields = arguments.formData.fields;
		var formDefaults = arguments.formData.formDefaults;
		var formData = {};
		for(var fieldName in fields) {
			var attributeStruct = convertFormFieldNameToAttributeStruct(fieldName);
			applyDefaultsToAttributeStruct(attributeStruct, formDefaults);
			validateAttributeStruct(attributeStruct);//Will throw exceptions if something is wrong.
			formData[fieldName] = loadFormFieldData(attributeStruct);
		}
		return formData;
	}

	private array function loadFormFieldData(required struct attributeStruct) output=false hint="" {
		//load last attribute in chain
		var lastAttribute = getAttributeObjectByFullName(arguments.attributeStruct["fieldname"]);
		return getActiveResultObjects(lastAttribute, arguments.attributeStruct);
	}

	private array function saveFormFields(required struct formFields) output=false hint="returns an array of messages" {
		var unsavedORMObjects = arrayNew(1);//Do not replace with HashSet to try to attempt unneccessary saves - order of objects must be preserved.
		var resultValueObjects = arrayNew(1);
		var messageArray = arrayNew(1);
		for(var formFieldName in arguments.formFields) {
			var formFieldStruct = arguments.formFields[formFieldName];
			var resultStruct = createORMObjectsForFormField(formFieldStruct.attributeStruct, formFieldStruct.value);
			arrayAppend(messageArray,resultStruct.messageResult);
			unsavedORMObjects.addAll(resultStruct.ormObjects);
			resultValueObjects.addAll(resultStruct.resultObjects);
		}
		ORMGetSession().setFlushMode(ORMGetSession().getFlushMode().parse("COMMIT"));
		var trans = ORMGetSession().beginTransaction();
		for(var unsavedORMObject in unsavedORMObjects) {
			EntitySave(unsavedORMObject);
		}
		trans.commit();
		for(var resultObject in resultValueObjects) {
			if(isInstanceOf(resultObject, "model.beans.storage.resultmonitor")) {
				resultObject.resolveResultReference();
			}
		}
		var trans2 = ORMGetSession().beginTransaction();
		for(var resultObject in resultValueObjects) {
			try {
				EntitySave(resultObject);
			} catch (any a) {
				writeDump(var=resultObject, top=4, abort=0);
				writeDump(var=local, top=4, abort=0);
				writeDump(var=a, top=4, abort=1);
			}
		}
		trans2.commit();
		return messageArray;
	}

	//result orm object which isn't saved yet
	private struct function createORMObjectsForFormField(required struct attributeStruct, required any fieldValue) output=false hint="" {
		var ormObjects = arrayNew(1);
		var resultValueObjects = arrayNew(1);
		var messageResult = arrayNew(1);
		var attributes = getOrCreateAttributeObjectsForFormField(arguments.attributeStruct);
		ormObjects.addAll(attributes);
		var lastAttribute = attributes[ArrayLen(attributes)];
		var saveNewData = false;
		//Determine whether the value has changed for this field by getting all active result objects and comparing their values with the new values
		var resultObjectArray = getActiveResultObjects(lastAttribute, arguments.attributeStruct);//Last attribute + ownerid combination
		if(!IsArray(arguments.fieldValue)) {
			arguments.fieldValue = [arguments.fieldValue];
			var singleValue = true;
		}
		//for loop fieldValue attribute.convertStringAnswerToNumeric
		for(var i = 1; i <= arrayLen(arguments.fieldValue); i++) {
			arguments.fieldValue[i] = lastAttribute.convertStringAnswerToNumeric(arguments.fieldValue[i]);
		}
		if (!arrayIsEmpty(resultObjectArray)) {
			for(var resultObject in resultObjectArray) {
				//determine if the data has changed
				var itemsEqual = 0;
				if (resultObject.getAttribute().getValueType() == 'boolean') {
					//uniform boolean data to prevent type mismatching (true,1,yes,etc)
					if ((isBoolean(arguments.fieldValue[1]) and !arguments.fieldValue[1]) or !len(arguments.fieldValue[1]) or listFindNoCase('0,false,no',arguments.fieldValue[1])) {
						arguments.fieldValue[1] = false;
					} else {
						arguments.fieldValue[1] = true;
					}
				}
				var saveChangesCurrentObj = checkDataChanges(resultObject,arguments.fieldValue);
				if (saveChangesCurrentObj) {
					resultObject.setIsActive(false);
					ArrayAppend(ormObjects, resultObject);
					saveNewData = true;
				}
			}
		} else {
			//all enw values so save them
			saveNewData = true;
		}
		//if there is new data save it
		if (saveNewData) {
			var result = createResultObjectForAttribute(arguments.attributeStruct, lastAttribute);
			for(var fieldVal in arguments.fieldValue) {
				//TODO: Use different datasource... how am I going to do this?
				if (!isNull(fieldVal) && len(fieldVal)) {
					var resultObject = EntityNew("resultmonitor");
					ArrayAppend(resultValueObjects, resultObject);
					resultObject.setResultReference(result);
					if(result.getValueType().equalsIgnoreCase("text")) {
						var resultTextObject = EntityNew("resultmonitortext");
						resultTextObject.setResultmonitor(resultObject);
						resultTextObject.setText(fieldVal);
						ArrayAppend(resultValueObjects, resultTextObject);
					} else {
						resultObject.setValue(fieldVal);
					}
				}
				//Additionally, resultId is unknown until entitySave.
			}
			ArrayAppend(ormObjects, result);
			arrayAppend(messageResult, { fieldname : arguments.attributeStruct.fieldName&':'&arguments.attributeStruct.valueType, message : 'success'});
		} else {
			arrayAppend(messageResult, { fieldname : arguments.attributeStruct.fieldName&':'&arguments.attributeStruct.valueType, message : 'no changes'});
		}
		return { ormObjects : ormObjects, resultObjects : resultValueObjects, messageResult : messageResult };
	}

	private boolean function checkDataChanges(required model.beans.storage.result resultObject, required array fieldValue) output=false hint="" {
		var itemsEqual = 0;
		//count the amount of changes in the data
		for(var valueObject in arguments.resultObject.getResultValue()) {
			if (arguments.resultObject.getAttribute().getValueType() != 'boolean' && arrayContains(arguments.fieldValue,valueObject.getValue())) {
				itemsEqual += 1;
			} else if (arguments.resultObject.getAttribute().getValueType() == 'boolean' && arrayLen(arguments.fieldValue) == 1 && valueObject.getValue() == arguments.fieldValue[1]) {
				//check for boolean fields like single checkbox and dual radio options (yes/no)
				itemsEqual += 1;
			}
		}
		//boolean to determine if the data is equal to what is stored in the Database
		var resultCount = arrayLen(arguments.resultObject.getResultValue());
		// number of records has changed (applies to multiselect fields) or the number of equal records does not match the current recordamount
		if (resultCount != arrayLen(arguments.fieldValue) || resultCount != itemsEqual) {
			//equal count of items and all are equal
			return true;
		}
		return false;
	}

	private array function getOrCreateAttributeObjectsForFormField(required struct attributeStruct) output=false hint="" {
		//attempt to load needed attribute object
		var lastAttribute = getAttributeObjectByFullName(arguments.attributeStruct["fieldname"]);
		if(!isNull(lastAttribute)) {
			return [lastAttribute];
		}
		//implicit else - loading failed, carefully construct/get entire chain one step at a time
		var ormObjects = arrayNew(1);
		var fieldName = listToArray(arguments.attributeStruct["fieldname"], ".");//Warning - a..b and a.b act the same
		//Separate function
		var rootAttribute = getOrCreateRootAttributeObject(arguments.attributeStruct);
		ArrayAppend(ormObjects, rootAttribute);
		var fieldNameVariableCount = ArrayLen(fieldName);
		var attributes = [rootAttribute];
		for(var i = 2; i <= fieldNameVariableCount; i++) {//ignore root element.
			var attribute = getOrCreateAttributeObjectWithParents(arguments.attributeStruct, attributes);
			ArrayAppend(attributes, attribute);
			ArrayAppend(ormObjects, attribute);
		}
		return ormObjects;
	}

	private model.beans.storage.result function createResultObjectForAttribute(required struct attributeStruct, required model.beans.storage.attribute attribute) output=false hint="" {
		var result = EntityNew("result");
		result.setFk_ownerId(arguments.attributeStruct["ownerid"]);
		result.setOwnerobjecttype(arguments.attributeStruct["ownerobjecttype"]);
		result.setAttribute(arguments.attribute);
		if(structKeyExists(arguments.attributeStruct,"istempdata") && arguments.attributeStruct.istempdata == 1) {
			result.setIsTempData(1);
		}
		if(arguments.attribute.getPeriodData() == true) {
			result.setIntervalType(arguments.attributeStruct["intervaltype"]);
			if(structKeyExists(arguments.attributeStruct, "resultyear") && !isEmptyOrNullValue(arguments.attributeStruct.resultyear)) {
				result.setResultYear(arguments.attributeStruct["resultyear"]);
			}
			if(structKeyExists(arguments.attributeStruct, "resultperiodinterval") && !arguments.attributeStruct.resultPeriodInterval.isEmpty()) {
				result.setResultPeriodInterval(arguments.attributeStruct["resultperiodinterval"]);
			} else if(arguments.attributeStruct["intervaltype"].equalsIgnoreCase(variables.defaultIntervalType)) {
				result.setIntervalType(variables.defaultIntervalTypeForNULLPeriodInterval);
				result.setResultPeriodInterval(getNull());
			}
		}
		return result;
	}

	private array function getActiveResultObjects(required model.beans.storage.attribute attribute, required struct attributeStruct) output=false hint="" {
		//isEmpty for possible null
		if(len(arguments.attribute.getId()) == 0) {
			return arrayNew(1);
		}
		var queryString = "FROM result WHERE isActive = 1 AND attribute = :id AND fk_ownerId = :ownerid AND ownerObjectType = :ownerobjecttype";
		var queryParams = {
			id : arguments.attribute
			, ownerid : arguments.attributeStruct["ownerId"]
			, ownerobjecttype : arguments.attributeStruct["ownerobjecttype"]
		};
		if(arguments.attribute.getPeriodData() == true) {
			if(structKeyExists(arguments.attributeStruct, "resultyear") && !isEmptyOrNullValue(arguments.attributeStruct.resultyear)) {
				queryString &= " AND resultYear = :resultyear";
				queryParams.resultyear = arguments.attributeStruct["resultyear"];
			}
			if(structKeyExists(arguments.attributeStruct, "resultperiodinterval") && !arguments.attributeStruct.resultPeriodInterval.isEmpty()) {
				queryString &= " AND resultPeriodInterval = :resultperiodinterval AND intervalType = :intervaltype";
				queryParams.resultperiodinterval = arguments.attributeStruct["resultperiodinterval"];
				queryParams.intervaltype = arguments.attributeStruct["intervaltype"];
			} else {
				queryString &= " AND intervalType = :intervaltype";
				if(arguments.attributeStruct["intervaltype"].equalsIgnoreCase(variables.defaultIntervalType)) {
					queryParams.intervaltype = variables.defaultIntervalTypeForNULLPeriodInterval;
				} else {
					queryParams.intervaltype = arguments.attributeStruct["intervaltype"];
				}
				queryString &= " AND resultPeriodInterval is null";
			}
		}
		// order by isTempData desc to make sure the tempData object is first in the result array
		queryString &= " ORDER BY isTempData desc";
		return ORMExecuteQuery(queryString, queryParams);
	}

	private model.beans.storage.attribute function getOrCreateRootAttributeObject(required struct attributeStruct) output=false hint="" {
		var rootAttributeName = listGetAt(arguments.attributeStruct["fieldname"], 1, ".");
		var rootAttribute = ORMExecuteQuery("FROM attribute WHERE name = :name AND parent is null", { name : rootAttributeName }, true);
		var isNil = isNull(rootAttribute);
		if(!isNil) {
			var isArr = isArray(rootAttribute);
		}
		if(isNil || (isArr && arrayIsEmpty(rootAttribute))) {
			//create new attribute object.
			if(structKeyExists(variables.attributes, rootAttributeName)) {
				return variables.attributes[rootAttributeName];
			}
			if(! application.development) {
				throw(message="No attribute "&rootAttributeName&" exists.", type="IllegalStateException");
			} else {
				writeDump("No attribute "&rootAttributeName&" exists, thus one was created.");
			}
			//WARNING Do not enable attribute creation on production servers - race condition can cause duplicate attributes,
			//leading to exceptions upon submitting any form that uses the duplicate attribute.
			var rootAttributeObject = createAttributeObjectFromAttributeStruct(arguments.attributeStruct);
			rootAttributeObject.setName(rootAttributeName);
			rootAttributeObject.setFullname(rootAttributeName);
			variables.attributes[rootAttributeName] = rootAttributeObject;
			return rootAttributeObject;
		} else if(isArr) {
			return rootAttribute[1];
		} else {
			return rootAttribute;
		}
	}

	private any function getAttributeObjectByFullName(required string fullName) output=false hint="" {
		lock name="#arguments.fullName#" type="exclusive" timeout="10" {
			var attributeObject = ORMExecuteQuery("FROM attribute WHERE fullname = :fullname", { fullname : arguments.fullName }, true);
			if(isNull(attributeObject)) {
				if(structKeyExists(variables.attributes, arguments.fullName)) {
					return variables.attributes[arguments.fullName];
				} else {
					return getNull();
				}
			} else {
				return attributeObject;
			}
		}
	}

	private model.beans.storage.attribute function getOrCreateAttributeObjectWithParents(required struct attributeStruct, required array parents) output=false hint="" {
		lock name="#arguments.attributeStruct["fieldname"]#" type="exclusive" timeout="10" {
			var parentCount = ArrayLen(arguments.parents);
			var attributeName = listGetAt(arguments.attributeStruct["fieldname"], parentCount+1, ".");
			if(parentCount == 0) {
				var fullName = attributeName;
			} else {
				var lastAttribute = arguments.parents[parentCount];
				var fullName = lastAttribute.getFullname()&"."&attributeName;
			}
			var attribute = getAttributeObjectByFullName(fullName);
			if(isNull(attribute)) {
				//create attribute.
				if(!development) {
					throw(message="No attribute "&arguments.attributeStruct["fieldname"]&" exists.", type="IllegalStateException");
				} else {
					writeDump("No attribute "&arguments.attributeStruct["fieldname"]&" exists, thus one was created.");
				}
				/* WARNING! Do not enable attribute creation on production servers - race condition can cause duplicate attributes, leading to exceptions upon submitting any form that uses the duplicate attribute. */
				attribute = createAttributeObjectFromAttributeStruct(arguments.attributeStruct);
				attribute.setName(attributeName);
				attribute.setFullname(fullName);
				if(parentCount != 0) {
					attribute.setParent(lastAttribute);
				}
				variables.attributes[attribute.getFullName()] = attribute;
			}
			return attribute;
		}
	}

	private model.beans.storage.attribute function createAttributeObjectFromAttributeStruct(required struct attributeStruct) output=false hint="creates Attribute entity from attribute struct. Important: Name is not set." {
		var attribute = EntityNew("Attribute");
		//TODO: Check for Enum
		var valueType = arguments.attributeStruct["valuetype"];
		attribute.setValueType(valueType);
		var resultYear = arguments.attributeStruct["resultyear"];
		var resultPeriodInterval = arguments.attributeStruct["resultperiodinterval"];
		attribute.setPeriodData(!isEmptyOrNullValue(resultYear) || !isEmptyOrNullValue(resultPeriodInterval));
		return attribute;
	}

	public struct function convertFormFieldNameToAttributeStruct(required string fieldName) output=false hint="" {
		var fieldProperties = createObject("java", "java.util.Arrays").asList(arguments.fieldName.split(":", -1));//because listToArray("a:b::d", ":") = ["a", "b", "d"]
		var fieldPropertyCount = fieldProperties.size();
		var formFieldFormatLength = variables.formFieldFormat.size();
		if(fieldPropertyCount > formFieldFormatLength) {
			throw(message ="The field (#arguments.fieldName#) contains #fieldPropertyCount# values, whereas #formFieldFormatLength# or less values were expected.", type="IllegalArgumentException");
		}
		var attributeStruct = {};
		for(var i = 1; i <= formFieldFormatLength; i++) {
			var formFieldFormatItem = variables.formFieldFormat[i];
			if(i > fieldPropertyCount) {
				attributeStruct[formFieldFormatItem] = "";
			} else {
				attributeStruct[formFieldFormatItem] = replace(fieldProperties[i],'[]','');
			}
		}
		return attributeStruct;
	}

	public void function applyDefaultsToAttributeStruct(required struct attributeStruct, required struct formDefaults) output=false hint="" {
		for(var formDefaultKey in arguments.formDefaults) {
			if(arguments.attributeStruct[formDefaultKey].isEmpty()) {
				arguments.attributeStruct[formDefaultKey] = arguments.formDefaults[formDefaultKey];
			}
		}
		if(arguments.attributeStruct["intervaltype"].isEmpty()) {
			arguments.attributeStruct["intervaltype"] = variables.defaultIntervalType;
		}
		if(arguments.attributeStruct["ownerobjecttype"].isEmpty()) {
			arguments.attributeStruct["ownerobjecttype"] = variables.defaultOwnerObjectType;
		}
		//if not defined or false set the value to false in the attributestruct
		if(!yesNoFormat(arguments.attributeStruct["isTempData"])) {
			arguments.attributeStruct["isTempData"] = variables.defaultIsTempData;
		}
	}

	private boolean function isEmptyOrNullValue(required string value) output=false hint="" {
		return isNull(arguments.value) || len(arguments.value) == 0 ? true : false;
	}

	public void function validateAttributeStruct(required struct attributeStruct) output=false hint="" {
		//fieldName (empty - error) OR (NULL - error)
		var fieldName = arguments.attributeStruct["fieldname"];
		if(isEmptyOrNullValue(fieldName)) {
			throw(message="FieldName (#fieldName#) is empty or explicitly null, despite being required.", type="IllegalArgumentException");
		}
		//valueType (empty - error) OR (NULL - error)
		var valueType = arguments.attributeStruct["valuetype"];
		if(isEmptyOrNullValue(valueType)) {
			throw(message="ValueType is empty or explicitly null, despite being required.", type="IllegalArgumentException");
		}
		//resultYear (!empty AND !NULL AND !isValidInteger - error)
		var resultYear = arguments.attributeStruct["resultyear"];
		if(!isEmptyOrNullValue(resultYear) && !isValidInteger(resultYear)) {
			throw(message="ResultYear must be of type integer or contain NULL - yet it was defined as "&resultYear&".", type="IllegalArgumentException");
		}
		//resultPeriodInterval (!empty AND !NULL AND !isValidInteger - error)
		var resultPeriodInterval = arguments.attributeStruct["resultperiodinterval"];
		var resultPeriodIntervalIsValidInteger = isValidInteger(resultPeriodInterval);
		if(!isEmptyOrNullValue(resultPeriodInterval) && !resultPeriodIntervalIsValidInteger) {
			throw(message="ResultPeriodInterval must be of type integer or contain NULL - yet it was defined as "&resultPeriodInterval&".", type="IllegalArgumentException");
		}
		//intervalType (resultPeriodInterval isValidInteger AND (empty OR NULL) - error)
		var intervalType = arguments.attributeStruct["intervaltype"];
		if(resultPeriodIntervalIsValidInteger && isEmptyOrNullValue(intervalType)) {
			throw(message="ResultPeriodInterval is defined, but no intervalType was provided (or intervalType was explicitly defined as NULL).", type="IllegalArgumentException");
		}
		//ownerID (empty - error) or (NULL - error) or (!empty AND !null AND !isValidGUID - error)
		var ownerID = arguments.attributeStruct["ownerid"];
		var ownerIDIsValidGUID = isValidGUID(ownerID);
		if(isEmptyOrNullValue(ownerID)) {
			throw(message ="OwnerID is empty or explicitly null, despite being required.", type="IllegalArgumentException");
		} else if(!ownerIDIsValidGUID) {
			throw(message="OwnerID must be of type guid - yet it was defined as #ownerID#.", type="IllegalArgumentException");
		}
		//ownerObjectType (empty - error) OR (NULL - error)
		var ownerObjectType = arguments.attributeStruct["ownerobjecttype"];
		if(isEmptyOrNullValue(ownerObjectType)) {
			throw(message="OwnerObjectType is empty or explicitly null, despite being required.", type="IllegalArgumentException");
		}
	}

	/**
	* @hint Validates whether the content of a string value is an integer.
	*
	* We don't use isValid(integer) because that will give true for "500,000" or "$500,000" or "$500,000$"
	**/
	private boolean function isValidInteger(required string integer) output=false hint="" {
		//^(0|((-?[1-9])\d*))$
		//whole string,
		//is 0 or
		//0 to 1 minus sign AND
		//one digit that is not 0
		//AND 0 to N digits
		return isValid("regex", arguments.integer, "^(0|((-?[1-9])\d*))$");
	}

	/**
	* @hint Validates whether the content of a string value is a GUID.
	*
	* We don't rely on isValid(guid) alone because it does not account for whitespace.
	**/
	private boolean function isValidGUID(required string guid) output=false hint="" {
		//magic number for length;
		//GUID is 8-4-4-4-12 characters.
		//So that's 8, 1, 4, 1, 4, 1, 4, 1, 12 characters (8, 9, 13, 14, 18, 19, 23, 24, 36)
		//36 characters are the amount of characters in a GUID.
		return (isValid("guid", arguments.guid) && arguments.guid.length() == 36);
	}

	//Returns a NULL value.
	private void function getNull() output=false{}

}