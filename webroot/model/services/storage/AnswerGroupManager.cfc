component accessors=true extends="model.services.default.dao" output=false {

	variables.objName = "answergroup";

	public model.beans.storage.answergroup function getAnswergroup(required string name) output=false hint="" {
		/* remove [] for array input values */
		arguments.name = replace(arguments.name, "[]", "");
		var result = ORMExecuteQuery("FROM answergroup WHERE name = :name", { name : arguments.name }, true);
		if(isNull(result)) {
			throw(message="No answergroup exists with name #name#.", type="IllegalArgumentException");
		}
		return result;
	}

	public array function getAnswergroupAnswers(required string name) output=false hint="" {
		var result = ORMExecuteQuery("
			SELECT NEW MAP (a.id as ID, a.name AS NAME, a.value AS VALUE)
			FROM answer a
				join a.answerGroup ag
			WHERE ag.isActive = 1
			AND a.isActive = 1
			AND ag.name = :agName
			ORDER BY a.listOrder
		", { agName : arguments.name });
		return result;
	}

	public string function getAnswer(required string answervalue, required string answergroup) output=false hint="returns the answer label based on the given answervalue and answergroup" {
		var result = ORMExecuteQuery("
			SELECT NEW MAP (a.name AS NAME)
			FROM answer a
				join a.answerGroup ag
			WHERE ag.isActive = 1
			AND a.isActive = 1
			AND ag.name = :answergroup
			AND a.value = :answervalue
			ORDER BY a.listOrder
		", { answergroup : arguments.answergroup, answervalue : arguments.answervalue });
		return result[1].name;
	}

}