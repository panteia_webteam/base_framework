/* The functions generate a string with the value and the data-isTempData element for an input type.
	The textarea is different as it consists of a start and closing tag with the value in between

	functions:
		text()
		radio()
		checkbox()

		select()
		optionValue()
		optionValueIsTempData()

		textArea()
		textAreaValue()
		textAreaIsTempData()

	formDefaults:
		{
			"resultyear", // if applicable the year the questionvalue belongs to
			"resultperiodinterval", // if applicable the interval the questionvalue belongs to
			"intervaltype", // if applicable the intervaltype, aka day, week, month, quarter
			"ownerid", // the id of the ownerobject
			"ownerobjecttype" // the ownerobject referencetype aka user, group, company, etc
		}

	datacalls:
		text(
			fieldName="", // fieldname to get value from
			formDefaults= {"resultyear", "resultperiodinterval", "intervaltype", "ownerid", "ownerobjecttype"},
			justReturnDefData= true/false // if false data-isTempData=true/false will be set to act upon on page
		)

		radio(
			fieldName="", // fieldname to get value from
			formDefaults= {"resultyear", "resultperiodinterval", "intervaltype", "ownerid", "ownerobjecttype"},
			value='', // value to compare too, needed for radio,checkbox and option to determine current selection (checked='checked')
			justReturnDefData= true/false // if false data-isTempData=true/false will be set to act upon on page
		)

		checkbox(
			fieldName="", // fieldname to get value from
			formDefaults= {"resultyear", "resultperiodinterval", "intervaltype", "ownerid", "ownerobjecttype"},
			value='', // value to compare too, needed for radio,checkbox and option to determine current selection (checked='checked')
			justReturnDefData= true/false // if false data-isTempData=true/false will be set to act upon on page
		)

		select(
			fieldName="", // fieldname to get value from
			formDefaults= {"resultyear", "resultperiodinterval", "intervaltype", "ownerid", "ownerobjecttype"},
			valueArray=[{label='',value=''}], // option label and values to use
			justReturnDefData= true/false, // if false data-isTempData=true/false will be set to act upon on page
			class='', // optional class to use
			id='' // optional id to insert
		)
		optionValue(
			fieldName="", // fieldname to get value from
			formDefaults= {"resultyear", "resultperiodinterval", "intervaltype", "ownerid", "ownerobjecttype"},
			value='', // value to compare too, needed for radio,checkbox and option to determine current selection (selected='selected')
			justReturnDefData= true/false // if false data-isTempData=true/false will be set to act upon on page
		)
		optionValueIsTempData(
			fieldName="", // fieldname to get value from
			formDefaults= {"resultyear", "resultperiodinterval", "intervaltype", "ownerid", "ownerobjecttype"}
		)

		textArea(
			fieldName="", // fieldname to get value from
			formDefaults= {"resultyear", "resultperiodinterval", "intervaltype", "ownerid", "ownerobjecttype"},
			justReturnDefData= true/false, // if false data-isTempData=true/false will be set to act upon on page
			class='', // optional class to use
			id='' // optional id to insert
		)
		textAreaValue(
			fieldName="", // fieldname to get value from
			formDefaults= {"resultyear", "resultperiodinterval", "intervaltype", "ownerid", "ownerobjecttype"},
			justReturnDefData= true/false, // if false data-isTempData=true/false will be set to act upon on page
		)
		textAreaIsTempData(
			fieldName="", // fieldname to get value from
			formDefaults= {"resultyear", "resultperiodinterval", "intervaltype", "ownerid", "ownerobjecttype"}
		)

	examples:

		<label>
			Test input
			<input type="text" #text(fieldName="testInput:int",formDefaults=rc.formDefaults)#  />
		</label>
	results in:
		<input type="text" name="testInput:int" value="2352" data-istempData=true />

		<label>
			<input type="checkbox" #checkbox("testCheckBox:int,rc.formDefaults",1,1)# />
			test checkbox element 1
		</label>
	results in:
		<label>
			<input type="checkbox" name="testCheckBox:int" value=1 checked="checked"  />
			test checkbox element 1
		</label>

		<label>
			<input type="radio" #radio("testRadio:int",rc.formDefaults,1)# />
		 	test radio 1
		</label>
		<label>
			<input type="radio" #radio(fieldName="testRadio:int",formDefaults=rc.formDefaults,value=2,justReturnDefData=false)# />
		 	test radio 2
		</label>
	results in:
		<label>
			<input type="radio" name="testRadio:int" value=1 data-isTempData=true />
		 	test radio 1
		</label>
		<label>
			<input type="radio" name="testRadio:int" value=2 data-isTempData=true checked="checked" />
		 	test radio 2
		</label>

		#select(fieldName="testSelect:int"
			, formDefaults=rc.formDefaults
			, valueArray=[{label='waarde 1',value=1},{label='waarde 2',value=2},{label='waarde 3',value=1}]
			, justReturnDefData=0
			, class="testClass"
			, id="idValue")#
	results in:
		<label>
			test select
			<select name="testSelect:int" data-isTempData=false class="testClass" id="idValue">
				<option value="1">waarde 1</option>
				<option value="2" selected="selected">waarde 2</option>
				<option value="3">waarde 3</option>
			</select>
		</label>

	Normally the options will be a loop over an answergroup
		<label>
			test select
			<select name="testSelect:int" class="something" #optionValueIsTempData("testSelect:int",rc.formDefaults)# >
				<option #optionValue("testSelect:int",rc.formDefaults,1)#>waarde 1</option>
				<option #optionValue("testSelect:int",rc.formDefaults,2)#>waarde 2</option>
				<option #optionValue("testSelect:int",rc.formDefaults,3)#>waarde 3</option>
			</select>
		</label>
	results in:
		<label>
			test select
			<select name="testSelect:varchar" data-isTempData=false class="testClass" id="idValue">
				<option value="1">waarde 1</option>
				<option value="2" selected="selected">waarde 2</option>
				<option value="3">waarde 3</option>
			</select>
		</label>

		<label>
			A textarea
			#textArea(fieldName="testTextarea:varchar",formDefaults=rc.formDefaults,class="testClass",id="idValue")#
		</label>
	results in:
		<textarea name="testTextarea:varchar" data-istempData=true class="testClass" id="idValue">A long string of text.</textarea>
	or as follows
		<label>
			Test textarea
			<textarea name="testTextarea:varchar" class="testClass">#text(fieldName="testTextarea:varchar",formDefaults=rc.formDefaults,justReturnDefData=true)#</textarea>
		</label>

		<textarea name="testTextarea:varchar" class="testClass" #textAreaIsTempData(ieldName="testTextarea:varchar",formDefaults=rc.formDefaults)#>#text(fieldName="testTextarea:varchar",formDefaults=rc.formDefaults,justReturnDefData=false)#</textarea>
	results in:
		<textarea name="testTextarea:varchar" class="testClass" data-istempData=false>A long string of text.</textarea>
*/
component extends="monitoringservice" accessors=true output=false {

	public boolean function getBoolean(required string fieldName, struct formDefaults={}, boolean justReturnDefData=false) {
		value = getValue(argumentcollection=arguments);
		return isBoolean(value) ? value : false;
	}

	public string function getValue(required string fieldName, struct formDefaults={}, boolean justReturnDefData=false) output=false hint="" {
		var values = getValues(argumentcollection=arguments);
		if (arrayLen(values)) {
			return values[1];
		}
		return "";
	}

	public array function getValues(required string fieldName, struct formDefaults={}, boolean justReturnDefData=false) output=false hint="" {
		return getResultObj(argumentcollection=arguments).getDirectValue();
	}

	public  model.beans.storage.result function getResultObj(required string fieldName, struct formDefaults={}, boolean justReturnDefData=false) output=false hint="" {
		var attributeStruct = convertFormFieldNameToAttributeStruct(arguments.fieldName);
		applyDefaultsToAttributeStruct(attributeStruct, arguments.formDefaults);
		validateAttributeStruct(attributeStruct);//Will throw exceptions if something is wrong.
		var attributeObj = getAttributeObjectByFullName(attributeStruct.fieldName);
		if (isNull(attributeObj)) {
			//todo adjust for different types
			return entityNew("result");
		}
		var resultObjectArray = getActiveResultObjects(attributeObj, attributeStruct);
		if (arrayIsEmpty(resultObjectArray)) {
			//todo adjust for different types
			return entityNew("result");
		}
		//temp/def
		if (!arguments.justReturnDefData) {
			//return the first found active result as they are sorted with temp data first
			return resultObjectArray[1];
		} else {
			//return defdata
			for (resultObject in resultObjectArray) {
				if(!resultObject.getIsTempData()) {
					return resultObject;
				}
			}
			//no data found, return empty object
			return entityNew("result");
		}
	}

	public boolean function valueExists(required array values, string value, string defaultValue="") output=false hint="" {
		arguments.value = trim(arguments.value);
		//single value
		if (arrayLen(arguments.values) == 1 && !isNull(arguments.values[1])) {
			var dbValue = arguments.values[1];
			//equals given value
			if (dbValue == arguments.value) {
				return true;
			} else if (!structKeyExists(arguments,'value') and isBoolean(dbValue) and yesNoFormat(dbValue)) {
				return true;
			}
		//multi value
		} else if (arrayLen(arguments.values) != 1 && arrayFindNoCase(arguments.values,arguments.value)) {
			return true;
		// bij geen data in de database de default waarde, meegegeven vanuit de radio, checkbox als selected aanmerken
		} else if ((arrayIsEmpty(arguments.values) || isNull(arguments.values[1])) and arguments.value == arguments.defaultValue) {
			return true;
		}
		return false;
	}

	public string function checkedValue(required string fieldName, struct formDefaults={}, string value, boolean justReturnDefData=false) output=false hint="" {
		var values = getValues(argumentcollection=arguments);
		if (valueExists(values,arguments.value)) {
			return "checked";
		}
		return "";
	}

	public boolean function hasValue(required string fieldName, struct formDefaults={}, string value, boolean justReturnDefData=false) output=false hint="" {
		var values = getValues(argumentcollection=arguments);
		if (valueExists(values,arguments.value)) {
			return true;
		}
		return false;
	}

	public string function selectedValue(required string fieldName, struct formDefaults={}, string value, boolean justReturnDefData=false) output=false hint="" {
		var values = getValues(argumentcollection=arguments);
		if (valueExists(values,arguments.value)) {
			return "selected";
		}
		return "";
	}

	public string function isTempData(required string fieldName, struct formDefaults={}, string value, boolean justReturnDefData=false) output=false hint="" {
 		var resultObj = getResultObj(argumentcollection=arguments);
 		return trueFalseFormat(resultObj.getIsTempData());
 	}

	/****************************Questiontype functions *****************************/
	public string function text(required string fieldName, struct formDefaults={}, boolean justReturnDefData=false) output=false hint="" {
		var resultObj = getResultObj(argumentcollection=arguments);
		var values = resultObj.getDirectValue();
		if (arrayLen(values) && !isNull(values[1])) {
			var value = values[1];
		} else {
			var value = "";
		}
		return "name='#arguments.fieldName#' value='#value#' data-istempData=#trueFalseFormat(resultObj.getIsTempData())#";
	}

	public string function textDate(required string fieldName, struct formDefaults={}, string mask="long", string locale="nl_NL", boolean justReturnDefData=false) output=false hint="" {
		var resultObj = getResultObj(argumentcollection=arguments);
		var values = resultObj.getDirectValue();
		if (arrayLen(values) && !isNull(values[1])) {
			var value = values[1];
		} else {
			var value = "";
		}
		return "name='#arguments.fieldName#' value='#lsDateFormat(value,arguments.mask,arguments.locale)#' data-istempData=#trueFalseFormat(resultObj.getIsTempData())#";
	}

	public string function textDateTime(required string fieldName, struct formDefaults={}, string dateMask="long", string timeMask="long", string locale="nl_NL", boolean justReturnDefData=false) output=false hint="" {
		var resultObj = getResultObj(argumentcollection=arguments);
		var values = resultObj.getDirectValue();
		if (arrayLen(values)) {
			var value = values[1];
		} else {
			var value = "";
		}
		return "name='#arguments.fieldName#' value='#lsDateFormat(value,arguments.mask,arguments.locale)# #lsDateFormat(value,arguments.mask,arguments.locale)#' data-istempData=#trueFalseFormat(resultObj.getIsTempData())#";
	}

	public string function radio(required string fieldName, struct formDefaults={}, string value, boolean justReturnDefData=false, string defaultValue="") output=false hint="" {
		var resultObj = getResultObj(argumentcollection=arguments);
		var values = resultObj.getDirectValue();
		var returnString = "name='#arguments.fieldName#' value='#arguments.value#'";
		if (valueExists(values,arguments.value, arguments.defaultValue)) {
			return returnString & " data-isTempData=#trueFalseFormat(resultObj.getIsTempData())# checked='checked'";
		} else {
			return returnString;
		}
	}

	public string function checkbox(required string fieldName, struct formDefaults={}, string value, boolean justReturnDefData=false, string defaultValue="") output=false hint="" {
		return radio(argumentcollection=arguments);
	}

	public string function select(required string fieldName, struct formDefaults={}, required array valueArray, boolean justReturnDefData=false, string class="", string id="") output=false hint="" {
		var resultObj = getResultObj(argumentcollection=arguments);
		var values = resultObj.getDirectValue();
		var startTag = "<select name='#arguments.fieldName#' data-isTempData=#trueFalseFormat(resultObj.getIsTempData())# class='#arguments.class#' id='#arguments.id#'>";
		var endTag = "</select>"
		var optionString = "";
		for (var optionItem in arguments.valueArray) {
			optionString &= "<option value='#optionItem.value#' #valueExists(values,optionItem.value) ? 'selected=''selected''' : ''#>#optionItem.label#</option>";
		}
		return startTag & optionString & endTag;
	}

 	public string function optionValue(required string fieldName, struct formDefaults={}, string value, boolean justReturnDefData=false) output=false hint="" {
 		var resultObj = getResultObj(argumentcollection=arguments);
		var values = resultObj.getDirectValue();
		var returnString = "name='#arguments.fieldName#' value='#arguments.value#'";
		if (valueExists(values,arguments.value)) {
			return returnString & " selected='selected'";
		} else {
			return returnString;
		}
 	}

 	public string function optionValueIsTempData(required string fieldName, struct formDefaults={}, string value, boolean justReturnDefData=false) output=false hint="" {
 		return "data-isTempData=" & isTempData(argumentcollection=arguments);
 	}

 	public string function textArea(required string fieldName, struct formDefaults={}, string value, boolean justReturnDefData=false, string class="", string id="") output=false hint="" {
 		var resultObj = getResultObj(argumentcollection=arguments);
 		var values = resultObj.getDirectValue();
		if (arrayLen(values)) {
			var value = trim(values[1]);
		} else {
			var value = "";
		}
 		return "<textarea name='#arguments.fieldName#' data-istempData=#trueFalseFormat(resultObj.getIsTempData())# class='#arguments.class#' id='#arguments.id#'>#value#</textarea>";
 	}

 	public string function textAreaValue(required string fieldName, struct formDefaults={}, string value, boolean justReturnDefData=false) output=false hint="" {
 		return getValue(argumentcollection=arguments);
 	}

 	public string function textAreaIsTempData(required string fieldName, struct formDefaults={}, string value, boolean justReturnDefData=false) output=false hint="" {
 		return "data-isTempData=" & isTempData(argumentcollection=arguments);
 	}
	/****************************End questiontype functions *****************************/

	private boolean function trueFalseFormat(value) output=false hint="" {
		if (value) {
			return True;
		} else {
			return False;
		}
	}

}