<cfscript>
	local.devURL = application.development == true ? "dev." : "";
	local.appStruct = {
		adminSkin : "purple"
		, applicationUrl : "https://APPNAAM.#local.devURL#onderzoek.nl/"
		/* Bootstrap */
			, bootstrapCSSVersionCdn : "https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
			, bootstrapJSVersionCdn : "https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
		/* ckEditor */
			, ckEditor5 : "https://cdn.ckeditor.com/ckeditor5/41.2.0/classic/ckeditor.js"
			, ckEditor5LangNL : "https://cdn.ckeditor.com/ckeditor5/41.2.0/classic/translations/nl.js"
		/* CSP headers */
			, cspHeaderConnect : "connect-src 'self' https://export.highcharts.com/ https://code.highcharts.com/mapdata/countries/"
			, cspHeaderFont : "font-src 'self' https://fonts.gstatic.com/s/ https://cdn.jsdelivr.net/npm/ https://cdnjs.cloudflare.com/ajax/libs/font-awesome/"
			, cspHeaderForm : ""/* deze is debatable, gaat hier iig altijd fout met form-action 'self', dus voor nu leeg-/weggelaten */
			, cspHeaderFrame : "frame-src 'none';frame-ancestors 'none'"/* LET OP! De ; moet er tussen staan! */
			, cspHeaderImg : "img-src 'self' data:"
			, cspHeaderObject : "object-src 'none'"
			, cspHeaderScript : "script-src 'self' https://cdn.jsdelivr.net/npm/ https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/"
			, cspHeaderStyle : "style-src 'self' 'unsafe-inline' https://cdn.jsdelivr.net/npm/ https://fonts.googleapis.com/css https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/ https://cdnjs.cloudflare.com/ajax/libs/font-awesome/"/* de 'unsafe-inline' is omdat onder andere FontAwesome, CKEditor en andere JS-libs anders gewoon niet werken */
		/* Datatables */
			, dataTablesCSS : "https://cdn.datatables.net/v/bs5/jszip-3.10.1/dt-1.13.8/b-2.4.2/b-colvis-2.4.2/b-html5-2.4.2/b-print-2.4.2/r-2.5.0/datatables.min.css"/* Data Tables latest, with Bootstrap 5 and Buttons extension (met daarbinnen HTML5 Export, Print View en Column Visibility) en dan nog de Responsive extension */
			, dataTablesJS : "https://cdn.datatables.net/v/bs5/jszip-3.10.1/dt-1.13.8/b-2.4.2/b-colvis-2.4.2/b-html5-2.4.2/b-print-2.4.2/r-2.5.0/datatables.min.js"
			, dataTablesDateSorting : "https://cdn.datatables.net/plug-ins/1.13.4/sorting/date-eu.js"
		/* Fontawesome */
			, fontAwesomeVersionCdn : "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css"
		/* default CSS for global 404, accessdenied, error, etc. */
			, globalPagesCss : "
				html,body { height:100%; }
				body { align-items:center; display:flex; padding-top:0; padding-bottom:100px; }
				.globalPages { margin:auto; max-width:600px; padding:15px; text-align:center; width:100%; }
			"
		/* Highcharts */
			, highCharts : "https://code.highcharts.com/highcharts.js"
			, highChartsAddons : {
				accessibility : "https://code.highcharts.com/modules/accessibility.js"
				, exporting : "https://code.highcharts.com/modules/exporting.js"
				, exportingData : "https://code.highcharts.com/modules/export-data.js"
				, more : "https://code.highcharts.com/highcharts-more.js"
			}
		/* naam webapp voor in subsystem registered */
			, homeTitle : "Dashboard APPNAAM"
		/* jQuery */
			, jQueryVersionCdn : "https://code.jquery.com/jquery-3.7.1.min.js"
	};
	StructAppend(application, local.appStruct);
	application.cspHeaderStyle &= " #application.dataTablesCSS#";
	application.cspHeaderScript &= " #application.ckEditor5# #application.highCharts# #application.highChartsAddons.accessibility# #application.highChartsAddons.exporting# #application.highChartsAddons.exportingData# #application.highChartsAddons.more#";
	/* to override the above variables for local development, create a file */
	if (application.development && fileExists("includes/applicationVariables.local.cfm")) {
		include "includes/applicationVariables.local.cfm";
	}
</cfscript>