﻿component output=false hint="" {

	public struct function getConfiguration() output=false hint="" {
		/* get Database config settings */
		var settingsService = new model.services.default.settingsService();
		var settingsObj = settingsService.getObjBy({ domainName : cgi.server_name });
		var config = {
			allowSpecialCharacters : ["backslash", "forwardslash"]
			, arrayExcluded : [
				".well-known"
				, "admin"
				, "assets"
				, "BIN"
				, "common"
				, "config"
				, "global"
				, "home"
				, "model"
				, "tasks"
				, "WEB-INF"
			]
			, arrSubsystem : ["registered"]
			, arrSubsystemSpecial : ["admin"]
			, bugLogApiKey : "5C18C546-9657-B7F8-A6945E8F769B8E41"
			, bugLogListener : "http://buglog.panteia.local/listeners/bugLogListenerREST.cfm"
			, copyrightUrl : settingsObj.getCopyrightUrl()
			, defaultLocale : settingsObj.getDefaultLocale()
			, displayName : settingsObj.getDisplayName()
			, mail : {
				error : "webteam@panteia.nl"
				, fail : "noreply@panteia.nl"
				, from : settingsObj.getEmailFrom()
				, helpdesk : settingsObj.getEmailHelpdesk()
				, template : {
					html : "/global/views/emailtemplates/emailhtml.cfm"
					, plain : "/global/views/emailtemplates/emailplain.cfm"
				}
				, urlLogo : settingsObj.getDomainName() & "/assets/gfx/logo.png"
			}
			, pdfFooterPagination : '<!doctype html>
<html>
<head>
<script>
	function subst() {
		var vars = {};
		var x = window.location.search.substring(1).split(''&'');
		for (var i in x) {
			var z = x[i].split(''='',2);
			vars[z[0]] = unescape(z[1]);
		}
		var x = [''frompage'',''topage'',''page'',''webpage'',''section'',''subsection'',''subsubsection''];
		for (var i in x) {
			var y = document.getElementsByClassName(x[i]);
			for (var j=0; j<y.length; ++j) {
				y[j].textContent = vars[x[i]];
			}
		}
	}
</script>
</head>
<body onload="subst();">
<div style="text-align:center;"><span class="page"></span> / <span class="topage"></span></div>
</body>
</html>'
			, pdfSettings : {
				"encoding" : "UTF-8"
				, "footer-spacing" : "0"
				, "header-spacing" : "0"
				, "javascript-delay" : "2000"
				, "margin-top" : "10mm"
				, "margin-right" : "10mm"
				, "margin-bottom" : "10mm"
				, "margin-left" : "10mm"
				, "orientation" : "portrait"
				, "page-size" : "A4"
			}
			, portalUrl : (settingsObj.getHasSSL() ? "https://" : "http://") & cgi.http_host
			, publicSubSystems : listToArray(settingsObj.getPublicSubSystems())
			, remote : {
				apiCall : "checkCredentials"
				, method : "post"
				, protocol : "https"
				, url : "toolbox.panteia.nl"
			}
			, security : {
				encryptsalt : "rf0f:c3S|z(K&+X5"
				, hasSSL : settingsObj.getHasSSL()
				, key : "T1xlQ175aNI7xsg2LDockQ=="/* key aanpassen, GenerateSecretKey("AES") */
				, mainSalt : ")`I8pPrO:GV\/VW>"/* for shortURL-service */
				, passwordValidInDays : 0
				, protectInput : settingsObj.getProtectInput()// 'to encrypt or not to encrypt' data, that's the question
			}
			, telephonePanteia : "+31 (0)79 322 0000"
			, uaKey : settingsObj.getUaKey()/* Google Analytics Key */
			, urlPanteia : "https://panteia.nl/"
			, version : settingsObj.getVersion()
			, wkhtmltopdfcommandPath : "d:/internetsites/global/wkhtmltopdf/bin/wkhtmltopdf.exe"
		};
		/* add custom application variables */
		var customPropertyStruct = {};
		for (var customProperty in settingsObj.getCustomProperties()) {
			customPropertyStruct[customProperty] = settingsObj.getPropertyValuesStruct()[customProperty];
			structAppend(config, customPropertyStruct);
		}
		/* to override these variables for local development, create a file */
		if (application.development && fileExists("includes/global.local.cfm")) {
			include "includes/global.local.cfm";
		}
		return config;
	}

}