<cfscript>
	/* for the purpose of security, check panteia user credentials */
	local.fullPathArray = listToArray(expandPath("."), "\");
	local.startString = "";
	for (elem in local.fullPathArray) {
		local.startString &= "..\";
	}
	include local.startString & "internetsites\global\datasources\credentials.cfm";
	this.datasource = "web_APPNAAM";
	/* to override these variables for local development, create a file */
	if (this.development && fileExists("includes/datasources.local.cfm")) {
		include "includes/datasources.local.cfm";
	}
	/* ------------------------ ORM SETTINGS ------------------------ */
	this.ormEnabled = true;
	this.ormSettings = {
		autogenmap : true
		, autoManageSession : false
		, cfclocation : "/model/beans/"
		, dbcreate : "none"
		, dialect : "MicrosoftSQLServer"
		, eventHandling : true
		, flushatrequestend : false
		, logsql : this.development
		, secondaryCacheEnabled : false
	};
</cfscript>