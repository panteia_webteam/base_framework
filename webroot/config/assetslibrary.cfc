﻿component output=false hint="" {

	public struct function getAssetsLibrary() output=false hint="" {
		var assetsLibrary = {
			/* admin, home */
			datepicker : {
				JS : {
					files : ["plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"]
					, localization : {
						nl_NL : "plugins/bootstrap-datepicker/locales/bootstrap-datepicker.nl.min.js"
						, en_US : "plugins/bootstrap-datepicker/locales/bootstrap-datepicker.en-GB.min.js"
					}
				}
				, CSS : ["plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"]
			}
			/* admin */
			, inputMask : {
				JS : ["plugins/input-mask/jquery.inputmask.bundle.min.js"]
				, CSS : []
			}
			/* admin */
			, jqueryKnob : {
				JS : ["plugins/jqueryKnob/jquery.knob.min.js"]
				, CSS : []
			}
			/* admin, home */
			, pwstrength : {
				JS : ["plugins/pwstrength-bootstrap/pwstrength.min.js"]
			}
			/* admin */
			, select2 : {
				JS : [
					"plugins/select2/js/select2.full.min.js"
					, "plugins/select2/js/i18n/nl.js"
				]
				, CSS : [
					"plugins/select2/css/select2.min.css"
					, "plugins/select2/css/select2-bootstrap.min.css"
				]
			}
			/* admin, home
				=> https://github.com/jquery-validation/jquery-validation/releases */
			, validation : {
				JS : [
					"plugins/jquery-validation/jquery.validate.min.js"
					, "plugins/jquery-validation/additional-methods.min.js"
					, "plugins/jquery-validation/bootstrap.validate.js"
					, "plugins/jquery-validation/localization/messages_nl.min.js"
				]
				, CSS : []
			}
		};
		return assetsLibrary;
	}

}