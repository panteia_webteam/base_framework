<cfscript>
	/* ------------------------ FW/1 SETTINGS ------------------------ */
	variables.framework = {
		cacheFileExists : ! this.development
		, defaultItem : "main"
		, defaultSubsystem : "home"
		, diConfig : {
			exclude : []
			, loadListener : factoryConfig
		}
		, diLocations: arrayToList([
			"controllers"
			, "model"
		])
		, environments : {
			dev : { reloadApplicationOnEveryRequest : true, trace : false }
		}
		, error : "global:main.error"
		, generateSES : true
		, reloadApplicationOnEveryRequest : false
		, routes: []
		, SESOmitIndex : true
		, siteWideLayoutSubsystem : "global"
		, trace : false
		, unhandledPaths : "/global,/lucee,/services,/tasks,/temp"
		, usingSubsystems : true
	};
	/* to override these variables for local development, create a file */
	if (this.development && fileExists("includes/fw1config.local.cfm")) {
		include "includes/fw1config.local.cfm";
	}
</cfscript>