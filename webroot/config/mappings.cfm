<cfscript>
	/* mapping naar Lucee Spreadsheets - staat op elke Panteia server op dezelfde plek */
	this.mappings["/luceeSpreadsheet"] = "D:\internetsites\global\lucee-spreadsheet-master";
	/* to override these variables for local development, create a file */
	if (this.development && fileExists("includes/mappings.local.cfm")) {
		include "includes/mappings.local.cfm";
	}
</cfscript>